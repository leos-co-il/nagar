<?php
get_header();
$query = get_queried_object();
$posts_all =
$posts = get_posts([
	'numberposts' => 9,
	'post_type' => 'post',
	'tax_query' => [
		[
			'taxonomy' => 'category',
			'field' => 'term_id',
			'terms' => $query->term_id,
		]
	]
]);
$posts_all = get_category($query->term_id)->count;
?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => $query->name,
		'text' => category_description(),
	]); ?>
	<div class="body-output">
		<?php if ($posts) : $counter = 1; ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php if ($posts_all > 9) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-link load-more-posts" data-term="<?= $query->term_id; ?>" data-type="post"
						 data-tax-type="category">
						טען עוד מאמרים
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'banner');
if (($content = get_field('seo_content', $query)) || ($img = get_field('seo_img', $query))) {
	get_template_part('views/partials/content', 'seo', [
			'content' => $content,
			'link' => get_field('seo_link', $query),
			'img' => $img ? $img['url'] : '',
	]);
}
get_footer(); ?>

