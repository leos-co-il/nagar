<?php

the_post();
get_header();
$fields = get_fields();
$images = [];
?>
<article class="page-body">
	<div class="about-body-output">
		<div class="container">
			<div class="row justify-content-center align-items-start">
				<div class="col-12 mob-title">
					<h1 class="base-title mb-3"><?php the_title(); ?></h1>
				</div>
				<div class="col post-content-col">
					<h1 class="base-title mb-3 hide-title"><?php the_title(); ?></h1>
					<div class="base-output">
						<?php the_content(); ?>
					</div>
					<?php if ($fields['post_video_link']) : ?>
								<div class="video-back-violet my-3">
									<div class="video-item"
										 style="background-image: url('<?= getYoutubeThumb($fields['post_video_link']); ?>')">
										<div class="put-video-here"></div>
										<span class="play-button" data-id="<?= getYoutubeId($fields['post_video_link']); ?>">
											<img src="<?= ICONS ?>play-button.png" alt="play-button">
										</span>
									</div>
								</div>
					<?php endif;
					if ($fields['post_video_content']) : ?>
						<div class="base-output">
							<?= $fields['post_video_content']; ?>
						</div>
					<?php endif; ?>
				</div>
				<?php if ($fields['post_gallery'] || has_post_thumbnail()) : ?>
					<div class="col-lg-6 col-12 gallery-main-col">
						<?php if (has_post_thumbnail()) : ?>
						<div class="gallery-col-pad">
							<div class="gallery-top-item gallery-list-item"
								 style="background-image: url('<?= postThumb(); ?>')">
								<div class="put-image-here"></div>
							</div>
						</div>
						<?php endif;
						if ($fields['post_gallery']) : ?>
							<div class="gallery-row">
								<?php foreach ($fields['post_gallery'] as $image) : ?>
									<div class="gallery-col">
										<div class="gallery-list-item photo-gal"
											 style="background-image: url('<?= $image['url'];?>')"
											 data-url="<?= $image['url'];?>">
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</article>
<?php get_template_part('views/partials/repeat', 'banner');
$postId = get_the_ID();
$text = $fields['same_text'] ?? '<h2>המאמרים נוספים</h2>';
$post_terms = wp_get_object_terms($postId, 'category', ['fields' => 'ids']);
$samePosts = [];
if ($fields['same_posts']) {
	$samePosts = $fields['same_posts'];
} else {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'post_type' => 'post',
			'post__not_in' => array($postId),
			'tax_query' => [
					[
							'taxonomy' => 'category',
							'field' => 'term_id',
							'terms' => $post_terms,
					],
			],
	]);
}
if ($samePosts === NULL) {
	$samePosts = get_posts([
			'posts_per_page' => 3,
			'orderby' => 'rand',
			'post_type' => 'post',
			'post__not_in' => array($postId),
	]);
} ?>
<section class="same-posts">
	<?php get_template_part('views/partials/content', 'block_text', [
			'text' => $text,
	]);
	if ($samePosts) : ?>
		<div class="container-fluid mb-5">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($samePosts as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
	<?php endif; ?>
</section>
<?php get_footer(); ?>
