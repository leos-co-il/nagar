<!DOCTYPE HTML>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <link rel="icon" href="<?= IMG ?>favicon.ico" type="image/x-icon">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body <?php body_class(ENV); ?>>

<?php if(ENV === 'dev'): ?>
    <script>
        const timerStart = Date.now();
    </script>
<div class="debug bg-danger border">
    <p class="width">
        <span>Width:</span>
        <span class="val"></span>
    </p>
    <p class="height">
        <span>Height:</span>
        <span class="val"></span>
    </p>
    <p class="media-query">
        <span>Media Query:</span>
        <span class="val"></span>
    </p>
    <p class="zoom">
        <span>Zoom:</span>
        <span class="val"></span>
    </p>
    <p class="dom-ready">
        <span>DOM Ready:</span>
        <span class="val"></span>
    </p>
    <p class="load-time">
        <span>Loading Time:</span>
        <span class="val"></span>
    </p>
</div>
<?php endif; ?>


<header>
	<div class="drop-menu">
		<nav class="drop-nav">
			<?php getMenu('header-menu', '2', '', ''); ?>
		</nav>
	</div>
    <div class="container-fluid">
		<div class="row align-items-md-end align-items-center">
			<div class="col d-flex align-items-center justify-content-start col-with-search">
				<button class="hamburger hamburger--spin menu-trigger" type="button">
					<span class="hamburger-box">
						<span class="hamburger-inner"></span>
					</span>
				</button>
				<div class="wrap-search">
					<?= get_search_form() ?>
				</div>
				<div class="pop-trigger mr-4">
					<img src="<?= ICONS ?>pop-trigger.png" alt="open-popup">
				</div>
			</div>
			<?php if ($logo = opt('logo')) : ?>
				<div class="col-md-2 col-4">
					<a href="/" class="logo">
						<img src="<?= $logo['url'] ?>" alt="/">
					</a>
				</div>
			<?php endif; ?>
			<div class="col d-flex justify-content-end align-items-md-center align-items-end flex-md-row flex-column">
				<?php echo do_shortcode( '[yith_wcwl_items_count]' );
			global $woocommerce; ?>
				<a href="<?= wc_get_cart_url(); ?>" class="cart-link">
					<img src="<?= ICONS ?>basket.png" alt="סל קניות">
					<div class="cart-count">
						<?= $woocommerce->cart->cart_contents_count; ?>
					</div>
				</a>
				<a href="<?= wp_registration_url(); ?>" class="register-link">
					<img src="<?= ICONS ?>user.png">
				</a>
			</div>
		</div>
        <div class="row">
        </div>
    </div>
	<div class="bottom-menu-wrap">
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto header-down-menu">
					<?php getMenu('header-down-menu', '2', 'header-menu-bottom', ''); ?>
				</div>
			</div>
		</div>
	</div>
</header>

<section class="pop-form">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-xl-6 col-lg-8 col-md-11 col-12 d-flex justify-content-center">
				<div class="float-form">
					<div class="form-col">
						<span class="close-form">
							<img src="<?= ICONS ?>close.png">
						</span>
						<?php if ($f_title = opt('pop_form_title')) : ?>
							<h2 class="mid-text mb-3 text-center"><?= $f_title; ?></h2>
						<?php endif;
						getForm('86'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
