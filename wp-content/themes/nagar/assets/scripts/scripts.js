(function($) {
	$.fn.slideFadeToggle  = function(speed, easing, callback) {
		return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
	};
    $( document ).ready(function() {
		$('.add-to-cart-btn').click(function (e) {
			e.preventDefault();

			var cartEl = $('.cart');
			var btn = $(this);
			btn.addClass('loading');
			$(this).addClass('adding-cart');
			var productId = $(this).data('id');
			jQuery.ajax({
				url: '/wp-admin/admin-ajax.php',
				type: 'POST',
				data: {
					action: 'add_product_to_cart',
					product_id: productId,
					quantity: 1,
				},

				success: function (results) {
					btn.removeClass('loading').addClass('clicked');
					cartEl.html(results);
					// update_cart_count();
				}
			});
		});
		$('.play-button').click(function() {
			var id = $(this).data('id');
			var iFrame = '<iframe src="https://www.youtube.com/embed/'+id+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
			$('.put-video-here').addClass('show').html(iFrame);
		});
		var menu = $( '.drop-menu' );
		$('.hamburger').click(function () {
			menu.slideFadeToggle();
			$(this).toggleClass('is-active');
		});
		$('.pop-trigger').click(function () {
			$('.pop-form').addClass('show-popup');
			$('.float-form').addClass('show-float-form');
			$('.pop-body').addClass('curr-body-hidden');
		});
		$('.close-form').click(function () {
			$('.pop-form').removeClass('show-popup');
			$('.float-form').removeClass('show-float-form');
			$('.pop-body').removeClass('curr-body-hidden');
		});
		$('.photo-gal').click(function() {
			var img = $(this).data('url');
			$('.put-image-here').css('background-image', 'url(' + img + ')').addClass('show-img');
		});
		$('.vari-card').click(function() {
			$(this).addClass('chosen-var');
			var varId = $(this).data('var_id');
			var varSize = $(this).data('size');
			$('.put-var-id').val(varId);
			$('.put-size').val(varSize);
			$( document.body ).trigger( 'check_variations' );
			$( document.body ).trigger( 'show_variation' );
		});
		var accordionProduct = $('#accordion-product');
		accordionProduct.on('hidden.bs.collapse', function () {
			var collapsed = $( '.collapse' );
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').removeClass('show-icon');
			collapsed.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').removeClass('hide-icon');
		});
		accordionProduct.on('shown.bs.collapse', function () {
			var show = $( '.show' );
			show.parent().children('.accordion-button-wrap').children('button').children('.minus-icon').addClass('show-icon');
			show.parent().children('.accordion-button-wrap').children('button').children('.plus-icon').addClass('hide-icon');
		});
		$('.upsells-slider').slick({
			slidesToShow: 6,
			slidesToScroll: 1,
			arrows: true,
			dots: false,
			rtl: true,
			responsive: [
				{
					breakpoint: 1500,
					settings: {
						slidesToShow: 5,
					}
				},
				{
					breakpoint: 1040,
					settings: {
						slidesToShow: 4,
					}
				},
				{
					breakpoint: 992,
					settings: {
						slidesToShow: 3,
					}
				},
				{
					breakpoint: 769,
					settings: {
						slidesToShow: 2,
					}
				},
				{
					breakpoint: 576,
					settings: {
						slidesToShow: 1,
					}
				},
			]
		});
		// $('.faq-block-item').each(function(i, faq) {
		// 	$(faq).children('#accordion-+i+').each(function(i, accordion) {
		// 		$(accordion).on('shown.bs.collapse', function () {
		// 			var show = $( '.show' );
		// 			show.parent().children('.question-title').addClass('violet-faq');
		// 			show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		// 		});
		// 		$(accordion).on('hidden.bs.collapse', function () {
		// 			var collapsed = $( '.collapse' );
		// 			collapsed.parent().children('.question-title').removeClass('violet-faq');
		// 			collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		// 		});
		// 	});
		// });


		// var accordion = $('.accordion-faq');
		// accordion.on('shown.bs.collapse', function () {
		// 	var show = $( '.show' );
		// 	show.parent().children('.question-title').addClass('violet-faq');
		// 	show.parent().children('.question-title').children('.arrow-top').addClass('arrow-bottom');
		// });
		// accordion.on('hidden.bs.collapse', function () {
		// 	var collapsed = $( '.collapse' );
		// 	collapsed.parent().children('.question-title').removeClass('violet-faq');
		// 	collapsed.parent().children('.question-title').children('.arrow-top').removeClass('arrow-bottom');
		// });
    });
	//More posts
	$('.load-more-posts').click(function() {
		var termID = $(this).data('term');
		var params = $('.take-json').html();
		var postType = $(this).data('type');
		var taxType = $(this).data('tax-type');

		var ids = '';
		$('.more-card').each(function(i, obj) {
			ids += $(obj).data('id') + ',';
		});
		jQuery.ajax({
			url: '/wp-admin/admin-ajax.php',
			dataType: 'json',
			data: {
				postType: postType,
				termID: termID,
				ids: ids,
				taxType: taxType,
				params: params,
				action: 'get_more_function',
			},
			success: function (data) {
				console.log(data);
				if (!data.html) {
					$('.load-more-posts').addClass('hide');
				}
				$('.put-here-posts').append(data.html);
			}
		});
	});
})( jQuery );
