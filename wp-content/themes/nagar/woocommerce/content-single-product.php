<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args' );
function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 3; // 4 related products
	return $args;
}
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked woocommerce_output_all_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}
//if ( $product->is_type( 'variable' ) ) {
//	$variations = $product->get_available_variations();
//	foreach ($variations as $key => $value){
//		$taxonomy = 'pa_size';
//		$meta = get_post_meta($value['variation_id'], 'attribute_'.$taxonomy, true);
//		$term = get_term_by('slug', $meta, $taxonomy);
//		$scs_wc_size = $term->name;
////		$scs_wc_size = $value['attributes']['attribute_pa_size'];
//
//		echo  $scs_wc_size. ' ' . $value['price_html'].'<br/>';
//	}
//}
$fields = get_fields();
?>
<div id="product-<?php the_ID(); ?>" <?php wc_product_class( 'pt-5', $product ); ?>>
	<div class="container-fluid product-page-body">
		<div class="row justify-content-center">
			<div class="col-xl-10 col-lg-11 col-12">
				<div class="row justify-content-center align-items-start">
					<?php
					/**
					 * Hook: woocommerce_before_single_product_summary.
					 *
					 * @hooked woocommerce_show_product_sale_flash - 10
					 * @hooked woocommerce_show_product_images - 20
					 */
					do_action( 'woocommerce_before_single_product_summary' );
					?>
					<div class="col">
						<?php
						/**
						 * Hook: woocommerce_single_product_summary.
						 *
						 * @hooked woocommerce_template_single_title - 5
						 * @hooked woocommerce_template_single_rating - 10
						 * @hooked woocommerce_template_single_price - 10
						 * @hooked woocommerce_template_single_excerpt - 20
						 * @hooked woocommerce_template_single_add_to_cart - 30
						 * @hooked woocommerce_template_single_meta - 40
						 * @hooked woocommerce_template_single_sharing - 50
						 * @hooked WC_Structured_Data::generate_product_data() - 60
						 */
						do_action( 'woocommerce_single_product_summary' );
						?>
						<?php if ($accordion_info = $fields['product_accordion']) : ?>
							<div id="accordion-product" class="product-info-acc mb-5 <?php echo !($product->is_type('variable')) ? 'order-5' : ''; ?>">
								<?php foreach ($accordion_info as $number => $info_item) : ?>
									<div class="card">
										<div class="prod-desc-header" id="heading_<?= $number; ?>">
											<div class="accordion-button-wrap">
												<h2 class="product-info-title"><?= $info_item['acc_title']; ?></h2>
												<button class="btn" data-toggle="collapse"
														data-target="#contactInfo<?= $number; ?>"
														aria-expanded="false" aria-controls="collapseOne">
															<span class="accordion-symbol plus-icon
															<?php echo $number === 0 ? 'hide-icon' : ''; ?>">+</span>
													<span class="accordion-symbol minus-icon
															<?php echo $number === 0 ? 'show-icon' : ''; ?>">-</span>
												</button>
											</div>
											<div id="contactInfo<?= $number; ?>" class="collapse
											<?php echo $number === 0 ? 'show' : ''; ?>"
												 aria-labelledby="heading_<?= $number; ?>" data-parent="#accordion-product">
												<div class="slider-output base-output">
													<?= $info_item['acc_content']; ?>
												</div>
											</div>
										</div>
									</div>
								<?php endforeach; ?>
							</div>
						<?php endif; ?>
					</div>
					<div class="col-xl-auto col-12">
						<?php do_action('woocommerce_get_vars'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	/**
	 * Hook: woocommerce_after_single_product_summary.
	 *
	 * @hooked woocommerce_output_product_data_tabs - 10
	 * @hooked woocommerce_upsell_display - 15
	 * @hooked woocommerce_output_related_products - 20
	 */
	do_action( 'woocommerce_after_single_product_summary' );
	?>
</div>

<?php do_action( 'woocommerce_after_single_product' ); ?>
