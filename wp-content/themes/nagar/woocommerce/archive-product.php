<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

$query = get_queried_object();
$title = get_field('home_cats_title', $query);
$cats = get_field('shop_cats', $query);
if (!$cats) {
	$cats = get_terms([
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'number' => 3,
	]);
}
?>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<header class="woocommerce-products-header">
					<div class="row">
						<div class="col-12">
							<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
								<h1 class="woocommerce-products-header__title base-title text-center">
									<?php woocommerce_page_title(); ?>
								</h1>
							<?php endif; ?>
							<div class="base-output text-center mb-4">

								<?php
								/**
								 * Hook: woocommerce_archive_description.
								 *
								 * @hooked woocommerce_taxonomy_archive_description - 10
								 * @hooked woocommerce_product_archive_description - 10
								 */
								do_action( 'woocommerce_archive_description' );

								?>
							</div>
						</div>
						<div class="col-12 mt-4 d-flex justify-content-center mb-3">
							<?php do_action('woocommerce_before_shop_loop'); ?>
						</div>
					</div>
				</header>
				<div class="home-products" id="home-shop">
					<div class="row justify-content-center">
						<div class="col-12">
							<?php if (woocommerce_product_loop()): ?>
								<div class="row justify-content-center">
									<?php if ( wc_get_loop_prop( 'total' ) ) {
										while ( have_posts() ) {
											the_post();
											/**
											 * Hook: woocommerce_shop_loop.
											 */
											do_action( 'woocommerce_shop_loop' );
											echo '<div class="col-xl-3 col-lg-4 col-sm-6 col-12 mb-3">';
											get_template_part('views/partials/card', 'product_shop',
													[
															'product' => get_the_ID(),
													]);
											echo '</div>';
										}
									}
									wp_reset_postdata();
									?>
									<div class="col-12 d-flex justify-content-end mt-3 mb-5">
										<?php do_action( 'woocommerce_after_shop_loop' ); ?>
									</div>
								</div>
							<?php else: ?>
								<?php

								/**
								 * Hook: woocommerce_no_products_found.
								 *
								 * @hooked wc_no_products_found - 10
								 */
								do_action( 'woocommerce_no_products_found' );
								?>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
<?php if ($cats) :  ?>
	<section class="block-home">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col mb-3">
					<h2 class="base-title text-center">
						<?= $title ? $title : 'קטגוריות דומות'; ?>
					</h2>
				</div>
			</div>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($cats as $cat) {
					get_template_part('views/partials/card', 'category', [
							'cat' => $cat,
					]);
				} ?>
			</div>
		</div>
	</section>
<?php endif;
get_footer( 'shop' );
