<?php
/*
Template Name: אודות
*/
get_header();
$fields = get_fields();

?>
<article class="page-body">
	<div class="about-body-output">
		<?php get_template_part('views/partials/content', 'seo', [
			'content' => get_the_content(),
			'img' => has_post_thumbnail() ? postThumb() : '',
		]);
		if ($fields['about_content_block']) : ?>
			<div class="repeating-about">
				<?php foreach ($fields['about_content_block'] as $item) : ?>
					<div class="base-slider-block">
						<?php if ($item['about_content']) : ?>
							<div class="<?php echo !($item['about_img']) ? 'slider-content-max-wrap' :
								'slider-content-wrap'; ?> wow fadeInDown">
								<div class="slider-text-wrap">
									<div class="base-output"><?= $item['about_content']; ?></div>
								</div>
							</div>
						<?php endif;
						if ($item['about_img']) : ?>
						<div class="slider-image-wrap wow fadeInUp">
							<img src="<?= $item['about_img']['url']; ?>" alt="flowers" class="slider-image">
						</div>
						<?php endif; ?>
					</div>
				<?php endforeach; ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if ($fields['about_video_text'] || $fields['about_video_link']) : ?>
		<div class="about-video-block">
			<?php get_template_part('views/partials/content', 'block_text', [
				'text' => $fields['about_video_text'],
			]);
			if ($fields['about_video_link']) : ?>
				<div class="container mt-2">
					<div class="row justify-content-center">
						<div class="col-xl-8 col-lg-9 col-md-11 col-12">
							<div class="video-back-violet">
								<div class="video-item" style="background-image: url('<?= getYoutubeThumb($fields['about_video_link']); ?>')">
									<div class="put-video-here"></div>
									<span class="play-button" data-id="<?= getYoutubeId($fields['about_video_link']); ?>">
										<img src="<?= ICONS ?>play-button.png" alt="play-button">
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'banner');
if ($fields['seo_content'] || $fields['seo_img']) {
	get_template_part('views/partials/content', 'seo', [
		'content' => $fields['seo_content'],
		'link' => $fields['seo_link'],
		'img' => $fields['seo_img'] ? $fields['seo_img']['url'] : '',
	]);
}
get_footer(); ?>

