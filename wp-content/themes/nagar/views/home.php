<?php
/*
Template Name: דף הבית
*/

get_header();
$fields = get_fields();

?>


<section class="home-main" <?php if (has_post_thumbnail()) : ?>
	style="background-image: url('<?= postThumb(); ?>')"
<?php endif; ?>>
	<div class="container">
		<div class="row justify-content-start">
			<div class="col-lg-auto col-md-9 col-11 d-flex flex-column justify-content-center align-items-start wow zoomIn" data-wow-delay="0.2s">
				<?php if ($fields['main_title']) : ?>
					<h2 class="main-home-title"><?= $fields['main_title']; ?></h2>
				<?php endif;
				if ($fields['main_subtitle']) : ?>
					<h2 class="main-home-subtitle"><?= $fields['main_subtitle']; ?></h2>
				<?php endif;
				if ($fields['main_link']) : ?>
				<a class="base-link home-main-link" href="<?= $fields['main_link']['url']; ?>">
					<?= ($fields['main_link'] && isset($fields['main_link']['title'])) ?
							$fields['main_link']['title'] : 'לקולקציה המלאה'; ?>
				</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<!--HERE PRODUCTS-->
<?php if ($fields['home_products_1']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'products' => $fields['home_products_1'],
					'title' => $fields['home_products_1_title'],
			]);
}
if ($fields['home_about_text'] || $fields['home_about_img']) : ?>
	<div class="about-home">
		<?php get_template_part('views/partials/content', 'seo', [
			'content' => $fields['home_about_text'],
			'link' => $fields['home_about_link'],
			'img' => $fields['home_about_img'] ? $fields['home_about_img']['url'] : '',
				]); ?>
	</div>
<?php endif;
if ($fields['home_cats']) :  ?>
	<section class="block-home">
		<div class="container">
			<?php if ($fields['home_cats_title']) : ?>
				<div class="row justify-content-center">
					<div class="col mb-3">
						<h2 class="base-title text-center">
							<?= $fields['home_cats_title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($fields['home_cats'] as $cat) {
					get_template_part('views/partials/card', 'category', [
							'cat' => $cat,
					]);
				} ?>
			</div>
			<?php if ($fields['home_cats_link']) : ?>
				<div class="row justify-content-center mt-3">
					<div class="col-auto">
						<a class="base-link" href="<?= $fields['home_cats_link']['url']; ?>">
							<?= (isset($fields['home_cats_link']['title']) && $fields['home_cats_link']['title']) ?
									$fields['home_cats_link']['title'] : 'לכל הקטגוריות'; ?>
						</a>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</section>
<?php endif;
if ($fields['home_products_2']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'products' => $fields['home_products_2'],
					'title' => $fields['home_products_2_title'],
			]);
}
get_template_part('views/partials/repeat', 'banner');
if ($fields['seo_content'] || $fields['seo_img']) {
	get_template_part('views/partials/content', 'seo', [
			'content' => $fields['seo_content'],
			'link' => $fields['seo_link'],
			'img' => $fields['seo_img'] ? $fields['seo_img']['url'] : '',
	]);
}
if ($fields['home_posts']) : ?>
<section class="block-home">
	<div class="container">
		<?php if ($fields['home_posts_title']) : ?>
			<div class="row justify-content-center">
				<div class="col mb-3">
					<h2 class="base-title text-center">
						<?= $fields['home_posts_title']; ?>
					</h2>
				</div>
			</div>
		<?php endif; ?>
		<div class="row justify-content-center align-items-stretch">
			<?php foreach ($fields['home_posts'] as $post) {
				get_template_part('views/partials/card', 'post_home', [
						'post' => $post,
				]);
			} ?>
		</div>
		<?php if ($fields['home_posts_link']) : ?>
			<div class="row justify-content-center mt-3">
				<div class="col-auto">
					<a class="base-link" href="<?= $fields['home_posts_link']['url']; ?>">
						<?= (isset($fields['home_posts_link']['title']) && $fields['home_posts_link']['title']) ?
								$fields['home_posts_link']['title'] : 'לכל המאמרים'; ?>
					</a>
				</div>
			</div>
		<?php endif; ?>
	</div>
</section>
<?php endif;
if ($fields['home_products_3']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'products' => $fields['home_products_3'],
					'title' => $fields['home_products_3_title'],
			]);
}
get_footer(); ?>
