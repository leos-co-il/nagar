<?php
/*
Template Name: מאמרים
*/
get_header();
$fields = get_fields();
$posts = get_posts([
	'numberposts' => 9,
	'post_type' => 'post',
]);
$posts_all = get_posts([
	'numberposts' => -1,
	'post_type' => 'post',
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => get_the_title(),
		'text' => get_the_content(),
	]); ?>
	<div class="body-output">
		<?php if ($posts) : $counter = 1; ?>
		<div class="container-fluid">
			<div class="row justify-content-center align-items-stretch put-here-posts">
				<?php foreach ($posts as $post) {
					get_template_part('views/partials/card', 'post', [
							'post' => $post,
					]);
				} ?>
			</div>
		</div>
		<?php endif; ?>
	</div>
	<?php if (count($posts_all) > 9) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-link load-more-posts" data-type="post" data-tax-type="category">
						טען עוד מאמרים
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php
get_template_part('views/partials/repeat', 'banner');
if ($fields['seo_content'] || $fields['seo_img']) {
	get_template_part('views/partials/content', 'seo', [
			'content' => $fields['seo_content'],
			'link' => $fields['seo_link'],
			'img' => $fields['seo_img'] ? $fields['seo_img']['url'] : '',
	]);
}
get_footer(); ?>

