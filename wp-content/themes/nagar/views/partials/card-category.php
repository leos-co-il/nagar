<?php if (isset($args['cat']) && $args['cat']) : $link = get_term_link($args['cat']->term_id); ?>
	<div class="col-lg-4 col-sm-6 col-11 post-col">
		<div class="post-card more-card cat-card" data-id="<?= $args['cat']->term_id; ?>">
			<a class="post-image cat-item-img" href="<?= $link; ?>"
					<?php
					$thumbnail_id = get_term_meta($args['cat']->term_id, 'thumbnail_id', true );
					$image = wp_get_attachment_url( $thumbnail_id );
					if ($image) : ?>
						style="background-image: url('<?= $image; ?>')"
					<?php endif;?>>
			</a>
			<div class="post-item-content">
				<div class="post-content-card">
					<a class="mid-text font-weight-bold mb-2" href="<?= $link; ?>"><?= $args['cat']->name; ?></a>
					<p class="base-text text-center">
						<?= text_preview(category_description($args['cat']), 10); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="category-link">
					לקטגוריה
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
