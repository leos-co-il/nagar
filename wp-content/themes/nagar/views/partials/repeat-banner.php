<section class="banner" <?php if ($back = opt('banner_img')) : ?>
	style="background-image: url('<?= $back['url']; ?>')"
<?php endif; ?>>
	<div class="banner-line wow fadeInLeft">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-auto">
					<?php if ($title = opt('banner_title')) : ?>
						<h2 class="banner-title"><?= $title; ?></h2>
					<?php endif;
					if ($subtitle = opt('banner_subtitle')) : ?>
						<h2 class="banner-subtitle"><?= $subtitle; ?></h2>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<?php if ($link = opt('banner_link')) : ?>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-auto">
				<a href="<?= $link['url']; ?>" class="base-link">
					<?= (isset($link['title']) && $link['title']) ? $link['title'] : 'לקטגורית הארועים'; ?>
				</a>
			</div>
		</div>
	</div>
	<?php endif; ?>
</section>
