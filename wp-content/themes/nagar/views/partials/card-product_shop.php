<?php if (isset($args['product']) && ($args['product'])) : $prod_item_id = $args['product'];
	$prod_item = wc_get_product($prod_item_id) ;
	$link = get_permalink($prod_item_id);
	$id = $prod_item->get_id(); ?>
	<div class="post-card cat-card product-card-small">
		<div class="post-image cat-item-img card-product-image"
			<?php if ($img = $prod_item->get_image_id()) : ?>
				style="background-image: url('<?= wp_get_attachment_image_url( $img, 'full' );?>')"
			<?php endif; ?>>
			<?php echo do_shortcode( '[yith_wcwl_add_to_wishlist product_id="'.$id.'"]' ); ?>
		</div>
		<div class="post-item-content pt-2">
			<a class="mid-text font-weight-bold text-center" href="<?= $link; ?>">
				<?= $prod_item->get_name(); ?>
			</a>
			<div class="post-content-card">
				<?php if ($prod_item->is_type( 'variable' )) : ?>
				<div class="card-prod-price">
					<?= $prod_item->get_price_html(); ?>
				</div>
				<?php else: ?>
					<div class="card-prod-price flex-column">
						<?php
						$price = $prod_item->get_regular_price();
						$price_sale = $prod_item->get_sale_price();
						if ($price) : ?>
						<span class="base-text text-center ml-2">החל מ-: </span>
						<h4 class="old-price-base">
							<?= get_woocommerce_currency_symbol().$price; ?>
						</h4>
						<?php endif;
						if ($price_sale) : ?>
						<h4 class="base-price-title">
							<?= get_woocommerce_currency_symbol().$price_sale; ?>
						</h4>
						<?php endif; ?>
					</div>
				<?php endif; ?>
				<?php if ($prod_item->is_type( 'variable' )) : ?>
					<p class="base-text text-center">
						<?= text_preview($prod_item->get_short_description(), 10); ?>
					</p>
				<?php endif; ?>
				<p class="base-text text-center">
					<?= text_preview($prod_item->get_description(), 10); ?>
				</p>
			</div>
			<a class="category-link product-link" href="<?= $link; ?>">
				צפו וקנו
			</a>
		</div>
	</div>
<?php endif; ?>
