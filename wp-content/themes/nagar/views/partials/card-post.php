<?php if(isset($args['post']) && $args['post']) :
	$link = get_the_permalink($args['post']); ?>
<div class="col-lg-4 col-md-6 col-sm-11 col-12 post-col">
	<div class="post-card more-card" data-id="<?= $args['post']->ID; ?>">
		<a class="post-image" <?php if (has_post_thumbnail($args['post'])) : ?>
			style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>
		   href="<?= $link; ?>">
		</a>
		<div class="post-item-content">
			<h3 class="mid-text font-weight-bold mb-2"><?= $args['post']->post_title; ?></h3>
			<div class="post-content-card">
				<p class="base-text">
					<?= text_preview($args['post']->post_content, 10); ?>
				</p>
				<a href="<?php the_permalink($args['post']); ?>" class="base-link post-link align-self-end">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
</div>
<?php endif; ?>
