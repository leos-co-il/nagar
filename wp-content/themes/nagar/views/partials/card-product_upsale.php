<?php if (isset($args['product']) && ($args['product'])) : $prod_item_id = $args['product'];
	$prod_item = wc_get_product($prod_item_id) ;
	$link = get_permalink($prod_item_id);
	$id = $prod_item->get_id(); ?>
	<div class="product-sale-card">
		<div class="absolute-price">
			<?php if ($prod_item->is_type( 'variable' )) : ?>
				<div class="card-prod-price">
					<h4 class="base-price"><?= $prod_item->get_price_html(); ?></h4>
				</div>
			<?php else: ?>
				<div class="card-prod-price">
					<?php
					$price = $prod_item->get_regular_price();
					$price_sale = $prod_item->get_sale_price();
					if ($price) : ?>
					<h4 class="small-price old-price">
						<?= get_woocommerce_currency_symbol().$price; ?>
					</h4>
					<?php endif;
					if ($price_sale) : ?>
					<h4 class="small-price">
						<?= get_woocommerce_currency_symbol().$price_sale; ?>
					</h4>
					<?php endif; ?>
				</div>
			<?php endif; ?>
		</div>
		<div class="product-sale-image position-relative"
			<?php if ($img = $prod_item->get_image_id()) : ?>
				style="background-image: url('<?= wp_get_attachment_image_url( $img, 'full' );?>')"
			<?php endif; ?>>
			<a class="plus-small" href="<?= $link; ?>"></a>
		</div>
		<div class="post-item-content upsale-card-contact pt-2">
			<a class="upsale-small-title mb-2" href="<?= $link; ?>">
				<?= $prod_item->get_name(); ?>
			</a>
<!--			<a class="category-link product-link small-prod-link" href="--><?//= get_site_url().'/?add-to-cart='.$id; ?><!--">-->
<!--				הוספה לחבילה-->
<!--			</a>-->
			<button role="button" class="btn prod-button add-prod-button add-to-cart-btn category-link product-link small-prod-link"
					data-id="<?= $id; ?>">
				<span class="iconic"><i class="fas fa-check"></i></span>
				<span class="text-but">				הוספה לחבילה</span>
			</button>
		</div>
	</div>
<?php endif; ?>
