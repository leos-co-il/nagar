<?php if (isset($args['products']) && $args['products']) : ?>
	<section class="products-block my-4">
		<div class="container-fluid">
			<?php if (isset($args['title']) && $args['title']) : ?>
				<div class="row justify-content-center">
					<div class="col-auto">
						<h2 class="base-title text-center mb-3">
							<?= $args['title']; ?>
						</h2>
					</div>
				</div>
			<?php endif; ?>
			<div class="row justify-content-center align-items-stretch">
				<?php foreach ($args['products'] as $product) : ?>
				<div class="col-xl-3 col-sm-6 col-12 post-col">
					<?php 	get_template_part('views/partials/card', 'product_shop',
						[
							'product' => $product,
							]); ?>
				</div>
				<?php endforeach; ?>
			</div>
		</div>
	</section>
<?php endif; ?>
