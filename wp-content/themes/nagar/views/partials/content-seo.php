<div class="base-slider-block">
	<?php if (isset($args['content']) && $args['content']) : ?>
		<div class="<?php echo !(isset($args['img']) && $args['img']) ? 'slider-content-max-wrap' :
			'slider-content-wrap'; ?> wow fadeInDown">
			<div class="slider-text-wrap">
				<div class="base-output"><?= $args['content']; ?></div>
				<?php if (isset($args['link']) && $args['link']) : ?>
					<a class="base-link" href="<?= $args['link']['url']; ?>">
						<?= (isset($args['link']['title']) && $args['link']['title']) ? $args['link']['title'] : 'קרא עוד'; ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	<?php endif; ?>
	<?php if ((isset($args['img']) && $args['img'])) : ?>
		<div class="slider-image-wrap wow fadeInUp">
			<img src="<?= $args['img']; ?>" alt="flowers" class="slider-image">
		</div>
	<?php endif; ?>
</div>
