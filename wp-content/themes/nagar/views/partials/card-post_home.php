<?php if (isset($args['post']) && $args['post']) : $link = get_the_permalink($args['post']); ?>
	<div class="col-lg-4 col-sm-6 col-11 post-col">
		<div class="post-card cat-card">
			<a class="post-image cat-item-img" href="<?= $link; ?>"
				<?php if (has_post_thumbnail($args['post'])) : ?>
					style="background-image: url('<?= postThumb($args['post']); ?>')" <?php endif; ?>>
			</a>
			<div class="post-item-content">
				<div class="post-content-card">
					<a class="mid-text font-weight-bold mb-2" href="<?= $link; ?>">
						<?= $args['post']->post_title; ?>
					</a>
					<p class="base-text text-center">
						<?= text_preview(($args['post']->post_content), 10); ?>
					</p>
				</div>
				<a href="<?= $link; ?>" class="category-link">
					קרא עוד
				</a>
			</div>
		</div>
	</div>
<?php endif; ?>
