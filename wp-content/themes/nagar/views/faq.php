<?php
/*
Template Name: שאלות ותשובות
*/
get_header();
$fields = get_fields();
?>
<article class="page-body faq-page-body">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => get_the_title(),
		'text' => get_the_content(),
	]);
	if ($fields['faq_block']) : ?>
		<div class="faq-page-body">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-xl-9 col-lg-10 col-sm-11 col-12">
						<?php foreach ($fields['faq_block'] as $x => $faq_block) : ?>
							<div class="faq-block-item">
								<?php if ($faq_block['faq_block_title']) : ?>
									<h3 class="mid-text mb-4 font-weight-bold"><?= $faq_block['faq_block_title']; ?></h3>
								<?php endif; ?>
								<div class="accordion-faq" id="accordion-<?= $x; ?>">
									<?php if ($faq_block['faq_item']) :
										foreach ($faq_block['faq_item'] as $num => $item) : ?>
											<div class="card question-card wow fadeInUp" data-wow-delay="0.<?= $num + $i = 1; ?>s" <?php $i++; ?>>
												<div class="question-header" id="heading_<?= $num; ?>">
													<button class="btn question-title" data-toggle="collapse"
															data-target="#faqChild<?= $num; ?>"
															aria-expanded="false" aria-controls="collapseOne">
														<?= $item['faq_question']; ?>
														<span class="arrow-top"></span>
													</button>
													<div id="faqChild<?= $num; ?>" class="collapse faq-item"
														 aria-labelledby="heading_<?= $num; ?>" data-parent="#accordion-<?= $x; ?>">
														<div class="answer-body base-output">
															<?= $item['faq_answer']; ?>
														</div>
													</div>
												</div>
											</div>
										<?php endforeach; endif; ?>
								</div>
							</div>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<?php get_footer(); ?>

