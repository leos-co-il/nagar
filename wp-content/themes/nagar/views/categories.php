<?php
/*
Template Name: קטגוריות
*/
get_header();
$fields = get_fields();
$cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
		'number' => 9,
]);
$all_cats = get_terms([
		'taxonomy' => 'product_cat',
		'hide_empty' => false,
]);
?>
<article class="page-body">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => get_the_title(),
		'text' => get_the_content(),
	]); ?>
	<div class="body-output">
		<?php if ($cats) :  ?>
			<div class="container">
				<div class="row justify-content-center align-items-stretch put-here-posts">
					<?php foreach ($cats as $cat) {
						get_template_part('views/partials/card', 'category', [
							'cat' => $cat,
						]);
					} ?>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php if (count($all_cats) > 9) : ?>
		<div class="container-fluid">
			<div class="row justify-content-center">
				<div class="col-auto">
					<div class="load-more-link load-more-posts" data-type="category" data-tax-type="product_cat">
						טען עוד קטגוריות
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
</article>
<!--HERE PRODUCTS-->
<?php if ($fields['cat_products']) {
	get_template_part('views/partials/content', 'products_output',
			[
					'products' => $fields['cat_products'],
					'title' => $fields['cat_products_title'],
			]);
}
get_footer(); ?>

