<?php
/*
Template Name: צור קשר
*/

get_header();
$tel = opt('tel');
$mail = opt('mail');
$address = opt('address');
?>

<article class="contact-page">
	<?php get_template_part('views/partials/content', 'block_text', [
		'title' => get_the_title(),
		'text' => get_the_content(),
	]); ?>
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="contact-wrap d-flex align-items-stretch flex-wrap">
					<div class="flex-50 contact-info-wrap">
						<div class="contact-col">
							<?php if ($tel) : ?>
								<a href="tel:<?= $tel; ?>" class="contact-item wow flipInX" data-wow-delay="0.2s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-tel.png">
									</div>
									<p class="contact-type"><?= $tel; ?></p>
								</a>
							<?php endif; ?>
							<?php if ($mail) : ?>
								<a href="mailto:<?= $mail; ?>" class="contact-item wow flipInX" data-wow-delay="0.4s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-mail.png">
									</div>
									<p class="contact-type"><?= $mail; ?></p>
								</a>
							<?php endif; ?>
							<?php if ($address) : ?>
								<a href="https://www.waze.com/ul?q=<?= $address; ?>" class="contact-item wow flipInX" data-wow-delay="0.6s">
									<div class="contact-icon-wrap">
										<img src="<?= ICONS ?>contact-geo.png">
									</div>
									<p class="contact-type"><?= $address; ?></p>
								</a>
							<?php endif; ?>
						</div>
					</div>
					<div class="flex-50">
						<div class="contact-wrap contact-wrap-form">
							<?php getForm('110'); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</article>
<?php get_footer(); ?>
