<?php
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 15);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
add_action( 'woocommerce_get_vars', 'woocommerce_template_single_add_to_cart' );

//remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
add_filter( 'woocommerce_breadcrumb_defaults', 'wps_breadcrumb_delimiter' );
add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 6, 2 );
add_filter( 'woocommerce_get_price_html', 'change_price_html', 10, 2 );
function wpa83367_price_html( $price, $product ){
	return str_replace( '<ins>', '<span>', $price );
}
function change_price_html( $price, $product ){
	return str_replace( '<bdi>', '<span>', $price );
}
add_filter('woocommerce_add_to_cart_fragments', 'iconic_cart_count_fragments', 10, 1);
function iconic_cart_count_fragments($fragments){
	$fragments['div.header-cart-count'] = '<div class="centered header-cart-count">' . WC()->cart->get_cart_contents_count() . '</div>';
	return $fragments;
}
add_filter( 'woocommerce_product_single_add_to_cart_text', 'woocommerce_custom_single_add_to_cart_text' );
function woocommerce_custom_single_add_to_cart_text() {
	return __( ' הוספו את החבילה לסל הקניות', 'woocommerce' );
}
//function move_variation_price() {
//	remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation', 10 );
//	add_action( 'woocommerce_after_single_variation', 'woocommerce_single_variation', 10 );
//}
//add_action( 'woocommerce_before_add_to_cart_form', 'move_variation_price' );
/**
 * Change number of products that are displayed per page (shop page)
 */
add_filter( 'loop_shop_per_page', 'new_loop_shop_per_page', 12 );

function new_loop_shop_per_page( $cols ) {
	// $cols contains the current number of products per page based on the value stored on Options –> Reading
	// Return the number of products you wanna show per page.
	$cols = 12;
	return $cols;
}
//remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
//add_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 39 );
?>

