<?php
//add_action('wp_ajax_nopriv_update_cart_count', 'update_cart_count');
//add_action('wp_ajax_update_cart_count', 'update_cart_count');
//function update_cart_count(){
//	global $woocommerce;
//	echo $woocommerce->cart->cart_contents_count;
//}

add_action('wp_ajax_nopriv_ajax_function', 'ajax_function');
add_action('wp_ajax_more_ajax_function', 'ajax_function');

function ajax_function()
{

    if (!wp_verify_nonce($_REQUEST['nonce'], "my_user_vote_nonce")) {
        exit("No naughty business please");
    }

    $result['type'] = "error";
    $result['var'] = 1;

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        $result = json_encode($result);
        echo $result;
    } else {
        header("Location: " . $_SERVER["HTTP_REFERER"]);
    }
    die();
}

add_action('wp_ajax_nopriv_get_more_function', 'get_more_function');
add_action('wp_ajax_get_more_function', 'get_more_function');

function get_more_function() {
	$ids_string = (isset($_REQUEST['ids'])) ? $_REQUEST['ids'] : '';
	$id_term = (isset($_REQUEST['termID'])) ? $_REQUEST['termID'] : '';
	$post_type = (isset($_REQUEST['postType'])) ? $_REQUEST['postType'] : '';
	$tax_name = (isset($_REQUEST['taxType'])) ? $_REQUEST['taxType'] : 'category';
	$params_string = (isset($_REQUEST['params'])) ? $_REQUEST['params'] : '';
	$hi = json_decode($params_string, true, 4);
	$ids = explode(',', $ids_string);
	$query = new WP_Query([
		'post_type' => $post_type ? $post_type : 'post',
		'posts_per_page' => 3,
		'post__not_in' => $ids,
		'tax_query' => $id_term ? [
			[
				'taxonomy' => $tax_name,
				'field' => 'term_id',
				'terms' => $id_term,
			]
		] : '',
	]);
	$html = '';
	$result['html'] = '';
	if ($post_type === 'post') {
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'post', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	} elseif ($post_type === 'category') {
		$cats = get_terms([
			'taxonomy' => 'product_cat',
			'hide_empty' => false,
			'exclude' => $ids,
		]);
		foreach ($cats as $cat) {
			$html = load_template_part('views/partials/card', 'category', [
				'cat' => $cat,
			]);
			$result['html'] .= $html;
		}
	} else {
		$hi['post__not_in'] = $ids;
		$hi['posts_per_page'] = 3;
		$hi['post_type'] = 'product';
		$result['html'] = '';
		$query = new WP_Query($hi);
		foreach ($query->posts as $item) {
			$html = load_template_part('views/partials/card', 'product', [
				'post' => $item,
			]);
			$result['html'] .= $html;
		}
	}

	if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
		$result = json_encode($result);
		echo $result;
	} else {
		header("Location: " . $_SERVER["HTTP_REFERER"]);
	}
	die();
}
add_action('wp_ajax_add_product_to_cart', 'add_product_to_cart');
add_action('wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart');

function add_product_to_cart() {
	$product_id = isset($_REQUEST['product_id']) ?  intval($_REQUEST['product_id']) : null;
	$type = isset($_REQUEST['type']) ? sanitize_text_field($_REQUEST['type']) : null;
	$custom_data = [];
	$_product = wc_get_product( $product_id );

	$cart = WC()->cart;
	if($_POST['delete'] === 'true'){
		$cart->remove_cart_item($product_id);
	}else{
		if($type == 'weight'){
			$custom_data['custom_data']['new_price'] = floatval(get_post_meta($product_id, 'price_per_weight', true ));

			if($_product->get_price() === ''){
				//update_post_meta($product->id, '_regular_price', (float)$value);
				update_post_meta($_product->get_id(), '_price', 0);
			}

		}



		$custom_data['custom_data']['type'] = $type;
		$cart->add_to_cart( $product_id, '1', '0', array(), $custom_data );
	}

	$result['type'] = "success";

//	wc_get_template( 'cart/cart.php' );

	die();
}
