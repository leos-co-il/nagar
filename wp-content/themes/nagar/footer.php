<?php
$facebook = opt('facebook');
$instagram = opt('instagram');
$current_id = get_the_ID();
$contact_id = getPageByTemplate('views/contact.php');
$flowers = opt('foo_form_img');
?>

<footer>
	<?php if ($current_id !== $contact_id) : ?>
		<div class="flowers-form-section">
			<div class="flowers-violet-back"></div>
			<div class="container">
				<div class="row justify-content-center flowers-form-wrap">
					<?php if ($flowers) : ?>
						<img src="<?= $flowers['url']; ?>" alt="flowers" class="flowers-abs">
						<div class="col-md-2"></div>
					<?php endif; ?>
					<div class="col">
						<div class="flowers-form">
							<?php if ($f_title = opt('foo_form_title')) : ?>
								<h2 class="big-title mb-2"><?= $f_title; ?></h2>
							<?php endif;
							if ($f_text = opt('foo_form_subtitle')) : ?>
								<h3 class="base-title mb-3"><?= $f_text; ?></h3>
							<?php endif;
							getForm('87'); ?>
							<p class="mid-text">שליחה מהווה אישור לקבלת דיוור למייל</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div class="footer-main">
		<a id="go-top">
			<span class="top-wrap">
				<img src="<?= ICONS ?>to-top.png" alt="to-top">
			</span>
			<span class="top-text">למעלה</span>
		</a>
		<div class="container mt-5">
			<div class="row justify-content-center">
				<div class="col-xl-10 col-12">
					<div class="row justify-content-between">
						<div class="col-lg-auto col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">בית</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-menu', '2'); ?>
							</div>
						</div>
						<div class="col-lg-auto col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">סוגי פרחים</h3>
							<div class="menu-border-top">
								<?php getMenu('footer-categories-menu', '2'); ?>
							</div>
						</div>
						<div class="col-lg-auto col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">עמודים מובילים</h3>
							<div class="menu-border-top contact-menu-foo">
								<?php getMenu('footer-blog-menu', '2'); ?>
							</div>
						</div>
						<div class="col-lg-3 col-sm-6 col-12 foo-menu">
							<h3 class="foo-title">צור קשר</h3>
							<div class="menu-border-top">
								<?php getForm('85'); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php if ($facebook || $instagram) : ?>
		<div class="foo-socials">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-auto">
						<div class="socials-footer">
							<?php if ($facebook) : ?>
								<a href="<?= $facebook; ?>" class="foo-social-link">
									<i class="fab fa-facebook-f"></i>
								</a>
							<?php endif; ?>
							<?php if ($instagram) : ?>
								<a href="<?= $instagram; ?>" class="foo-social-link">
									<i class="fab fa-instagram"></i>
								</a>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php endif; ?>
	<div id="leos">
		<a href="http://www.leos.co.il/" title="לאוס מדיה ואינטראקטיב">
			<img src="<?= IMG . 'leos_logo.png' ?>"
				 alt="" title="קידום אתרים עם לאוס מדיה ואינטראקטיב | חברה לקידום אתרים ובניית אתרים" />
			<span></span>
		</a>
	</div>
</footer>

<?php wp_footer(); ?>

<?php

if(ENV === 'dev'):
	require_once THEMEPATH . "/inc/debug.php"
	?>
	<script>

		function _fetchHeader($_el){
			let res = {
				'count' : 0,
				'content' : ''
			} ;
			$($_el).each(function () {
				res.count++;
				res.content += ' [' + $(this).text() + '] ';
			});
			return 'Count: ' + res.count + '. Text: ' + res.content;
		}

		function _fetchMeta($_meta){
			return $('meta[name='+$_meta+']').attr("content");
		}




		phpdebugbar.addDataSet({
			"SEO Local": {
				'H1' : _fetchHeader('h1'),
				'H2' : _fetchHeader('h2'),
				'H3' : _fetchHeader('h3'),
				'Meta Title' : _fetchMeta('title'),
				'Meta Description' : _fetchMeta('description'),
				'Meta Keywords' : _fetchMeta('keywords'),
			}
		});
	</script>

<?php endif; ?>

</body>
</html>
