<?php
get_header();
$fields = get_fields();
?>
<div class="post-output-block pt-5">
	<div class="container-fluid">
		<?php
		$s = get_search_query();
		$args = array(
			's' =>$s
		);
		$the_query = new WP_Query( $args );
		if ( $the_query->have_posts() ) { ?>
		<h4 class="base-title text-center my-5"><?= esc_html__('תוצאות חיפוש עבור:','leos');?><?= get_query_var('s') ?></h4>
		<div class="row justify-content-center align-items-stretch">
			<?php  while ( $the_query->have_posts() ) { $the_query->the_post(); $hey = 1; ?>
				<div class="col-lg-4 col-md-6 col-sm-11 col-12 post-col wow fadeInUp" data-wow-delay="0.<?= $hey * 2; ?>s">
					<div class="post-card more-card">
						<?php $type = get_post_type(); ?>
						<div class="post-image"
							<?php if (has_post_thumbnail()) : ?>
								style="background-image: url('<?= postThumb(); ?>')"
							<?php endif;?>>
						</div>
						<div class="post-item-content">
							<h3 class="mid-text font-weight-bold mb-2"><?php the_title(); ?></h3>
							<div class="post-content-card">
								<p class="base-text">
									<?= text_preview(get_the_content(), 10); ?>
								</p>
								<a href="<?php the_permalink(); ?>" class="base-link post-link align-self-end">
									קרא עוד
								</a>
							</div>
						</div>
					</div>
				</div>
			<?php }
			} else{ ?>
				<div class="text-center pt-5">
					<h4 class="base-block-title text-center">
						<?= esc_html__('שום דבר לא נמצא','leos'); ?>
					</h4>
				</div>
				<div class="alert alert-info text-center mt-5">
					<p><?= esc_html__('מצטערים, אך שום דבר לא תאם את קריטריוני החיפוש שלך. אנא נסה שוב עם מילות מפתח שונות.','leos'); ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
<?php get_footer(); ?>
