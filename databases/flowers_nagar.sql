-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Jun 16, 2021 at 03:13 PM
-- Server version: 5.7.26
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flowers_nagar`
--

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_actions`
--

CREATE TABLE `fmn_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_actions`
--

INSERT INTO `fmn_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(7, 'action_scheduler/migration_hook', 'complete', '2021-06-08 20:49:29', '2021-06-08 23:49:29', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623185369;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623185369;}', 1, 1, '2021-06-08 20:49:37', '2021-06-08 23:49:37', 0, NULL),
(8, 'action_scheduler/migration_hook', 'complete', '2021-06-08 20:50:37', '2021-06-08 23:50:37', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623185437;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623185437;}', 1, 1, '2021-06-08 20:50:38', '2021-06-08 23:50:38', 0, NULL),
(9, 'wc-admin_import_customers', 'complete', '2021-06-09 08:57:27', '2021-06-09 11:57:27', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623229047;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623229047;}', 2, 1, '2021-06-09 08:57:27', '2021-06-09 11:57:27', 0, NULL),
(10, 'adjust_download_permissions', 'complete', '2021-06-09 09:14:02', '2021-06-09 12:14:02', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623230042;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623230042;}', 0, 1, '2021-06-09 09:14:09', '2021-06-09 12:14:09', 0, NULL),
(11, 'adjust_download_permissions', 'complete', '2021-06-09 09:14:40', '2021-06-09 12:14:40', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623230080;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623230080;}', 0, 1, '2021-06-09 09:15:34', '2021-06-09 12:15:34', 0, NULL),
(12, 'adjust_download_permissions', 'complete', '2021-06-09 09:15:35', '2021-06-09 12:15:35', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623230135;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623230135;}', 0, 1, '2021-06-09 09:16:26', '2021-06-09 12:16:26', 0, NULL),
(13, 'adjust_download_permissions', 'complete', '2021-06-09 09:17:53', '2021-06-09 12:17:53', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623230273;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623230273;}', 0, 1, '2021-06-09 09:18:56', '2021-06-09 12:18:56', 0, NULL),
(14, 'wc-admin_import_customers', 'complete', '2021-06-10 08:10:14', '2021-06-10 11:10:14', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623312614;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623312614;}', 2, 1, '2021-06-10 08:10:46', '2021-06-10 11:10:46', 0, NULL),
(15, 'wc-admin_import_customers', 'complete', '2021-06-14 07:32:16', '2021-06-14 10:32:16', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623655936;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623655936;}', 2, 1, '2021-06-14 07:34:16', '2021-06-14 10:34:16', 0, NULL),
(16, 'action_scheduler/migration_hook', 'complete', '2021-06-14 08:11:59', '2021-06-14 11:11:59', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623658319;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623658319;}', 1, 1, '2021-06-14 08:12:02', '2021-06-14 11:12:02', 0, NULL),
(17, 'action_scheduler/migration_hook', 'complete', '2021-06-14 08:13:35', '2021-06-14 11:13:35', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623658415;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623658415;}', 1, 1, '2021-06-14 08:13:36', '2021-06-14 11:13:36', 0, NULL),
(18, 'action_scheduler/migration_hook', 'complete', '2021-06-14 08:15:02', '2021-06-14 11:15:02', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623658502;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623658502;}', 1, 1, '2021-06-14 08:15:04', '2021-06-14 11:15:04', 0, NULL),
(19, 'adjust_download_permissions', 'complete', '2021-06-14 10:41:23', '2021-06-14 13:41:23', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623667283;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623667283;}', 0, 1, '2021-06-14 10:42:22', '2021-06-14 13:42:22', 0, NULL),
(20, 'adjust_download_permissions', 'complete', '2021-06-14 10:42:24', '2021-06-14 13:42:24', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623667344;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623667344;}', 0, 1, '2021-06-14 10:42:27', '2021-06-14 13:42:27', 0, NULL),
(21, 'wc-admin_import_customers', 'complete', '2021-06-15 12:00:37', '2021-06-15 15:00:37', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623758437;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623758437;}', 2, 1, '2021-06-15 12:00:53', '2021-06-15 15:00:53', 0, NULL),
(22, 'adjust_download_permissions', 'complete', '2021-06-15 17:58:00', '2021-06-15 20:58:00', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623779880;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623779880;}', 0, 1, '2021-06-15 17:59:19', '2021-06-15 20:59:19', 0, NULL),
(23, 'adjust_download_permissions', 'complete', '2021-06-15 18:27:20', '2021-06-15 21:27:20', '[12]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623781640;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623781640;}', 0, 1, '2021-06-15 18:27:27', '2021-06-15 21:27:27', 0, NULL),
(24, 'wc-admin_import_customers', 'complete', '2021-06-16 06:30:25', '2021-06-16 09:30:25', '[1]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1623825025;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1623825025;}', 2, 1, '2021-06-16 06:31:40', '2021-06-16 09:31:40', 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_claims`
--

CREATE TABLE `fmn_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_groups`
--

CREATE TABLE `fmn_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_groups`
--

INSERT INTO `fmn_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wc-admin-data');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_actionscheduler_logs`
--

CREATE TABLE `fmn_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_actionscheduler_logs`
--

INSERT INTO `fmn_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 7, 'action created', '2021-06-08 20:48:29', '2021-06-08 23:48:29'),
(2, 7, 'action started via WP Cron', '2021-06-08 20:49:37', '2021-06-08 23:49:37'),
(3, 7, 'action complete via WP Cron', '2021-06-08 20:49:37', '2021-06-08 23:49:37'),
(4, 8, 'action created', '2021-06-08 20:49:37', '2021-06-08 23:49:37'),
(5, 8, 'action started via Async Request', '2021-06-08 20:50:38', '2021-06-08 23:50:38'),
(6, 8, 'action complete via Async Request', '2021-06-08 20:50:38', '2021-06-08 23:50:38'),
(7, 9, 'הפעולה נוצרה', '2021-06-09 08:57:22', '2021-06-09 11:57:22'),
(8, 9, 'הפעולה התחילה דרך WP Cron', '2021-06-09 08:57:27', '2021-06-09 11:57:27'),
(9, 9, 'הפעולה הושלמה דרך WP Cron', '2021-06-09 08:57:27', '2021-06-09 11:57:27'),
(10, 10, 'action created', '2021-06-09 09:14:01', '2021-06-09 12:14:01'),
(11, 10, 'action started via Async Request', '2021-06-09 09:14:09', '2021-06-09 12:14:09'),
(12, 10, 'action complete via Async Request', '2021-06-09 09:14:09', '2021-06-09 12:14:09'),
(13, 11, 'action created', '2021-06-09 09:14:39', '2021-06-09 12:14:39'),
(14, 11, 'הפעולה התחילה דרך WP Cron', '2021-06-09 09:15:34', '2021-06-09 12:15:34'),
(15, 11, 'הפעולה הושלמה דרך WP Cron', '2021-06-09 09:15:34', '2021-06-09 12:15:34'),
(16, 12, 'action created', '2021-06-09 09:15:34', '2021-06-09 12:15:34'),
(17, 12, 'הפעולה התחילה דרך WP Cron', '2021-06-09 09:16:26', '2021-06-09 12:16:26'),
(18, 12, 'הפעולה הושלמה דרך WP Cron', '2021-06-09 09:16:26', '2021-06-09 12:16:26'),
(19, 13, 'הפעולה נוצרה', '2021-06-09 09:17:52', '2021-06-09 12:17:52'),
(20, 13, 'הפעולה התחילה דרך WP Cron', '2021-06-09 09:18:56', '2021-06-09 12:18:56'),
(21, 13, 'הפעולה הושלמה דרך WP Cron', '2021-06-09 09:18:56', '2021-06-09 12:18:56'),
(22, 14, 'הפעולה נוצרה', '2021-06-10 08:10:09', '2021-06-10 11:10:09'),
(23, 14, 'הפעולה התחילה דרך WP Cron', '2021-06-10 08:10:46', '2021-06-10 11:10:46'),
(24, 14, 'הפעולה הושלמה דרך WP Cron', '2021-06-10 08:10:46', '2021-06-10 11:10:46'),
(25, 15, 'הפעולה נוצרה', '2021-06-14 07:32:11', '2021-06-14 10:32:11'),
(26, 15, 'הפעולה התחילה דרך WP Cron', '2021-06-14 07:34:16', '2021-06-14 10:34:16'),
(27, 15, 'הפעולה הושלמה דרך WP Cron', '2021-06-14 07:34:16', '2021-06-14 10:34:16'),
(28, 16, 'הפעולה נוצרה', '2021-06-14 08:10:59', '2021-06-14 11:10:59'),
(29, 16, 'הפעולה התחילה דרך Async Request', '2021-06-14 08:12:02', '2021-06-14 11:12:02'),
(30, 16, 'הפעולה הושלמה דרך Async Request', '2021-06-14 08:12:02', '2021-06-14 11:12:02'),
(31, 17, 'הפעולה נוצרה', '2021-06-14 08:12:35', '2021-06-14 11:12:35'),
(32, 17, 'הפעולה התחילה דרך WP Cron', '2021-06-14 08:13:36', '2021-06-14 11:13:36'),
(33, 17, 'הפעולה הושלמה דרך WP Cron', '2021-06-14 08:13:36', '2021-06-14 11:13:36'),
(34, 18, 'הפעולה נוצרה', '2021-06-14 08:14:02', '2021-06-14 11:14:02'),
(35, 18, 'הפעולה התחילה דרך WP Cron', '2021-06-14 08:15:04', '2021-06-14 11:15:04'),
(36, 18, 'הפעולה הושלמה דרך WP Cron', '2021-06-14 08:15:04', '2021-06-14 11:15:04'),
(37, 19, 'הפעולה נוצרה', '2021-06-14 10:41:22', '2021-06-14 13:41:22'),
(38, 19, 'הפעולה התחילה דרך WP Cron', '2021-06-14 10:42:22', '2021-06-14 13:42:22'),
(39, 19, 'הפעולה הושלמה דרך WP Cron', '2021-06-14 10:42:22', '2021-06-14 13:42:22'),
(40, 20, 'הפעולה נוצרה', '2021-06-14 10:42:23', '2021-06-14 13:42:23'),
(41, 20, 'הפעולה התחילה דרך WP Cron', '2021-06-14 10:42:27', '2021-06-14 13:42:27'),
(42, 20, 'הפעולה הושלמה דרך WP Cron', '2021-06-14 10:42:27', '2021-06-14 13:42:27'),
(43, 21, 'הפעולה נוצרה', '2021-06-15 12:00:32', '2021-06-15 15:00:32'),
(44, 21, 'הפעולה התחילה דרך Async Request', '2021-06-15 12:00:53', '2021-06-15 15:00:53'),
(45, 21, 'הפעולה הושלמה דרך Async Request', '2021-06-15 12:00:53', '2021-06-15 15:00:53'),
(46, 22, 'הפעולה נוצרה', '2021-06-15 17:57:59', '2021-06-15 20:57:59'),
(47, 22, 'הפעולה התחילה דרך WP Cron', '2021-06-15 17:59:19', '2021-06-15 20:59:19'),
(48, 22, 'הפעולה הושלמה דרך WP Cron', '2021-06-15 17:59:19', '2021-06-15 20:59:19'),
(49, 23, 'הפעולה נוצרה', '2021-06-15 18:27:19', '2021-06-15 21:27:19'),
(50, 23, 'הפעולה התחילה דרך WP Cron', '2021-06-15 18:27:26', '2021-06-15 21:27:26'),
(51, 23, 'הפעולה הושלמה דרך WP Cron', '2021-06-15 18:27:27', '2021-06-15 21:27:27'),
(52, 24, 'הפעולה נוצרה', '2021-06-16 06:30:20', '2021-06-16 09:30:20'),
(53, 24, 'הפעולה התחילה דרך WP Cron', '2021-06-16 06:31:40', '2021-06-16 09:31:40'),
(54, 24, 'הפעולה הושלמה דרך WP Cron', '2021-06-16 06:31:40', '2021-06-16 09:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_commentmeta`
--

CREATE TABLE `fmn_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_comments`
--

CREATE TABLE `fmn_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_comments`
--

INSERT INTO `fmn_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'מגיב וורדפרס', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2021-06-08 23:40:24', '2021-06-08 20:40:24', 'היי, זו תגובה.\nכדי לשנות, לערוך, או למחוק תגובות, יש לגשת למסך התגובות בלוח הבקרה.\nצלמית המשתמש של המגיב מגיעה מתוך <a href=\"https://gravatar.com\">גראווטר</a>.', 0, '1', '', 'comment', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_links`
--

CREATE TABLE `fmn_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_options`
--

CREATE TABLE `fmn_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_options`
--

INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://flowers.nagar:8888', 'yes'),
(2, 'home', 'http://flowers.nagar:8888', 'yes'),
(3, 'blogname', 'nagar', 'yes'),
(4, 'blogdescription', 'אתר וורדפרס חדש', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'maxf@leos.co.il', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'j בF Y', 'yes'),
(24, 'time_format', 'G:i', 'yes'),
(25, 'links_updated_date_format', 'j בF Y G:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:163:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:47:\"(([^/]+/)*wishlist)(/(.*))?/page/([0-9]{1,})/?$\";s:76:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]&paged=$matches[5]\";s:30:\"(([^/]+/)*wishlist)(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&wishlist-action=$matches[4]\";s:7:\"shop/?$\";s:27:\"index.php?post_type=product\";s:37:\"shop/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"shop/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"shop/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:55:\"product-category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:50:\"product-category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:31:\"product-category/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:43:\"product-category/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:25:\"product-category/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"product/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"product/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"product/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"product/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"product/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"product/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"product/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"product/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"product/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"product/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"product/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"product/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"product/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"product/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"product/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"product/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"product/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"product/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"product/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"product/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=28&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:25:\"([^/]+)/wc-api(/(.*))?/?$\";s:45:\"index.php?name=$matches[1]&wc-api=$matches[3]\";s:31:\"[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\"[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:5:{i:0;s:34:\"advanced-custom-fields-pro/acf.php\";i:1;s:33:\"classic-editor/classic-editor.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:27:\"woocommerce/woocommerce.php\";i:4;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'nagar', 'yes'),
(41, 'stylesheet', 'nagar', 'yes'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '49752', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'Asia/Jerusalem', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '28', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'admin_email_lifespan', '1638736824', 'yes'),
(94, 'initial_db_version', '45805', 'yes'),
(95, 'fmn_user_roles', 'a:8:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:129:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:22:\"edit_wpt_product_table\";b:1;s:23:\"edit_wpt_product_tables\";b:1;s:30:\"edit_others_wpt_product_tables\";b:1;s:26:\"publish_wpt_product_tables\";b:1;s:22:\"read_wpt_product_table\";b:1;s:31:\"read_private_wpt_product_tables\";b:1;s:24:\"delete_wpt_product_table\";b:1;s:24:\"manage_wpt_product_table\";b:1;s:21:\"edit_wc_product_table\";b:1;s:22:\"edit_wc_product_tables\";b:1;s:29:\"edit_others_wc_product_tables\";b:1;s:25:\"publish_wc_product_tables\";b:1;s:21:\"read_wc_product_table\";b:1;s:30:\"read_private_wc_product_tables\";b:1;s:23:\"delete_wc_product_table\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:11:\"leos_client\";a:2:{s:4:\"name\";s:11:\"Leos Client\";s:12:\"capabilities\";a:124:{s:16:\"activate_plugins\";b:0;s:19:\"delete_others_pages\";b:1;s:19:\"delete_others_posts\";b:1;s:12:\"delete_pages\";b:1;s:12:\"delete_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:14:\"edit_dashboard\";b:1;s:17:\"edit_others_pages\";b:1;s:17:\"edit_others_posts\";b:1;s:10:\"edit_pages\";b:1;s:10:\"edit_posts\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:18:\"edit_theme_options\";b:1;s:6:\"export\";b:0;s:6:\"import\";b:0;s:10:\"list_users\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:13:\"promote_users\";b:0;s:13:\"publish_pages\";b:1;s:13:\"publish_posts\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:4:\"read\";b:1;s:12:\"remove_users\";b:0;s:13:\"switch_themes\";b:0;s:12:\"upload_files\";b:1;s:11:\"update_core\";b:0;s:14:\"update_plugins\";b:0;s:13:\"update_themes\";b:0;s:15:\"install_plugins\";b:0;s:14:\"install_themes\";b:0;s:13:\"delete_themes\";b:0;s:14:\"delete_plugins\";b:0;s:12:\"edit_plugins\";b:0;s:11:\"edit_themes\";b:0;s:10:\"edit_files\";b:0;s:10:\"edit_users\";b:0;s:9:\"add_users\";b:0;s:12:\"create_users\";b:0;s:12:\"delete_users\";b:0;s:15:\"unfiltered_html\";b:1;s:18:\"manage_woocommerce\";b:1;s:25:\"manage_woocommerce_orders\";b:1;s:26:\"manage_woocommerce_coupons\";b:1;s:27:\"manage_woocommerce_products\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:10:\"copy_posts\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:17:\"edit_shop_webhook\";b:1;s:17:\"read_shop_webhook\";b:1;s:19:\"delete_shop_webhook\";b:1;s:18:\"edit_shop_webhooks\";b:1;s:25:\"edit_others_shop_webhooks\";b:1;s:21:\"publish_shop_webhooks\";b:1;s:26:\"read_private_shop_webhooks\";b:1;s:20:\"delete_shop_webhooks\";b:1;s:28:\"delete_private_shop_webhooks\";b:1;s:30:\"delete_published_shop_webhooks\";b:1;s:27:\"delete_others_shop_webhooks\";b:1;s:26:\"edit_private_shop_webhooks\";b:1;s:28:\"edit_published_shop_webhooks\";b:1;s:25:\"manage_shop_webhook_terms\";b:1;s:23:\"edit_shop_webhook_terms\";b:1;s:25:\"delete_shop_webhook_terms\";b:1;s:25:\"assign_shop_webhook_terms\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(96, 'fresh_site', '0', 'yes'),
(97, 'WPLANG', 'he_IL', 'yes'),
(98, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'sidebars_widgets', 'a:2:{s:19:\"wp_inactive_widgets\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(104, 'cron', 'a:17:{i:1623845606;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1623846900;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1623847225;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1623847708;a:1:{s:33:\"wc_admin_process_orders_milestone\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1623847715;a:1:{s:29:\"wc_admin_unsnooze_admin_notes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1623850525;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1623876025;a:4:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1623876052;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623876053;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623876208;a:2:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1623876509;a:1:{s:14:\"wc_admin_daily\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623877200;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623915335;a:2:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623926125;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1623926648;a:1:{s:34:\"yith_wcwl_delete_expired_wishlists\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1624952185;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:11:\"fifteendays\";s:4:\"args\";a:0:{}s:8:\"interval\";i:1296000;}}}s:7:\"version\";i:2;}', 'yes'),
(105, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'recovery_keys', 'a:0:{}', 'yes'),
(138, 'disallowed_keys', '', 'no'),
(139, 'comment_previously_approved', '1', 'yes'),
(140, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(141, 'auto_update_core_dev', 'enabled', 'yes'),
(142, 'auto_update_core_minor', 'enabled', 'yes'),
(143, 'auto_update_core_major', 'unset', 'yes'),
(144, 'finished_updating_comment_type', '1', 'yes'),
(145, 'db_upgraded', '', 'yes'),
(149, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:26:\"בקשת HTTPS נכשלה.\";}}', 'yes'),
(150, 'can_compress_scripts', '1', 'no'),
(151, 'recently_activated', 'a:3:{s:30:\"wc-product-table-lite/main.php\";i:1623658441;s:59:\"block-for-woo-product-table/block-for-woo-product-table.php\";i:1623658354;s:39:\"woo-product-table/woo-product-table.php\";i:1623658258;}', 'yes'),
(152, 'theme_mods_twentytwenty', 'a:1:{s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1623185046;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(153, 'current_theme', 'Nagar', 'yes'),
(154, 'theme_mods_twentytwentyone', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1623185048;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(155, 'theme_switched', '', 'yes'),
(157, 'theme_mods_nagar', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:6:{s:11:\"header-menu\";i:16;s:13:\"dropdown-menu\";i:17;s:22:\"footer-categories-menu\";i:22;s:16:\"footer-blog-menu\";i:23;s:11:\"footer-menu\";i:24;s:16:\"header-down-menu\";i:17;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(167, 'acf_version', '5.9.6', 'yes'),
(170, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.4.1\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1623185183;s:7:\"version\";s:5:\"5.4.1\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(179, 'acf_pro_license', 'YToyOntzOjM6ImtleSI7czo3MjoiYjNKa1pYSmZhV1E5TmpRNE5USjhkSGx3WlQxd1pYSnpiMjVoYkh4a1lYUmxQVEl3TVRVdE1Ea3RNak1nTURVNk1qQTZNVFE9IjtzOjM6InVybCI7czoyNToiaHR0cDovL2Zsb3dlcnMubmFnYXI6ODg4OCI7fQ==', 'yes'),
(184, 'action_scheduler_hybrid_store_demarkation', '6', 'yes'),
(185, 'schema-ActionScheduler_StoreSchema', '3.0.1623185306', 'yes'),
(186, 'schema-ActionScheduler_LoggerSchema', '2.0.1623185306', 'yes'),
(189, 'woocommerce_schema_version', '430', 'yes'),
(190, 'woocommerce_store_address', 'Haifa', 'yes'),
(191, 'woocommerce_store_address_2', '', 'yes'),
(192, 'woocommerce_store_city', 'Haifa', 'yes'),
(193, 'woocommerce_default_country', 'IL', 'yes'),
(194, 'woocommerce_store_postcode', '3100300', 'yes'),
(195, 'woocommerce_allowed_countries', 'all', 'yes'),
(196, 'woocommerce_all_except_countries', 'a:0:{}', 'yes'),
(197, 'woocommerce_specific_allowed_countries', 'a:0:{}', 'yes'),
(198, 'woocommerce_ship_to_countries', '', 'yes'),
(199, 'woocommerce_specific_ship_to_countries', 'a:0:{}', 'yes'),
(200, 'woocommerce_default_customer_address', 'base', 'yes'),
(201, 'woocommerce_calc_taxes', 'no', 'yes'),
(202, 'woocommerce_enable_coupons', 'yes', 'yes'),
(203, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(204, 'woocommerce_currency', 'USD', 'yes'),
(205, 'woocommerce_currency_pos', 'left', 'yes'),
(206, 'woocommerce_price_thousand_sep', '', 'yes'),
(207, 'woocommerce_price_decimal_sep', '', 'yes'),
(208, 'woocommerce_price_num_decimals', '0', 'yes'),
(209, 'woocommerce_shop_page_id', '7', 'yes'),
(210, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(211, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(212, 'woocommerce_placeholder_image', '6', 'yes'),
(213, 'woocommerce_weight_unit', 'kg', 'yes'),
(214, 'woocommerce_dimension_unit', 'cm', 'yes'),
(215, 'woocommerce_enable_reviews', 'yes', 'yes'),
(216, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(217, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(218, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(219, 'woocommerce_review_rating_required', 'yes', 'no'),
(220, 'woocommerce_manage_stock', 'yes', 'yes'),
(221, 'woocommerce_hold_stock_minutes', '60', 'no'),
(222, 'woocommerce_notify_low_stock', 'yes', 'no'),
(223, 'woocommerce_notify_no_stock', 'yes', 'no'),
(224, 'woocommerce_stock_email_recipient', 'maxf@leos.co.il', 'no'),
(225, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(226, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(227, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(228, 'woocommerce_stock_format', '', 'yes'),
(229, 'woocommerce_file_download_method', 'force', 'no'),
(230, 'woocommerce_downloads_require_login', 'no', 'no'),
(231, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(232, 'woocommerce_downloads_add_hash_to_filename', 'yes', 'yes'),
(233, 'woocommerce_prices_include_tax', 'no', 'yes'),
(234, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(235, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(236, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(237, 'woocommerce_tax_classes', '', 'yes'),
(238, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(239, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(240, 'woocommerce_price_display_suffix', '', 'yes'),
(241, 'woocommerce_tax_total_display', 'itemized', 'no'),
(242, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(243, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(244, 'woocommerce_ship_to_destination', 'billing', 'no'),
(245, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(246, 'woocommerce_enable_guest_checkout', 'yes', 'no'),
(247, 'woocommerce_enable_checkout_login_reminder', 'no', 'no'),
(248, 'woocommerce_enable_signup_and_login_from_checkout', 'no', 'no'),
(249, 'woocommerce_enable_myaccount_registration', 'no', 'no'),
(250, 'woocommerce_registration_generate_username', 'yes', 'no'),
(251, 'woocommerce_registration_generate_password', 'yes', 'no'),
(252, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(253, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(254, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(255, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(256, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(257, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(258, 'woocommerce_trash_pending_orders', '', 'no'),
(259, 'woocommerce_trash_failed_orders', '', 'no'),
(260, 'woocommerce_trash_cancelled_orders', '', 'no'),
(261, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(262, 'woocommerce_email_from_name', 'nagar', 'no'),
(263, 'woocommerce_email_from_address', 'maxf@leos.co.il', 'no'),
(264, 'woocommerce_email_header_image', '', 'no'),
(265, 'woocommerce_email_footer_text', '{site_title} &mdash; Built with {WooCommerce}', 'no'),
(266, 'woocommerce_email_base_color', '#96588a', 'no'),
(267, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(268, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(269, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(270, 'woocommerce_merchant_email_notifications', 'no', 'no'),
(271, 'woocommerce_cart_page_id', '8', 'no'),
(272, 'woocommerce_checkout_page_id', '9', 'no'),
(273, 'woocommerce_myaccount_page_id', '10', 'no'),
(274, 'woocommerce_terms_page_id', '', 'no'),
(275, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(276, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(277, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(278, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(279, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(280, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(281, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(282, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(283, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(284, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(285, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(286, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(287, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(288, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(289, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(290, 'woocommerce_api_enabled', 'no', 'yes'),
(291, 'woocommerce_allow_tracking', 'no', 'no'),
(292, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(293, 'woocommerce_single_image_width', '600', 'yes'),
(294, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(295, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(296, 'woocommerce_demo_store', 'no', 'no'),
(297, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:8:\"/product\";s:13:\"category_base\";s:16:\"product-category\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(298, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(299, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(302, 'default_product_cat', '15', 'yes'),
(305, 'woocommerce_version', '5.4.1', 'yes'),
(306, 'woocommerce_db_version', '5.4.1', 'yes'),
(307, 'woocommerce_inbox_variant_assignment', '5', 'yes'),
(311, '_transient_jetpack_autoloader_plugin_paths', 'a:1:{i:0;s:29:\"{{WP_PLUGIN_DIR}}/woocommerce\";}', 'yes'),
(312, 'action_scheduler_lock_async-request-runner', '1623845338', 'yes'),
(313, 'woocommerce_admin_notices', 'a:1:{i:0;s:20:\"no_secure_connection\";}', 'yes'),
(314, 'woocommerce_maxmind_geolocation_settings', 'a:1:{s:15:\"database_prefix\";s:32:\"00u0BylapOqybQO0ufgtgz8qynTiOL7h\";}', 'yes'),
(315, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(316, 'widget_woocommerce_widget_cart', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(317, 'widget_woocommerce_layered_nav_filters', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(318, 'widget_woocommerce_layered_nav', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(319, 'widget_woocommerce_price_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(320, 'widget_woocommerce_product_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(321, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(322, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(323, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(324, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(325, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(326, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(327, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(330, 'woocommerce_admin_version', '2.3.1', 'yes'),
(331, 'woocommerce_admin_install_timestamp', '1623185309', 'yes'),
(332, 'wc_remote_inbox_notifications_wca_updated', '', 'yes'),
(333, 'wc_remote_inbox_notifications_specs', 'a:25:{s:20:\"paypal_ppcp_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"paypal_ppcp_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Offer more options with the new PayPal\";s:7:\"content\";s:113:\"Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:36:\"open_wc_paypal_payments_product_page\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:61:\"https://woocommerce.com/products/woocommerce-paypal-payments/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-04-05 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-04-21 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:7:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:30:\"woocommerce-gateway-paypal-pro\";}}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:37:\"woocommerce-gateway-paypal-pro-hosted\";}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:35:\"woocommerce-gateway-paypal-advanced\";}}i:4;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:40:\"woocommerce-gateway-paypal-digital-goods\";}}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:31:\"woocommerce-paypal-here-gateway\";}}i:6;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:44:\"woocommerce-gateway-paypal-adaptive-payments\";}}}}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:27:\"woocommerce-paypal-payments\";}}}}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:27:\"woocommerce-paypal-payments\";s:7:\"version\";s:5:\"1.2.1\";s:8:\"operator\";s:1:\"<\";}}}}}s:23:\"facebook_pixel_api_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:23:\"facebook_pixel_api_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:44:\"Improve the performance of your Facebook ads\";s:7:\"content\";s:152:\"Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"upgrade_now_facebook_pixel_api\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Upgrade now\";}}s:3:\"url\";s:67:\"plugin-install.php?tab=plugin-information&plugin=&section=changelog\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-17 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-14 00:00:00\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:24:\"facebook-for-woocommerce\";s:7:\"version\";s:5:\"2.4.0\";s:8:\"operator\";s:2:\"<=\";}}}s:16:\"facebook_ec_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:16:\"facebook_ec_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:59:\"Sync your product catalog with Facebook to help boost sales\";s:7:\"content\";s:170:\"A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:22:\"learn_more_facebook_ec\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:42:\"https://woocommerce.com/products/facebook/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-03-01 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-03-15 00:00:00\";}i:2;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:24:\"facebook-for-woocommerce\";}}}}s:37:\"ecomm-need-help-setting-up-your-store\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"ecomm-need-help-setting-up-your-store\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:32:\"Need help setting up your Store?\";s:7:\"content\";s:350:\"Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:16:\"set-up-concierge\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:21:\"Schedule free session\";}}s:3:\"url\";s:34:\"https://wordpress.com/me/concierge\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}}}s:20:\"woocommerce-services\";O:8:\"stdClass\":8:{s:4:\"slug\";s:20:\"woocommerce-services\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"WooCommerce Shipping & Tax\";s:7:\"content\";s:255:\"WooCommerce Shipping & Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:84:\"https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-services\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:32:\"ecomm-unique-shopping-experience\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"ecomm-unique-shopping-experience\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"For a shopping experience as unique as your customers\";s:7:\"content\";s:274:\"Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:43:\"learn-more-ecomm-unique-shopping-experience\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:71:\"https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:3:{i:0;s:35:\"woocommerce-shipping-australia-post\";i:1;s:32:\"woocommerce-shipping-canada-post\";i:2;s:30:\"woocommerce-shipping-royalmail\";}}i:1;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\"<\";s:4:\"days\";i:2;}}}s:37:\"wc-admin-getting-started-in-ecommerce\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-getting-started-in-ecommerce\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:38:\"Getting Started in eCommerce - webinar\";s:7:\"content\";s:174:\"We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:17:\"watch-the-webinar\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:17:\"Watch the webinar\";}}s:3:\"url\";s:28:\"https://youtu.be/V_2XtCOyZ7o\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:12:\"setup_client\";s:9:\"operation\";s:2:\"!=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_count\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:1:\"0\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:4:\"none\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:7:\"revenue\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";s:10:\"up-to-2500\";}}}}}s:18:\"your-first-product\";O:8:\"stdClass\":8:{s:4:\"slug\";s:18:\"your-first-product\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:18:\"Your first product\";s:7:\"content\";s:461:\"That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br/><br/>Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_were_no_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:12:\"stored_state\";s:5:\"index\";s:22:\"there_are_now_products\";s:9:\"operation\";s:1:\"=\";s:5:\"value\";b:1;}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:13:\"product_count\";s:9:\"operation\";s:2:\">=\";s:5:\"value\";i:1;}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:18:\"onboarding_profile\";s:5:\"index\";s:13:\"product_types\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"physical\";s:7:\"default\";a:0:{}}}}s:31:\"wc-square-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wc-square-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:191:\"Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:97:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:38:\"wc-square-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:38:\"wc-square-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:38:\"wc-square-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"Grow your business with Square and Apple Pay \";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:104:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:9:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:18:\"woocommerce-square\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"2.3\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:27:\"wc_square_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:4;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:31:\"wc-square-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:5;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:7;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wcpay-apple-pay-is-now-available\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wcpay-apple-pay-is-now-available\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:53:\"Apple Pay is now available with WooCommerce Payments!\";s:7:\"content\";s:397:\"Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.\";}}s:7:\"actions\";a:2:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:13:\"add-apple-pay\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:13:\"Add Apple Pay\";}}s:3:\"url\";s:69:\"/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}i:1;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:121:\"https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:20:\"woocommerce-payments\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:5:\"2.3.0\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";b:0;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}s:27:\"wcpay-apple-pay-boost-sales\";O:8:\"stdClass\":8:{s:4:\"slug\";s:27:\"wcpay-apple-pay-boost-sales\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:26:\"Boost sales with Apple Pay\";s:7:\"content\";s:205:\"Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:27:\"boost-sales-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:96:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:1;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:34:\"wcpay-apple-pay-grow-your-business\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:34:\"wcpay-apple-pay-grow-your-business\";O:8:\"stdClass\":8:{s:4:\"slug\";s:34:\"wcpay-apple-pay-grow-your-business\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Grow your business with WooCommerce Payments and Apple Pay\";s:7:\"content\";s:178:\"Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:34:\"grow-your-business-marketing-guide\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"See marketing guide\";}}s:3:\"url\";s:103:\"https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.8\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"wcpay_is_apple_pay_enabled\";s:5:\"value\";i:2;s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:3;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:27:\"wcpay-apple-pay-boost-sales\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:37:\"wc-admin-optimizing-the-checkout-flow\";O:8:\"stdClass\":8:{s:4:\"slug\";s:37:\"wc-admin-optimizing-the-checkout-flow\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:28:\"Optimizing the checkout flow\";s:7:\"content\";s:171:\"It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:28:\"optimizing-the-checkout-flow\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:78:\"https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:3;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:8:\"payments\";s:7:\"default\";a:0:{}}}}s:39:\"wc-admin-first-five-things-to-customize\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-admin-first-five-things-to-customize\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:45:\"The first 5 things to customize in your store\";s:7:\"content\";s:173:\"Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:10:\"Learn more\";}}s:3:\"url\";s:82:\"https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:1:\">\";s:4:\"days\";i:2;}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:5:\"value\";s:9:\"NOT EMPTY\";s:7:\"default\";s:9:\"NOT EMPTY\";s:9:\"operation\";s:2:\"!=\";}}}s:32:\"wc-payments-qualitative-feedback\";O:8:\"stdClass\":8:{s:4:\"slug\";s:32:\"wc-payments-qualitative-feedback\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:55:\"WooCommerce Payments setup - let us know what you think\";s:7:\"content\";s:146:\"Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:35:\"qualitative-feedback-from-new-users\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:39:\"https://automattic.survey.fm/wc-pay-new\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:45:\"woocommerce_task_list_tracked_completed_tasks\";s:9:\"operation\";s:8:\"contains\";s:5:\"value\";s:20:\"woocommerce-payments\";s:7:\"default\";a:0:{}}}}s:29:\"share-your-feedback-on-paypal\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"share-your-feedback-on-paypal\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:29:\"Share your feedback on PayPal\";s:7:\"content\";s:127:\"Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:14:\"share-feedback\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:14:\"Share feedback\";}}s:3:\"url\";s:43:\"http://automattic.survey.fm/paypal-feedback\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:10:\"unactioned\";}}s:5:\"rules\";a:2:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:26:\"woocommerce-gateway-stripe\";}}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:43:\"woocommerce-gateway-paypal-express-checkout\";}}}}s:31:\"wcpay_instant_deposits_gtm_2021\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"wcpay_instant_deposits_gtm_2021\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:69:\"Get paid within minutes – Instant Deposits for WooCommerce Payments\";s:7:\"content\";s:384:\"Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:10:\"learn-more\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:40:\"Learn about Instant Deposits eligibility\";}}s:3:\"url\";s:136:\"https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:4:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-05-18 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:19:\"publish_before_time\";s:14:\"publish_before\";s:19:\"2021-06-01 00:00:00\";}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}s:31:\"google_listings_and_ads_install\";O:8:\"stdClass\":8:{s:4:\"slug\";s:31:\"google_listings_and_ads_install\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:35:\"Drive traffic and sales with Google\";s:7:\"content\";s:123:\"Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:11:\"get-started\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:11:\"Get started\";}}s:3:\"url\";s:56:\"https://woocommerce.com/products/google-listings-and-ads\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:3:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:18:\"publish_after_time\";s:13:\"publish_after\";s:19:\"2021-06-09 00:00:00\";}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:23:\"google_listings_and_ads\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:11:\"order_count\";s:9:\"operation\";s:1:\">\";s:5:\"value\";i:10;}}}s:39:\"wc-subscriptions-security-update-3-0-15\";O:8:\"stdClass\":8:{s:4:\"slug\";s:39:\"wc-subscriptions-security-update-3-0-15\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:42:\"WooCommerce Subscriptions security update!\";s:7:\"content\";s:736:\"We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br/><br/>Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br/><br/>We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br/><br/>If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:30:\"update-wc-subscriptions-3-0-15\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:19:\"View latest version\";}}s:3:\"url\";s:30:\"&page=wc-addons&section=helper\";s:18:\"url_is_admin_query\";b:1;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:25:\"woocommerce-subscriptions\";s:8:\"operator\";s:1:\"<\";s:7:\"version\";s:6:\"3.0.15\";}}}s:29:\"woocommerce-core-update-5-4-0\";O:8:\"stdClass\":8:{s:4:\"slug\";s:29:\"woocommerce-core-update-5-4-0\";s:4:\"type\";s:4:\"info\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:31:\"Update to WooCommerce 5.4.1 now\";s:7:\"content\";s:140:\"WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:20:\"update-wc-core-5-4-0\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:25:\"How to update WooCommerce\";}}s:3:\"url\";s:64:\"https://docs.woocommerce.com/document/how-to-update-woocommerce/\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:1:{i:0;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:1:\"=\";s:7:\"version\";s:5:\"5.4.0\";}}}s:19:\"wcpay-promo-2020-11\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-11\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-11\";s:7:\"content\";s:19:\"wcpay-promo-2020-11\";}}s:5:\"rules\";a:0:{}}s:19:\"wcpay-promo-2020-12\";O:8:\"stdClass\":7:{s:4:\"slug\";s:19:\"wcpay-promo-2020-12\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:19:\"wcpay-promo-2020-12\";s:7:\"content\";s:19:\"wcpay-promo-2020-12\";}}s:5:\"rules\";a:0:{}}s:30:\"wcpay-promo-2021-6-incentive-1\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-1\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:74:\"Special offer: Save 50% on transaction fees for up to $125,000 in payments\";s:7:\"content\";s:715:\"Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp222\">WooCommerce Payments</a> to your store today.\n                Get a discounted rate of 1.5% + $0.15 on all transactions – that’s 50% off the standard fee on up to $125,000 in processed payments (or six months, whichever comes first). Act now – this offer is available for a limited time only.\n                <br/><br/>By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-half-off-six-promotion/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp222\">Terms of Service</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:25:\"get-woo-commerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"1\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"3\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"5\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"7\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"9\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"11\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}s:30:\"wcpay-promo-2021-6-incentive-2\";O:8:\"stdClass\":8:{s:4:\"slug\";s:30:\"wcpay-promo-2021-6-incentive-2\";s:4:\"type\";s:9:\"marketing\";s:6:\"status\";s:10:\"unactioned\";s:12:\"is_snoozable\";i:0;s:6:\"source\";s:15:\"woocommerce.com\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":3:{s:6:\"locale\";s:5:\"en_US\";s:5:\"title\";s:58:\"Special offer: No transaction fees* for up to three months\";s:7:\"content\";s:684:\"Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp233\">WooCommerce Payments</a> to your store today. Pay zero transaction fees* on up to $25,000 in processed payments (or three months, whichever comes first). Act now – this offer is available for a limited time only. *Currency conversion fees excluded.\n                <br/><br/>By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-no-transaction-fees-for-three-promotion/?utm_medium=notification&utm_source=product&utm_campaign=wcpay_exp233\">Terms of Service</a>.\";}}s:7:\"actions\";a:1:{i:0;O:8:\"stdClass\":6:{s:4:\"name\";s:24:\"get-woocommerce-payments\";s:7:\"locales\";a:1:{i:0;O:8:\"stdClass\":2:{s:6:\"locale\";s:5:\"en_US\";s:5:\"label\";s:24:\"Get WooCommerce Payments\";}}s:3:\"url\";s:57:\"admin.php?page=wc-admin&action=setup-woocommerce-payments\";s:18:\"url_is_admin_query\";b:0;s:10:\"is_primary\";b:1;s:6:\"status\";s:8:\"actioned\";}}s:5:\"rules\";a:12:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:2:\"or\";s:8:\"operands\";a:6:{i:0;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"2\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:1;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"4\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:2;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"6\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:1:\"8\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"10\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:5;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:36:\"woocommerce_inbox_variant_assignment\";s:5:\"value\";s:2:\"12\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}}}i:1;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:4:{i:0;s:17:\"crowdsignal-forms\";i:1;s:11:\"layout-grid\";i:2;s:17:\"full-site-editing\";i:3;s:13:\"page-optimize\";}}}}i:2;O:8:\"stdClass\":3:{s:4:\"type\";s:21:\"base_location_country\";s:5:\"value\";s:2:\"US\";s:9:\"operation\";s:1:\"=\";}i:3;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:26:\"woocommerce_allow_tracking\";s:5:\"value\";s:3:\"yes\";s:7:\"default\";b:0;s:9:\"operation\";s:1:\"=\";}i:4;O:8:\"stdClass\":3:{s:4:\"type\";s:18:\"wcadmin_active_for\";s:9:\"operation\";s:2:\">=\";s:4:\"days\";i:31;}i:5;O:8:\"stdClass\":2:{s:4:\"type\";s:3:\"not\";s:7:\"operand\";a:1:{i:0;O:8:\"stdClass\":2:{s:4:\"type\";s:17:\"plugins_activated\";s:7:\"plugins\";a:1:{i:0;s:20:\"woocommerce-payments\";}}}}i:6;O:8:\"stdClass\":4:{s:4:\"type\";s:14:\"plugin_version\";s:6:\"plugin\";s:11:\"woocommerce\";s:8:\"operator\";s:2:\">=\";s:7:\"version\";s:3:\"4.0\";}i:7;O:8:\"stdClass\":5:{s:4:\"type\";s:6:\"option\";s:11:\"option_name\";s:10:\"db_version\";s:5:\"value\";s:5:\"45805\";s:7:\"default\";i:0;s:9:\"operation\";s:2:\">=\";}i:8;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:9;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-11\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}i:10;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:8:\"actioned\";s:9:\"operation\";s:2:\"!=\";}i:11;O:8:\"stdClass\":4:{s:4:\"type\";s:11:\"note_status\";s:9:\"note_name\";s:19:\"wcpay-promo-2020-12\";s:6:\"status\";s:10:\"unactioned\";s:9:\"operation\";s:2:\"!=\";}}}}', 'yes'),
(334, 'wc_remote_inbox_notifications_stored_state', 'O:8:\"stdClass\":3:{s:22:\"there_were_no_products\";b:1;s:22:\"there_are_now_products\";b:1;s:17:\"new_product_count\";i:0;}', 'yes'),
(338, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:8:\"approved\";s:1:\"1\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(339, 'wc_blocks_db_schema_version', '260', 'yes'),
(340, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(344, '_transient_woocommerce_reports-transient-version', '1623185386', 'yes'),
(345, '_transient_timeout_orders-all-statuses', '1623790237', 'no'),
(346, '_transient_orders-all-statuses', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";a:0:{}}', 'no'),
(356, 'woocommerce_onboarding_profile', 'a:8:{s:12:\"setup_client\";b:1;s:8:\"industry\";a:1:{i:0;a:1:{s:4:\"slug\";s:21:\"home-furniture-garden\";}}s:13:\"product_types\";a:1:{i:0;s:8:\"physical\";}s:13:\"product_count\";s:8:\"101-1000\";s:14:\"selling_venues\";s:2:\"no\";s:19:\"business_extensions\";a:0:{}s:5:\"theme\";s:5:\"nagar\";s:9:\"completed\";b:1;}', 'yes'),
(357, '_transient_timeout_wc_report_orders_stats_515e6df22739fa144e911bef802bbbc7', '1623790241', 'no'),
(358, '_transient_wc_report_orders_stats_515e6df22739fa144e911bef802bbbc7', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:50:39\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:50:39\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(359, '_transient_timeout_wc_report_orders_stats_0a9571b1a1ae3b737bc1e44e81fe54cd', '1623790241', 'no'),
(360, '_transient_wc_report_orders_stats_0a9571b1a1ae3b737bc1e44e81fe54cd', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:50:39\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:50:39\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(361, 'woocommerce_task_list_tracked_completed_tasks', 'a:2:{i:0;s:13:\"store_details\";i:1;s:8:\"products\";}', 'yes'),
(362, '_transient_timeout_wc_report_orders_stats_a49fbb01a9a96166d4523dbdf3ebef2e', '1623790241', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(363, '_transient_wc_report_orders_stats_a49fbb01a9a96166d4523dbdf3ebef2e', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(364, '_transient_timeout_wc_report_orders_stats_d60822039fd7c3033096f4fb419f7723', '1623790241', 'no'),
(365, '_transient_wc_report_orders_stats_d60822039fd7c3033096f4fb419f7723', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(366, 'woocommerce_task_list_welcome_modal_dismissed', 'yes', 'yes'),
(370, '_transient_product_query-transient-version', '1623834177', 'yes'),
(371, '_transient_product-transient-version', '1623781639', 'yes'),
(396, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(448, 'woocommerce_task_list_hidden', 'no', 'yes'),
(449, '_transient_timeout_wc_report_orders_stats_4a43eb7d67ebb72bd5b52b2c447899c1', '1623834582', 'no'),
(450, '_transient_timeout_wc_report_orders_stats_f34bc4c6cfc6a379bd27283b0791a3d6', '1623834582', 'no'),
(451, '_transient_wc_report_orders_stats_4a43eb7d67ebb72bd5b52b2c447899c1', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-09 12:09:39\";s:12:\"date_end_gmt\";s:19:\"2021-06-09 09:09:39\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(452, '_transient_wc_report_orders_stats_f34bc4c6cfc6a379bd27283b0791a3d6', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(453, '_transient_timeout_wc_report_orders_stats_827598f797cad9007a09d270bdf74b30', '1623834582', 'no'),
(454, '_transient_timeout_wc_report_orders_stats_626c9e87007b4fefad828e72d64b1559', '1623834582', 'no'),
(455, '_transient_wc_report_orders_stats_827598f797cad9007a09d270bdf74b30', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-09 12:09:39\";s:12:\"date_end_gmt\";s:19:\"2021-06-09 09:09:39\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(456, '_transient_wc_report_orders_stats_626c9e87007b4fefad828e72d64b1559', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-24\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:1;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(639, 'category_children', 'a:0:{}', 'yes'),
(654, 'options_tel', '000-0000-000', 'no'),
(655, '_options_tel', 'field_5dd3a33f42e0c', 'no'),
(656, 'options_mail', 'xxxxx@xxxxx.com', 'no'),
(657, '_options_mail', 'field_5dd3a35642e0d', 'no'),
(658, 'options_fax', '', 'no'),
(659, '_options_fax', 'field_5dd3a36942e0e', 'no'),
(660, 'options_address', 'מיקום לורם היפסום מיקום', 'no'),
(661, '_options_address', 'field_5dd3a4d142e0f', 'no'),
(662, 'options_map_image', '', 'no'),
(663, '_options_map_image', 'field_5ddbd67734310', 'no'),
(664, 'options_facebook', 'https://www.facebook.com/facebook', 'no'),
(665, '_options_facebook', 'field_5ddbd6b3cc0cd', 'no'),
(666, 'options_whatsapp', '', 'no'),
(667, '_options_whatsapp', 'field_5ddbd6cdcc0ce', 'no'),
(668, 'options_banner_title', 'ארוע משמח קורה רק עם פרחים יפים', 'no'),
(669, '_options_banner_title', 'field_60c0a398df0fa', 'no'),
(670, 'options_banner_subtitle', 'הפכו את הארוע שלכם למשמח', 'no'),
(671, '_options_banner_subtitle', 'field_60c0a39bdf0fb', 'no'),
(672, 'options_banner_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}', 'no'),
(673, '_options_banner_link', 'field_60c0a3a1df0fc', 'no'),
(674, 'options_logo', '82', 'no'),
(675, '_options_logo', 'field_5dd3a310760b9', 'no'),
(680, 'options_banner_img', '84', 'no'),
(681, '_options_banner_img', 'field_60c0a48f64354', 'no'),
(686, 'options_instagram', 'https://www.instagram.com/', 'no'),
(687, '_options_instagram', 'field_60c0cda68ec80', 'no'),
(690, 'options_foo_form_title', 'העונה מתחלפת, הפרחים תמיד יפים', 'no'),
(691, '_options_foo_form_title', 'field_60c0cdeab29cf', 'no'),
(692, 'options_foo_form_subtitle', 'הישארו רעננים וגלו מה חדש אצלנו', 'no'),
(693, '_options_foo_form_subtitle', 'field_60c0cdefb29d0', 'no'),
(694, 'options_foo_form_img', '107', 'no'),
(695, '_options_foo_form_img', 'field_60c0cdf3b29d1', 'no'),
(738, 'secret_key', '9wHv jEcH:T^zQoe8s}H$lf~f/<@V$VZlh~(4cbEO>P6_1]`(eP6bs?[:;1;s>Lm', 'no'),
(750, '_transient_health-check-site-status-result', '{\"good\":12,\"recommended\":7,\"critical\":1}', 'yes'),
(861, '_transient_wc_attribute_taxonomies', 'a:1:{i:0;O:8:\"stdClass\":6:{s:12:\"attribute_id\";s:1:\"1\";s:14:\"attribute_name\";s:4:\"size\";s:15:\"attribute_label\";s:8:\"מידה\";s:14:\"attribute_type\";s:6:\"select\";s:17:\"attribute_orderby\";s:4:\"name\";s:16:\"attribute_public\";s:1:\"0\";}}', 'yes'),
(892, 'wpt_configure_options', 'a:51:{s:29:\"custom_message_on_single_page\";b:1;s:19:\"disable_plugin_noti\";s:2:\"on\";s:11:\"footer_cart\";s:11:\"always_show\";s:16:\"footer_cart_size\";s:2:\"74\";s:15:\"footer_bg_color\";s:7:\"#0a7f9c\";s:16:\"footer_possition\";s:16:\"footer_possition\";s:10:\"item_count\";s:3:\"all\";s:16:\"sort_mini_filter\";s:3:\"ASC\";s:21:\"sort_searchbox_filter\";s:3:\"ASC\";s:18:\"custom_add_to_cart\";s:18:\"add_cart_left_icon\";s:17:\"thumbs_image_size\";i:60;s:15:\"thumbs_lightbox\";s:1:\"1\";s:12:\"popup_notice\";s:1:\"1\";s:20:\"disable_cat_tag_link\";s:1:\"0\";s:19:\"product_link_target\";s:6:\"_blank\";s:19:\"product_not_founded\";s:21:\"Products Not founded!\";s:14:\"load_more_text\";s:9:\"Load more\";s:19:\"quick_view_btn_text\";s:10:\"Quick View\";s:17:\"loading_more_text\";s:9:\"Loading..\";s:18:\"search_button_text\";s:6:\"Search\";s:19:\"search_keyword_text\";s:14:\"Search Keyword\";s:20:\"disable_loading_more\";s:16:\"load_more_hidden\";s:21:\"instant_search_filter\";s:1:\"0\";s:11:\"filter_text\";s:7:\"Filter:\";s:19:\"filter_reset_button\";s:5:\"Reset\";s:19:\"instant_search_text\";s:16:\"Instant Search..\";s:16:\"yith_browse_list\";s:15:\"Browse the list\";s:22:\"yith_add_to_quote_text\";s:12:\"Add to Quote\";s:24:\"yith_add_to_quote_adding\";s:8:\"Adding..\";s:23:\"yith_add_to_quote_added\";s:6:\"Quoted\";s:4:\"item\";s:4:\"Item\";s:5:\"items\";s:5:\"Items\";s:23:\"add2cart_all_added_text\";s:5:\"Added\";s:25:\"right_combination_message\";s:13:\"Not available\";s:29:\"right_combination_message_alt\";s:92:\"Product variations is not set Properly. May be: price is not inputted. may be: Out of Stock.\";s:21:\"no_more_query_message\";s:49:\"There is no more products based on current Query.\";s:24:\"select_all_items_message\";s:24:\"Please select all items.\";s:20:\"out_of_stock_message\";s:12:\"Out of Stock\";s:18:\"adding_in_progress\";s:18:\"Adding in Progress\";s:20:\"no_right_combination\";s:20:\"No Right Combination\";s:18:\"sorry_out_of_stock\";s:20:\"Sorry! Out of Stock!\";s:17:\"type_your_message\";s:18:\"Type your Message.\";s:27:\"sorry_plz_right_combination\";s:39:\"Sorry, Please choose right combination.\";s:28:\"all_selected_direct_checkout\";s:2:\"no\";s:23:\"product_direct_checkout\";s:2:\"no\";s:16:\"search_box_title\";s:47:\"Search Box (<small>All Fields Optional</small>)\";s:24:\"search_box_searchkeyword\";s:14:\"Search Keyword\";s:18:\"search_box_orderby\";s:8:\"Order By\";s:16:\"search_box_order\";s:5:\"Order\";s:11:\"plugin_name\";s:29:\"Product Table for WooCommerce\";s:14:\"plugin_version\";s:5:\"2.9.1\";}', 'yes'),
(902, '_transient_timeout_wc_report_orders_stats_841d0f1ff37c86e3c9041f52ad787640', '1624263137', 'no'),
(903, '_transient_timeout_wc_report_orders_stats_881a7a8b6c007f54c5a09b6277711e07', '1624263137', 'no'),
(904, '_transient_wc_report_orders_stats_841d0f1ff37c86e3c9041f52ad787640', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-25\";s:10:\"date_start\";s:19:\"2021-06-13 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 00:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 11:12:16\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 08:12:16\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(905, '_transient_wc_report_orders_stats_881a7a8b6c007f54c5a09b6277711e07', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-25\";s:10:\"date_start\";s:19:\"2020-06-14 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(906, '_transient_timeout_wc_report_orders_stats_32fe1e4831d93adb0a68b98ac6258207', '1624263137', 'no'),
(907, '_transient_wc_report_orders_stats_32fe1e4831d93adb0a68b98ac6258207', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2020-25\";s:10:\"date_start\";s:19:\"2020-06-14 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-14 00:00:00\";s:8:\"date_end\";s:19:\"2020-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(908, '_transient_timeout_wc_report_orders_stats_753221ef6b872596dac2f5c918baba62', '1624263137', 'no'),
(909, '_transient_wc_report_orders_stats_753221ef6b872596dac2f5c918baba62', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-25\";s:10:\"date_start\";s:19:\"2021-06-13 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 00:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 11:12:16\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 08:12:16\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(910, '_transient_timeout_wc_report_orders_stats_7c4affc95a83cd790146f521b4234c35', '1624263137', 'no'),
(911, '_transient_wc_report_orders_stats_7c4affc95a83cd790146f521b4234c35', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":7:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:14:{i:0;a:6:{s:8:\"interval\";s:10:\"2021-06-01\";s:10:\"date_start\";s:19:\"2021-06-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-05-31 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2021-06-02\";s:10:\"date_start\";s:19:\"2021-06-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-01 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-02 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2021-06-03\";s:10:\"date_start\";s:19:\"2021-06-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-02 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-03 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2021-06-04\";s:10:\"date_start\";s:19:\"2021-06-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-03 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-04 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2021-06-05\";s:10:\"date_start\";s:19:\"2021-06-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-04 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-05 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2021-06-06\";s:10:\"date_start\";s:19:\"2021-06-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-05 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-06 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2021-06-07\";s:10:\"date_start\";s:19:\"2021-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2021-06-08\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2021-06-09\";s:10:\"date_start\";s:19:\"2021-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2021-06-10\";s:10:\"date_start\";s:19:\"2021-06-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2021-06-11\";s:10:\"date_start\";s:19:\"2021-06-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-10 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-11 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2021-06-12\";s:10:\"date_start\";s:19:\"2021-06-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-11 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-12 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2021-06-13\";s:10:\"date_start\";s:19:\"2021-06-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-12 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-13 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2021-06-14\";s:10:\"date_start\";s:19:\"2021-06-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:14;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(912, '_transient_timeout_wc_report_orders_stats_6e4e2242dc356619da838e21c472e054', '1624263138', 'no'),
(913, '_transient_wc_report_orders_stats_6e4e2242dc356619da838e21c472e054', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":7:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:14:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-06-01\";s:10:\"date_start\";s:19:\"2020-06-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-05-31 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-06-02\";s:10:\"date_start\";s:19:\"2020-06-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-01 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-02 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-06-03\";s:10:\"date_start\";s:19:\"2020-06-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-02 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-03 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-06-04\";s:10:\"date_start\";s:19:\"2020-06-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-03 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-04 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-06-05\";s:10:\"date_start\";s:19:\"2020-06-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-04 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-05 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-06-06\";s:10:\"date_start\";s:19:\"2020-06-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-05 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-06 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-06-07\";s:10:\"date_start\";s:19:\"2020-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-06-08\";s:10:\"date_start\";s:19:\"2020-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-06-09\";s:10:\"date_start\";s:19:\"2020-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-06-10\";s:10:\"date_start\";s:19:\"2020-06-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-09 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-06-11\";s:10:\"date_start\";s:19:\"2020-06-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-10 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-11 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-06-12\";s:10:\"date_start\";s:19:\"2020-06-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-11 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-12 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-06-13\";s:10:\"date_start\";s:19:\"2020-06-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-12 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-13 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-06-14\";s:10:\"date_start\";s:19:\"2020-06-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-13 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":6:{s:11:\"total_sales\";d:0;s:11:\"net_revenue\";d:0;s:7:\"refunds\";d:0;s:8:\"shipping\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:14;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(914, '_transient_timeout_wc_report_orders_stats_41fae52828884a84bdbb1726140f28f1', '1624263138', 'no'),
(915, '_transient_wc_report_orders_stats_41fae52828884a84bdbb1726140f28f1', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":5:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:14:{i:0;a:6:{s:8:\"interval\";s:10:\"2021-06-01\";s:10:\"date_start\";s:19:\"2021-06-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-05-31 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2021-06-02\";s:10:\"date_start\";s:19:\"2021-06-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-01 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-02 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2021-06-03\";s:10:\"date_start\";s:19:\"2021-06-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-02 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-03 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2021-06-04\";s:10:\"date_start\";s:19:\"2021-06-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-03 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-04 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2021-06-05\";s:10:\"date_start\";s:19:\"2021-06-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-04 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-05 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2021-06-06\";s:10:\"date_start\";s:19:\"2021-06-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-05 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-06 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2021-06-07\";s:10:\"date_start\";s:19:\"2021-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2021-06-08\";s:10:\"date_start\";s:19:\"2021-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2021-06-09\";s:10:\"date_start\";s:19:\"2021-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2021-06-10\";s:10:\"date_start\";s:19:\"2021-06-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-09 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2021-06-11\";s:10:\"date_start\";s:19:\"2021-06-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-10 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-11 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2021-06-12\";s:10:\"date_start\";s:19:\"2021-06-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-11 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-12 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2021-06-13\";s:10:\"date_start\";s:19:\"2021-06-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-12 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-13 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2021-06-14\";s:10:\"date_start\";s:19:\"2021-06-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 21:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:14;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(916, '_transient_timeout_wc_report_orders_stats_f8c2bf5c89a5bf6839c0a3305ebe9b61', '1624263138', 'no'),
(917, '_transient_wc_report_orders_stats_f8c2bf5c89a5bf6839c0a3305ebe9b61', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":5:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:8:\"products\";i:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:14:{i:0;a:6:{s:8:\"interval\";s:10:\"2020-06-01\";s:10:\"date_start\";s:19:\"2020-06-01 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-05-31 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-01 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-01 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:1;a:6:{s:8:\"interval\";s:10:\"2020-06-02\";s:10:\"date_start\";s:19:\"2020-06-02 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-01 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-02 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-02 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:2;a:6:{s:8:\"interval\";s:10:\"2020-06-03\";s:10:\"date_start\";s:19:\"2020-06-03 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-02 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-03 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-03 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:3;a:6:{s:8:\"interval\";s:10:\"2020-06-04\";s:10:\"date_start\";s:19:\"2020-06-04 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-03 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-04 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-04 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:4;a:6:{s:8:\"interval\";s:10:\"2020-06-05\";s:10:\"date_start\";s:19:\"2020-06-05 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-04 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-05 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-05 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:5;a:6:{s:8:\"interval\";s:10:\"2020-06-06\";s:10:\"date_start\";s:19:\"2020-06-06 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-05 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-06 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-06 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:6;a:6:{s:8:\"interval\";s:10:\"2020-06-07\";s:10:\"date_start\";s:19:\"2020-06-07 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-06 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-07 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-07 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:7;a:6:{s:8:\"interval\";s:10:\"2020-06-08\";s:10:\"date_start\";s:19:\"2020-06-08 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-07 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-08 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-08 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:8;a:6:{s:8:\"interval\";s:10:\"2020-06-09\";s:10:\"date_start\";s:19:\"2020-06-09 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-08 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-09 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-09 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:9;a:6:{s:8:\"interval\";s:10:\"2020-06-10\";s:10:\"date_start\";s:19:\"2020-06-10 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-09 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-10 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-10 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:10;a:6:{s:8:\"interval\";s:10:\"2020-06-11\";s:10:\"date_start\";s:19:\"2020-06-11 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-10 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-11 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-11 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:11;a:6:{s:8:\"interval\";s:10:\"2020-06-12\";s:10:\"date_start\";s:19:\"2020-06-12 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-11 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-12 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-12 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:12;a:6:{s:8:\"interval\";s:10:\"2020-06-13\";s:10:\"date_start\";s:19:\"2020-06-13 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-12 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-13 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-13 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}i:13;a:6:{s:8:\"interval\";s:10:\"2020-06-14\";s:10:\"date_start\";s:19:\"2020-06-14 00:00:00\";s:14:\"date_start_gmt\";s:19:\"2020-06-13 21:00:00\";s:8:\"date_end\";s:19:\"2020-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2020-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":4:{s:12:\"orders_count\";i:0;s:15:\"avg_order_value\";d:0;s:13:\"coupons_count\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:14;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(918, '_transient_timeout_wc_report_orders_stats_4b85fbeb5a4a338287cdfa7725741ac1', '1624263138', 'no'),
(919, '_transient_wc_report_orders_stats_4b85fbeb5a4a338287cdfa7725741ac1', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-25\";s:10:\"date_start\";s:19:\"2021-06-13 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 00:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(920, '_transient_timeout_wc_report_orders_stats_3f682ec1163d423d018f62809cf5f7a2', '1624263138', 'no'),
(921, '_transient_wc_report_orders_stats_3f682ec1163d423d018f62809cf5f7a2', 'a:2:{s:7:\"version\";s:10:\"1623185386\";s:5:\"value\";O:8:\"stdClass\":5:{s:6:\"totals\";O:8:\"stdClass\":15:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"products\";i:0;s:8:\"segments\";a:0:{}}s:9:\"intervals\";a:1:{i:0;a:6:{s:8:\"interval\";s:7:\"2021-25\";s:10:\"date_start\";s:19:\"2021-06-13 03:00:00\";s:14:\"date_start_gmt\";s:19:\"2021-06-13 00:00:00\";s:8:\"date_end\";s:19:\"2021-06-14 23:59:59\";s:12:\"date_end_gmt\";s:19:\"2021-06-14 20:59:59\";s:9:\"subtotals\";O:8:\"stdClass\":14:{s:12:\"orders_count\";i:0;s:14:\"num_items_sold\";i:0;s:11:\"gross_sales\";d:0;s:11:\"total_sales\";d:0;s:7:\"coupons\";d:0;s:13:\"coupons_count\";i:0;s:7:\"refunds\";d:0;s:5:\"taxes\";d:0;s:8:\"shipping\";d:0;s:11:\"net_revenue\";d:0;s:19:\"avg_items_per_order\";d:0;s:15:\"avg_order_value\";d:0;s:15:\"total_customers\";i:0;s:8:\"segments\";a:0:{}}}}s:5:\"total\";i:3;s:5:\"pages\";i:1;s:7:\"page_no\";i:1;}}', 'no'),
(927, 'wcpt_settings', '{\\\"version\\\":\\\"1.9.3\\\",\\\"archive_override\\\":{\\\"default\\\":\\\"\\\",\\\"shop\\\":\\\"default\\\",\\\"search\\\":\\\"default\\\",\\\"category\\\":{\\\"default\\\":\\\"default\\\",\\\"other_rules\\\":[{\\\"category\\\":[],\\\"table_id\\\":\\\"\\\"}]},\\\"attribute\\\":{\\\"default\\\":\\\"default\\\",\\\"other_rules\\\":[{\\\"attribute\\\":[],\\\"table_id\\\":\\\"\\\"}]},\\\"tag\\\":{\\\"default\\\":\\\"default\\\",\\\"other_rules\\\":[{\\\"tag\\\":[],\\\"table_id\\\":\\\"\\\"}]}},\\\"cart_widget\\\":{\\\"toggle\\\":\\\"enabled\\\",\\\"r_toggle\\\":\\\"enabled\\\",\\\"labels\\\":{\\\"item\\\":\\\"en_US: Item\\\\r\\\\nfr_FR: Article\\\",\\\"items\\\":\\\"en_US: Items\\\\r\\\\nfr_FR: Articles\\\",\\\"view_cart\\\":\\\"en_US: View Cart\\\\r\\\\nfr_FR: Voir le panier\\\",\\\"extra_charges\\\":\\\"en_US: Extra charges may apply\\\\r\\\\nfr_FR: Les taxes peuvent s\\\\\\\\\\\'appliquer\\\"},\\\"style\\\":{\\\"background-color\\\":\\\"#4CAF50\\\",\\\"border-color\\\":\\\"rgba(0, 0, 0, .1)\\\",\\\"bottom\\\":\\\"50\\\"}},\\\"modals\\\":{\\\"labels\\\":{\\\"filters\\\":\\\"en_US: Filters\\\\r\\\\nfr_FR: Filtres\\\",\\\"sort\\\":\\\"en_US: Sort results\\\\r\\\\nfr_FR: Trier les r\\\\u00e9sultats\\\",\\\"reset\\\":\\\"en_US: Reset\\\\r\\\\nfr_FR: Rafra\\\\u00eechir\\\",\\\"apply\\\":\\\"en_US: Apply\\\\r\\\\nfr_FR: Appliquer\\\"},\\\"style\\\":{\\\".wcpt-nm-apply\\\":{\\\"background-color\\\":\\\"#4CAF50\\\"}}},\\\"no_results\\\":{\\\"label\\\":\\\"No results found. [link]Clear filters[\\\\/link] and try again?\\\"}}', 'yes'),
(929, 'action_scheduler_migration_status', 'complete', 'yes'),
(1004, 'product_cat_children', 'a:0:{}', 'yes'),
(1032, 'options_pop_form_title', 'כותרת צור קשר לורם היפסום', 'no'),
(1033, '_options_pop_form_title', 'field_60c7518ebd6e4', 'no'),
(1057, '_transient_timeout_wc_onboarding_product_data', '1623845322', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1058, '_transient_wc_onboarding_product_data', 'a:6:{s:7:\"headers\";O:42:\"Requests_Utility_CaseInsensitiveDictionary\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";s:5:\"nginx\";s:4:\"date\";s:29:\"Tue, 15 Jun 2021 12:08:42 GMT\";s:12:\"content-type\";s:31:\"application/json; charset=UTF-8\";s:14:\"content-length\";s:5:\"11622\";s:12:\"x-robots-tag\";s:7:\"noindex\";s:4:\"link\";s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";s:22:\"x-content-type-options\";s:7:\"nosniff\";s:29:\"access-control-expose-headers\";s:33:\"X-WP-Total, X-WP-TotalPages, Link\";s:28:\"access-control-allow-headers\";s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";s:13:\"x-wccom-cache\";s:3:\"HIT\";s:13:\"cache-control\";s:10:\"max-age=60\";s:5:\"allow\";s:3:\"GET\";s:16:\"content-encoding\";s:4:\"gzip\";s:4:\"x-rq\";s:16:\"ams5 87 207 3186\";s:3:\"age\";s:1:\"3\";s:7:\"x-cache\";s:3:\"hit\";s:4:\"vary\";s:23:\"Accept-Encoding, Origin\";s:13:\"accept-ranges\";s:5:\"bytes\";}}s:4:\"body\";s:48319:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"PayPal Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Dark.png\",\"excerpt\":\"PayPal Checkout now with Smart Payment Buttons\\u2122, dynamically displays, PayPal, Venmo, PayPal Credit, or other local payment options in a single stack giving customers the choice to pay with their preferred option.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"69e6cba62ac4021df9e117cc3f716d07\",\"slug\":\"woocommerce-gateway-paypal-express-checkout\",\"id\":1597922},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"PayPal Payments Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Payments-Pro-Dark.png\",\"excerpt\":\"Take credit card payments directly on your checkout using PayPal Pro.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/paypal-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6d23ba7f0e0198937c0029f9e865b40e\",\"slug\":\"woocommerce-gateway-paypal-pro\",\"id\":18594},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:8:\"response\";a:2:{s:4:\"code\";i:200;s:7:\"message\";s:2:\"OK\";}s:7:\"cookies\";a:0:{}s:8:\"filename\";N;s:13:\"http_response\";O:25:\"WP_HTTP_Requests_Response\":5:{s:11:\"\0*\0response\";O:17:\"Requests_Response\":10:{s:4:\"body\";s:48319:\"{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"PayPal Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Dark.png\",\"excerpt\":\"PayPal Checkout now with Smart Payment Buttons\\u2122, dynamically displays, PayPal, Venmo, PayPal Credit, or other local payment options in a single stack giving customers the choice to pay with their preferred option.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"69e6cba62ac4021df9e117cc3f716d07\",\"slug\":\"woocommerce-gateway-paypal-express-checkout\",\"id\":1597922},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"PayPal Payments Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Payments-Pro-Dark.png\",\"excerpt\":\"Take credit card payments directly on your checkout using PayPal Pro.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/paypal-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6d23ba7f0e0198937c0029f9e865b40e\",\"slug\":\"woocommerce-gateway-paypal-pro\",\"id\":18594},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:3:\"raw\";s:48956:\"HTTP/1.1 200 OK\r\nServer: nginx\r\nDate: Tue, 15 Jun 2021 12:08:42 GMT\r\nContent-Type: application/json; charset=UTF-8\r\nContent-Length: 11622\r\nConnection: close\r\nX-Robots-Tag: noindex\r\nLink: <https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\r\nX-Content-Type-Options: nosniff\r\nAccess-Control-Expose-Headers: X-WP-Total, X-WP-TotalPages, Link\r\nAccess-Control-Allow-Headers: Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\r\nX-WCCOM-Cache: HIT\r\nCache-Control: max-age=60\r\nAllow: GET\r\nContent-Encoding: gzip\r\nX-rq: ams5 87 207 3186\r\nAge: 3\r\nX-Cache: hit\r\nVary: Accept-Encoding, Origin\r\nAccept-Ranges: bytes\r\n\r\n{\"products\":[{\"title\":\"WooCommerce Google Analytics\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/GA-Dark.png\",\"excerpt\":\"Understand your customers and increase revenue with world\\u2019s leading analytics platform - integrated with WooCommerce for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2d21f7de14dfb8e9885a4622be701ddf\",\"slug\":\"woocommerce-google-analytics-integration\",\"id\":1442927},{\"title\":\"WooCommerce Tax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Tax-Dark.png\",\"excerpt\":\"Automatically calculate how much sales tax should be collected for WooCommerce orders - by city, country, or state - at checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/tax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":3220291},{\"title\":\"Stripe\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Stripe-Dark-1.png\",\"excerpt\":\"Accept all major debit and credit cards as well as local payment methods with Stripe.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/stripe\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"50bb7a985c691bb943a9da4d2c8b5efd\",\"slug\":\"woocommerce-gateway-stripe\",\"id\":18627},{\"title\":\"Jetpack\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Jetpack-Dark.png\",\"excerpt\":\"Power up and protect your store with Jetpack\\r\\n\\r\\nFor free security, insights and monitoring, connect to Jetpack. It\'s everything you need for a strong, secure start.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jetpack\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"d5bfef9700b62b2b132c74c74c3193eb\",\"slug\":\"jetpack\",\"id\":2725249},{\"title\":\"Facebook for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Facebook-Dark.png\",\"excerpt\":\"Get the Official Facebook for WooCommerce plugin for three powerful ways to help grow your business.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/facebook\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"0ea4fe4c2d7ca6338f8a322fb3e4e187\",\"slug\":\"facebook-for-woocommerce\",\"id\":2127297},{\"title\":\"Amazon Pay\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Amazon-Pay-Dark.png\",\"excerpt\":\"Amazon Pay is embedded in your WooCommerce store. Transactions take place via\\u00a0Amazon widgets, so the buyer never leaves your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/pay-with-amazon\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9865e043bbbe4f8c9735af31cb509b53\",\"slug\":\"woocommerce-gateway-amazon-payments-advanced\",\"id\":238816},{\"title\":\"Square for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Square-Dark.png\",\"excerpt\":\"Accepting payments is easy with Square. Clear rates, fast deposits (1-2 business days). Sell online and in person, and sync all payments, items and inventory.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/square\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e907be8b86d7df0c8f8e0d0020b52638\",\"slug\":\"woocommerce-square\",\"id\":1770503},{\"title\":\"WooCommerce Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Dark-1.png\",\"excerpt\":\"Print USPS and DHL labels right from your WooCommerce dashboard and instantly save up to 90%. WooCommerce Shipping is free to use and saves you time and money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"f31b3b9273cce188cc2b27f7849d02dd\",\"slug\":\"woocommerce-services\",\"id\":2165910},{\"title\":\"WooCommerce Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Pay-Dark.png\",\"excerpt\":\"Securely accept payments, track cash flow, and manage recurring revenue from your dashboard \\u2014 all without setup costs or monthly fees.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"8c6319ca-8f41-4e69-be63-6b15ee37773b\",\"slug\":\"woocommerce-payments\",\"id\":5278104},{\"title\":\"Mailchimp for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/09\\/logo-mailchimp-dark-v2.png\",\"excerpt\":\"Increase traffic, drive repeat purchases, and personalize your marketing when you connect to Mailchimp.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/mailchimp-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b4481616ebece8b1ff68fc59b90c1a91\",\"slug\":\"mailchimp-for-woocommerce\",\"id\":2545166},{\"title\":\"WooCommerce Subscriptions\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Subscriptions-Dark.png\",\"excerpt\":\"Let customers subscribe to your products or services and pay on a weekly, monthly or annual basis.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscriptions\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"6115e6d7e297b623a169fdcf5728b224\",\"slug\":\"woocommerce-subscriptions\",\"id\":27147},{\"title\":\"PayPal Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Dark.png\",\"excerpt\":\"PayPal Checkout now with Smart Payment Buttons\\u2122, dynamically displays, PayPal, Venmo, PayPal Credit, or other local payment options in a single stack giving customers the choice to pay with their preferred option.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"69e6cba62ac4021df9e117cc3f716d07\",\"slug\":\"woocommerce-gateway-paypal-express-checkout\",\"id\":1597922},{\"title\":\"ShipStation Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Shipstation-Dark.png\",\"excerpt\":\"Fulfill all your Woo orders (and wherever else you sell) quickly and easily using ShipStation. Try it free for 30 days today!\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipstation-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"9de8640767ba64237808ed7f245a49bb\",\"slug\":\"woocommerce-shipstation-integration\",\"id\":18734},{\"title\":\"Product Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-Add-Ons-Dark.png\",\"excerpt\":\"Offer add-ons like gift wrapping, special messages or other special options for your products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"147d0077e591e16db9d0d67daeb8c484\",\"slug\":\"woocommerce-product-addons\",\"id\":18618},{\"title\":\"PayFast Payment Gateway\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Payfast-Dark-1.png\",\"excerpt\":\"Take payments on your WooCommerce store via PayFast (redirect method).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/payfast-payment-gateway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"557bf07293ad916f20c207c6c9cd15ff\",\"slug\":\"woocommerce-payfast-gateway\",\"id\":18596},{\"title\":\"Google Ads &amp; Marketing by Kliken\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/02\\/GA-for-Woo-Logo-374x192px-qu3duk.png\",\"excerpt\":\"Get in front of shoppers and drive traffic to your store so you can grow your business with Smart Shopping Campaigns and free listings.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-ads-and-marketing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"bf66e173-a220-4da7-9512-b5728c20fc16\",\"slug\":\"kliken-marketing-for-google\",\"id\":3866145},{\"title\":\"USPS Shipping Method\",\"image\":\"\",\"excerpt\":\"Get shipping rates from the USPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/usps-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"83d1524e8f5f1913e58889f83d442c32\",\"slug\":\"woocommerce-shipping-usps\",\"id\":18657},{\"title\":\"UPS Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/UPS-Shipping-Method-Dark.png\",\"excerpt\":\"Get shipping rates from the UPS API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ups-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8dae58502913bac0fbcdcaba515ea998\",\"slug\":\"woocommerce-shipping-ups\",\"id\":18665},{\"title\":\"Shipment Tracking\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Ship-Tracking-Dark-1.png\",\"excerpt\":\"Add shipment tracking information to your orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipment-tracking\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"1968e199038a8a001c9f9966fd06bf88\",\"slug\":\"woocommerce-shipment-tracking\",\"id\":18693},{\"title\":\"Table Rate Shipping\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Product-Table-Rate-Shipping-Dark.png\",\"excerpt\":\"Advanced, flexible shipping. Define multiple shipping rates based on location, price, weight, shipping class or item count.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/table-rate-shipping\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"3034ed8aff427b0f635fe4c86bbf008a\",\"slug\":\"woocommerce-table-rate-shipping\",\"id\":18718},{\"title\":\"Checkout Field Editor\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Checkout-Field-Editor-Dark.png\",\"excerpt\":\"Optimize your checkout process by adding, removing or editing fields to suit your needs.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-field-editor\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"2b8029f0d7cdd1118f4d843eb3ab43ff\",\"slug\":\"woocommerce-checkout-field-editor\",\"id\":184594},{\"title\":\"Braintree for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/02\\/braintree-black-copy.png\",\"excerpt\":\"Accept PayPal, credit cards and debit cards with a single payment gateway solution \\u2014 PayPal Powered by Braintree.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-paypal-powered-by-braintree\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"27f010c8e34ca65b205ddec88ad14536\",\"slug\":\"woocommerce-gateway-paypal-powered-by-braintree\",\"id\":1489837},{\"title\":\"WooCommerce Bookings\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Dark.png\",\"excerpt\":\"Allow customers to book appointments, make reservations or rent equipment without leaving your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/hotel\\/\",\"price\":\"&#36;249.00\",\"hash\":\"911c438934af094c2b38d5560b9f50f3\",\"slug\":\"WooCommerce Bookings\",\"id\":390890},{\"title\":\"WooCommerce Memberships\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/06\\/Thumbnail-Memberships-updated.png\",\"excerpt\":\"Give members access to restricted content or products, for a fee or for free.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-memberships\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;199.00\",\"hash\":\"9288e7609ad0b487b81ef6232efa5cfc\",\"slug\":\"woocommerce-memberships\",\"id\":958589},{\"title\":\"Product Bundles\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-PB.png?v=1\",\"excerpt\":\"Offer personalized product bundles, bulk discount packages, and assembled\\u00a0products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-bundles\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa2518b5-ab19-4b75-bde9-60ca51e20f28\",\"slug\":\"woocommerce-product-bundles\",\"id\":18716},{\"title\":\"Multichannel for WooCommerce: Google, Amazon, eBay &amp; Walmart Integration\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/10\\/Woo-Extension-Store-Logo-v2.png\",\"excerpt\":\"Get the official Google, Amazon, eBay and Walmart extension and create, sync and manage multichannel listings directly from WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-ebay-integration\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"e4000666-9275-4c71-8619-be61fb41c9f9\",\"slug\":\"woocommerce-amazon-ebay-integration\",\"id\":3545890},{\"title\":\"Min\\/Max Quantities\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Min-Max-Qua-Dark.png\",\"excerpt\":\"Specify minimum and maximum allowed product quantities for orders to be completed.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/minmax-quantities\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"2b5188d90baecfb781a5aa2d6abb900a\",\"slug\":\"woocommerce-min-max-quantities\",\"id\":18616},{\"title\":\"FedEx Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/01\\/FedEx_Logo_Wallpaper.jpeg\",\"excerpt\":\"Get shipping rates from the FedEx API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/fedex-shipping-module\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1a48b598b47a81559baadef15e320f64\",\"slug\":\"woocommerce-shipping-fedex\",\"id\":18620},{\"title\":\"LiveChat for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2015\\/11\\/LC_woo_regular-zmiaym.png\",\"excerpt\":\"Live Chat and messaging platform for sales and support -- increase average order value and overall sales through live conversations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/livechat\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.livechat.com\\/livechat-for-ecommerce\\/?a=woocommerce&amp;utm_source=woocommerce.com&amp;utm_medium=integration&amp;utm_campaign=woocommerce.com\",\"price\":\"&#36;0.00\",\"hash\":\"5344cc1f-ed4a-4d00-beff-9d67f6d372f3\",\"slug\":\"livechat-woocommerce\",\"id\":1348888},{\"title\":\"Product CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Product-CSV-Import-Dark.png\",\"excerpt\":\"Import, merge, and export products and variations to and from WooCommerce using a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"7ac9b00a1fe980fb61d28ab54d167d0d\",\"slug\":\"woocommerce-product-csv-import-suite\",\"id\":18680},{\"title\":\"Follow-Ups\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Follow-Ups-Dark.png\",\"excerpt\":\"Automatically contact customers after purchase - be it everyone, your most loyal or your biggest spenders - and keep your store top-of-mind.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/follow-up-emails\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"05ece68fe94558e65278fe54d9ec84d2\",\"slug\":\"woocommerce-follow-up-emails\",\"id\":18686},{\"title\":\"Authorize.Net\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2013\\/04\\/Thumbnail-Authorize.net-updated.png\",\"excerpt\":\"Authorize.Net gateway with support for pre-orders and subscriptions.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/authorize-net\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8b61524fe53add7fdd1a8d1b00b9327d\",\"slug\":\"woocommerce-gateway-authorize-net-cim\",\"id\":178481},{\"title\":\"Australia Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/australia-post.gif\",\"excerpt\":\"Get shipping rates for your WooCommerce store from the Australia Post API, which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/australia-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1dbd4dc6bd91a9cda1bd6b9e7a5e4f43\",\"slug\":\"woocommerce-shipping-australia-post\",\"id\":18622},{\"title\":\"Canada Post Shipping Method\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/canada-post.png\",\"excerpt\":\"Get shipping rates from the Canada Post Ratings API which handles both domestic and international parcels.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/canada-post-shipping-method\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ac029cdf3daba20b20c7b9be7dc00e0e\",\"slug\":\"woocommerce-shipping-canada-post\",\"id\":18623},{\"title\":\"WooCommerce Customer \\/ Order \\/ Coupon Export\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-Customer-Order-Coupon-Export-updated.png\",\"excerpt\":\"Export customers, orders, and coupons from WooCommerce manually or on an automated schedule.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/ordercustomer-csv-export\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"914de15813a903c767b55445608bf290\",\"slug\":\"woocommerce-customer-order-csv-export\",\"id\":18652},{\"title\":\"Product Vendors\",\"image\":\"\",\"excerpt\":\"Turn your store into a multi-vendor marketplace\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-vendors\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"a97d99fccd651bbdd728f4d67d492c31\",\"slug\":\"woocommerce-product-vendors\",\"id\":219982},{\"title\":\"WooCommerce Accommodation Bookings\",\"image\":\"\",\"excerpt\":\"Book accommodation using WooCommerce and the WooCommerce Bookings extension.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-accommodation-bookings\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"99b2a7a4af90b6cefd2a733b3b1f78e7\",\"slug\":\"woocommerce-accommodation-bookings\",\"id\":1412069},{\"title\":\"Smart Coupons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/10\\/wc-product-smart-coupons.png\",\"excerpt\":\"Everything you need for discounts, coupons, credits, gift cards, product giveaways, offers, and promotions. Most popular and complete coupons plugin for WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/smart-coupons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demo.storeapps.org\\/?demo=sc\",\"price\":\"&#36;99.00\",\"hash\":\"05c45f2aa466106a466de4402fff9dde\",\"slug\":\"woocommerce-smart-coupons\",\"id\":18729},{\"title\":\"WooCommerce Brands\",\"image\":\"\",\"excerpt\":\"Create, assign and list brands for products, and allow customers to view by brand.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/brands\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"8a88c7cbd2f1e73636c331c7a86f818c\",\"slug\":\"woocommerce-brands\",\"id\":18737},{\"title\":\"Xero\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/08\\/xero2.png\",\"excerpt\":\"Save time with automated sync between WooCommerce and your Xero account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/xero\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"f0dd29d338d3c67cf6cee88eddf6869b\",\"slug\":\"woocommerce-xero\",\"id\":18733},{\"title\":\"Royal Mail\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/04\\/royalmail.png\",\"excerpt\":\"Offer Royal Mail shipping rates to your customers\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/royal-mail\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"03839cca1a16c4488fcb669aeb91a056\",\"slug\":\"woocommerce-shipping-royalmail\",\"id\":182719},{\"title\":\"AutomateWoo\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-AutomateWoo-Dark-1.png\",\"excerpt\":\"Powerful marketing automation for WooCommerce. AutomateWoo has the tools you need to grow your store and make more money.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/automatewoo\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"ba9299b8-1dba-4aa0-a313-28bc1755cb88\",\"slug\":\"automatewoo\",\"id\":4652610},{\"title\":\"Advanced Notifications\",\"image\":\"\",\"excerpt\":\"Easily setup \\\"new order\\\" and stock email notifications for multiple recipients of your choosing.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/advanced-notifications\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"112372c44b002fea2640bd6bfafbca27\",\"slug\":\"woocommerce-advanced-notifications\",\"id\":18740},{\"title\":\"WooCommerce Points and Rewards\",\"image\":\"\",\"excerpt\":\"Reward your customers for purchases and other actions with points which can be redeemed for discounts.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-points-and-rewards\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"1649b6cca5da8b923b01ca56b5cdd246\",\"slug\":\"woocommerce-points-and-rewards\",\"id\":210259},{\"title\":\"WooCommerce Zapier\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/woocommerce-zapier-logo.png\",\"excerpt\":\"Integrate with 3000+ cloud apps and services today.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-zapier\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;59.00\",\"hash\":\"0782bdbe932c00f4978850268c6cfe40\",\"slug\":\"woocommerce-zapier\",\"id\":243589},{\"title\":\"Dynamic Pricing\",\"image\":\"\",\"excerpt\":\"Bulk discounts, role-based pricing and much more\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/dynamic-pricing\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"9a41775bb33843f52c93c922b0053986\",\"slug\":\"woocommerce-dynamic-pricing\",\"id\":18643},{\"title\":\"WooCommerce Pre-Orders\",\"image\":\"\",\"excerpt\":\"Allow customers to order products before they are available.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-pre-orders\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"b2dc75e7d55e6f5bbfaccb59830f66b7\",\"slug\":\"woocommerce-pre-orders\",\"id\":178477},{\"title\":\"Name Your Price\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2012\\/09\\/nyp-icon-dark-v83owf.png\",\"excerpt\":\"Allow customers to define the product price. Also useful for accepting user-set donations.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/name-your-price\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"31b4e11696cd99a3c0572975a84f1c08\",\"slug\":\"woocommerce-name-your-price\",\"id\":18738},{\"title\":\"WooCommerce Subscription Downloads\",\"image\":\"\",\"excerpt\":\"Offer additional downloads to your subscribers, via downloadable products listed in your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-subscription-downloads\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5be9e21c13953253e4406d2a700382ec\",\"slug\":\"woocommerce-subscription-downloads\",\"id\":420458},{\"title\":\"WooCommerce Print Invoices &amp; Packing lists\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/03\\/Thumbnail-Print-Invoices-Packing-lists-updated.png\",\"excerpt\":\"Generate invoices, packing slips, and pick lists for your WooCommerce orders.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/print-invoices-packing-lists\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"465de1126817cdfb42d97ebca7eea717\",\"slug\":\"woocommerce-pip\",\"id\":18666},{\"title\":\"WooCommerce Additional Variation Images\",\"image\":\"\",\"excerpt\":\"Add gallery images per variation on variable products within WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-additional-variation-images\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/themes.woocommerce.com\\/storefront\\/product\\/woo-single-1\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c61dd6de57dcecb32bd7358866de4539\",\"slug\":\"woocommerce-additional-variation-images\",\"id\":477384},{\"title\":\"WooCommerce Deposits\",\"image\":\"\",\"excerpt\":\"Enable customers to pay for products using a deposit or a payment plan.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-deposits\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;179.00\",\"hash\":\"de192a6cf12c4fd803248da5db700762\",\"slug\":\"woocommerce-deposits\",\"id\":977087},{\"title\":\"Google Product Feed\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2011\\/11\\/logo-regular-lscryp.png\",\"excerpt\":\"Feed product data to Google Merchant Center for setting up Google product listings &amp; product ads.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/google-product-feed\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d55b4f852872025741312839f142447e\",\"slug\":\"woocommerce-product-feeds\",\"id\":18619},{\"title\":\"Amazon S3 Storage\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/09\\/amazon.png\",\"excerpt\":\"Serve digital products via Amazon S3\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/amazon-s3-storage\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"473bf6f221b865eff165c97881b473bb\",\"slug\":\"woocommerce-amazon-s3-storage\",\"id\":18663},{\"title\":\"Cart Add-ons\",\"image\":\"\",\"excerpt\":\"A powerful tool for driving incremental and impulse purchases by customers once they are in the shopping cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/cart-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"3a8ef25334396206f5da4cf208adeda3\",\"slug\":\"woocommerce-cart-add-ons\",\"id\":18717},{\"title\":\"Shipping Multiple Addresses\",\"image\":\"\",\"excerpt\":\"Allow your customers to ship individual items in a single order to multiple addresses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/shipping-multiple-addresses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"aa0eb6f777846d329952d5b891d6f8cc\",\"slug\":\"woocommerce-shipping-multiple-addresses\",\"id\":18741},{\"title\":\"WooCommerce AvaTax\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-Avalara-updated.png\",\"excerpt\":\"Get 100% accurate sales tax calculations and on time tax return filing. No more tracking sales tax rates, rules, or jurisdictional boundaries.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-avatax\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"57077a4b28ba71cacf692bcf4a1a7f60\",\"slug\":\"woocommerce-avatax\",\"id\":1389326},{\"title\":\"Klarna Checkout\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2018\\/01\\/Partner_marketing_Klarna_Checkout_Black-1.png\",\"excerpt\":\"Klarna Checkout is a full checkout experience embedded on your site that includes all popular payment methods (Pay Now, Pay Later, Financing, Installments).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/klarna-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.krokedil.se\\/klarnacheckout\\/\",\"price\":\"&#36;0.00\",\"hash\":\"90f8ce584e785fcd8c2d739fd4f40d78\",\"slug\":\"klarna-checkout-for-woocommerce\",\"id\":2754152},{\"title\":\"Bulk Stock Management\",\"image\":\"\",\"excerpt\":\"Edit product and variation stock levels in bulk via this handy interface\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bulk-stock-management\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"02f4328d52f324ebe06a78eaaae7934f\",\"slug\":\"woocommerce-bulk-stock-management\",\"id\":18670},{\"title\":\"PayPal Payments Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Paypal-Payments-Pro-Dark.png\",\"excerpt\":\"Take credit card payments directly on your checkout using PayPal Pro.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/paypal-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6d23ba7f0e0198937c0029f9e865b40e\",\"slug\":\"woocommerce-gateway-paypal-pro\",\"id\":18594},{\"title\":\"TaxJar\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/10\\/taxjar-logotype.png\",\"excerpt\":\"Save hours every month by putting your sales tax on autopilot. Automated, multi-state sales tax calculation, reporting, and filing for your WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/taxjar\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"12072d8e-e933-4561-97b1-9db3c7eeed91\",\"slug\":\"taxjar-simplified-taxes-for-woocommerce\",\"id\":514914},{\"title\":\"WooCommerce Email Customizer\",\"image\":\"\",\"excerpt\":\"Connect with your customers with each email you send by visually modifying your email templates via the WordPress Customizer.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-email-customizer\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"bd909fa97874d431f203b5336c7e8873\",\"slug\":\"woocommerce-email-customizer\",\"id\":853277},{\"title\":\"Force Sells\",\"image\":\"\",\"excerpt\":\"Force products to be added to the cart\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/force-sells\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"3ebddfc491ca168a4ea4800b893302b0\",\"slug\":\"woocommerce-force-sells\",\"id\":18678},{\"title\":\"WooCommerce Quick View\",\"image\":\"\",\"excerpt\":\"Show a quick-view button to view product details and add to cart via lightbox popup\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-quick-view\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"619c6e57ce72c49c4b57e15b06eddb65\",\"slug\":\"woocommerce-quick-view\",\"id\":187509},{\"title\":\"Gravity Forms Product Add-ons\",\"image\":\"\",\"excerpt\":\"Powerful product add-ons, Gravity style\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/gravity-forms-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/gravity-forms\\/\",\"price\":\"&#36;99.00\",\"hash\":\"a6ac0ab1a1536e3a357ccf24c0650ed0\",\"slug\":\"woocommerce-gravityforms-product-addons\",\"id\":18633},{\"title\":\"WooCommerce Purchase Order Gateway\",\"image\":\"\",\"excerpt\":\"Receive purchase orders via your WooCommerce-powered online store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gateway-purchase-order\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"573a92318244ece5facb449d63e74874\",\"slug\":\"woocommerce-gateway-purchase-order\",\"id\":478542},{\"title\":\"Composite Products\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CP.png?v=1\",\"excerpt\":\"Create product kit builders and custom product configurators using existing products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/composite-products\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;99.00\",\"hash\":\"0343e0115bbcb97ccd98442b8326a0af\",\"slug\":\"woocommerce-composite-products\",\"id\":216836},{\"title\":\"Returns and Warranty Requests\",\"image\":\"\",\"excerpt\":\"Manage the RMA process, add warranties to products &amp; let customers request &amp; manage returns \\/ exchanges from their account.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/warranty-requests\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"9b4c41102e6b61ea5f558e16f9b63e25\",\"slug\":\"woocommerce-warranty\",\"id\":228315},{\"title\":\"Product Enquiry Form\",\"image\":\"\",\"excerpt\":\"Allow visitors to contact you directly from the product details page via a reCAPTCHA protected form to enquire about a product.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/product-enquiry-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"5a0f5d72519a8ffcc86669f042296937\",\"slug\":\"woocommerce-product-enquiry-form\",\"id\":18601},{\"title\":\"WooCommerce Box Office\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-BO-Dark.png\",\"excerpt\":\"Sell tickets for your next event, concert, function, fundraiser or conference directly on your own site\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-box-office\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"e704c9160de318216a8fa657404b9131\",\"slug\":\"woocommerce-box-office\",\"id\":1628717},{\"title\":\"WooCommerce Order Barcodes\",\"image\":\"\",\"excerpt\":\"Generates a unique barcode for each order on your site - perfect for e-tickets, packing slips, reservations and a variety of other uses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-barcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"889835bb29ee3400923653e1e44a3779\",\"slug\":\"woocommerce-order-barcodes\",\"id\":391708},{\"title\":\"WooCommerce 360\\u00ba Image\",\"image\":\"\",\"excerpt\":\"An easy way to add a dynamic, controllable 360\\u00ba image rotation to your WooCommerce site, by adding a group of images to a product\\u2019s gallery.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-360-image\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"24eb2cfa3738a66bf3b2587876668cd2\",\"slug\":\"woocommerce-360-image\",\"id\":512186},{\"title\":\"WooCommerce Photography\",\"image\":\"\",\"excerpt\":\"Sell photos in the blink of an eye using this simple as dragging &amp; dropping interface.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-photography\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"ee76e8b9daf1d97ca4d3874cc9e35687\",\"slug\":\"woocommerce-photography\",\"id\":583602},{\"title\":\"WooCommerce Bookings Availability\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/06\\/Logo-Woo-Bookings-Aval-Dark.png\",\"excerpt\":\"Sell more bookings by presenting a calendar or schedule of available slots in a page or post.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/bookings-availability\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"30770d2a-e392-4e82-baaa-76cfc7d02ae3\",\"slug\":\"woocommerce-bookings-availability\",\"id\":4228225},{\"title\":\"Software Add-on\",\"image\":\"\",\"excerpt\":\"Sell License Keys for Software\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/software-add-on\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"79f6dbfe1f1d3a56a86f0509b6d6b04b\",\"slug\":\"woocommerce-software-add-on\",\"id\":18683},{\"title\":\"WooCommerce Products Compare\",\"image\":\"\",\"excerpt\":\"WooCommerce Products Compare will allow your potential customers to easily compare products within your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-products-compare\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"c3ba0a4a3199a0cc7a6112eb24414548\",\"slug\":\"woocommerce-products-compare\",\"id\":853117},{\"title\":\"WooCommerce Store Catalog PDF Download\",\"image\":\"\",\"excerpt\":\"Offer your customers a PDF download of your product catalog, generated by WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-store-catalog-pdf-download\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"79ca7aadafe706364e2d738b7c1090c4\",\"slug\":\"woocommerce-store-catalog-pdf-download\",\"id\":675790},{\"title\":\"WooCommerce Paid Courses\",\"image\":\"\",\"excerpt\":\"Sell your online courses using the most popular eCommerce platform on the web \\u2013 WooCommerce.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paid-courses\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;129.00\",\"hash\":\"bad2a02a063555b7e2bee59924690763\",\"slug\":\"woothemes-sensei\",\"id\":152116},{\"title\":\"eWAY\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/eway-logo-3000-2000.jpg\",\"excerpt\":\"Take credit card payments securely via eWay (SG, MY, HK, AU, and NZ) keeping customers on your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eway\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"2c497769d98d025e0d340cd0b5ea5da1\",\"slug\":\"woocommerce-gateway-eway\",\"id\":18604},{\"title\":\"Catalog Visibility Options\",\"image\":\"\",\"excerpt\":\"Transform WooCommerce into an online catalog by removing eCommerce functionality\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/catalog-visibility-options\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"12e791110365fdbb5865c8658907967e\",\"slug\":\"woocommerce-catalog-visibility-options\",\"id\":18648},{\"title\":\"WooCommerce Blocks\",\"image\":\"\",\"excerpt\":\"WooCommerce Blocks offers a range of Gutenberg blocks you can use to build and customise your site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-gutenberg-products-block\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c2e9f13a-f90c-4ffe-a8a5-b432399ec263\",\"slug\":\"woo-gutenberg-products-block\",\"id\":3076677},{\"title\":\"QuickBooks Sync for WooCommerce\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2019\\/04\\/woocommerce-com-logo-1-hyhzbh.png\",\"excerpt\":\"Automatic two-way sync for orders, customers, products, inventory and more between WooCommerce and QuickBooks (Online, Desktop, or POS).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/quickbooks-sync-for-woocommerce\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"c5e32e20-7c1f-4585-8b15-d930c2d842ac\",\"slug\":\"myworks-woo-sync-for-quickbooks-online\",\"id\":4065824},{\"title\":\"Sequential Order Numbers Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/05\\/Thumbnail-Sequential-Order-Numbers-Pro-updated.png\",\"excerpt\":\"Tame your order numbers! Advanced &amp; sequential order numbers with optional prefixes \\/ suffixes\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sequential-order-numbers-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"0b18a2816e016ba9988b93b1cd8fe766\",\"slug\":\"woocommerce-sequential-order-numbers-pro\",\"id\":18688},{\"title\":\"Conditional Shipping and Payments\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2020\\/07\\/Logo-CSP.png?v=1\",\"excerpt\":\"Use conditional logic to restrict the shipping and payment options available on your store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/conditional-shipping-and-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"1f56ff002fa830b77017b0107505211a\",\"slug\":\"woocommerce-conditional-shipping-and-payments\",\"id\":680253},{\"title\":\"WooCommerce Order Status Manager\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2015\\/02\\/Thumbnail-Order-Status-Manager-updated.png\",\"excerpt\":\"Create, edit, and delete completely custom order statuses and integrate them seamlessly into your order management flow.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-manager\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;49.00\",\"hash\":\"51fd9ab45394b4cad5a0ebf58d012342\",\"slug\":\"woocommerce-order-status-manager\",\"id\":588398},{\"title\":\"WooCommerce Checkout Add-Ons\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/07\\/Thumbnail-Checkout-Add-Ons-updated.png\",\"excerpt\":\"Highlight relevant products, offers like free shipping and other up-sells during checkout.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-checkout-add-ons\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"8fdca00b4000b7a8cc26371d0e470a8f\",\"slug\":\"woocommerce-checkout-add-ons\",\"id\":466854},{\"title\":\"WooCommerce Google Analytics Pro\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2016\\/01\\/Thumbnail-GAPro-updated.png\",\"excerpt\":\"Add advanced event tracking and enhanced eCommerce tracking to your WooCommerce site.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-google-analytics-pro\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"d8aed8b7306b509eec1589e59abe319f\",\"slug\":\"woocommerce-google-analytics-pro\",\"id\":1312497},{\"title\":\"PayPal\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2020\\/10\\/PPCP-Tile-PayPal-Logo-and-Cart-Art-2x-2-uozwz8.jpg\",\"excerpt\":\"PayPal\\u2019s latest, most complete payment processing solution. Accept PayPal exclusives, credit\\/debit cards and local payment methods. Turn on only PayPal options or process a full suite of payment methods. Enable global transactions with extensive currency and country coverage. Built and supported by WooCommerce and PayPal.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-paypal-payments\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"934115ab-e3f3-4435-9580-345b1ce21899\",\"slug\":\"woocommerce-paypal-payments\",\"id\":6410731},{\"title\":\"WooCommerce One Page Checkout\",\"image\":\"\",\"excerpt\":\"Create special pages where customers can choose products, checkout &amp; pay all on the one page.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-one-page-checkout\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"c9ba8f8352cd71b5508af5161268619a\",\"slug\":\"woocommerce-one-page-checkout\",\"id\":527886},{\"title\":\"WooCommerce Product Search\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2014\\/10\\/woocommerce-product-search-product-image-1870x960-1-jvsljj.png\",\"excerpt\":\"The perfect search engine helps customers to find and buy products quickly \\u2013 essential for every WooCommerce store.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-product-search\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/demo.itthinx.com\\/wps\\/\",\"price\":\"&#36;49.00\",\"hash\":\"c84cc8ca16ddac3408e6b6c5871133a8\",\"slug\":\"woocommerce-product-search\",\"id\":512174},{\"title\":\"First Data\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/02\\/Thumbnail-FirstData-updated.png\",\"excerpt\":\"FirstData gateway for WooCommerce\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/firstdata\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb3e32663ec0810592eaf0d097796230\",\"slug\":\"woocommerce-gateway-firstdata\",\"id\":18645},{\"title\":\"Coupon Shortcodes\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/woocommerce_uploads\\/2013\\/09\\/woocommerce-coupon-shortcodes-product-image-1870x960-1-vc5gux.png\",\"excerpt\":\"Show coupon discount info using shortcodes. Allows to render coupon information and content conditionally, based on the validity of coupons.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/coupon-shortcodes\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"ac5d9d51-70b2-4d8f-8b89-24200eea1394\",\"slug\":\"woocommerce-coupon-shortcodes\",\"id\":244762},{\"title\":\"WooSlider\",\"image\":\"\",\"excerpt\":\"WooSlider is the ultimate responsive slideshow WordPress slider plugin\\r\\n\\r\\n\\u00a0\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/wooslider\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/www.wooslider.com\\/\",\"price\":\"&#36;49.00\",\"hash\":\"209d98f3ccde6cc3de7e8732a2b20b6a\",\"slug\":\"wooslider\",\"id\":46506},{\"title\":\"WooCommerce Social Login\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/08\\/Thumbnail-Social-Login-updated.png\",\"excerpt\":\"Enable Social Login for seamless checkout and account creation.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-social-login\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"http:\\/\\/demos.skyverge.com\\/woocommerce-social-login\\/\",\"price\":\"&#36;79.00\",\"hash\":\"b231cd6367a79cc8a53b7d992d77525d\",\"slug\":\"woocommerce-social-login\",\"id\":473617},{\"title\":\"Jilt\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2017\\/12\\/Thumbnail-Jilt-updated.png\",\"excerpt\":\"All-in-one email marketing platform built for WooCommerce stores. Send newsletters, abandoned cart reminders, win-backs, welcome automations, and more.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/jilt\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;0.00\",\"hash\":\"b53aafb64dca33835e41ee06de7e9816\",\"slug\":\"jilt-for-woocommerce\",\"id\":2754876},{\"title\":\"WooCommerce Order Status Control\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2014\\/06\\/Thumbnail-Order-Status-Control-updated.png\",\"excerpt\":\"Use this extension to automatically change the order status to \\\"completed\\\" after successful payment.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/woocommerce-order-status-control\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"32400e509c7c36dcc1cd368e8267d981\",\"slug\":\"woocommerce-order-status-control\",\"id\":439037},{\"title\":\"Variation Swatches and Photos\",\"image\":\"\",\"excerpt\":\"Show color and image swatches instead of dropdowns for variable products.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/variation-swatches-and-photos\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"https:\\/\\/www.elementstark.com\\/woocommerce-extension-demos\\/product-category\\/swatches-and-photos\\/\",\"price\":\"&#36;99.00\",\"hash\":\"37bea8d549df279c8278878d081b062f\",\"slug\":\"woocommerce-variation-swatches-and-photos\",\"id\":18697},{\"title\":\"Opayo Payment Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2011\\/10\\/Opayo_logo_RGB.png\",\"excerpt\":\"Take payments on your WooCommerce store via Opayo (formally SagePay).\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/sage-pay-form\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"6bc0cca47d0274d8ef9b164f6fbec1cc\",\"slug\":\"woocommerce-gateway-sagepay-form\",\"id\":18599},{\"title\":\"EU VAT Number\",\"image\":\"\",\"excerpt\":\"Collect VAT numbers at checkout and remove the VAT charge for eligible EU businesses.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/eu-vat-number\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;29.00\",\"hash\":\"d2720c4b4bb8d6908e530355b7a2d734\",\"slug\":\"woocommerce-eu-vat-number\",\"id\":18592},{\"title\":\"Customer\\/Order\\/Coupon CSV Import Suite\",\"image\":\"https:\\/\\/woocommerce.com\\/wp-content\\/uploads\\/2012\\/07\\/Thumbnail-Customer-Order-Coupon-CSV-Import-Suite-updated.png\",\"excerpt\":\"Import both customers and orders into WooCommerce from a CSV file.\",\"link\":\"https:\\/\\/woocommerce.com\\/products\\/customerorder-csv-import-suite\\/?utm_source=product&utm_medium=upsell&utm_campaign=wcaddons\",\"demo_url\":\"\",\"price\":\"&#36;79.00\",\"hash\":\"eb00ca8317a0f64dbe185c995e5ea3df\",\"slug\":\"woocommerce-customer-order-csv-import\",\"id\":18709}]}\";s:7:\"headers\";O:25:\"Requests_Response_Headers\":1:{s:7:\"\0*\0data\";a:18:{s:6:\"server\";a:1:{i:0;s:5:\"nginx\";}s:4:\"date\";a:1:{i:0;s:29:\"Tue, 15 Jun 2021 12:08:42 GMT\";}s:12:\"content-type\";a:1:{i:0;s:31:\"application/json; charset=UTF-8\";}s:14:\"content-length\";a:1:{i:0;s:5:\"11622\";}s:12:\"x-robots-tag\";a:1:{i:0;s:7:\"noindex\";}s:4:\"link\";a:1:{i:0;s:60:\"<https://woocommerce.com/wp-json/>; rel=\"https://api.w.org/\"\";}s:22:\"x-content-type-options\";a:1:{i:0;s:7:\"nosniff\";}s:29:\"access-control-expose-headers\";a:1:{i:0;s:33:\"X-WP-Total, X-WP-TotalPages, Link\";}s:28:\"access-control-allow-headers\";a:1:{i:0;s:73:\"Authorization, X-WP-Nonce, Content-Disposition, Content-MD5, Content-Type\";}s:13:\"x-wccom-cache\";a:1:{i:0;s:3:\"HIT\";}s:13:\"cache-control\";a:1:{i:0;s:10:\"max-age=60\";}s:5:\"allow\";a:1:{i:0;s:3:\"GET\";}s:16:\"content-encoding\";a:1:{i:0;s:4:\"gzip\";}s:4:\"x-rq\";a:1:{i:0;s:16:\"ams5 87 207 3186\";}s:3:\"age\";a:1:{i:0;s:1:\"3\";}s:7:\"x-cache\";a:1:{i:0;s:3:\"hit\";}s:4:\"vary\";a:1:{i:0;s:23:\"Accept-Encoding, Origin\";}s:13:\"accept-ranges\";a:1:{i:0;s:5:\"bytes\";}}}s:11:\"status_code\";i:200;s:16:\"protocol_version\";d:1.1;s:7:\"success\";b:1;s:9:\"redirects\";i:0;s:3:\"url\";s:59:\"https://woocommerce.com/wp-json/wccom-extensions/1.0/search\";s:7:\"history\";a:0:{}s:7:\"cookies\";O:19:\"Requests_Cookie_Jar\":1:{s:10:\"\0*\0cookies\";a:0:{}}}s:11:\"\0*\0filename\";N;s:4:\"data\";N;s:7:\"headers\";N;s:6:\"status\";N;}}', 'no');
INSERT INTO `fmn_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1151, '_transient_timeout_wc_product_children_12', '1626373639', 'no'),
(1152, '_transient_wc_product_children_12', 'a:2:{s:3:\"all\";a:3:{i:0;i:33;i:1;i:32;i:2;i:31;}s:7:\"visible\";a:3:{i:0;i:33;i:1;i:32;i:2;i:31;}}', 'no'),
(1154, '_transient_timeout_wc_term_counts', '1626424968', 'no'),
(1155, '_transient_wc_term_counts', 'a:6:{i:15;s:2:\"11\";i:25;s:1:\"1\";i:26;s:0:\"\";i:27;s:1:\"1\";i:34;s:0:\"\";i:29;s:1:\"1\";}', 'no'),
(1156, '_transient_timeout_wc_var_prices_12', '1626373647', 'no'),
(1157, '_transient_wc_var_prices_12', '{\"version\":\"1623781639\",\"f9e544f77b7eac7add281ef28ca5559f\":{\"price\":{\"33\":\"210\",\"32\":\"80\",\"31\":\"100\"},\"regular_price\":{\"33\":\"250\",\"32\":\"90\",\"31\":\"140\"},\"sale_price\":{\"33\":\"210\",\"32\":\"80\",\"31\":\"100\"}}}', 'no'),
(1158, '_transient_timeout_wc_related_12', '1623868047', 'no'),
(1159, '_transient_wc_related_12', 'a:1:{s:211:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=12&exclude_ids%5B2%5D=180&exclude_ids%5B3%5D=195&exclude_ids%5B4%5D=196&exclude_ids%5B5%5D=197&exclude_ids%5B6%5D=198&exclude_ids%5B7%5D=199&exclude_ids%5B8%5D=200\";a:3:{i:0;s:3:\"201\";i:1;s:3:\"202\";i:2;s:3:\"203\";}}', 'no'),
(1164, '_transient_shipping-transient-version', '1623781963', 'yes'),
(1165, '_transient_timeout_wc_shipping_method_count_legacy', '1626373963', 'no'),
(1166, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1623781963\";s:5:\"value\";i:0;}', 'no'),
(1202, '_transient_timeout_wc_related_202', '1623914987', 'no'),
(1203, '_transient_wc_related_202', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=202\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"198\";i:6;s:3:\"199\";i:7;s:3:\"200\";i:8;s:3:\"201\";i:9;s:3:\"203\";}}', 'no'),
(1208, '_transient_timeout_wc_related_203', '1623915274', 'no'),
(1209, '_transient_wc_related_203', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=203\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"198\";i:6;s:3:\"199\";i:7;s:3:\"200\";i:8;s:3:\"201\";i:9;s:3:\"202\";}}', 'no'),
(1210, '_transient_timeout_wc_related_201', '1623915281', 'no'),
(1211, '_transient_wc_related_201', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=201\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"198\";i:6;s:3:\"199\";i:7;s:3:\"200\";i:8;s:3:\"202\";i:9;s:3:\"203\";}}', 'no'),
(1226, '_transient_timeout_wc_related_200', '1623916276', 'no'),
(1227, '_transient_wc_related_200', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=200\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"198\";i:6;s:3:\"199\";i:7;s:3:\"201\";i:8;s:3:\"202\";i:9;s:3:\"203\";}}', 'no'),
(1237, '_transient_timeout_wc_related_198', '1623917146', 'no'),
(1238, '_transient_wc_related_198', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=198\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";i:8;s:3:\"202\";i:9;s:3:\"203\";}}', 'no'),
(1240, '_transient_timeout_wc_related_199', '1623917358', 'no'),
(1241, '_transient_wc_related_199', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=199\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"195\";i:3;s:3:\"196\";i:4;s:3:\"197\";i:5;s:3:\"198\";i:6;s:3:\"200\";i:7;s:3:\"201\";i:8;s:3:\"202\";i:9;s:3:\"203\";}}', 'no'),
(1244, '_transient_timeout_wc_related_195', '1623917430', 'no'),
(1245, '_transient_wc_related_195', 'a:1:{s:51:\"limit=3&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=195\";a:10:{i:0;s:2:\"12\";i:1;s:3:\"180\";i:2;s:3:\"196\";i:3;s:3:\"197\";i:4;s:3:\"198\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";i:8;s:3:\"202\";i:9;s:3:\"203\";}}', 'no'),
(1264, '_transient_timeout_acf_plugin_updates', '1624005636', 'no'),
(1265, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"5.9.6\";}}', 'no'),
(1270, '_transient_timeout__woocommerce_helper_updates', '1623876036', 'no'),
(1271, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1623832836;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(1273, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.2.zip\";s:6:\"locale\";s:5:\"he_IL\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/he_IL/wordpress-5.7.2.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.2\";s:7:\"version\";s:5:\"5.7.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1623840739;s:15:\"version_checked\";s:5:\"5.7.2\";s:12:\"translations\";a:0:{}}', 'no'),
(1274, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1623840744;s:7:\"checked\";a:2:{s:5:\"nagar\";s:3:\"1.0\";s:15:\"twentytwentyone\";s:3:\"1.3\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:15:\"twentytwentyone\";a:6:{s:5:\"theme\";s:15:\"twentytwentyone\";s:11:\"new_version\";s:3:\"1.3\";s:3:\"url\";s:45:\"https://wordpress.org/themes/twentytwentyone/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/theme/twentytwentyone.1.3.zip\";s:8:\"requires\";s:3:\"5.3\";s:12:\"requires_php\";s:3:\"5.6\";}}s:12:\"translations\";a:0:{}}', 'no'),
(1341, '_transient_timeout__woocommerce_helper_subscriptions', '1623841054', 'no'),
(1342, '_transient__woocommerce_helper_subscriptions', 'a:0:{}', 'no'),
(1343, '_site_transient_timeout_theme_roots', '1623841954', 'no'),
(1344, '_site_transient_theme_roots', 'a:2:{s:5:\"nagar\";s:7:\"/themes\";s:15:\"twentytwentyone\";s:7:\"/themes\";}', 'no'),
(1345, '_site_transient_timeout_poptags_40cd750bba9870f18aada2478b24840a', '1623850960', 'no'),
(1346, '_site_transient_poptags_40cd750bba9870f18aada2478b24840a', 'O:8:\"stdClass\":100:{s:11:\"woocommerce\";a:3:{s:4:\"name\";s:11:\"woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:5:\"count\";i:4945;}s:6:\"widget\";a:3:{s:4:\"name\";s:6:\"widget\";s:4:\"slug\";s:6:\"widget\";s:5:\"count\";i:4771;}s:4:\"post\";a:3:{s:4:\"name\";s:4:\"post\";s:4:\"slug\";s:4:\"post\";s:5:\"count\";i:2716;}s:5:\"admin\";a:3:{s:4:\"name\";s:5:\"admin\";s:4:\"slug\";s:5:\"admin\";s:5:\"count\";i:2593;}s:5:\"posts\";a:3:{s:4:\"name\";s:5:\"posts\";s:4:\"slug\";s:5:\"posts\";s:5:\"count\";i:2003;}s:9:\"shortcode\";a:3:{s:4:\"name\";s:9:\"shortcode\";s:4:\"slug\";s:9:\"shortcode\";s:5:\"count\";i:1859;}s:8:\"comments\";a:3:{s:4:\"name\";s:8:\"comments\";s:4:\"slug\";s:8:\"comments\";s:5:\"count\";i:1837;}s:6:\"images\";a:3:{s:4:\"name\";s:6:\"images\";s:4:\"slug\";s:6:\"images\";s:5:\"count\";i:1518;}s:6:\"google\";a:3:{s:4:\"name\";s:6:\"google\";s:4:\"slug\";s:6:\"google\";s:5:\"count\";i:1506;}s:3:\"seo\";a:3:{s:4:\"name\";s:3:\"seo\";s:4:\"slug\";s:3:\"seo\";s:5:\"count\";i:1499;}s:7:\"twitter\";a:3:{s:4:\"name\";s:7:\"twitter\";s:4:\"slug\";s:7:\"twitter\";s:5:\"count\";i:1497;}s:5:\"image\";a:3:{s:4:\"name\";s:5:\"image\";s:4:\"slug\";s:5:\"image\";s:5:\"count\";i:1483;}s:8:\"facebook\";a:3:{s:4:\"name\";s:8:\"facebook\";s:4:\"slug\";s:8:\"facebook\";s:5:\"count\";i:1471;}s:7:\"sidebar\";a:3:{s:4:\"name\";s:7:\"sidebar\";s:4:\"slug\";s:7:\"sidebar\";s:5:\"count\";i:1311;}s:5:\"email\";a:3:{s:4:\"name\";s:5:\"email\";s:4:\"slug\";s:5:\"email\";s:5:\"count\";i:1266;}s:9:\"ecommerce\";a:3:{s:4:\"name\";s:9:\"ecommerce\";s:4:\"slug\";s:9:\"ecommerce\";s:5:\"count\";i:1255;}s:7:\"gallery\";a:3:{s:4:\"name\";s:7:\"gallery\";s:4:\"slug\";s:7:\"gallery\";s:5:\"count\";i:1230;}s:4:\"page\";a:3:{s:4:\"name\";s:4:\"page\";s:4:\"slug\";s:4:\"page\";s:5:\"count\";i:1153;}s:6:\"social\";a:3:{s:4:\"name\";s:6:\"social\";s:4:\"slug\";s:6:\"social\";s:5:\"count\";i:1127;}s:5:\"login\";a:3:{s:4:\"name\";s:5:\"login\";s:4:\"slug\";s:5:\"login\";s:5:\"count\";i:1059;}s:8:\"security\";a:3:{s:4:\"name\";s:8:\"security\";s:4:\"slug\";s:8:\"security\";s:5:\"count\";i:958;}s:5:\"video\";a:3:{s:4:\"name\";s:5:\"video\";s:4:\"slug\";s:5:\"video\";s:5:\"count\";i:932;}s:7:\"widgets\";a:3:{s:4:\"name\";s:7:\"widgets\";s:4:\"slug\";s:7:\"widgets\";s:5:\"count\";i:908;}s:5:\"links\";a:3:{s:4:\"name\";s:5:\"links\";s:4:\"slug\";s:5:\"links\";s:5:\"count\";i:887;}s:10:\"e-commerce\";a:3:{s:4:\"name\";s:10:\"e-commerce\";s:4:\"slug\";s:10:\"e-commerce\";s:5:\"count\";i:878;}s:4:\"spam\";a:3:{s:4:\"name\";s:4:\"spam\";s:4:\"slug\";s:4:\"spam\";s:5:\"count\";i:827;}s:6:\"slider\";a:3:{s:4:\"name\";s:6:\"slider\";s:4:\"slug\";s:6:\"slider\";s:5:\"count\";i:822;}s:7:\"content\";a:3:{s:4:\"name\";s:7:\"content\";s:4:\"slug\";s:7:\"content\";s:5:\"count\";i:810;}s:9:\"analytics\";a:3:{s:4:\"name\";s:9:\"analytics\";s:4:\"slug\";s:9:\"analytics\";s:5:\"count\";i:807;}s:4:\"form\";a:3:{s:4:\"name\";s:4:\"form\";s:4:\"slug\";s:4:\"form\";s:5:\"count\";i:785;}s:10:\"buddypress\";a:3:{s:4:\"name\";s:10:\"buddypress\";s:4:\"slug\";s:10:\"buddypress\";s:5:\"count\";i:763;}s:5:\"media\";a:3:{s:4:\"name\";s:5:\"media\";s:4:\"slug\";s:5:\"media\";s:5:\"count\";i:747;}s:3:\"rss\";a:3:{s:4:\"name\";s:3:\"rss\";s:4:\"slug\";s:3:\"rss\";s:5:\"count\";i:728;}s:6:\"search\";a:3:{s:4:\"name\";s:6:\"search\";s:4:\"slug\";s:6:\"search\";s:5:\"count\";i:728;}s:6:\"editor\";a:3:{s:4:\"name\";s:6:\"editor\";s:4:\"slug\";s:6:\"editor\";s:5:\"count\";i:718;}s:5:\"pages\";a:3:{s:4:\"name\";s:5:\"pages\";s:4:\"slug\";s:5:\"pages\";s:5:\"count\";i:714;}s:7:\"payment\";a:3:{s:4:\"name\";s:7:\"payment\";s:4:\"slug\";s:7:\"payment\";s:5:\"count\";i:683;}s:4:\"menu\";a:3:{s:4:\"name\";s:4:\"menu\";s:4:\"slug\";s:4:\"menu\";s:5:\"count\";i:682;}s:4:\"feed\";a:3:{s:4:\"name\";s:4:\"feed\";s:4:\"slug\";s:4:\"feed\";s:5:\"count\";i:666;}s:6:\"jquery\";a:3:{s:4:\"name\";s:6:\"jquery\";s:4:\"slug\";s:6:\"jquery\";s:5:\"count\";i:664;}s:12:\"contact-form\";a:3:{s:4:\"name\";s:12:\"contact form\";s:4:\"slug\";s:12:\"contact-form\";s:5:\"count\";i:663;}s:8:\"category\";a:3:{s:4:\"name\";s:8:\"category\";s:4:\"slug\";s:8:\"category\";s:5:\"count\";i:660;}s:5:\"embed\";a:3:{s:4:\"name\";s:5:\"embed\";s:4:\"slug\";s:5:\"embed\";s:5:\"count\";i:654;}s:4:\"ajax\";a:3:{s:4:\"name\";s:4:\"ajax\";s:4:\"slug\";s:4:\"ajax\";s:5:\"count\";i:649;}s:9:\"gutenberg\";a:3:{s:4:\"name\";s:9:\"gutenberg\";s:4:\"slug\";s:9:\"gutenberg\";s:5:\"count\";i:643;}s:15:\"payment-gateway\";a:3:{s:4:\"name\";s:15:\"payment gateway\";s:4:\"slug\";s:15:\"payment-gateway\";s:5:\"count\";i:613;}s:7:\"youtube\";a:3:{s:4:\"name\";s:7:\"youtube\";s:4:\"slug\";s:7:\"youtube\";s:5:\"count\";i:599;}s:3:\"css\";a:3:{s:4:\"name\";s:3:\"css\";s:4:\"slug\";s:3:\"css\";s:5:\"count\";i:598;}s:4:\"link\";a:3:{s:4:\"name\";s:4:\"link\";s:4:\"slug\";s:4:\"link\";s:5:\"count\";i:590;}s:10:\"javascript\";a:3:{s:4:\"name\";s:10:\"javascript\";s:4:\"slug\";s:10:\"javascript\";s:5:\"count\";i:590;}s:9:\"affiliate\";a:3:{s:4:\"name\";s:9:\"affiliate\";s:4:\"slug\";s:9:\"affiliate\";s:5:\"count\";i:575;}s:5:\"share\";a:3:{s:4:\"name\";s:5:\"share\";s:4:\"slug\";s:5:\"share\";s:5:\"count\";i:573;}s:10:\"responsive\";a:3:{s:4:\"name\";s:10:\"responsive\";s:4:\"slug\";s:10:\"responsive\";s:5:\"count\";i:566;}s:5:\"theme\";a:3:{s:4:\"name\";s:5:\"theme\";s:4:\"slug\";s:5:\"theme\";s:5:\"count\";i:561;}s:9:\"dashboard\";a:3:{s:4:\"name\";s:9:\"dashboard\";s:4:\"slug\";s:9:\"dashboard\";s:5:\"count\";i:559;}s:7:\"comment\";a:3:{s:4:\"name\";s:7:\"comment\";s:4:\"slug\";s:7:\"comment\";s:5:\"count\";i:557;}s:3:\"api\";a:3:{s:4:\"name\";s:3:\"api\";s:4:\"slug\";s:3:\"api\";s:5:\"count\";i:547;}s:3:\"ads\";a:3:{s:4:\"name\";s:3:\"ads\";s:4:\"slug\";s:3:\"ads\";s:5:\"count\";i:546;}s:7:\"contact\";a:3:{s:4:\"name\";s:7:\"contact\";s:4:\"slug\";s:7:\"contact\";s:5:\"count\";i:545;}s:6:\"custom\";a:3:{s:4:\"name\";s:6:\"custom\";s:4:\"slug\";s:6:\"custom\";s:5:\"count\";i:539;}s:10:\"categories\";a:3:{s:4:\"name\";s:10:\"categories\";s:4:\"slug\";s:10:\"categories\";s:5:\"count\";i:530;}s:4:\"user\";a:3:{s:4:\"name\";s:4:\"user\";s:4:\"slug\";s:4:\"user\";s:5:\"count\";i:522;}s:9:\"elementor\";a:3:{s:4:\"name\";s:9:\"elementor\";s:4:\"slug\";s:9:\"elementor\";s:5:\"count\";i:517;}s:6:\"button\";a:3:{s:4:\"name\";s:6:\"button\";s:4:\"slug\";s:6:\"button\";s:5:\"count\";i:512;}s:5:\"block\";a:3:{s:4:\"name\";s:5:\"block\";s:4:\"slug\";s:5:\"block\";s:5:\"count\";i:508;}s:6:\"events\";a:3:{s:4:\"name\";s:6:\"events\";s:4:\"slug\";s:6:\"events\";s:5:\"count\";i:504;}s:4:\"tags\";a:3:{s:4:\"name\";s:4:\"tags\";s:4:\"slug\";s:4:\"tags\";s:5:\"count\";i:500;}s:6:\"mobile\";a:3:{s:4:\"name\";s:6:\"mobile\";s:4:\"slug\";s:6:\"mobile\";s:5:\"count\";i:496;}s:9:\"marketing\";a:3:{s:4:\"name\";s:9:\"marketing\";s:4:\"slug\";s:9:\"marketing\";s:5:\"count\";i:492;}s:5:\"users\";a:3:{s:4:\"name\";s:5:\"users\";s:4:\"slug\";s:5:\"users\";s:5:\"count\";i:486;}s:4:\"chat\";a:3:{s:4:\"name\";s:4:\"chat\";s:4:\"slug\";s:4:\"chat\";s:5:\"count\";i:478;}s:5:\"popup\";a:3:{s:4:\"name\";s:5:\"popup\";s:4:\"slug\";s:5:\"popup\";s:5:\"count\";i:467;}s:8:\"calendar\";a:3:{s:4:\"name\";s:8:\"calendar\";s:4:\"slug\";s:8:\"calendar\";s:5:\"count\";i:461;}s:5:\"forms\";a:3:{s:4:\"name\";s:5:\"forms\";s:4:\"slug\";s:5:\"forms\";s:5:\"count\";i:459;}s:14:\"contact-form-7\";a:3:{s:4:\"name\";s:14:\"contact form 7\";s:4:\"slug\";s:14:\"contact-form-7\";s:5:\"count\";i:452;}s:10:\"newsletter\";a:3:{s:4:\"name\";s:10:\"newsletter\";s:4:\"slug\";s:10:\"newsletter\";s:5:\"count\";i:446;}s:10:\"navigation\";a:3:{s:4:\"name\";s:10:\"navigation\";s:4:\"slug\";s:10:\"navigation\";s:5:\"count\";i:446;}s:5:\"photo\";a:3:{s:4:\"name\";s:5:\"photo\";s:4:\"slug\";s:5:\"photo\";s:5:\"count\";i:445;}s:8:\"shipping\";a:3:{s:4:\"name\";s:8:\"shipping\";s:4:\"slug\";s:8:\"shipping\";s:5:\"count\";i:445;}s:9:\"slideshow\";a:3:{s:4:\"name\";s:9:\"slideshow\";s:4:\"slug\";s:9:\"slideshow\";s:5:\"count\";i:443;}s:5:\"stats\";a:3:{s:4:\"name\";s:5:\"stats\";s:4:\"slug\";s:5:\"stats\";s:5:\"count\";i:432;}s:6:\"photos\";a:3:{s:4:\"name\";s:6:\"photos\";s:4:\"slug\";s:6:\"photos\";s:5:\"count\";i:426;}s:10:\"statistics\";a:3:{s:4:\"name\";s:10:\"statistics\";s:4:\"slug\";s:10:\"statistics\";s:5:\"count\";i:420;}s:11:\"performance\";a:3:{s:4:\"name\";s:11:\"performance\";s:4:\"slug\";s:11:\"performance\";s:5:\"count\";i:413;}s:12:\"social-media\";a:3:{s:4:\"name\";s:12:\"social media\";s:4:\"slug\";s:12:\"social-media\";s:5:\"count\";i:411;}s:4:\"news\";a:3:{s:4:\"name\";s:4:\"news\";s:4:\"slug\";s:4:\"news\";s:5:\"count\";i:408;}s:8:\"redirect\";a:3:{s:4:\"name\";s:8:\"redirect\";s:4:\"slug\";s:8:\"redirect\";s:5:\"count\";i:407;}s:10:\"shortcodes\";a:3:{s:4:\"name\";s:10:\"shortcodes\";s:4:\"slug\";s:10:\"shortcodes\";s:5:\"count\";i:400;}s:12:\"notification\";a:3:{s:4:\"name\";s:12:\"notification\";s:4:\"slug\";s:12:\"notification\";s:5:\"count\";i:394;}s:4:\"code\";a:3:{s:4:\"name\";s:4:\"code\";s:4:\"slug\";s:4:\"code\";s:5:\"count\";i:389;}s:7:\"plugins\";a:3:{s:4:\"name\";s:7:\"plugins\";s:4:\"slug\";s:7:\"plugins\";s:5:\"count\";i:388;}s:9:\"multisite\";a:3:{s:4:\"name\";s:9:\"multisite\";s:4:\"slug\";s:9:\"multisite\";s:5:\"count\";i:380;}s:3:\"url\";a:3:{s:4:\"name\";s:3:\"url\";s:4:\"slug\";s:3:\"url\";s:5:\"count\";i:379;}s:8:\"tracking\";a:3:{s:4:\"name\";s:8:\"tracking\";s:4:\"slug\";s:8:\"tracking\";s:5:\"count\";i:378;}s:4:\"meta\";a:3:{s:4:\"name\";s:4:\"meta\";s:4:\"slug\";s:4:\"meta\";s:5:\"count\";i:371;}s:4:\"list\";a:3:{s:4:\"name\";s:4:\"list\";s:4:\"slug\";s:4:\"list\";s:5:\"count\";i:367;}s:6:\"import\";a:3:{s:4:\"name\";s:6:\"import\";s:4:\"slug\";s:6:\"import\";s:5:\"count\";i:366;}s:16:\"google-analytics\";a:3:{s:4:\"name\";s:16:\"google analytics\";s:4:\"slug\";s:16:\"google-analytics\";s:5:\"count\";i:358;}s:5:\"cache\";a:3:{s:4:\"name\";s:5:\"cache\";s:4:\"slug\";s:5:\"cache\";s:5:\"count\";i:357;}s:16:\"custom-post-type\";a:3:{s:4:\"name\";s:16:\"custom post type\";s:4:\"slug\";s:16:\"custom-post-type\";s:5:\"count\";i:348;}}', 'no'),
(1349, 'yit_recently_activated', 'a:1:{i:0;s:34:\"yith-woocommerce-wishlist/init.php\";}', 'yes'),
(1350, 'yith_wcwl_wishlist_page_id', '253', 'yes'),
(1351, 'yith_wcwl_version', '3.0.22', 'yes'),
(1352, 'yith_wcwl_db_version', '3.0.0', 'yes'),
(1353, 'yith_wcwl_ajax_enable', 'no', 'yes'),
(1354, 'yith_wfbt_enable_integration', 'yes', 'yes'),
(1355, 'yith_wcwl_after_add_to_wishlist_behaviour', 'view', 'yes'),
(1356, 'yith_wcwl_show_on_loop', 'no', 'yes'),
(1357, 'yith_wcwl_loop_position', 'after_add_to_cart', 'yes'),
(1358, 'yith_wcwl_button_position', 'shortcode', 'yes'),
(1359, 'yith_wcwl_add_to_wishlist_text', 'Add to wishlist', 'yes'),
(1360, 'yith_wcwl_product_added_text', 'המוצר נשמר!', 'yes'),
(1361, 'yith_wcwl_browse_wishlist_text', 'עיון ברשימת המשאלות', 'yes'),
(1362, 'yith_wcwl_already_in_wishlist_text', 'המוצר כבר ברשימת המשאלות שלך!', 'yes'),
(1363, 'yith_wcwl_add_to_wishlist_style', 'link', 'yes'),
(1364, 'yith_wcwl_rounded_corners_radius', '16', 'yes'),
(1365, 'yith_wcwl_add_to_wishlist_icon', 'fa-heart-o', 'yes'),
(1366, 'yith_wcwl_add_to_wishlist_custom_icon', '', 'yes'),
(1367, 'yith_wcwl_added_to_wishlist_icon', 'fa-heart', 'yes'),
(1368, 'yith_wcwl_added_to_wishlist_custom_icon', '', 'yes'),
(1369, 'yith_wcwl_custom_css', '', 'yes'),
(1370, 'yith_wcwl_variation_show', 'no', 'yes'),
(1371, 'yith_wcwl_price_show', 'yes', 'yes'),
(1372, 'yith_wcwl_stock_show', 'yes', 'yes'),
(1373, 'yith_wcwl_show_dateadded', 'no', 'yes'),
(1374, 'yith_wcwl_add_to_cart_show', 'yes', 'yes'),
(1375, 'yith_wcwl_show_remove', 'yes', 'yes'),
(1376, 'yith_wcwl_repeat_remove_button', 'no', 'yes'),
(1377, 'yith_wcwl_redirect_cart', 'no', 'yes'),
(1378, 'yith_wcwl_remove_after_add_to_cart', 'yes', 'yes'),
(1379, 'yith_wcwl_enable_share', 'yes', 'yes'),
(1380, 'yith_wcwl_share_fb', 'yes', 'yes'),
(1381, 'yith_wcwl_share_twitter', 'yes', 'yes'),
(1382, 'yith_wcwl_share_pinterest', 'yes', 'yes'),
(1383, 'yith_wcwl_share_email', 'yes', 'yes'),
(1384, 'yith_wcwl_share_whatsapp', 'yes', 'yes'),
(1385, 'yith_wcwl_share_url', 'no', 'yes'),
(1386, 'yith_wcwl_socials_title', 'רשימת המשאלות שלי ב-nagar', 'yes'),
(1387, 'yith_wcwl_socials_text', '', 'yes'),
(1388, 'yith_wcwl_socials_image_url', '', 'yes'),
(1389, 'yith_wcwl_wishlist_title', 'רשימת המשאלות שלך', 'yes'),
(1390, 'yith_wcwl_add_to_cart_text', 'Add to cart', 'yes'),
(1391, 'yith_wcwl_add_to_cart_style', 'link', 'yes'),
(1392, 'yith_wcwl_add_to_cart_rounded_corners_radius', '16', 'yes'),
(1393, 'yith_wcwl_add_to_cart_icon', 'fa-shopping-cart', 'yes'),
(1394, 'yith_wcwl_add_to_cart_custom_icon', '', 'yes'),
(1395, 'yith_wcwl_color_headers_background', '#F4F4F4', 'yes'),
(1396, 'yith_wcwl_fb_button_icon', 'fa-facebook', 'yes'),
(1397, 'yith_wcwl_fb_button_custom_icon', '', 'yes'),
(1398, 'yith_wcwl_tw_button_icon', 'fa-twitter', 'yes'),
(1399, 'yith_wcwl_tw_button_custom_icon', '', 'yes'),
(1400, 'yith_wcwl_pr_button_icon', 'fa-pinterest', 'yes'),
(1401, 'yith_wcwl_pr_button_custom_icon', '', 'yes'),
(1402, 'yith_wcwl_em_button_icon', 'fa-envelope-o', 'yes'),
(1403, 'yith_wcwl_em_button_custom_icon', '', 'yes'),
(1404, 'yith_wcwl_wa_button_icon', 'fa-whatsapp', 'yes'),
(1405, 'yith_wcwl_wa_button_custom_icon', '', 'yes'),
(1406, 'yit_plugin_fw_panel_wc_default_options_set', 'a:1:{s:15:\"yith_wcwl_panel\";b:1;}', 'yes'),
(1407, 'yith_plugin_fw_promo_2019_bis', '1', 'yes'),
(1408, '_site_transient_timeout_yith_promo_message', '3247777576', 'no'),
(1409, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n</promotions>', 'no'),
(1415, '_transient_timeout_yith_wcwl_hidden_products', '1626432414', 'no'),
(1416, '_transient_yith_wcwl_hidden_products', 'a:0:{}', 'no'),
(1423, '_site_transient_update_plugins', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1623840743;s:8:\"response\";a:0:{}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2021-02-05 18:15:06\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.4.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:5:\"5.4.1\";s:7:\"updated\";s:19:\"2021-06-10 19:42:09\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/5.4.1/he_IL.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"he_IL\";s:7:\"version\";s:4:\"16.5\";s:7:\"updated\";s:19:\"2021-03-22 15:34:46\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/16.5/he_IL.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:8:{s:33:\"classic-editor/classic-editor.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/classic-editor\";s:4:\"slug\";s:14:\"classic-editor\";s:6:\"plugin\";s:33:\"classic-editor/classic-editor.php\";s:11:\"new_version\";s:3:\"1.6\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/classic-editor/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/classic-editor.1.6.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-256x256.png?rev=1998671\";s:2:\"1x\";s:67:\"https://ps.w.org/classic-editor/assets/icon-128x128.png?rev=1998671\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/classic-editor/assets/banner-1544x500.png?rev=1998671\";s:2:\"1x\";s:69:\"https://ps.w.org/classic-editor/assets/banner-772x250.png?rev=1998676\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.4.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.1.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:32:\"w.org/plugins/contact-form-cfdb7\";s:4:\"slug\";s:18:\"contact-form-cfdb7\";s:6:\"plugin\";s:42:\"contact-form-cfdb7/contact-form-cfdb-7.php\";s:11:\"new_version\";s:7:\"1.2.5.9\";s:3:\"url\";s:49:\"https://wordpress.org/plugins/contact-form-cfdb7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-cfdb7.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-256x256.png?rev=1619878\";s:2:\"1x\";s:71:\"https://ps.w.org/contact-form-cfdb7/assets/icon-128x128.png?rev=1619878\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:73:\"https://ps.w.org/contact-form-cfdb7/assets/banner-772x250.png?rev=1619902\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"fancy-gallery/plugin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/fancy-gallery\";s:4:\"slug\";s:13:\"fancy-gallery\";s:6:\"plugin\";s:24:\"fancy-gallery/plugin.php\";s:11:\"new_version\";s:6:\"1.6.56\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/fancy-gallery/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/fancy-gallery.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:66:\"https://ps.w.org/fancy-gallery/assets/icon-128x128.png?rev=1723946\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/fancy-gallery/assets/banner-772x250.png?rev=1723946\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"jquery-updater/jquery-updater.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/jquery-updater\";s:4:\"slug\";s:14:\"jquery-updater\";s:6:\"plugin\";s:33:\"jquery-updater/jquery-updater.php\";s:11:\"new_version\";s:7:\"3.6.0.1\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/jquery-updater/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/jquery-updater.3.6.0.1.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:58:\"https://s.w.org/plugins/geopattern-icon/jquery-updater.svg\";}s:7:\"banners\";a:0:{}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"5.4.1\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.5.4.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2366418\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2366418\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2366418\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2366418\";}s:11:\"banners_rtl\";a:0:{}}s:34:\"yith-woocommerce-wishlist/init.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:39:\"w.org/plugins/yith-woocommerce-wishlist\";s:4:\"slug\";s:25:\"yith-woocommerce-wishlist\";s:6:\"plugin\";s:34:\"yith-woocommerce-wishlist/init.php\";s:11:\"new_version\";s:6:\"3.0.22\";s:3:\"url\";s:56:\"https://wordpress.org/plugins/yith-woocommerce-wishlist/\";s:7:\"package\";s:75:\"https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.3.0.22.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:78:\"https://ps.w.org/yith-woocommerce-wishlist/assets/icon-128x128.jpg?rev=2215573\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:81:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-1544x500.jpg?rev=2209192\";s:2:\"1x\";s:80:\"https://ps.w.org/yith-woocommerce-wishlist/assets/banner-772x250.jpg?rev=2209192\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"16.5\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.16.5.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=2363699\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=2363699\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no'),
(1425, 'yith_wcwl_color_add_to_cart', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#4F4F4F\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#4F4F4F\";}', 'yes'),
(1426, 'yith_wcwl_color_button_style_1', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#4F4F4F\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#4F4F4F\";}', 'yes'),
(1427, 'yith_wcwl_color_button_style_2', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#4F4F4F\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#4F4F4F\";}', 'yes'),
(1428, 'yith_wcwl_color_wishlist_table', 'a:3:{s:10:\"background\";s:7:\"#FFFFFF\";s:4:\"text\";s:7:\"#6d6c6c\";s:6:\"border\";s:7:\"#FFFFFF\";}', 'yes'),
(1429, 'yith_wcwl_color_share_button', 'a:2:{s:5:\"color\";s:7:\"#FFFFFF\";s:11:\"color_hover\";s:7:\"#FFFFFF\";}', 'yes'),
(1430, 'yith_wcwl_color_fb_button', 'a:2:{s:10:\"background\";s:7:\"#39599E\";s:16:\"background_hover\";s:7:\"#595A5A\";}', 'yes'),
(1431, 'yith_wcwl_color_tw_button', 'a:2:{s:10:\"background\";s:7:\"#45AFE2\";s:16:\"background_hover\";s:7:\"#595A5A\";}', 'yes'),
(1432, 'yith_wcwl_color_pr_button', 'a:2:{s:10:\"background\";s:7:\"#AB2E31\";s:16:\"background_hover\";s:7:\"#595A5A\";}', 'yes'),
(1433, 'yith_wcwl_color_em_button', 'a:2:{s:10:\"background\";s:7:\"#FBB102\";s:16:\"background_hover\";s:7:\"#595A5A\";}', 'yes'),
(1434, 'yith_wcwl_color_wa_button', 'a:2:{s:10:\"background\";s:7:\"#00A901\";s:16:\"background_hover\";s:7:\"#595A5A\";}', 'yes'),
(1437, 'yith_wcwl_color_add_to_wishlist', 'a:6:{s:10:\"background\";s:7:\"#333333\";s:4:\"text\";s:7:\"#FFFFFF\";s:6:\"border\";s:7:\"#333333\";s:16:\"background_hover\";s:7:\"#333333\";s:10:\"text_hover\";s:7:\"#FFFFFF\";s:12:\"border_hover\";s:7:\"#333333\";}', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_postmeta`
--

CREATE TABLE `fmn_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_postmeta`
--

INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(9, 6, '_wp_attached_file', 'woocommerce-placeholder.png'),
(10, 6, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:5:{s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:33:\"woocommerce-placeholder-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 12, '_edit_last', '1'),
(12, 12, '_edit_lock', '1623832154:1'),
(13, 12, 'total_sales', '0'),
(14, 12, '_tax_status', 'taxable'),
(15, 12, '_tax_class', ''),
(16, 12, '_manage_stock', 'yes'),
(17, 12, '_backorders', 'no'),
(18, 12, '_sold_individually', 'no'),
(19, 12, '_virtual', 'no'),
(20, 12, '_downloadable', 'no'),
(21, 12, '_download_limit', '-1'),
(22, 12, '_download_expiry', '-1'),
(24, 12, '_stock_status', 'instock'),
(25, 12, '_wc_average_rating', '0'),
(26, 12, '_wc_review_count', '0'),
(27, 12, '_product_version', '5.4.1'),
(29, 1, '_edit_lock', '1623312819:1'),
(30, 13, '_edit_last', '1'),
(31, 13, '_edit_lock', '1623278626:1'),
(32, 15, '_edit_last', '1'),
(33, 15, '_edit_lock', '1623315991:1'),
(34, 19, '_wp_attached_file', '2021/06/test-post-2.png'),
(35, 19, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:226;s:6:\"height\";i:181;s:4:\"file\";s:23:\"2021/06/test-post-2.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-2-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(36, 20, '_wp_attached_file', '2021/06/test-post-3.png'),
(37, 20, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:226;s:6:\"height\";i:181;s:4:\"file\";s:23:\"2021/06/test-post-3.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-3-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(38, 21, '_wp_attached_file', '2021/06/test-post-4.png'),
(39, 21, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:226;s:6:\"height\";i:181;s:4:\"file\";s:23:\"2021/06/test-post-4.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-4-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(40, 22, '_wp_attached_file', '2021/06/test-post-5.png'),
(41, 22, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:226;s:6:\"height\";i:181;s:4:\"file\";s:23:\"2021/06/test-post-5.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-5-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 23, '_wp_attached_file', '2021/06/test-post-6.png'),
(43, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:226;s:6:\"height\";i:181;s:4:\"file\";s:23:\"2021/06/test-post-6.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-6-117x94.png\";s:5:\"width\";i:117;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 24, '_wp_attached_file', '2021/06/test-post-1.png'),
(45, 24, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:688;s:6:\"height\";i:550;s:4:\"file\";s:23:\"2021/06/test-post-1.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"test-post-1-300x240.png\";s:5:\"width\";i:300;s:6:\"height\";i:240;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"test-post-1-118x94.png\";s:5:\"width\";i:118;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"test-post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"test-post-1-600x480.png\";s:5:\"width\";i:600;s:6:\"height\";i:480;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"test-post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"test-post-1-600x480.png\";s:5:\"width\";i:600;s:6:\"height\";i:480;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"test-post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 1, '_edit_last', '1'),
(47, 1, '_thumbnail_id', '24'),
(49, 1, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(50, 1, '_post_gallery', 'field_60bfd9909cfa3'),
(51, 1, 'title_tag', ''),
(52, 1, '_title_tag', 'field_5ddbe7577e0e7'),
(53, 1, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(54, 1, '_same_text', 'field_60bfdb8d2a515'),
(55, 1, 'same_posts', 'a:3:{i:0;s:2:\"54\";i:1;s:2:\"56\";i:2;s:2:\"58\";}'),
(56, 1, '_same_posts', 'field_60bfdb9f2a516'),
(57, 1, 'single_slider_seo', ''),
(58, 1, '_single_slider_seo', 'field_5ddbde5499115'),
(59, 1, 'single_gallery', ''),
(60, 1, '_single_gallery', 'field_5ddbe5aea4275'),
(61, 1, 'single_video_slider', ''),
(62, 1, '_single_video_slider', 'field_5ddbe636a4276'),
(63, 26, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(64, 26, '_post_gallery', 'field_60bfd9909cfa3'),
(65, 26, 'title_tag', ''),
(66, 26, '_title_tag', 'field_5ddbe7577e0e7'),
(67, 26, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(68, 26, '_same_text', 'field_60bfdb8d2a515'),
(69, 26, 'same_posts', ''),
(70, 26, '_same_posts', 'field_60bfdb9f2a516'),
(71, 26, 'single_slider_seo', ''),
(72, 26, '_single_slider_seo', 'field_5ddbde5499115'),
(73, 26, 'single_gallery', ''),
(74, 26, '_single_gallery', 'field_5ddbe5aea4275'),
(75, 26, 'single_video_slider', ''),
(76, 26, '_single_video_slider', 'field_5ddbe636a4276'),
(77, 27, '_menu_item_type', 'post_type'),
(78, 27, '_menu_item_menu_item_parent', '0'),
(79, 27, '_menu_item_object_id', '1'),
(80, 27, '_menu_item_object', 'post'),
(81, 27, '_menu_item_target', ''),
(82, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(83, 27, '_menu_item_xfn', ''),
(84, 27, '_menu_item_url', ''),
(86, 28, '_edit_last', '1'),
(87, 28, '_edit_lock', '1623674067:1'),
(88, 28, '_wp_page_template', 'views/home.php'),
(89, 28, 'title_tag', ''),
(90, 28, '_title_tag', 'field_5ddbe7577e0e7'),
(91, 28, 'single_slider_seo', ''),
(92, 28, '_single_slider_seo', 'field_5ddbde5499115'),
(93, 28, 'single_gallery', ''),
(94, 28, '_single_gallery', 'field_5ddbe5aea4275'),
(95, 28, 'single_video_slider', ''),
(96, 28, '_single_video_slider', 'field_5ddbe636a4276'),
(97, 29, 'title_tag', ''),
(98, 29, '_title_tag', 'field_5ddbe7577e0e7'),
(99, 29, 'single_slider_seo', ''),
(100, 29, '_single_slider_seo', 'field_5ddbde5499115'),
(101, 29, 'single_gallery', ''),
(102, 29, '_single_gallery', 'field_5ddbe5aea4275'),
(103, 29, 'single_video_slider', ''),
(104, 29, '_single_video_slider', 'field_5ddbe636a4276'),
(105, 12, '_product_attributes', 'a:1:{s:7:\"pa_size\";a:6:{s:4:\"name\";s:7:\"pa_size\";s:5:\"value\";s:0:\"\";s:8:\"position\";i:0;s:10:\"is_visible\";i:1;s:12:\"is_variation\";i:1;s:11:\"is_taxonomy\";i:1;}}'),
(106, 12, '_sku', '1234'),
(108, 30, '_wp_attached_file', '2021/06/product-1.png'),
(109, 30, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:360;s:6:\"height\";i:470;s:4:\"file\";s:21:\"2021/06/product-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-1-230x300.png\";s:5:\"width\";i:230;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-1-72x94.png\";s:5:\"width\";i:72;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(112, 12, '_stock', '20'),
(113, 31, '_variation_description', ''),
(114, 31, 'total_sales', '0'),
(115, 31, '_tax_status', 'taxable'),
(116, 31, '_tax_class', 'parent'),
(117, 31, '_manage_stock', 'no'),
(118, 31, '_backorders', 'no'),
(119, 31, '_sold_individually', 'no'),
(120, 31, '_virtual', 'no'),
(121, 31, '_downloadable', 'no'),
(122, 31, '_download_limit', '-1'),
(123, 31, '_download_expiry', '-1'),
(124, 31, '_stock', '0'),
(125, 31, '_stock_status', 'instock'),
(126, 31, '_wc_average_rating', '0'),
(127, 31, '_wc_review_count', '0'),
(128, 31, 'attribute_pa_size', 'big'),
(129, 31, '_product_version', '5.4.0'),
(130, 31, '_regular_price', '140'),
(131, 31, '_sale_price', '100'),
(132, 31, '_thumbnail_id', '30'),
(133, 31, '_price', '100'),
(135, 32, '_variation_description', ''),
(136, 32, 'total_sales', '0'),
(137, 32, '_tax_status', 'taxable'),
(138, 32, '_tax_class', 'parent'),
(139, 32, '_manage_stock', 'no'),
(140, 32, '_backorders', 'no'),
(141, 32, '_sold_individually', 'no'),
(142, 32, '_virtual', 'no'),
(143, 32, '_downloadable', 'no'),
(144, 32, '_download_limit', '-1'),
(145, 32, '_download_expiry', '-1'),
(146, 32, '_stock', '0'),
(147, 32, '_stock_status', 'instock'),
(148, 32, '_wc_average_rating', '0'),
(149, 32, '_wc_review_count', '0'),
(150, 32, 'attribute_pa_size', 'regular'),
(151, 32, '_product_version', '5.4.0'),
(153, 32, '_regular_price', '90'),
(154, 32, '_sale_price', '80'),
(155, 32, '_price', '80'),
(158, 33, '_variation_description', ''),
(159, 33, 'total_sales', '0'),
(160, 33, '_tax_status', 'taxable'),
(161, 33, '_tax_class', 'parent'),
(162, 33, '_manage_stock', 'no'),
(163, 33, '_backorders', 'no'),
(164, 33, '_sold_individually', 'no'),
(165, 33, '_virtual', 'no'),
(166, 33, '_downloadable', 'no'),
(167, 33, '_download_limit', '-1'),
(168, 33, '_download_expiry', '-1'),
(169, 33, '_stock', '0'),
(170, 33, '_stock_status', 'instock'),
(171, 33, '_wc_average_rating', '0'),
(172, 33, '_wc_review_count', '0'),
(173, 33, 'attribute_pa_size', 'large'),
(174, 33, '_product_version', '5.4.0'),
(177, 32, '_thumbnail_id', '21'),
(178, 33, '_regular_price', '250'),
(179, 33, '_sale_price', '210'),
(180, 33, '_thumbnail_id', '21'),
(181, 33, '_price', '210'),
(182, 12, '_price', '80'),
(183, 12, '_price', '100'),
(184, 12, '_price', '210'),
(185, 12, '_thumbnail_id', '30'),
(187, 34, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(188, 34, '_post_gallery', 'field_60bfd9909cfa3'),
(189, 34, 'title_tag', ''),
(190, 34, '_title_tag', 'field_5ddbe7577e0e7'),
(191, 34, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(192, 34, '_same_text', 'field_60bfdb8d2a515'),
(193, 34, 'same_posts', ''),
(194, 34, '_same_posts', 'field_60bfdb9f2a516'),
(195, 34, 'single_slider_seo', ''),
(196, 34, '_single_slider_seo', 'field_5ddbde5499115'),
(197, 34, 'single_gallery', ''),
(198, 34, '_single_gallery', 'field_5ddbe5aea4275'),
(199, 34, 'single_video_slider', ''),
(200, 34, '_single_video_slider', 'field_5ddbe636a4276'),
(207, 35, '_edit_lock', '1623758529:1'),
(208, 35, '_edit_last', '1'),
(209, 43, '_edit_last', '1'),
(210, 43, '_edit_lock', '1623233873:1'),
(211, 44, '_wp_attached_file', '2021/06/post-1-e1623233987672.png'),
(212, 44, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:612;s:6:\"height\";i:336;s:4:\"file\";s:33:\"2021/06/post-1-e1623233987672.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-1-e1623233987672-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-1-e1623233987672-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(213, 45, '_wp_attached_file', '2021/06/post-2-e1623234009372.png'),
(214, 45, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:615;s:6:\"height\";i:337;s:4:\"file\";s:33:\"2021/06/post-2-e1623234009372.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-300x164.png\";s:5:\"width\";i:300;s:6:\"height\";i:164;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-2-e1623234009372-134x73.png\";s:5:\"width\";i:134;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-2-e1623234009372-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(215, 46, '_wp_attached_file', '2021/06/post-3-e1623234033923.png'),
(216, 46, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:338;s:4:\"file\";s:33:\"2021/06/post-3-e1623234033923.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-300x167.png\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-3-e1623234033923-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-600x333.png\";s:5:\"width\";i:600;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-600x333.png\";s:5:\"width\";i:600;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-3-e1623234033923-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(217, 47, '_wp_attached_file', '2021/06/post-4-e1623233944504.png'),
(218, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:337;s:4:\"file\";s:33:\"2021/06/post-4-e1623233944504.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-300x166.png\";s:5:\"width\";i:300;s:6:\"height\";i:166;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-4-e1623233944504-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-600x332.png\";s:5:\"width\";i:600;s:6:\"height\";i:332;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-600x332.png\";s:5:\"width\";i:600;s:6:\"height\";i:332;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-4-e1623233944504-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(219, 48, '_wp_attached_file', '2021/06/post-5-e1623234111636.png'),
(220, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:617;s:6:\"height\";i:335;s:4:\"file\";s:33:\"2021/06/post-5-e1623234111636.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-300x163.png\";s:5:\"width\";i:300;s:6:\"height\";i:163;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-5-e1623234111636-134x73.png\";s:5:\"width\";i:134;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-600x326.png\";s:5:\"width\";i:600;s:6:\"height\";i:326;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-600x326.png\";s:5:\"width\";i:600;s:6:\"height\";i:326;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-5-e1623234111636-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(221, 49, '_wp_attached_file', '2021/06/post-6-e1623234134132.png'),
(222, 49, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:608;s:6:\"height\";i:334;s:4:\"file\";s:33:\"2021/06/post-6-e1623234134132.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-6-e1623234134132-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-6-e1623234134132-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 50, '_wp_attached_file', '2021/06/post-7-e1623234172102.png'),
(224, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:612;s:6:\"height\";i:337;s:4:\"file\";s:33:\"2021/06/post-7-e1623234172102.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-7-e1623234172102-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-7-e1623234172102-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(225, 51, '_wp_attached_file', '2021/06/post-8-e1623234201426.png'),
(226, 51, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:609;s:6:\"height\";i:335;s:4:\"file\";s:33:\"2021/06/post-8-e1623234201426.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-8-e1623234201426-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-600x330.png\";s:5:\"width\";i:600;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-8-e1623234201426-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(227, 52, '_wp_attached_file', '2021/06/post-9-e1623234226437.png'),
(228, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:604;s:6:\"height\";i:340;s:4:\"file\";s:33:\"2021/06/post-9-e1623234226437.png\";s:5:\"sizes\";a:9:{s:6:\"medium\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:32:\"post-9-e1623234226437-134x75.png\";s:5:\"width\";i:134;s:6:\"height\";i:75;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-600x338.png\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-600x338.png\";s:5:\"width\";i:600;s:6:\"height\";i:338;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"post-9-e1623234226437-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(229, 43, '_thumbnail_id', '44'),
(231, 43, 'post_gallery', ''),
(232, 43, '_post_gallery', 'field_60bfd9909cfa3'),
(233, 43, 'title_tag', ''),
(234, 43, '_title_tag', 'field_5ddbe7577e0e7'),
(235, 43, 'same_text', ''),
(236, 43, '_same_text', 'field_60bfdb8d2a515'),
(237, 43, 'same_posts', ''),
(238, 43, '_same_posts', 'field_60bfdb9f2a516'),
(239, 43, 'single_slider_seo', ''),
(240, 43, '_single_slider_seo', 'field_5ddbde5499115'),
(241, 43, 'slider_img', ''),
(242, 43, '_slider_img', 'field_60c08d4ff9afa'),
(243, 53, 'post_gallery', ''),
(244, 53, '_post_gallery', 'field_60bfd9909cfa3'),
(245, 53, 'title_tag', ''),
(246, 53, '_title_tag', 'field_5ddbe7577e0e7'),
(247, 53, 'same_text', ''),
(248, 53, '_same_text', 'field_60bfdb8d2a515'),
(249, 53, 'same_posts', ''),
(250, 53, '_same_posts', 'field_60bfdb9f2a516'),
(251, 53, 'single_slider_seo', ''),
(252, 53, '_single_slider_seo', 'field_5ddbde5499115'),
(253, 53, 'slider_img', ''),
(254, 53, '_slider_img', 'field_60c08d4ff9afa'),
(255, 54, '_edit_last', '1'),
(256, 54, '_thumbnail_id', '45'),
(258, 54, 'post_gallery', ''),
(259, 54, '_post_gallery', 'field_60bfd9909cfa3'),
(260, 54, 'title_tag', ''),
(261, 54, '_title_tag', 'field_5ddbe7577e0e7'),
(262, 54, 'same_text', ''),
(263, 54, '_same_text', 'field_60bfdb8d2a515'),
(264, 54, 'same_posts', ''),
(265, 54, '_same_posts', 'field_60bfdb9f2a516'),
(266, 54, 'single_slider_seo', ''),
(267, 54, '_single_slider_seo', 'field_5ddbde5499115'),
(268, 54, 'slider_img', ''),
(269, 54, '_slider_img', 'field_60c08d4ff9afa'),
(270, 55, 'post_gallery', ''),
(271, 55, '_post_gallery', 'field_60bfd9909cfa3'),
(272, 55, 'title_tag', ''),
(273, 55, '_title_tag', 'field_5ddbe7577e0e7'),
(274, 55, 'same_text', ''),
(275, 55, '_same_text', 'field_60bfdb8d2a515'),
(276, 55, 'same_posts', ''),
(277, 55, '_same_posts', 'field_60bfdb9f2a516'),
(278, 55, 'single_slider_seo', ''),
(279, 55, '_single_slider_seo', 'field_5ddbde5499115'),
(280, 55, 'slider_img', ''),
(281, 55, '_slider_img', 'field_60c08d4ff9afa'),
(282, 54, '_edit_lock', '1623233875:1'),
(283, 56, '_edit_last', '1'),
(284, 56, '_thumbnail_id', '46'),
(286, 56, 'post_gallery', ''),
(287, 56, '_post_gallery', 'field_60bfd9909cfa3'),
(288, 56, 'title_tag', ''),
(289, 56, '_title_tag', 'field_5ddbe7577e0e7'),
(290, 56, 'same_text', ''),
(291, 56, '_same_text', 'field_60bfdb8d2a515'),
(292, 56, 'same_posts', ''),
(293, 56, '_same_posts', 'field_60bfdb9f2a516'),
(294, 56, 'single_slider_seo', ''),
(295, 56, '_single_slider_seo', 'field_5ddbde5499115'),
(296, 56, 'slider_img', ''),
(297, 56, '_slider_img', 'field_60c08d4ff9afa'),
(298, 57, 'post_gallery', ''),
(299, 57, '_post_gallery', 'field_60bfd9909cfa3'),
(300, 57, 'title_tag', ''),
(301, 57, '_title_tag', 'field_5ddbe7577e0e7'),
(302, 57, 'same_text', ''),
(303, 57, '_same_text', 'field_60bfdb8d2a515'),
(304, 57, 'same_posts', ''),
(305, 57, '_same_posts', 'field_60bfdb9f2a516'),
(306, 57, 'single_slider_seo', ''),
(307, 57, '_single_slider_seo', 'field_5ddbde5499115'),
(308, 57, 'slider_img', ''),
(309, 57, '_slider_img', 'field_60c08d4ff9afa'),
(310, 56, '_edit_lock', '1623233945:1'),
(311, 58, '_edit_last', '1'),
(312, 58, '_edit_lock', '1623233953:1'),
(313, 58, '_thumbnail_id', '47'),
(315, 58, 'post_gallery', ''),
(316, 58, '_post_gallery', 'field_60bfd9909cfa3'),
(317, 58, 'title_tag', ''),
(318, 58, '_title_tag', 'field_5ddbe7577e0e7'),
(319, 58, 'same_text', ''),
(320, 58, '_same_text', 'field_60bfdb8d2a515'),
(321, 58, 'same_posts', ''),
(322, 58, '_same_posts', 'field_60bfdb9f2a516'),
(323, 58, 'single_slider_seo', ''),
(324, 58, '_single_slider_seo', 'field_5ddbde5499115'),
(325, 58, 'slider_img', ''),
(326, 58, '_slider_img', 'field_60c08d4ff9afa'),
(327, 59, 'post_gallery', ''),
(328, 59, '_post_gallery', 'field_60bfd9909cfa3'),
(329, 59, 'title_tag', ''),
(330, 59, '_title_tag', 'field_5ddbe7577e0e7'),
(331, 59, 'same_text', ''),
(332, 59, '_same_text', 'field_60bfdb8d2a515'),
(333, 59, 'same_posts', ''),
(334, 59, '_same_posts', 'field_60bfdb9f2a516'),
(335, 59, 'single_slider_seo', ''),
(336, 59, '_single_slider_seo', 'field_5ddbde5499115'),
(337, 59, 'slider_img', ''),
(338, 59, '_slider_img', 'field_60c08d4ff9afa'),
(339, 60, '_edit_last', '1'),
(340, 60, '_thumbnail_id', '48'),
(342, 60, 'post_gallery', ''),
(343, 60, '_post_gallery', 'field_60bfd9909cfa3'),
(344, 60, 'title_tag', ''),
(345, 60, '_title_tag', 'field_5ddbe7577e0e7'),
(346, 60, 'same_text', ''),
(347, 60, '_same_text', 'field_60bfdb8d2a515'),
(348, 60, 'same_posts', ''),
(349, 60, '_same_posts', 'field_60bfdb9f2a516'),
(350, 60, 'single_slider_seo', ''),
(351, 60, '_single_slider_seo', 'field_5ddbde5499115'),
(352, 60, 'slider_img', ''),
(353, 60, '_slider_img', 'field_60c08d4ff9afa'),
(354, 61, 'post_gallery', ''),
(355, 61, '_post_gallery', 'field_60bfd9909cfa3'),
(356, 61, 'title_tag', ''),
(357, 61, '_title_tag', 'field_5ddbe7577e0e7'),
(358, 61, 'same_text', ''),
(359, 61, '_same_text', 'field_60bfdb8d2a515'),
(360, 61, 'same_posts', ''),
(361, 61, '_same_posts', 'field_60bfdb9f2a516'),
(362, 61, 'single_slider_seo', ''),
(363, 61, '_single_slider_seo', 'field_5ddbde5499115'),
(364, 61, 'slider_img', ''),
(365, 61, '_slider_img', 'field_60c08d4ff9afa'),
(366, 60, '_edit_lock', '1623234000:1'),
(367, 62, '_edit_last', '1'),
(368, 62, '_edit_lock', '1623234002:1'),
(369, 62, '_thumbnail_id', '49'),
(371, 62, 'post_gallery', ''),
(372, 62, '_post_gallery', 'field_60bfd9909cfa3'),
(373, 62, 'title_tag', ''),
(374, 62, '_title_tag', 'field_5ddbe7577e0e7'),
(375, 62, 'same_text', ''),
(376, 62, '_same_text', 'field_60bfdb8d2a515'),
(377, 62, 'same_posts', ''),
(378, 62, '_same_posts', 'field_60bfdb9f2a516'),
(379, 62, 'single_slider_seo', ''),
(380, 62, '_single_slider_seo', 'field_5ddbde5499115'),
(381, 62, 'slider_img', ''),
(382, 62, '_slider_img', 'field_60c08d4ff9afa'),
(383, 63, 'post_gallery', ''),
(384, 63, '_post_gallery', 'field_60bfd9909cfa3'),
(385, 63, 'title_tag', ''),
(386, 63, '_title_tag', 'field_5ddbe7577e0e7'),
(387, 63, 'same_text', ''),
(388, 63, '_same_text', 'field_60bfdb8d2a515'),
(389, 63, 'same_posts', ''),
(390, 63, '_same_posts', 'field_60bfdb9f2a516'),
(391, 63, 'single_slider_seo', ''),
(392, 63, '_single_slider_seo', 'field_5ddbde5499115'),
(393, 63, 'slider_img', ''),
(394, 63, '_slider_img', 'field_60c08d4ff9afa'),
(395, 64, '_edit_last', '1'),
(396, 64, '_edit_lock', '1623234054:1'),
(397, 64, '_thumbnail_id', '50'),
(399, 64, 'post_gallery', ''),
(400, 64, '_post_gallery', 'field_60bfd9909cfa3'),
(401, 64, 'title_tag', ''),
(402, 64, '_title_tag', 'field_5ddbe7577e0e7'),
(403, 64, 'same_text', ''),
(404, 64, '_same_text', 'field_60bfdb8d2a515'),
(405, 64, 'same_posts', ''),
(406, 64, '_same_posts', 'field_60bfdb9f2a516'),
(407, 64, 'single_slider_seo', ''),
(408, 64, '_single_slider_seo', 'field_5ddbde5499115'),
(409, 64, 'slider_img', ''),
(410, 64, '_slider_img', 'field_60c08d4ff9afa'),
(411, 65, 'post_gallery', ''),
(412, 65, '_post_gallery', 'field_60bfd9909cfa3'),
(413, 65, 'title_tag', ''),
(414, 65, '_title_tag', 'field_5ddbe7577e0e7'),
(415, 65, 'same_text', ''),
(416, 65, '_same_text', 'field_60bfdb8d2a515'),
(417, 65, 'same_posts', ''),
(418, 65, '_same_posts', 'field_60bfdb9f2a516'),
(419, 65, 'single_slider_seo', ''),
(420, 65, '_single_slider_seo', 'field_5ddbde5499115'),
(421, 65, 'slider_img', ''),
(422, 65, '_slider_img', 'field_60c08d4ff9afa'),
(423, 66, '_edit_last', '1'),
(424, 66, '_thumbnail_id', '51'),
(426, 66, 'post_gallery', ''),
(427, 66, '_post_gallery', 'field_60bfd9909cfa3'),
(428, 66, 'title_tag', ''),
(429, 66, '_title_tag', 'field_5ddbe7577e0e7'),
(430, 66, 'same_text', ''),
(431, 66, '_same_text', 'field_60bfdb8d2a515'),
(432, 66, 'same_posts', ''),
(433, 66, '_same_posts', 'field_60bfdb9f2a516'),
(434, 66, 'single_slider_seo', ''),
(435, 66, '_single_slider_seo', 'field_5ddbde5499115'),
(436, 66, 'slider_img', ''),
(437, 66, '_slider_img', 'field_60c08d4ff9afa'),
(438, 67, 'post_gallery', ''),
(439, 67, '_post_gallery', 'field_60bfd9909cfa3'),
(440, 67, 'title_tag', ''),
(441, 67, '_title_tag', 'field_5ddbe7577e0e7'),
(442, 67, 'same_text', ''),
(443, 67, '_same_text', 'field_60bfdb8d2a515'),
(444, 67, 'same_posts', ''),
(445, 67, '_same_posts', 'field_60bfdb9f2a516'),
(446, 67, 'single_slider_seo', ''),
(447, 67, '_single_slider_seo', 'field_5ddbde5499115'),
(448, 67, 'slider_img', ''),
(449, 67, '_slider_img', 'field_60c08d4ff9afa'),
(450, 66, '_edit_lock', '1623234096:1'),
(451, 68, '_edit_last', '1'),
(452, 68, '_edit_lock', '1623234095:1'),
(453, 68, '_thumbnail_id', '52'),
(455, 68, 'post_gallery', ''),
(456, 68, '_post_gallery', 'field_60bfd9909cfa3'),
(457, 68, 'title_tag', ''),
(458, 68, '_title_tag', 'field_5ddbe7577e0e7'),
(459, 68, 'same_text', ''),
(460, 68, '_same_text', 'field_60bfdb8d2a515'),
(461, 68, 'same_posts', ''),
(462, 68, '_same_posts', 'field_60bfdb9f2a516'),
(463, 68, 'single_slider_seo', ''),
(464, 68, '_single_slider_seo', 'field_5ddbde5499115'),
(465, 68, 'slider_img', ''),
(466, 68, '_slider_img', 'field_60c08d4ff9afa'),
(467, 69, 'post_gallery', ''),
(468, 69, '_post_gallery', 'field_60bfd9909cfa3'),
(469, 69, 'title_tag', ''),
(470, 69, '_title_tag', 'field_5ddbe7577e0e7'),
(471, 69, 'same_text', ''),
(472, 69, '_same_text', 'field_60bfdb8d2a515'),
(473, 69, 'same_posts', ''),
(474, 69, '_same_posts', 'field_60bfdb9f2a516'),
(475, 69, 'single_slider_seo', ''),
(476, 69, '_single_slider_seo', 'field_5ddbde5499115'),
(477, 69, 'slider_img', ''),
(478, 69, '_slider_img', 'field_60c08d4ff9afa'),
(479, 70, '_edit_last', '1'),
(480, 70, '_thumbnail_id', '23'),
(482, 70, 'post_gallery', ''),
(483, 70, '_post_gallery', 'field_60bfd9909cfa3'),
(484, 70, 'title_tag', ''),
(485, 70, '_title_tag', 'field_5ddbe7577e0e7'),
(486, 70, 'same_text', ''),
(487, 70, '_same_text', 'field_60bfdb8d2a515'),
(488, 70, 'same_posts', ''),
(489, 70, '_same_posts', 'field_60bfdb9f2a516'),
(490, 70, 'single_slider_seo', ''),
(491, 70, '_single_slider_seo', 'field_5ddbde5499115'),
(492, 70, 'slider_img', ''),
(493, 70, '_slider_img', 'field_60c08d4ff9afa'),
(494, 71, 'post_gallery', ''),
(495, 71, '_post_gallery', 'field_60bfd9909cfa3'),
(496, 71, 'title_tag', ''),
(497, 71, '_title_tag', 'field_5ddbe7577e0e7'),
(498, 71, 'same_text', ''),
(499, 71, '_same_text', 'field_60bfdb8d2a515'),
(500, 71, 'same_posts', ''),
(501, 71, '_same_posts', 'field_60bfdb9f2a516'),
(502, 71, 'single_slider_seo', ''),
(503, 71, '_single_slider_seo', 'field_5ddbde5499115'),
(504, 71, 'slider_img', ''),
(505, 71, '_slider_img', 'field_60c08d4ff9afa'),
(506, 70, '_edit_lock', '1623236379:1'),
(507, 72, '_edit_last', '1'),
(508, 72, '_edit_lock', '1623233812:1'),
(509, 72, '_wp_page_template', 'views/blog.php'),
(510, 72, 'title_tag', ''),
(511, 72, '_title_tag', 'field_5ddbe7577e0e7'),
(512, 72, 'single_slider_seo_0_content', ''),
(513, 72, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(514, 72, 'single_slider_seo_0_link', ''),
(515, 72, '_single_slider_seo_0_link', 'field_60c08d70f9afb'),
(516, 72, 'single_slider_seo', '1'),
(517, 72, '_single_slider_seo', 'field_5ddbde5499115'),
(518, 72, 'slider_img', ''),
(519, 72, '_slider_img', 'field_60c08d4ff9afa'),
(520, 73, 'title_tag', ''),
(521, 73, '_title_tag', 'field_5ddbe7577e0e7'),
(522, 73, 'single_slider_seo_0_content', ''),
(523, 73, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(524, 73, 'single_slider_seo_0_link', ''),
(525, 73, '_single_slider_seo_0_link', 'field_60c08d70f9afb'),
(526, 73, 'single_slider_seo', '1'),
(527, 73, '_single_slider_seo', 'field_5ddbde5499115'),
(528, 73, 'slider_img', ''),
(529, 73, '_slider_img', 'field_60c08d4ff9afa'),
(530, 47, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:674;s:6:\"height\";i:370;s:4:\"file\";s:10:\"post-4.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-4-300x165.png\";s:5:\"width\";i:300;s:6:\"height\";i:165;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-4-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-4-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-4-600x329.png\";s:5:\"width\";i:600;s:6:\"height\";i:329;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(531, 72, 'seo_content', '<h2>לורם איפסום כותרת לקידום</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(532, 72, '_seo_content', 'field_5ddbde7399116'),
(533, 72, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:110:\"/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10/\";s:6:\"target\";s:0:\"\";}'),
(534, 72, '_seo_link', 'field_60c08d70f9afb'),
(535, 72, 'seo_img', '47'),
(536, 72, '_seo_img', 'field_60c08d4ff9afa'),
(537, 74, 'title_tag', ''),
(538, 74, '_title_tag', 'field_5ddbe7577e0e7'),
(539, 74, 'single_slider_seo_0_content', ''),
(540, 74, '_single_slider_seo_0_content', 'field_5ddbde7399116'),
(541, 74, 'single_slider_seo_0_link', ''),
(542, 74, '_single_slider_seo_0_link', 'field_60c08d70f9afb'),
(543, 74, 'single_slider_seo', '1'),
(544, 74, '_single_slider_seo', 'field_5ddbde5499115'),
(545, 74, 'slider_img', ''),
(546, 74, '_slider_img', 'field_60c08d4ff9afa');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(547, 74, 'seo_content', '<h2>לורם איפסום כותרת לקידום</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(548, 74, '_seo_content', 'field_5ddbde7399116'),
(549, 74, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:110:\"/%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10/\";s:6:\"target\";s:0:\"\";}'),
(550, 74, '_seo_link', 'field_60c08d70f9afb'),
(551, 74, 'seo_img', '47'),
(552, 74, '_seo_img', 'field_60c08d4ff9afa'),
(553, 44, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:640;s:6:\"height\";i:385;s:4:\"file\";s:10:\"post-1.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-1-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-1-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-1-600x361.png\";s:5:\"width\";i:600;s:6:\"height\";i:361;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-1-600x361.png\";s:5:\"width\";i:600;s:6:\"height\";i:361;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(555, 45, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:675;s:6:\"height\";i:396;s:4:\"file\";s:10:\"post-2.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-2-300x176.png\";s:5:\"width\";i:300;s:6:\"height\";i:176;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-2-134x79.png\";s:5:\"width\";i:134;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-2-600x352.png\";s:5:\"width\";i:600;s:6:\"height\";i:352;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-2-600x352.png\";s:5:\"width\";i:600;s:6:\"height\";i:352;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(557, 46, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:674;s:6:\"height\";i:396;s:4:\"file\";s:10:\"post-3.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-3-300x176.png\";s:5:\"width\";i:300;s:6:\"height\";i:176;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-3-134x79.png\";s:5:\"width\";i:134;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-3-600x353.png\";s:5:\"width\";i:600;s:6:\"height\";i:353;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-3-600x353.png\";s:5:\"width\";i:600;s:6:\"height\";i:353;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(559, 48, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:664;s:6:\"height\";i:388;s:4:\"file\";s:10:\"post-5.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-5-300x175.png\";s:5:\"width\";i:300;s:6:\"height\";i:175;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-5-134x78.png\";s:5:\"width\";i:134;s:6:\"height\";i:78;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-5-600x351.png\";s:5:\"width\";i:600;s:6:\"height\";i:351;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-5-600x351.png\";s:5:\"width\";i:600;s:6:\"height\";i:351;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(561, 49, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:649;s:6:\"height\";i:390;s:4:\"file\";s:10:\"post-6.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-6-300x180.png\";s:5:\"width\";i:300;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-6-134x81.png\";s:5:\"width\";i:134;s:6:\"height\";i:81;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-6-600x361.png\";s:5:\"width\";i:600;s:6:\"height\";i:361;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-6-600x361.png\";s:5:\"width\";i:600;s:6:\"height\";i:361;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(563, 50, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:620;s:6:\"height\";i:396;s:4:\"file\";s:10:\"post-7.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-7-300x192.png\";s:5:\"width\";i:300;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-7-134x86.png\";s:5:\"width\";i:134;s:6:\"height\";i:86;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-7-600x383.png\";s:5:\"width\";i:600;s:6:\"height\";i:383;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-7-600x383.png\";s:5:\"width\";i:600;s:6:\"height\";i:383;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(565, 51, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:675;s:6:\"height\";i:381;s:4:\"file\";s:10:\"post-8.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-8-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-8-134x76.png\";s:5:\"width\";i:134;s:6:\"height\";i:76;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-8-600x339.png\";s:5:\"width\";i:600;s:6:\"height\";i:339;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-8-600x339.png\";s:5:\"width\";i:600;s:6:\"height\";i:339;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(567, 52, '_wp_attachment_backup_sizes', 'a:10:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:674;s:6:\"height\";i:396;s:4:\"file\";s:10:\"post-9.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:18:\"post-9-300x176.png\";s:5:\"width\";i:300;s:6:\"height\";i:176;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:17:\"post-9-134x79.png\";s:5:\"width\";i:134;s:6:\"height\";i:79;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:18:\"post-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:18:\"post-9-600x353.png\";s:5:\"width\";i:600;s:6:\"height\";i:353;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:18:\"post-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:18:\"post-9-600x353.png\";s:5:\"width\";i:600;s:6:\"height\";i:353;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:18:\"post-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(569, 75, '_menu_item_type', 'post_type'),
(570, 75, '_menu_item_menu_item_parent', '0'),
(571, 75, '_menu_item_object_id', '72'),
(572, 75, '_menu_item_object', 'page'),
(573, 75, '_menu_item_target', ''),
(574, 75, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(575, 75, '_menu_item_xfn', ''),
(576, 75, '_menu_item_url', ''),
(578, 76, '_menu_item_type', 'taxonomy'),
(579, 76, '_menu_item_menu_item_parent', '0'),
(580, 76, '_menu_item_object_id', '1'),
(581, 76, '_menu_item_object', 'category'),
(582, 76, '_menu_item_target', ''),
(583, 76, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(584, 76, '_menu_item_xfn', ''),
(585, 76, '_menu_item_url', ''),
(588, 77, '_edit_last', '1'),
(589, 77, '_edit_lock', '1623675150:1'),
(590, 82, '_wp_attached_file', '2021/06/logo.png'),
(591, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:263;s:6:\"height\";i:111;s:4:\"file\";s:16:\"2021/06/logo.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-150x111.png\";s:5:\"width\";i:150;s:6:\"height\";i:111;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"logo-134x57.png\";s:5:\"width\";i:134;s:6:\"height\";i:57;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"logo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(592, 84, '_wp_attached_file', '2021/06/middle-im-e1623237849705.png'),
(593, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:691;s:4:\"file\";s:36:\"2021/06/middle-im-e1623237849705.png\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-300x108.png\";s:5:\"width\";i:300;s:6:\"height\";i:108;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"middle-im-e1623237849705-1024x369.png\";s:5:\"width\";i:1024;s:6:\"height\";i:369;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-768x276.png\";s:5:\"width\";i:768;s:6:\"height\";i:276;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:37:\"middle-im-e1623237849705-1536x553.png\";s:5:\"width\";i:1536;s:6:\"height\";i:553;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:35:\"middle-im-e1623237849705-134x48.png\";s:5:\"width\";i:134;s:6:\"height\";i:48;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-600x216.png\";s:5:\"width\";i:600;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-600x216.png\";s:5:\"width\";i:600;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:36:\"middle-im-e1623237849705-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(594, 84, '_wp_attachment_backup_sizes', 'a:13:{s:9:\"full-orig\";a:3:{s:5:\"width\";i:1920;s:6:\"height\";i:753;s:4:\"file\";s:13:\"middle-im.png\";}s:14:\"thumbnail-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"medium-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-300x118.png\";s:5:\"width\";i:300;s:6:\"height\";i:118;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"medium_large-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-768x301.png\";s:5:\"width\";i:768;s:6:\"height\";i:301;s:9:\"mime-type\";s:9:\"image/png\";}s:10:\"large-orig\";a:4:{s:4:\"file\";s:22:\"middle-im-1024x402.png\";s:5:\"width\";i:1024;s:6:\"height\";i:402;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"1536x1536-orig\";a:4:{s:4:\"file\";s:22:\"middle-im-1536x602.png\";s:5:\"width\";i:1536;s:6:\"height\";i:602;s:9:\"mime-type\";s:9:\"image/png\";}s:18:\"gallery-thumb-orig\";a:4:{s:4:\"file\";s:20:\"middle-im-134x53.png\";s:5:\"width\";i:134;s:6:\"height\";i:53;s:9:\"mime-type\";s:9:\"image/png\";}s:26:\"woocommerce_thumbnail-orig\";a:5:{s:4:\"file\";s:21:\"middle-im-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:23:\"woocommerce_single-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-600x235.png\";s:5:\"width\";i:600;s:6:\"height\";i:235;s:9:\"mime-type\";s:9:\"image/png\";}s:34:\"woocommerce_gallery_thumbnail-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:17:\"shop_catalog-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:16:\"shop_single-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-600x235.png\";s:5:\"width\";i:600;s:6:\"height\";i:235;s:9:\"mime-type\";s:9:\"image/png\";}s:19:\"shop_thumbnail-orig\";a:4:{s:4:\"file\";s:21:\"middle-im-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}'),
(595, 85, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-12\">\n		[text* text-399 placeholder \"שם:\"]\n  </div>\n    <div class=\" col-12\">\n			[tel* tel-197 placeholder \"טלפון:\"]\n  </div>\n	    <div class=\"col-12\">\n				[textarea textarea-978 placeholder \"תגובה:\"]\n  </div>\n    <div class=\"col-12\">\n			[submit \"שלח\"]\n	</div>\n</div>'),
(596, 85, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(597, 85, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(598, 85, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(599, 85, '_additional_settings', ''),
(600, 85, '_locale', 'he_IL'),
(602, 85, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(603, 86, '_form', '<div class=\"form-row d-flex justify-content-center align-items-center\">\n  <div class=\"col-md-6 col-12\">\n		[text* text-625 placeholder \"שם:\"]\n  </div>\n    <div class=\"col-md-6 col-12\">\n			[tel* tel-732 placeholder \"טלפון:\"]\n  </div>\n	    <div class=\"col-12\">\n				[email* email-347 placeholder \"מייל:\"]\n  </div>\n	    <div class=\"col-12\">\n				[textarea textarea-192 placeholder \"סיבת פניה:\"]\n	</div>\n    <div class=\"col-auto\">\n			[submit \"תחזרו אלי בהקדם\"]\n	</div>\n</div>'),
(604, 86, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(605, 86, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(606, 86, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(607, 86, '_additional_settings', ''),
(608, 86, '_locale', 'he_IL'),
(610, 87, '_form', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\n  <div class=\"col-sm col-12\">\n		[email* email-819 placeholder \"המייל שלכם:\"]\n  </div>\n    <div class=\"col-sm-auto col-12\">\n			[submit \"הרשמה\"]\n	</div>\n</div>'),
(611, 87, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(612, 87, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(613, 87, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(614, 87, '_additional_settings', ''),
(615, 87, '_locale', 'he_IL'),
(617, 88, '_menu_item_type', 'taxonomy'),
(618, 88, '_menu_item_menu_item_parent', '0'),
(619, 88, '_menu_item_object_id', '15'),
(620, 88, '_menu_item_object', 'product_cat'),
(621, 88, '_menu_item_target', ''),
(622, 88, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(623, 88, '_menu_item_xfn', ''),
(624, 88, '_menu_item_url', ''),
(626, 89, '_menu_item_type', 'post_type'),
(627, 89, '_menu_item_menu_item_parent', '0'),
(628, 89, '_menu_item_object_id', '58'),
(629, 89, '_menu_item_object', 'post'),
(630, 89, '_menu_item_target', ''),
(631, 89, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(632, 89, '_menu_item_xfn', ''),
(633, 89, '_menu_item_url', ''),
(635, 90, '_menu_item_type', 'post_type'),
(636, 90, '_menu_item_menu_item_parent', '0'),
(637, 90, '_menu_item_object_id', '56'),
(638, 90, '_menu_item_object', 'post'),
(639, 90, '_menu_item_target', ''),
(640, 90, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(641, 90, '_menu_item_xfn', ''),
(642, 90, '_menu_item_url', ''),
(644, 91, '_menu_item_type', 'post_type'),
(645, 91, '_menu_item_menu_item_parent', '0'),
(646, 91, '_menu_item_object_id', '54'),
(647, 91, '_menu_item_object', 'post'),
(648, 91, '_menu_item_target', ''),
(649, 91, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(650, 91, '_menu_item_xfn', ''),
(651, 91, '_menu_item_url', ''),
(653, 92, '_menu_item_type', 'post_type'),
(654, 92, '_menu_item_menu_item_parent', '0'),
(655, 92, '_menu_item_object_id', '43'),
(656, 92, '_menu_item_object', 'post'),
(657, 92, '_menu_item_target', ''),
(658, 92, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(659, 92, '_menu_item_xfn', ''),
(660, 92, '_menu_item_url', ''),
(662, 93, '_menu_item_type', 'post_type'),
(663, 93, '_menu_item_menu_item_parent', '0'),
(664, 93, '_menu_item_object_id', '1'),
(665, 93, '_menu_item_object', 'post'),
(666, 93, '_menu_item_target', ''),
(667, 93, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(668, 93, '_menu_item_xfn', ''),
(669, 93, '_menu_item_url', ''),
(671, 94, '_edit_lock', '1623248171:1'),
(672, 94, '_edit_last', '1'),
(673, 107, '_wp_attached_file', '2021/06/flowers-form.png'),
(674, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:271;s:6:\"height\";i:366;s:4:\"file\";s:24:\"2021/06/flowers-form.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"flowers-form-222x300.png\";s:5:\"width\";i:222;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"flowers-form-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"flowers-form-70x94.png\";s:5:\"width\";i:70;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"flowers-form-271x300.png\";s:5:\"width\";i:271;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"flowers-form-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"flowers-form-271x300.png\";s:5:\"width\";i:271;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"flowers-form-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(675, 108, '_menu_item_type', 'post_type'),
(676, 108, '_menu_item_menu_item_parent', '0'),
(677, 108, '_menu_item_object_id', '28'),
(678, 108, '_menu_item_object', 'page'),
(679, 108, '_menu_item_target', ''),
(680, 108, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(681, 108, '_menu_item_xfn', ''),
(682, 108, '_menu_item_url', ''),
(684, 109, '_menu_item_type', 'post_type'),
(685, 109, '_menu_item_menu_item_parent', '0'),
(686, 109, '_menu_item_object_id', '72'),
(687, 109, '_menu_item_object', 'page'),
(688, 109, '_menu_item_target', ''),
(689, 109, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(690, 109, '_menu_item_xfn', ''),
(691, 109, '_menu_item_url', ''),
(693, 87, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(694, 110, '_form', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\n  <div class=\"col-md-6 col-12\">\n		[text* text-477 placeholder \"שם:\"]\n  </div>\n    <div class=\"col-md-6 col-12\">\n			[tel* tel-300 placeholder \"טלפון:\"]\n  </div>\n	    <div class=\"col-12\">\n				[email* email-239 \"מייל:\"]\n  </div>\n	    <div class=\"col-12\">\n				[textarea textarea-757 placeholder \"סיבת פניה:\"]\n	</div>\n    <div class=\"col-lg-6 col-12\">\n			[submit \"תחזרו אלי בהקדם\"]\n	</div>\n</div>'),
(695, 110, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:4:\"body\";s:190:\"מאת: [your-name] <[your-email]>\nנושא: [your-subject]\n\nתוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(696, 110, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:39:\"[_site_title] <wordpress@flowers.nagar>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:129:\"תוכן ההודעה:\n[your-message]\n\n-- \nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(697, 110, '_messages', 'a:22:{s:12:\"mail_sent_ok\";s:24:\"ההודעה נשלחה.\";s:12:\"mail_sent_ng\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:16:\"validation_error\";s:89:\"קיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\";s:4:\"spam\";s:46:\"ארעה שגיאה בשליחת ההודעה.\";s:12:\"accept_terms\";s:68:\"עליך להסכים לתנאים לפני שליחת ההודעה.\";s:16:\"invalid_required\";s:23:\"זהו שדה חובה.\";s:16:\"invalid_too_long\";s:25:\"השדה ארוך מדי.\";s:17:\"invalid_too_short\";s:23:\"השדה קצר מדי.\";s:13:\"upload_failed\";s:51:\"שגיאה לא ידועה בהעלאת הקובץ.\";s:24:\"upload_file_type_invalid\";s:67:\"אין לך הרשאות להעלות קבצים בפורמט זה.\";s:21:\"upload_file_too_large\";s:27:\"הקובץ גדול מדי.\";s:23:\"upload_failed_php_error\";s:35:\"שגיאה בהעלאת הקובץ.\";s:12:\"invalid_date\";s:38:\"שדה התאריך אינו נכון.\";s:14:\"date_too_early\";s:50:\"התאריך מוקדם מהתאריך המותר.\";s:13:\"date_too_late\";s:50:\"התאריך מאוחר מהתאריך המותר.\";s:14:\"invalid_number\";s:40:\"פורמט המספר אינו תקין.\";s:16:\"number_too_small\";s:48:\"המספר קטן מהמינימום המותר.\";s:16:\"number_too_large\";s:50:\"המספר גדול מהמקסימום המותר.\";s:23:\"quiz_answer_not_correct\";s:59:\"התשובה לשאלת הביטחון אינה נכונה.\";s:13:\"invalid_email\";s:59:\"כתובת האימייל שהוזנה אינה תקינה.\";s:11:\"invalid_url\";s:31:\"הקישור אינו תקין.\";s:11:\"invalid_tel\";s:40:\"מספר הטלפון אינו תקין.\";}'),
(698, 110, '_additional_settings', ''),
(699, 110, '_locale', 'he_IL'),
(701, 111, '_edit_last', '1'),
(702, 111, '_edit_lock', '1623275215:1'),
(703, 111, '_wp_page_template', 'views/contact.php'),
(704, 111, 'title_tag', ''),
(705, 111, '_title_tag', 'field_5ddbe7577e0e7'),
(706, 111, 'seo_content', ''),
(707, 111, '_seo_content', 'field_5ddbde7399116'),
(708, 111, 'seo_link', ''),
(709, 111, '_seo_link', 'field_60c08d70f9afb'),
(710, 111, 'seo_img', ''),
(711, 111, '_seo_img', 'field_60c08d4ff9afa'),
(712, 112, 'title_tag', ''),
(713, 112, '_title_tag', 'field_5ddbe7577e0e7'),
(714, 112, 'seo_content', ''),
(715, 112, '_seo_content', 'field_5ddbde7399116'),
(716, 112, 'seo_link', ''),
(717, 112, '_seo_link', 'field_60c08d70f9afb'),
(718, 112, 'seo_img', ''),
(719, 112, '_seo_img', 'field_60c08d4ff9afa'),
(720, 113, '_menu_item_type', 'post_type'),
(721, 113, '_menu_item_menu_item_parent', '0'),
(722, 113, '_menu_item_object_id', '111'),
(723, 113, '_menu_item_object', 'page'),
(724, 113, '_menu_item_target', ''),
(725, 113, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(726, 113, '_menu_item_xfn', ''),
(727, 113, '_menu_item_url', ''),
(731, 110, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(732, 114, 'title_tag', ''),
(733, 114, '_title_tag', 'field_5ddbe7577e0e7'),
(734, 114, 'seo_content', ''),
(735, 114, '_seo_content', 'field_5ddbde7399116'),
(736, 114, 'seo_link', ''),
(737, 114, '_seo_link', 'field_60c08d70f9afb'),
(738, 114, 'seo_img', ''),
(739, 114, '_seo_img', 'field_60c08d4ff9afa'),
(740, 115, '_edit_last', '1'),
(741, 115, '_edit_lock', '1623277640:1'),
(742, 115, '_wp_page_template', 'views/about.php'),
(743, 115, 'title_tag', ''),
(744, 115, '_title_tag', 'field_5ddbe7577e0e7'),
(745, 115, 'seo_content', '<h2>לורם איפסום 123</h2>\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(746, 115, '_seo_content', 'field_5ddbde7399116'),
(747, 115, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(748, 115, '_seo_link', 'field_60c08d70f9afb'),
(749, 115, 'seo_img', '47'),
(750, 115, '_seo_img', 'field_60c08d4ff9afa'),
(751, 116, 'title_tag', ''),
(752, 116, '_title_tag', 'field_5ddbe7577e0e7'),
(753, 116, 'seo_content', ''),
(754, 116, '_seo_content', 'field_5ddbde7399116'),
(755, 116, 'seo_link', ''),
(756, 116, '_seo_link', 'field_60c08d70f9afb'),
(757, 116, 'seo_img', ''),
(758, 116, '_seo_img', 'field_60c08d4ff9afa'),
(759, 117, '_edit_last', '1'),
(760, 117, '_edit_lock', '1623277152:1'),
(761, 125, 'title_tag', ''),
(762, 125, '_title_tag', 'field_5ddbe7577e0e7'),
(763, 125, 'seo_content', ''),
(764, 125, '_seo_content', 'field_5ddbde7399116'),
(765, 125, 'seo_link', ''),
(766, 125, '_seo_link', 'field_60c08d70f9afb'),
(767, 125, 'seo_img', ''),
(768, 125, '_seo_img', 'field_60c08d4ff9afa'),
(769, 126, '_wp_attached_file', '2021/06/about-img-2.png'),
(770, 126, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1103;s:6:\"height\";i:613;s:4:\"file\";s:23:\"2021/06/about-img-2.png\";s:5:\"sizes\";a:11:{s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"about-img-2-300x167.png\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"about-img-2-1024x569.png\";s:5:\"width\";i:1024;s:6:\"height\";i:569;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"about-img-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"about-img-2-768x427.png\";s:5:\"width\";i:768;s:6:\"height\";i:427;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:22:\"about-img-2-134x74.png\";s:5:\"width\";i:134;s:6:\"height\";i:74;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:23:\"about-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:23:\"about-img-2-600x333.png\";s:5:\"width\";i:600;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:23:\"about-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:23:\"about-img-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:23:\"about-img-2-600x333.png\";s:5:\"width\";i:600;s:6:\"height\";i:333;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:23:\"about-img-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(771, 127, '_wp_attached_file', '2021/06/about-img.png'),
(772, 127, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:419;s:4:\"file\";s:21:\"2021/06/about-img.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"about-img-300x131.png\";s:5:\"width\";i:300;s:6:\"height\";i:131;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"about-img-768x335.png\";s:5:\"width\";i:768;s:6:\"height\";i:335;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"about-img-134x58.png\";s:5:\"width\";i:134;s:6:\"height\";i:58;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"about-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:21:\"about-img-600x262.png\";s:5:\"width\";i:600;s:6:\"height\";i:262;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"about-img-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:21:\"about-img-600x262.png\";s:5:\"width\";i:600;s:6:\"height\";i:262;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"about-img-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(773, 115, '_thumbnail_id', '127'),
(774, 115, 'about_content_block_0_about_content', '<h2>החזון שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת.\r\n\r\nלורם איפסום דולור סיט אמט, ליבם סולבגורמי מגמש סחטיר בלובק. תצטנפל לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(775, 115, '_about_content_block_0_about_content', 'field_60c137f65b6cb'),
(776, 115, 'about_content_block_0_about_img', '126'),
(777, 115, '_about_content_block_0_about_img', 'field_60c1380f5b6cc'),
(778, 115, 'about_content_block', '1'),
(779, 115, '_about_content_block', 'field_60c137e45b6ca'),
(780, 115, 'about_video_text', '<h2>צפו איתנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ'),
(781, 115, '_about_video_text', 'field_60c1383d2f169'),
(782, 115, 'about_video_link', 'https://www.youtube.com/watch?v=aS-LUW5Jim0'),
(783, 115, '_about_video_link', 'field_60c1384d2f16a'),
(784, 128, 'title_tag', ''),
(785, 128, '_title_tag', 'field_5ddbe7577e0e7'),
(786, 128, 'seo_content', '<h2>לורם איפסום 123</h2>\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(787, 128, '_seo_content', 'field_5ddbde7399116'),
(788, 128, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(789, 128, '_seo_link', 'field_60c08d70f9afb'),
(790, 128, 'seo_img', '47'),
(791, 128, '_seo_img', 'field_60c08d4ff9afa'),
(792, 128, 'about_content_block_0_about_content', '<h2>החזון שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת.\r\n\r\nלורם איפסום דולור סיט אמט, ליבם סולבגורמי מגמש סחטיר בלובק. תצטנפל לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(793, 128, '_about_content_block_0_about_content', 'field_60c137f65b6cb'),
(794, 128, 'about_content_block_0_about_img', '126'),
(795, 128, '_about_content_block_0_about_img', 'field_60c1380f5b6cc'),
(796, 128, 'about_content_block', '1'),
(797, 128, '_about_content_block', 'field_60c137e45b6ca'),
(798, 128, 'about_video_text', ''),
(799, 128, '_about_video_text', 'field_60c1383d2f169'),
(800, 128, 'about_video_link', ''),
(801, 128, '_about_video_link', 'field_60c1384d2f16a'),
(802, 129, '_menu_item_type', 'post_type'),
(803, 129, '_menu_item_menu_item_parent', '0'),
(804, 129, '_menu_item_object_id', '115'),
(805, 129, '_menu_item_object', 'page'),
(806, 129, '_menu_item_target', ''),
(807, 129, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(808, 129, '_menu_item_xfn', ''),
(809, 129, '_menu_item_url', ''),
(811, 27, '_wp_old_date', '2021-06-09'),
(812, 75, '_wp_old_date', '2021-06-09'),
(813, 76, '_wp_old_date', '2021-06-09'),
(814, 113, '_wp_old_date', '2021-06-09'),
(815, 130, 'title_tag', ''),
(816, 130, '_title_tag', 'field_5ddbe7577e0e7'),
(817, 130, 'seo_content', '<h2>לורם איפסום 123</h2>\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(818, 130, '_seo_content', 'field_5ddbde7399116'),
(819, 130, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(820, 130, '_seo_link', 'field_60c08d70f9afb'),
(821, 130, 'seo_img', '47'),
(822, 130, '_seo_img', 'field_60c08d4ff9afa'),
(823, 130, 'about_content_block_0_about_content', '<h2>החזון שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת.\r\n\r\nלורם איפסום דולור סיט אמט, ליבם סולבגורמי מגמש סחטיר בלובק. תצטנפל לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(824, 130, '_about_content_block_0_about_content', 'field_60c137f65b6cb'),
(825, 130, 'about_content_block_0_about_img', '126'),
(826, 130, '_about_content_block_0_about_img', 'field_60c1380f5b6cc'),
(827, 130, 'about_content_block', '1'),
(828, 130, '_about_content_block', 'field_60c137e45b6ca');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(829, 130, 'about_video_text', '<h2>צפו איתנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ'),
(830, 130, '_about_video_text', 'field_60c1383d2f169'),
(831, 130, 'about_video_link', 'https://www.youtube.com/watch?v=aS-LUW5Jim0'),
(832, 130, '_about_video_link', 'field_60c1384d2f16a'),
(834, 131, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(835, 131, '_post_gallery', 'field_60bfd9909cfa3'),
(836, 131, 'title_tag', ''),
(837, 131, '_title_tag', 'field_5ddbe7577e0e7'),
(838, 131, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(839, 131, '_same_text', 'field_60bfdb8d2a515'),
(840, 131, 'same_posts', 'a:3:{i:0;s:2:\"54\";i:1;s:2:\"56\";i:2;s:2:\"58\";}'),
(841, 131, '_same_posts', 'field_60bfdb9f2a516'),
(842, 131, 'single_slider_seo', ''),
(843, 131, '_single_slider_seo', 'field_5ddbde5499115'),
(844, 131, 'single_gallery', ''),
(845, 131, '_single_gallery', 'field_5ddbe5aea4275'),
(846, 131, 'single_video_slider', ''),
(847, 131, '_single_video_slider', 'field_5ddbe636a4276'),
(849, 132, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(850, 132, '_post_gallery', 'field_60bfd9909cfa3'),
(851, 132, 'title_tag', ''),
(852, 132, '_title_tag', 'field_5ddbe7577e0e7'),
(853, 132, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(854, 132, '_same_text', 'field_60bfdb8d2a515'),
(855, 132, 'same_posts', 'a:3:{i:0;s:2:\"54\";i:1;s:2:\"56\";i:2;s:2:\"58\";}'),
(856, 132, '_same_posts', 'field_60bfdb9f2a516'),
(857, 132, 'single_slider_seo', ''),
(858, 132, '_single_slider_seo', 'field_5ddbde5499115'),
(859, 132, 'single_gallery', ''),
(860, 132, '_single_gallery', 'field_5ddbe5aea4275'),
(861, 132, 'single_video_slider', ''),
(862, 132, '_single_video_slider', 'field_5ddbe636a4276'),
(864, 133, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(865, 133, '_post_gallery', 'field_60bfd9909cfa3'),
(866, 133, 'title_tag', ''),
(867, 133, '_title_tag', 'field_5ddbe7577e0e7'),
(868, 133, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(869, 133, '_same_text', 'field_60bfdb8d2a515'),
(870, 133, 'same_posts', 'a:3:{i:0;s:2:\"54\";i:1;s:2:\"56\";i:2;s:2:\"58\";}'),
(871, 133, '_same_posts', 'field_60bfdb9f2a516'),
(872, 133, 'single_slider_seo', ''),
(873, 133, '_single_slider_seo', 'field_5ddbde5499115'),
(874, 133, 'single_gallery', ''),
(875, 133, '_single_gallery', 'field_5ddbe5aea4275'),
(876, 133, 'single_video_slider', ''),
(877, 133, '_single_video_slider', 'field_5ddbe636a4276'),
(879, 1, 'post_video_link', 'https://www.youtube.com/watch?v=KnTV9ioFi-Y'),
(880, 1, '_post_video_link', 'field_60c1c9074ad5a'),
(881, 1, 'post_video_content', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(882, 1, '_post_video_content', 'field_60c1c91c4ad5b'),
(883, 137, 'post_gallery', 'a:5:{i:0;s:2:\"19\";i:1;s:2:\"20\";i:2;s:2:\"21\";i:3;s:2:\"22\";i:4;s:2:\"23\";}'),
(884, 137, '_post_gallery', 'field_60bfd9909cfa3'),
(885, 137, 'title_tag', ''),
(886, 137, '_title_tag', 'field_5ddbe7577e0e7'),
(887, 137, 'same_text', '<h2>המאמרים נוספים</h2>\r\nלורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק'),
(888, 137, '_same_text', 'field_60bfdb8d2a515'),
(889, 137, 'same_posts', 'a:3:{i:0;s:2:\"54\";i:1;s:2:\"56\";i:2;s:2:\"58\";}'),
(890, 137, '_same_posts', 'field_60bfdb9f2a516'),
(891, 137, 'single_slider_seo', ''),
(892, 137, '_single_slider_seo', 'field_5ddbde5499115'),
(893, 137, 'single_gallery', ''),
(894, 137, '_single_gallery', 'field_5ddbe5aea4275'),
(895, 137, 'single_video_slider', ''),
(896, 137, '_single_video_slider', 'field_5ddbe636a4276'),
(897, 137, 'post_video_link', 'https://www.youtube.com/watch?v=KnTV9ioFi-Y'),
(898, 137, '_post_video_link', 'field_60c1c9074ad5a'),
(899, 137, 'post_video_content', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(900, 137, '_post_video_content', 'field_60c1c91c4ad5b'),
(902, 138, '_edit_last', '1'),
(903, 138, '_edit_lock', '1623317051:1'),
(904, 144, '_edit_last', '1'),
(905, 144, '_wp_page_template', 'views/faq.php'),
(906, 144, 'title_tag', ''),
(907, 144, '_title_tag', 'field_5ddbe7577e0e7'),
(908, 144, 'faq_block', '2'),
(909, 144, '_faq_block', 'field_60c1d7b1a823a'),
(910, 144, 'seo_content', ''),
(911, 144, '_seo_content', 'field_5ddbde7399116'),
(912, 144, 'seo_link', ''),
(913, 144, '_seo_link', 'field_60c08d70f9afb'),
(914, 144, 'seo_img', ''),
(915, 144, '_seo_img', 'field_60c08d4ff9afa'),
(916, 145, 'title_tag', ''),
(917, 145, '_title_tag', 'field_5ddbe7577e0e7'),
(918, 145, 'faq_block', ''),
(919, 145, '_faq_block', 'field_60c1d7b1a823a'),
(920, 145, 'seo_content', ''),
(921, 145, '_seo_content', 'field_5ddbde7399116'),
(922, 145, 'seo_link', ''),
(923, 145, '_seo_link', 'field_60c08d70f9afb'),
(924, 145, 'seo_img', ''),
(925, 145, '_seo_img', 'field_60c08d4ff9afa'),
(926, 144, '_edit_lock', '1623316560:1'),
(927, 146, 'title_tag', ''),
(928, 146, '_title_tag', 'field_5ddbe7577e0e7'),
(929, 146, 'faq_block', ''),
(930, 146, '_faq_block', 'field_60c1d7b1a823a'),
(931, 146, 'seo_content', ''),
(932, 146, '_seo_content', 'field_5ddbde7399116'),
(933, 146, 'seo_link', ''),
(934, 146, '_seo_link', 'field_60c08d70f9afb'),
(935, 146, 'seo_img', ''),
(936, 146, '_seo_img', 'field_60c08d4ff9afa'),
(937, 144, 'faq_block_0_faq_block_title', 'תת נושא לורם היפסום 1'),
(938, 144, '_faq_block_0_faq_block_title', 'field_60c1d7cda823b'),
(939, 144, 'faq_block_0_faq_item_0_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם?'),
(940, 144, '_faq_block_0_faq_item_0_faq_question', 'field_60c1d7f9a823d'),
(941, 144, 'faq_block_0_faq_item_0_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף.\r\n\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(942, 144, '_faq_block_0_faq_item_0_faq_answer', 'field_60c1d80da823e'),
(943, 144, 'faq_block_0_faq_item_1_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם 2?'),
(944, 144, '_faq_block_0_faq_item_1_faq_question', 'field_60c1d7f9a823d'),
(945, 144, 'faq_block_0_faq_item_1_faq_answer', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(946, 144, '_faq_block_0_faq_item_1_faq_answer', 'field_60c1d80da823e'),
(947, 144, 'faq_block_0_faq_item_2_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם 3?'),
(948, 144, '_faq_block_0_faq_item_2_faq_question', 'field_60c1d7f9a823d'),
(949, 144, 'faq_block_0_faq_item_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, לורם איפסום דולור סיט אמט, קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(950, 144, '_faq_block_0_faq_item_2_faq_answer', 'field_60c1d80da823e'),
(951, 144, 'faq_block_0_faq_item', '3'),
(952, 144, '_faq_block_0_faq_item', 'field_60c1d7dea823c'),
(953, 144, 'faq_block_1_faq_block_title', 'תת נושא לורם היפסום 2'),
(954, 144, '_faq_block_1_faq_block_title', 'field_60c1d7cda823b'),
(955, 144, 'faq_block_1_faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט 1?'),
(956, 144, '_faq_block_1_faq_item_0_faq_question', 'field_60c1d7f9a823d'),
(957, 144, 'faq_block_1_faq_item_0_faq_answer', 'וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(958, 144, '_faq_block_1_faq_item_0_faq_answer', 'field_60c1d80da823e'),
(959, 144, 'faq_block_1_faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט 2?'),
(960, 144, '_faq_block_1_faq_item_1_faq_question', 'field_60c1d7f9a823d'),
(961, 144, 'faq_block_1_faq_item_1_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(962, 144, '_faq_block_1_faq_item_1_faq_answer', 'field_60c1d80da823e'),
(963, 144, 'faq_block_1_faq_item', '2'),
(964, 144, '_faq_block_1_faq_item', 'field_60c1d7dea823c'),
(965, 147, 'title_tag', ''),
(966, 147, '_title_tag', 'field_5ddbe7577e0e7'),
(967, 147, 'faq_block', '2'),
(968, 147, '_faq_block', 'field_60c1d7b1a823a'),
(969, 147, 'seo_content', ''),
(970, 147, '_seo_content', 'field_5ddbde7399116'),
(971, 147, 'seo_link', ''),
(972, 147, '_seo_link', 'field_60c08d70f9afb'),
(973, 147, 'seo_img', ''),
(974, 147, '_seo_img', 'field_60c08d4ff9afa'),
(975, 147, 'faq_block_0_faq_block_title', 'תת נושא לורם היפסום 1'),
(976, 147, '_faq_block_0_faq_block_title', 'field_60c1d7cda823b'),
(977, 147, 'faq_block_0_faq_item_0_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם?'),
(978, 147, '_faq_block_0_faq_item_0_faq_question', 'field_60c1d7f9a823d'),
(979, 147, 'faq_block_0_faq_item_0_faq_answer', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף.\r\n\r\nאודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.'),
(980, 147, '_faq_block_0_faq_item_0_faq_answer', 'field_60c1d80da823e'),
(981, 147, 'faq_block_0_faq_item_1_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם 2?'),
(982, 147, '_faq_block_0_faq_item_1_faq_question', 'field_60c1d7f9a823d'),
(983, 147, 'faq_block_0_faq_item_1_faq_answer', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(984, 147, '_faq_block_0_faq_item_1_faq_answer', 'field_60c1d80da823e'),
(985, 147, 'faq_block_0_faq_item_2_faq_question', 'שאלה לדוגמא לורם היונסקסינג אלית סחטיר בלובק  פסום לורם 3?'),
(986, 147, '_faq_block_0_faq_item_2_faq_question', 'field_60c1d7f9a823d'),
(987, 147, 'faq_block_0_faq_item_2_faq_answer', 'הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, לורם איפסום דולור סיט אמט, קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(988, 147, '_faq_block_0_faq_item_2_faq_answer', 'field_60c1d80da823e'),
(989, 147, 'faq_block_0_faq_item', '3'),
(990, 147, '_faq_block_0_faq_item', 'field_60c1d7dea823c'),
(991, 147, 'faq_block_1_faq_block_title', 'תת נושא לורם היפסום 2'),
(992, 147, '_faq_block_1_faq_block_title', 'field_60c1d7cda823b'),
(993, 147, 'faq_block_1_faq_item_0_faq_question', 'לורם איפסום דולור סיט אמט 1?'),
(994, 147, '_faq_block_1_faq_item_0_faq_question', 'field_60c1d7f9a823d'),
(995, 147, 'faq_block_1_faq_item_0_faq_answer', 'וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(996, 147, '_faq_block_1_faq_item_0_faq_answer', 'field_60c1d80da823e'),
(997, 147, 'faq_block_1_faq_item_1_faq_question', 'לורם איפסום דולור סיט אמט 2?'),
(998, 147, '_faq_block_1_faq_item_1_faq_question', 'field_60c1d7f9a823d'),
(999, 147, 'faq_block_1_faq_item_1_faq_answer', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1000, 147, '_faq_block_1_faq_item_1_faq_answer', 'field_60c1d80da823e'),
(1001, 147, 'faq_block_1_faq_item', '2'),
(1002, 147, '_faq_block_1_faq_item', 'field_60c1d7dea823c'),
(1003, 148, '_menu_item_type', 'post_type'),
(1004, 148, '_menu_item_menu_item_parent', '0'),
(1005, 148, '_menu_item_object_id', '144'),
(1006, 148, '_menu_item_object', 'page'),
(1007, 148, '_menu_item_target', ''),
(1008, 148, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1009, 148, '_menu_item_xfn', ''),
(1010, 148, '_menu_item_url', ''),
(1012, 149, '_menu_item_type', 'post_type'),
(1013, 149, '_menu_item_menu_item_parent', '0'),
(1014, 149, '_menu_item_object_id', '115'),
(1015, 149, '_menu_item_object', 'page'),
(1016, 149, '_menu_item_target', ''),
(1017, 149, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1018, 149, '_menu_item_xfn', ''),
(1019, 149, '_menu_item_url', ''),
(1021, 150, '_menu_item_type', 'post_type'),
(1022, 150, '_menu_item_menu_item_parent', '0'),
(1023, 150, '_menu_item_object_id', '111'),
(1024, 150, '_menu_item_object', 'page'),
(1025, 150, '_menu_item_target', ''),
(1026, 150, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1027, 150, '_menu_item_xfn', ''),
(1028, 150, '_menu_item_url', ''),
(1030, 108, '_wp_old_date', '2021-06-09'),
(1031, 109, '_wp_old_date', '2021-06-09'),
(1032, 151, '_menu_item_type', 'post_type'),
(1033, 151, '_menu_item_menu_item_parent', '0'),
(1034, 151, '_menu_item_object_id', '144'),
(1035, 151, '_menu_item_object', 'page'),
(1036, 151, '_menu_item_target', ''),
(1037, 151, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1038, 151, '_menu_item_xfn', ''),
(1039, 151, '_menu_item_url', ''),
(1040, 152, '_menu_item_type', 'post_type'),
(1041, 152, '_menu_item_menu_item_parent', '0'),
(1042, 152, '_menu_item_object_id', '12'),
(1043, 152, '_menu_item_object', 'product'),
(1044, 152, '_menu_item_target', ''),
(1045, 152, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1046, 152, '_menu_item_xfn', ''),
(1047, 152, '_menu_item_url', ''),
(1049, 27, '_wp_old_date', '2021-06-10'),
(1050, 75, '_wp_old_date', '2021-06-10'),
(1051, 76, '_wp_old_date', '2021-06-10'),
(1052, 129, '_wp_old_date', '2021-06-10'),
(1053, 151, '_wp_old_date', '2021-06-10'),
(1054, 113, '_wp_old_date', '2021-06-10'),
(1055, 153, '_edit_last', '1'),
(1056, 153, '_edit_lock', '1623779643:1'),
(1057, 180, '_edit_last', '1'),
(1058, 180, '_edit_lock', '1623667244:1'),
(1059, 181, '_wp_attached_file', '2021/06/product-2.png'),
(1060, 181, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:460;s:6:\"height\";i:396;s:4:\"file\";s:21:\"2021/06/product-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-2-300x258.png\";s:5:\"width\";i:300;s:6:\"height\";i:258;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-2-109x94.png\";s:5:\"width\";i:109;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1061, 182, '_wp_attached_file', '2021/06/product-3.png'),
(1062, 182, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:405;s:6:\"height\";i:447;s:4:\"file\";s:21:\"2021/06/product-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-3-272x300.png\";s:5:\"width\";i:272;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-3-85x94.png\";s:5:\"width\";i:85;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1063, 183, '_wp_attached_file', '2021/06/product-4.png'),
(1064, 183, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:381;s:6:\"height\";i:381;s:4:\"file\";s:21:\"2021/06/product-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-4-94x94.png\";s:5:\"width\";i:94;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1065, 184, '_wp_attached_file', '2021/06/product-5.png'),
(1066, 184, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:456;s:6:\"height\";i:447;s:4:\"file\";s:21:\"2021/06/product-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-5-300x294.png\";s:5:\"width\";i:300;s:6:\"height\";i:294;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-5-96x94.png\";s:5:\"width\";i:96;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1067, 185, '_wp_attached_file', '2021/06/product-6.png'),
(1068, 185, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:461;s:6:\"height\";i:446;s:4:\"file\";s:21:\"2021/06/product-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-6-300x290.png\";s:5:\"width\";i:300;s:6:\"height\";i:290;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-6-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1069, 186, '_wp_attached_file', '2021/06/product-7.png'),
(1070, 186, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:456;s:6:\"height\";i:424;s:4:\"file\";s:21:\"2021/06/product-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-7-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-7-101x94.png\";s:5:\"width\";i:101;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1071, 187, '_wp_attached_file', '2021/06/product-8.png'),
(1072, 187, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:455;s:6:\"height\";i:449;s:4:\"file\";s:21:\"2021/06/product-8.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-8-300x296.png\";s:5:\"width\";i:300;s:6:\"height\";i:296;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-8-95x94.png\";s:5:\"width\";i:95;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1073, 188, '_wp_attached_file', '2021/06/product-9.png'),
(1074, 188, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:452;s:6:\"height\";i:449;s:4:\"file\";s:21:\"2021/06/product-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"product-9-300x298.png\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:19:\"product-9-95x94.png\";s:5:\"width\";i:95;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:21:\"product-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:21:\"product-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"product-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1075, 189, '_wp_attached_file', '2021/06/product-10.png'),
(1076, 189, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:453;s:6:\"height\";i:445;s:4:\"file\";s:22:\"2021/06/product-10.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-10-300x295.png\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-10-96x94.png\";s:5:\"width\";i:96;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-10-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-10-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1077, 190, '_wp_attached_file', '2021/06/product-11.png'),
(1078, 190, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:462;s:6:\"height\";i:449;s:4:\"file\";s:22:\"2021/06/product-11.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"product-11-300x292.png\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:20:\"product-11-97x94.png\";s:5:\"width\";i:97;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"product-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"product-11-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"product-11-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1079, 180, '_thumbnail_id', '181'),
(1080, 180, '_regular_price', '33'),
(1081, 180, '_sale_price', '28'),
(1082, 180, 'total_sales', '0'),
(1083, 180, '_tax_status', 'taxable'),
(1084, 180, '_tax_class', ''),
(1085, 180, '_manage_stock', 'no'),
(1086, 180, '_backorders', 'no'),
(1087, 180, '_sold_individually', 'no'),
(1088, 180, '_virtual', 'no'),
(1089, 180, '_downloadable', 'no'),
(1090, 180, '_download_limit', '-1'),
(1091, 180, '_download_expiry', '-1'),
(1092, 180, '_stock', NULL),
(1093, 180, '_stock_status', 'instock'),
(1094, 180, '_wc_average_rating', '0'),
(1095, 180, '_wc_review_count', '0'),
(1096, 180, '_product_version', '5.4.1'),
(1097, 180, '_price', '28'),
(1098, 191, '_edit_last', '1'),
(1099, 191, '_edit_lock', '1623840011:1'),
(1100, 12, 'product_accordion', '3'),
(1101, 12, '_product_accordion', 'field_60c7320f43497'),
(1102, 12, 'product_accordion_0_acc_title', 'משלוחים'),
(1103, 12, '_product_accordion_0_acc_title', 'field_60c7322b43498'),
(1104, 12, 'product_accordion_0_acc_content', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1105, 12, '_product_accordion_0_acc_content', 'field_60c7323443499'),
(1106, 12, 'product_accordion_1_acc_title', 'החזרות'),
(1107, 12, '_product_accordion_1_acc_title', 'field_60c7322b43498'),
(1108, 12, 'product_accordion_1_acc_content', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1109, 12, '_product_accordion_1_acc_content', 'field_60c7323443499'),
(1110, 12, 'product_accordion_2_acc_title', 'לורם איפסום דולור'),
(1111, 12, '_product_accordion_2_acc_title', 'field_60c7322b43498'),
(1112, 12, 'product_accordion_2_acc_content', 'קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.'),
(1113, 12, '_product_accordion_2_acc_content', 'field_60c7323443499'),
(1114, 195, '_edit_last', '1'),
(1115, 195, '_thumbnail_id', '182'),
(1116, 195, 'total_sales', '0'),
(1117, 195, '_tax_status', 'taxable'),
(1118, 195, '_tax_class', ''),
(1119, 195, '_manage_stock', 'no'),
(1120, 195, '_backorders', 'no'),
(1121, 195, '_sold_individually', 'no'),
(1122, 195, '_virtual', 'no'),
(1123, 195, '_downloadable', 'no'),
(1124, 195, '_download_limit', '-1'),
(1125, 195, '_download_expiry', '-1'),
(1126, 195, '_stock', NULL),
(1127, 195, '_stock_status', 'instock'),
(1128, 195, '_wc_average_rating', '0'),
(1129, 195, '_wc_review_count', '0'),
(1130, 195, '_product_version', '5.4.1'),
(1131, 195, 'product_accordion', ''),
(1132, 195, '_product_accordion', 'field_60c7320f43497'),
(1133, 195, '_edit_lock', '1623667289:1'),
(1134, 195, '_regular_price', '120'),
(1135, 195, '_sale_price', '111'),
(1136, 195, '_price', '111'),
(1137, 196, '_edit_last', '1'),
(1138, 196, '_thumbnail_id', '183'),
(1139, 196, '_regular_price', '89'),
(1140, 196, '_sale_price', '67'),
(1141, 196, 'total_sales', '0'),
(1142, 196, '_tax_status', 'taxable'),
(1143, 196, '_tax_class', ''),
(1144, 196, '_manage_stock', 'no'),
(1145, 196, '_backorders', 'no'),
(1146, 196, '_sold_individually', 'no'),
(1147, 196, '_virtual', 'no'),
(1148, 196, '_downloadable', 'no'),
(1149, 196, '_download_limit', '-1'),
(1150, 196, '_download_expiry', '-1'),
(1151, 196, '_stock', NULL),
(1152, 196, '_stock_status', 'instock'),
(1153, 196, '_wc_average_rating', '0'),
(1154, 196, '_wc_review_count', '0'),
(1155, 196, '_product_version', '5.4.1'),
(1156, 196, '_price', '67'),
(1157, 196, 'product_accordion', ''),
(1158, 196, '_product_accordion', 'field_60c7320f43497'),
(1159, 196, '_edit_lock', '1623667353:1'),
(1160, 197, '_edit_last', '1'),
(1161, 197, '_edit_lock', '1623667379:1'),
(1162, 197, '_thumbnail_id', '184'),
(1163, 197, 'total_sales', '0'),
(1164, 197, '_tax_status', 'taxable'),
(1165, 197, '_tax_class', ''),
(1166, 197, '_manage_stock', 'no'),
(1167, 197, '_backorders', 'no'),
(1168, 197, '_sold_individually', 'no'),
(1169, 197, '_virtual', 'no'),
(1170, 197, '_downloadable', 'no'),
(1171, 197, '_download_limit', '-1'),
(1172, 197, '_download_expiry', '-1'),
(1173, 197, '_stock', NULL),
(1174, 197, '_stock_status', 'instock'),
(1175, 197, '_wc_average_rating', '0'),
(1176, 197, '_wc_review_count', '0'),
(1177, 197, '_product_version', '5.4.1'),
(1178, 197, 'product_accordion', ''),
(1179, 197, '_product_accordion', 'field_60c7320f43497'),
(1180, 198, '_edit_last', '1'),
(1181, 198, '_thumbnail_id', '185'),
(1182, 198, '_regular_price', '210'),
(1183, 198, '_sale_price', '179'),
(1184, 198, 'total_sales', '0'),
(1185, 198, '_tax_status', 'taxable'),
(1186, 198, '_tax_class', ''),
(1187, 198, '_manage_stock', 'no'),
(1188, 198, '_backorders', 'no'),
(1189, 198, '_sold_individually', 'no'),
(1190, 198, '_virtual', 'no'),
(1191, 198, '_downloadable', 'no'),
(1192, 198, '_download_limit', '-1'),
(1193, 198, '_download_expiry', '-1'),
(1194, 198, '_stock', NULL),
(1195, 198, '_stock_status', 'instock'),
(1196, 198, '_wc_average_rating', '0'),
(1197, 198, '_wc_review_count', '0'),
(1198, 198, '_product_version', '5.4.1'),
(1199, 198, '_price', '179'),
(1200, 198, 'product_accordion', ''),
(1201, 198, '_product_accordion', 'field_60c7320f43497'),
(1202, 198, '_edit_lock', '1623667410:1'),
(1203, 199, '_edit_last', '1'),
(1204, 199, '_thumbnail_id', '186'),
(1205, 199, '_regular_price', '80'),
(1206, 199, '_sale_price', '60'),
(1207, 199, 'total_sales', '0'),
(1208, 199, '_tax_status', 'taxable'),
(1209, 199, '_tax_class', ''),
(1210, 199, '_manage_stock', 'no'),
(1211, 199, '_backorders', 'no'),
(1212, 199, '_sold_individually', 'no'),
(1213, 199, '_virtual', 'no'),
(1214, 199, '_downloadable', 'no'),
(1215, 199, '_download_limit', '-1'),
(1216, 199, '_download_expiry', '-1'),
(1217, 199, '_stock', NULL),
(1218, 199, '_stock_status', 'instock'),
(1219, 199, '_wc_average_rating', '0'),
(1220, 199, '_wc_review_count', '0'),
(1221, 199, '_product_version', '5.4.1'),
(1222, 199, '_price', '60'),
(1223, 199, 'product_accordion', ''),
(1224, 199, '_product_accordion', 'field_60c7320f43497'),
(1225, 199, '_edit_lock', '1623667456:1'),
(1226, 200, '_edit_last', '1'),
(1227, 200, '_edit_lock', '1623667481:1'),
(1228, 200, '_thumbnail_id', '187'),
(1229, 200, 'total_sales', '0'),
(1230, 200, '_tax_status', 'taxable'),
(1231, 200, '_tax_class', ''),
(1232, 200, '_manage_stock', 'no'),
(1233, 200, '_backorders', 'no'),
(1234, 200, '_sold_individually', 'no'),
(1235, 200, '_virtual', 'no'),
(1236, 200, '_downloadable', 'no'),
(1237, 200, '_download_limit', '-1'),
(1238, 200, '_download_expiry', '-1'),
(1239, 200, '_stock', NULL),
(1240, 200, '_stock_status', 'instock'),
(1241, 200, '_wc_average_rating', '0'),
(1242, 200, '_wc_review_count', '0'),
(1243, 200, '_product_version', '5.4.1'),
(1244, 200, 'product_accordion', ''),
(1245, 200, '_product_accordion', 'field_60c7320f43497'),
(1246, 201, '_edit_last', '1'),
(1247, 201, '_thumbnail_id', '188'),
(1248, 201, '_regular_price', '98'),
(1249, 201, '_sale_price', '88'),
(1250, 201, 'total_sales', '0'),
(1251, 201, '_tax_status', 'taxable'),
(1252, 201, '_tax_class', ''),
(1253, 201, '_manage_stock', 'no'),
(1254, 201, '_backorders', 'no'),
(1255, 201, '_sold_individually', 'no'),
(1256, 201, '_virtual', 'no'),
(1257, 201, '_downloadable', 'no'),
(1258, 201, '_download_limit', '-1'),
(1259, 201, '_download_expiry', '-1'),
(1260, 201, '_stock', NULL),
(1261, 201, '_stock_status', 'instock'),
(1262, 201, '_wc_average_rating', '0'),
(1263, 201, '_wc_review_count', '0'),
(1264, 201, '_product_version', '5.4.1'),
(1265, 201, '_price', '88'),
(1266, 201, 'product_accordion', ''),
(1267, 201, '_product_accordion', 'field_60c7320f43497'),
(1268, 201, '_edit_lock', '1623667512:1'),
(1269, 202, '_edit_last', '1'),
(1270, 202, '_edit_lock', '1623667537:1'),
(1271, 202, '_thumbnail_id', '189'),
(1272, 202, 'total_sales', '0'),
(1273, 202, '_tax_status', 'taxable'),
(1274, 202, '_tax_class', ''),
(1275, 202, '_manage_stock', 'no'),
(1276, 202, '_backorders', 'no'),
(1277, 202, '_sold_individually', 'no'),
(1278, 202, '_virtual', 'no'),
(1279, 202, '_downloadable', 'no'),
(1280, 202, '_download_limit', '-1'),
(1281, 202, '_download_expiry', '-1'),
(1282, 202, '_stock', NULL),
(1283, 202, '_stock_status', 'instock'),
(1284, 202, '_wc_average_rating', '0'),
(1285, 202, '_wc_review_count', '0'),
(1286, 202, '_product_version', '5.4.1'),
(1287, 202, 'product_accordion', ''),
(1288, 202, '_product_accordion', 'field_60c7320f43497'),
(1289, 203, '_edit_last', '1'),
(1290, 203, '_thumbnail_id', '190'),
(1291, 203, 'total_sales', '0'),
(1292, 203, '_tax_status', 'taxable'),
(1293, 203, '_tax_class', ''),
(1294, 203, '_manage_stock', 'no'),
(1295, 203, '_backorders', 'no'),
(1296, 203, '_sold_individually', 'no'),
(1297, 203, '_virtual', 'no'),
(1298, 203, '_downloadable', 'no'),
(1299, 203, '_download_limit', '-1'),
(1300, 203, '_download_expiry', '-1'),
(1301, 203, '_stock', NULL),
(1302, 203, '_stock_status', 'instock'),
(1303, 203, '_wc_average_rating', '0'),
(1304, 203, '_wc_review_count', '0'),
(1305, 203, '_product_version', '5.4.1'),
(1306, 203, 'product_accordion', ''),
(1307, 203, '_product_accordion', 'field_60c7320f43497'),
(1308, 203, '_edit_lock', '1623667738:1'),
(1309, 204, '_wp_attached_file', '2021/06/cat-1.png'),
(1310, 204, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:454;s:6:\"height\";i:574;s:4:\"file\";s:17:\"2021/06/cat-1.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-1-237x300.png\";s:5:\"width\";i:237;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-1-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1311, 205, '_wp_attached_file', '2021/06/cat-2.png'),
(1312, 205, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:458;s:6:\"height\";i:569;s:4:\"file\";s:17:\"2021/06/cat-2.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-2-241x300.png\";s:5:\"width\";i:241;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-2-76x94.png\";s:5:\"width\";i:76;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-2-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-2-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1313, 206, '_wp_attached_file', '2021/06/cat-3.png'),
(1314, 206, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:451;s:6:\"height\";i:583;s:4:\"file\";s:17:\"2021/06/cat-3.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-3-232x300.png\";s:5:\"width\";i:232;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-3-73x94.png\";s:5:\"width\";i:73;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-3-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-3-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1315, 207, '_wp_attached_file', '2021/06/cat-4.png');
INSERT INTO `fmn_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1316, 207, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:456;s:6:\"height\";i:579;s:4:\"file\";s:17:\"2021/06/cat-4.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-4-236x300.png\";s:5:\"width\";i:236;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-4-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-4-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-4-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1317, 208, '_wp_attached_file', '2021/06/cat-5.png'),
(1318, 208, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:454;s:6:\"height\";i:576;s:4:\"file\";s:17:\"2021/06/cat-5.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-5-236x300.png\";s:5:\"width\";i:236;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-5-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-5-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-5-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1319, 209, '_wp_attached_file', '2021/06/cat-6.png'),
(1320, 209, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:450;s:6:\"height\";i:573;s:4:\"file\";s:17:\"2021/06/cat-6.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-6-236x300.png\";s:5:\"width\";i:236;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-6-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-6-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-6-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1321, 210, '_wp_attached_file', '2021/06/cat-7.png'),
(1322, 210, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:461;s:6:\"height\";i:582;s:4:\"file\";s:17:\"2021/06/cat-7.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-7-238x300.png\";s:5:\"width\";i:238;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-7-74x94.png\";s:5:\"width\";i:74;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-7-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-7-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1323, 211, '_wp_attached_file', '2021/06/cat-8.png'),
(1324, 211, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:454;s:6:\"height\";i:583;s:4:\"file\";s:17:\"2021/06/cat-8.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-8-234x300.png\";s:5:\"width\";i:234;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-8-73x94.png\";s:5:\"width\";i:73;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-8-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-8-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1325, 212, '_wp_attached_file', '2021/06/cat-9.png'),
(1326, 212, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:450;s:6:\"height\";i:566;s:4:\"file\";s:17:\"2021/06/cat-9.png\";s:5:\"sizes\";a:7:{s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"cat-9-239x300.png\";s:5:\"width\";i:239;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:15:\"cat-9-75x94.png\";s:5:\"width\";i:75;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"cat-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"cat-9-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"cat-9-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1327, 213, '_edit_last', '1'),
(1328, 213, '_edit_lock', '1623775648:1'),
(1329, 213, '_wp_page_template', 'views/categories.php'),
(1330, 213, 'title_tag', ''),
(1331, 213, '_title_tag', 'field_5ddbe7577e0e7'),
(1332, 213, 'seo_content', ''),
(1333, 213, '_seo_content', 'field_5ddbde7399116'),
(1334, 213, 'seo_link', ''),
(1335, 213, '_seo_link', 'field_60c08d70f9afb'),
(1336, 213, 'seo_img', ''),
(1337, 213, '_seo_img', 'field_60c08d4ff9afa'),
(1338, 214, 'title_tag', ''),
(1339, 214, '_title_tag', 'field_5ddbe7577e0e7'),
(1340, 214, 'seo_content', ''),
(1341, 214, '_seo_content', 'field_5ddbde7399116'),
(1342, 214, 'seo_link', ''),
(1343, 214, '_seo_link', 'field_60c08d70f9afb'),
(1344, 214, 'seo_img', ''),
(1345, 214, '_seo_img', 'field_60c08d4ff9afa'),
(1346, 215, '_edit_last', '1'),
(1347, 215, '_edit_lock', '1623775544:1'),
(1348, 218, 'title_tag', ''),
(1349, 218, '_title_tag', 'field_5ddbe7577e0e7'),
(1350, 218, 'seo_content', ''),
(1351, 218, '_seo_content', 'field_5ddbde7399116'),
(1352, 218, 'seo_link', ''),
(1353, 218, '_seo_link', 'field_60c08d70f9afb'),
(1354, 218, 'seo_img', ''),
(1355, 218, '_seo_img', 'field_60c08d4ff9afa'),
(1356, 213, 'cat_products_title', 'עכשיו בעונה'),
(1357, 213, '_cat_products_title', 'field_60c7386a2c527'),
(1358, 213, 'cat_products', 'a:4:{i:0;s:3:\"195\";i:1;s:3:\"196\";i:2;s:3:\"197\";i:3;s:3:\"198\";}'),
(1359, 213, '_cat_products', 'field_60c7387e2c528'),
(1360, 219, 'title_tag', ''),
(1361, 219, '_title_tag', 'field_5ddbe7577e0e7'),
(1362, 219, 'seo_content', ''),
(1363, 219, '_seo_content', 'field_5ddbde7399116'),
(1364, 219, 'seo_link', ''),
(1365, 219, '_seo_link', 'field_60c08d70f9afb'),
(1366, 219, 'seo_img', ''),
(1367, 219, '_seo_img', 'field_60c08d4ff9afa'),
(1368, 219, 'cat_products_title', 'עכשיו בעונה'),
(1369, 219, '_cat_products_title', 'field_60c7386a2c527'),
(1370, 219, 'cat_products', 'a:4:{i:0;s:3:\"195\";i:1;s:3:\"196\";i:2;s:3:\"197\";i:3;s:3:\"198\";}'),
(1371, 219, '_cat_products', 'field_60c7387e2c528'),
(1372, 220, 'title_tag', ''),
(1373, 220, '_title_tag', 'field_5ddbe7577e0e7'),
(1374, 220, 'seo_content', ''),
(1375, 220, '_seo_content', 'field_5ddbde7399116'),
(1376, 220, 'seo_link', ''),
(1377, 220, '_seo_link', 'field_60c08d70f9afb'),
(1378, 220, 'seo_img', ''),
(1379, 220, '_seo_img', 'field_60c08d4ff9afa'),
(1380, 220, 'cat_products_title', 'עכשיו בעונה'),
(1381, 220, '_cat_products_title', 'field_60c7386a2c527'),
(1382, 220, 'cat_products', 'a:4:{i:0;s:3:\"195\";i:1;s:3:\"196\";i:2;s:3:\"197\";i:3;s:3:\"198\";}'),
(1383, 220, '_cat_products', 'field_60c7387e2c528'),
(1384, 221, '_menu_item_type', 'post_type'),
(1385, 221, '_menu_item_menu_item_parent', '0'),
(1386, 221, '_menu_item_object_id', '213'),
(1387, 221, '_menu_item_object', 'page'),
(1388, 221, '_menu_item_target', ''),
(1389, 221, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1390, 221, '_menu_item_xfn', ''),
(1391, 221, '_menu_item_url', ''),
(1393, 222, '_wp_attached_file', '2021/06/תמונת-ראשי.png'),
(1394, 222, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:618;s:4:\"file\";s:31:\"2021/06/תמונת-ראשי.png\";s:5:\"sizes\";a:12:{s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"תמונת-ראשי-300x97.png\";s:5:\"width\";i:300;s:6:\"height\";i:97;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"תמונת-ראשי-1024x330.png\";s:5:\"width\";i:1024;s:6:\"height\";i:330;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-768x247.png\";s:5:\"width\";i:768;s:6:\"height\";i:247;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"1536x1536\";a:4:{s:4:\"file\";s:32:\"תמונת-ראשי-1536x494.png\";s:5:\"width\";i:1536;s:6:\"height\";i:494;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:30:\"תמונת-ראשי-134x43.png\";s:5:\"width\";i:134;s:6:\"height\";i:43;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:31:\"תמונת-ראשי-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-600x193.png\";s:5:\"width\";i:600;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-600x193.png\";s:5:\"width\";i:600;s:6:\"height\";i:193;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:31:\"תמונת-ראשי-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1395, 28, '_thumbnail_id', '222'),
(1396, 28, 'main_title', 'רגע מאושר מתחיל בזר פרחים'),
(1397, 28, '_main_title', 'field_60c72e2d046e8'),
(1398, 28, 'main_subtitle', 'הפכו את הארוע שלכם ליפיפה '),
(1399, 28, '_main_subtitle', 'field_60c72e38046e9'),
(1400, 28, 'main_link', 'a:3:{s:5:\"title\";s:27:\"לקולקציה המלאה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1401, 28, '_main_link', 'field_60c72e17bf1c4'),
(1402, 28, 'home_products_1_title', 'המפנקים שלנו '),
(1403, 28, '_home_products_1_title', 'field_60c72e697b8e2'),
(1404, 28, 'home_products_1', 'a:4:{i:0;s:3:\"203\";i:1;s:3:\"202\";i:2;s:3:\"201\";i:3;s:3:\"200\";}'),
(1405, 28, '_home_products_1', 'field_60c72e517b8e1'),
(1406, 28, 'home_about_text', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(1407, 28, '_home_about_text', 'field_60c72e8f509ec'),
(1408, 28, 'home_about_img', '225'),
(1409, 28, '_home_about_img', 'field_60c72ea2509ed'),
(1410, 28, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:40:\"http://h/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1411, 28, '_home_about_link', 'field_60c72ebd509ee'),
(1412, 28, 'home_cats_title', 'קטגוריות מומלצות'),
(1413, 28, '_home_cats_title', 'field_60c72edc509f0'),
(1414, 28, 'home_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1415, 28, '_home_cats', 'field_60c72ee8509f1'),
(1416, 28, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1417, 28, '_home_cats_link', 'field_60c72efb509f2'),
(1418, 28, 'home_products_2_title', 'המומלצים שלנו'),
(1419, 28, '_home_products_2_title', 'field_60c72f2bc06e4'),
(1420, 28, 'home_products_2', 'a:8:{i:0;s:3:\"180\";i:1;s:3:\"195\";i:2;s:3:\"196\";i:3;s:3:\"197\";i:4;s:3:\"198\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";}'),
(1421, 28, '_home_products_2', 'field_60c72f25c06e3'),
(1422, 28, 'home_posts_title', 'פרחים על פי ארועים'),
(1423, 28, '_home_posts_title', 'field_60c72f9216256'),
(1424, 28, 'home_posts', 'a:3:{i:0;s:1:\"1\";i:1;s:2:\"43\";i:2;s:2:\"70\";}'),
(1425, 28, '_home_posts', 'field_60c72fa116257'),
(1426, 28, 'home_posts_link', 'a:3:{s:5:\"title\";s:19:\"כל הארועים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1427, 28, '_home_posts_link', 'field_60c72fb016258'),
(1428, 28, 'home_products_3_title', 'עכשיו בעונה'),
(1429, 28, '_home_products_3_title', 'field_60c72fef7fbd3'),
(1430, 28, 'home_products_3', 'a:4:{i:0;s:3:\"202\";i:1;s:3:\"201\";i:2;s:3:\"200\";i:3;s:3:\"199\";}'),
(1431, 28, '_home_products_3', 'field_60c72ffd7fbd4'),
(1432, 28, 'seo_content', '<h2>לורם איפסום בדף הבית</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nלהאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1433, 28, '_seo_content', 'field_5ddbde7399116'),
(1434, 28, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1435, 28, '_seo_link', 'field_60c08d70f9afb'),
(1436, 28, 'seo_img', '51'),
(1437, 28, '_seo_img', 'field_60c08d4ff9afa'),
(1438, 223, 'title_tag', ''),
(1439, 223, '_title_tag', 'field_5ddbe7577e0e7'),
(1440, 223, 'single_slider_seo', ''),
(1441, 223, '_single_slider_seo', 'field_5ddbde5499115'),
(1442, 223, 'single_gallery', ''),
(1443, 223, '_single_gallery', 'field_5ddbe5aea4275'),
(1444, 223, 'single_video_slider', ''),
(1445, 223, '_single_video_slider', 'field_5ddbe636a4276'),
(1446, 223, 'main_title', 'רגע מאושר מתחיל בזר פרחים'),
(1447, 223, '_main_title', 'field_60c72e2d046e8'),
(1448, 223, 'main_subtitle', 'הפכו את הארוע שלכם ליפיפה '),
(1449, 223, '_main_subtitle', 'field_60c72e38046e9'),
(1450, 223, 'main_link', 'a:3:{s:5:\"title\";s:27:\"לקולקציה המלאה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1451, 223, '_main_link', 'field_60c72e17bf1c4'),
(1452, 223, 'home_products_1_title', 'המפנקים שלנו '),
(1453, 223, '_home_products_1_title', 'field_60c72e697b8e2'),
(1454, 223, 'home_products_1', 'a:4:{i:0;s:3:\"203\";i:1;s:3:\"202\";i:2;s:3:\"201\";i:3;s:3:\"200\";}'),
(1455, 223, '_home_products_1', 'field_60c72e517b8e1'),
(1456, 223, 'home_about_text', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(1457, 223, '_home_about_text', 'field_60c72e8f509ec'),
(1458, 223, 'home_about_img', ''),
(1459, 223, '_home_about_img', 'field_60c72ea2509ed'),
(1460, 223, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:40:\"http://h/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1461, 223, '_home_about_link', 'field_60c72ebd509ee'),
(1462, 223, 'home_cats_title', 'קטגוריות מומלצות'),
(1463, 223, '_home_cats_title', 'field_60c72edc509f0'),
(1464, 223, 'home_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1465, 223, '_home_cats', 'field_60c72ee8509f1'),
(1466, 223, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1467, 223, '_home_cats_link', 'field_60c72efb509f2'),
(1468, 223, 'home_products_2_title', 'המומלצים שלנו'),
(1469, 223, '_home_products_2_title', 'field_60c72f2bc06e4'),
(1470, 223, 'home_products_2', 'a:8:{i:0;s:3:\"180\";i:1;s:3:\"195\";i:2;s:3:\"196\";i:3;s:3:\"197\";i:4;s:3:\"198\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";}'),
(1471, 223, '_home_products_2', 'field_60c72f25c06e3'),
(1472, 223, 'home_posts_title', 'פרחים על פי ארועים'),
(1473, 223, '_home_posts_title', 'field_60c72f9216256'),
(1474, 223, 'home_posts', 'a:3:{i:0;s:1:\"1\";i:1;s:2:\"43\";i:2;s:2:\"70\";}'),
(1475, 223, '_home_posts', 'field_60c72fa116257'),
(1476, 223, 'home_posts_link', 'a:3:{s:5:\"title\";s:19:\"כל הארועים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1477, 223, '_home_posts_link', 'field_60c72fb016258'),
(1478, 223, 'home_products_3_title', 'עכשיו בעונה'),
(1479, 223, '_home_products_3_title', 'field_60c72fef7fbd3'),
(1480, 223, 'home_products_3', 'a:4:{i:0;s:3:\"202\";i:1;s:3:\"201\";i:2;s:3:\"200\";i:3;s:3:\"199\";}'),
(1481, 223, '_home_products_3', 'field_60c72ffd7fbd4'),
(1482, 223, 'seo_content', ''),
(1483, 223, '_seo_content', 'field_5ddbde7399116'),
(1484, 223, 'seo_link', ''),
(1485, 223, '_seo_link', 'field_60c08d70f9afb'),
(1486, 223, 'seo_img', ''),
(1487, 223, '_seo_img', 'field_60c08d4ff9afa'),
(1488, 224, 'title_tag', ''),
(1489, 224, '_title_tag', 'field_5ddbe7577e0e7'),
(1490, 224, 'single_slider_seo', ''),
(1491, 224, '_single_slider_seo', 'field_5ddbde5499115'),
(1492, 224, 'single_gallery', ''),
(1493, 224, '_single_gallery', 'field_5ddbe5aea4275'),
(1494, 224, 'single_video_slider', ''),
(1495, 224, '_single_video_slider', 'field_5ddbe636a4276'),
(1496, 224, 'main_title', 'רגע מאושר מתחיל בזר פרחים'),
(1497, 224, '_main_title', 'field_60c72e2d046e8'),
(1498, 224, 'main_subtitle', 'הפכו את הארוע שלכם ליפיפה '),
(1499, 224, '_main_subtitle', 'field_60c72e38046e9'),
(1500, 224, 'main_link', 'a:3:{s:5:\"title\";s:27:\"לקולקציה המלאה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1501, 224, '_main_link', 'field_60c72e17bf1c4'),
(1502, 224, 'home_products_1_title', 'המפנקים שלנו '),
(1503, 224, '_home_products_1_title', 'field_60c72e697b8e2'),
(1504, 224, 'home_products_1', 'a:4:{i:0;s:3:\"203\";i:1;s:3:\"202\";i:2;s:3:\"201\";i:3;s:3:\"200\";}'),
(1505, 224, '_home_products_1', 'field_60c72e517b8e1'),
(1506, 224, 'home_about_text', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(1507, 224, '_home_about_text', 'field_60c72e8f509ec'),
(1508, 224, 'home_about_img', ''),
(1509, 224, '_home_about_img', 'field_60c72ea2509ed'),
(1510, 224, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:40:\"http://h/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1511, 224, '_home_about_link', 'field_60c72ebd509ee'),
(1512, 224, 'home_cats_title', 'קטגוריות מומלצות'),
(1513, 224, '_home_cats_title', 'field_60c72edc509f0'),
(1514, 224, 'home_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1515, 224, '_home_cats', 'field_60c72ee8509f1'),
(1516, 224, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1517, 224, '_home_cats_link', 'field_60c72efb509f2'),
(1518, 224, 'home_products_2_title', 'המומלצים שלנו'),
(1519, 224, '_home_products_2_title', 'field_60c72f2bc06e4'),
(1520, 224, 'home_products_2', 'a:8:{i:0;s:3:\"180\";i:1;s:3:\"195\";i:2;s:3:\"196\";i:3;s:3:\"197\";i:4;s:3:\"198\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";}'),
(1521, 224, '_home_products_2', 'field_60c72f25c06e3'),
(1522, 224, 'home_posts_title', 'פרחים על פי ארועים'),
(1523, 224, '_home_posts_title', 'field_60c72f9216256'),
(1524, 224, 'home_posts', 'a:3:{i:0;s:1:\"1\";i:1;s:2:\"43\";i:2;s:2:\"70\";}'),
(1525, 224, '_home_posts', 'field_60c72fa116257'),
(1526, 224, 'home_posts_link', 'a:3:{s:5:\"title\";s:19:\"כל הארועים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1527, 224, '_home_posts_link', 'field_60c72fb016258'),
(1528, 224, 'home_products_3_title', 'עכשיו בעונה'),
(1529, 224, '_home_products_3_title', 'field_60c72fef7fbd3'),
(1530, 224, 'home_products_3', 'a:4:{i:0;s:3:\"202\";i:1;s:3:\"201\";i:2;s:3:\"200\";i:3;s:3:\"199\";}'),
(1531, 224, '_home_products_3', 'field_60c72ffd7fbd4'),
(1532, 224, 'seo_content', '<h2>לורם איפסום בדף הבית</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nלהאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1533, 224, '_seo_content', 'field_5ddbde7399116'),
(1534, 224, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1535, 224, '_seo_link', 'field_60c08d70f9afb'),
(1536, 224, 'seo_img', '51'),
(1537, 224, '_seo_img', 'field_60c08d4ff9afa'),
(1538, 225, '_wp_attached_file', '2021/06/about-home.png'),
(1539, 225, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:789;s:6:\"height\";i:567;s:4:\"file\";s:22:\"2021/06/about-home.png\";s:5:\"sizes\";a:10:{s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"about-home-300x216.png\";s:5:\"width\";i:300;s:6:\"height\";i:216;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"about-home-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"about-home-768x552.png\";s:5:\"width\";i:768;s:6:\"height\";i:552;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"gallery-thumb\";a:4:{s:4:\"file\";s:21:\"about-home-131x94.png\";s:5:\"width\";i:131;s:6:\"height\";i:94;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:22:\"about-home-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:22:\"about-home-600x431.png\";s:5:\"width\";i:600;s:6:\"height\";i:431;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:22:\"about-home-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:22:\"about-home-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:22:\"about-home-600x431.png\";s:5:\"width\";i:600;s:6:\"height\";i:431;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:22:\"about-home-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(1540, 226, 'title_tag', ''),
(1541, 226, '_title_tag', 'field_5ddbe7577e0e7'),
(1542, 226, 'single_slider_seo', ''),
(1543, 226, '_single_slider_seo', 'field_5ddbde5499115'),
(1544, 226, 'single_gallery', ''),
(1545, 226, '_single_gallery', 'field_5ddbe5aea4275'),
(1546, 226, 'single_video_slider', ''),
(1547, 226, '_single_video_slider', 'field_5ddbe636a4276'),
(1548, 226, 'main_title', 'רגע מאושר מתחיל בזר פרחים'),
(1549, 226, '_main_title', 'field_60c72e2d046e8'),
(1550, 226, 'main_subtitle', 'הפכו את הארוע שלכם ליפיפה '),
(1551, 226, '_main_subtitle', 'field_60c72e38046e9'),
(1552, 226, 'main_link', 'a:3:{s:5:\"title\";s:27:\"לקולקציה המלאה\";s:3:\"url\";s:6:\"/shop/\";s:6:\"target\";s:0:\"\";}'),
(1553, 226, '_main_link', 'field_60c72e17bf1c4'),
(1554, 226, 'home_products_1_title', 'המפנקים שלנו '),
(1555, 226, '_home_products_1_title', 'field_60c72e697b8e2'),
(1556, 226, 'home_products_1', 'a:4:{i:0;s:3:\"203\";i:1;s:3:\"202\";i:2;s:3:\"201\";i:3;s:3:\"200\";}'),
(1557, 226, '_home_products_1', 'field_60c72e517b8e1'),
(1558, 226, 'home_about_text', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק'),
(1559, 226, '_home_about_text', 'field_60c72e8f509ec'),
(1560, 226, 'home_about_img', '225'),
(1561, 226, '_home_about_img', 'field_60c72ea2509ed'),
(1562, 226, 'home_about_link', 'a:3:{s:5:\"title\";s:17:\"עוד עלינו\";s:3:\"url\";s:40:\"http://h/%d7%90%d7%95%d7%93%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1563, 226, '_home_about_link', 'field_60c72ebd509ee'),
(1564, 226, 'home_cats_title', 'קטגוריות מומלצות'),
(1565, 226, '_home_cats_title', 'field_60c72edc509f0'),
(1566, 226, 'home_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1567, 226, '_home_cats', 'field_60c72ee8509f1'),
(1568, 226, 'home_cats_link', 'a:3:{s:5:\"title\";s:25:\"לכל הקטגוריות\";s:3:\"url\";s:50:\"/%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa/\";s:6:\"target\";s:0:\"\";}'),
(1569, 226, '_home_cats_link', 'field_60c72efb509f2'),
(1570, 226, 'home_products_2_title', 'המומלצים שלנו'),
(1571, 226, '_home_products_2_title', 'field_60c72f2bc06e4'),
(1572, 226, 'home_products_2', 'a:8:{i:0;s:3:\"180\";i:1;s:3:\"195\";i:2;s:3:\"196\";i:3;s:3:\"197\";i:4;s:3:\"198\";i:5;s:3:\"199\";i:6;s:3:\"200\";i:7;s:3:\"201\";}'),
(1573, 226, '_home_products_2', 'field_60c72f25c06e3'),
(1574, 226, 'home_posts_title', 'פרחים על פי ארועים'),
(1575, 226, '_home_posts_title', 'field_60c72f9216256'),
(1576, 226, 'home_posts', 'a:3:{i:0;s:1:\"1\";i:1;s:2:\"43\";i:2;s:2:\"70\";}'),
(1577, 226, '_home_posts', 'field_60c72fa116257'),
(1578, 226, 'home_posts_link', 'a:3:{s:5:\"title\";s:19:\"כל הארועים\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1579, 226, '_home_posts_link', 'field_60c72fb016258'),
(1580, 226, 'home_products_3_title', 'עכשיו בעונה'),
(1581, 226, '_home_products_3_title', 'field_60c72fef7fbd3'),
(1582, 226, 'home_products_3', 'a:4:{i:0;s:3:\"202\";i:1;s:3:\"201\";i:2;s:3:\"200\";i:3;s:3:\"199\";}'),
(1583, 226, '_home_products_3', 'field_60c72ffd7fbd4'),
(1584, 226, 'seo_content', '<h2>לורם איפסום בדף הבית</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nלהאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.'),
(1585, 226, '_seo_content', 'field_5ddbde7399116'),
(1586, 226, 'seo_link', 'a:3:{s:5:\"title\";s:0:\"\";s:3:\"url\";s:38:\"/%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d/\";s:6:\"target\";s:0:\"\";}'),
(1587, 226, '_seo_link', 'field_60c08d70f9afb'),
(1588, 226, 'seo_img', '51'),
(1589, 226, '_seo_img', 'field_60c08d4ff9afa'),
(1590, 86, '_config_errors', 'a:1:{s:23:\"mail.additional_headers\";a:1:{i:0;a:2:{s:4:\"code\";i:102;s:4:\"args\";a:3:{s:7:\"message\";s:62:\"תחביר כתובת מייל בשדה %name% לא תקינה\";s:6:\"params\";a:1:{s:4:\"name\";s:8:\"Reply-To\";}s:4:\"link\";s:68:\"https://contactform7.com/configuration-errors/invalid-mailbox-syntax\";}}}}'),
(1591, 7, '_edit_lock', '1623758744:1'),
(1592, 7, '_edit_last', '1'),
(1593, 7, 'title_tag', ''),
(1594, 7, '_title_tag', 'field_5ddbe7577e0e7'),
(1595, 7, 'seo_content', ''),
(1596, 7, '_seo_content', 'field_5ddbde7399116'),
(1597, 7, 'seo_link', ''),
(1598, 7, '_seo_link', 'field_60c08d70f9afb'),
(1599, 7, 'seo_img', ''),
(1600, 7, '_seo_img', 'field_60c08d4ff9afa'),
(1601, 229, 'title_tag', ''),
(1602, 229, '_title_tag', 'field_5ddbe7577e0e7'),
(1603, 229, 'seo_content', ''),
(1604, 229, '_seo_content', 'field_5ddbde7399116'),
(1605, 229, 'seo_link', ''),
(1606, 229, '_seo_link', 'field_60c08d70f9afb'),
(1607, 229, 'seo_img', ''),
(1608, 229, '_seo_img', 'field_60c08d4ff9afa'),
(1609, 230, '_edit_last', '1'),
(1610, 230, '_edit_lock', '1623771329:1'),
(1611, 7, 'shop_cats_title', 'קטגוריות דומות'),
(1612, 7, '_shop_cats_title', 'field_60c89789309c8'),
(1613, 7, 'shop_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1614, 7, '_shop_cats', 'field_60c8979f309c9'),
(1615, 234, 'title_tag', ''),
(1616, 234, '_title_tag', 'field_5ddbe7577e0e7'),
(1617, 234, 'seo_content', ''),
(1618, 234, '_seo_content', 'field_5ddbde7399116'),
(1619, 234, 'seo_link', ''),
(1620, 234, '_seo_link', 'field_60c08d70f9afb'),
(1621, 234, 'seo_img', ''),
(1622, 234, '_seo_img', 'field_60c08d4ff9afa'),
(1623, 234, 'shop_cats_title', 'קטגוריות דומות'),
(1624, 234, '_shop_cats_title', 'field_60c89789309c8'),
(1625, 234, 'shop_cats', 'a:3:{i:0;s:2:\"25\";i:1;s:2:\"26\";i:2;s:2:\"27\";}'),
(1626, 234, '_shop_cats', 'field_60c8979f309c9'),
(1627, 235, '_menu_item_type', 'post_type'),
(1628, 235, '_menu_item_menu_item_parent', '0'),
(1629, 235, '_menu_item_object_id', '7'),
(1630, 235, '_menu_item_object', 'page'),
(1631, 235, '_menu_item_target', ''),
(1632, 235, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1633, 235, '_menu_item_xfn', ''),
(1634, 235, '_menu_item_url', ''),
(1636, 152, '_wp_old_date', '2021-06-14'),
(1637, 27, '_wp_old_date', '2021-06-14'),
(1638, 75, '_wp_old_date', '2021-06-14'),
(1639, 76, '_wp_old_date', '2021-06-14'),
(1640, 129, '_wp_old_date', '2021-06-14'),
(1641, 151, '_wp_old_date', '2021-06-14'),
(1642, 221, '_wp_old_date', '2021-06-14'),
(1643, 113, '_wp_old_date', '2021-06-14'),
(1644, 236, '_menu_item_type', 'taxonomy'),
(1645, 236, '_menu_item_menu_item_parent', '0'),
(1646, 236, '_menu_item_object_id', '25'),
(1647, 236, '_menu_item_object', 'product_cat'),
(1648, 236, '_menu_item_target', ''),
(1649, 236, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1650, 236, '_menu_item_xfn', ''),
(1651, 236, '_menu_item_url', ''),
(1652, 237, '_edit_last', '1'),
(1653, 237, '_edit_lock', '1623771385:1'),
(1654, 237, '_regular_price', '112'),
(1655, 237, '_sale_price', '90'),
(1656, 237, 'total_sales', '0'),
(1657, 237, '_tax_status', 'taxable'),
(1658, 237, '_tax_class', ''),
(1659, 237, '_manage_stock', 'no'),
(1660, 237, '_backorders', 'no'),
(1661, 237, '_sold_individually', 'no'),
(1662, 237, '_virtual', 'no'),
(1663, 237, '_downloadable', 'no'),
(1664, 237, '_download_limit', '-1'),
(1665, 237, '_download_expiry', '-1'),
(1666, 237, '_stock', NULL),
(1667, 237, '_stock_status', 'instock'),
(1668, 237, '_wc_average_rating', '0'),
(1669, 237, '_wc_review_count', '0'),
(1670, 237, '_product_version', '5.4.1'),
(1671, 237, '_price', '90'),
(1672, 237, '_product_image_gallery', '107'),
(1673, 237, 'product_accordion', ''),
(1674, 237, '_product_accordion', 'field_60c7320f43497'),
(1675, 237, 'seo_content', ''),
(1676, 237, '_seo_content', 'field_5ddbde7399116'),
(1677, 237, 'seo_link', ''),
(1678, 237, '_seo_link', 'field_60c08d70f9afb'),
(1679, 237, 'seo_img', ''),
(1680, 237, '_seo_img', 'field_60c08d4ff9afa'),
(1681, 238, '_edit_last', '1'),
(1682, 238, '_thumbnail_id', '183'),
(1683, 238, 'total_sales', '0'),
(1684, 238, '_tax_status', 'taxable'),
(1685, 238, '_tax_class', ''),
(1686, 238, '_manage_stock', 'no'),
(1687, 238, '_backorders', 'no'),
(1688, 238, '_sold_individually', 'no'),
(1689, 238, '_virtual', 'no'),
(1690, 238, '_downloadable', 'no'),
(1691, 238, '_download_limit', '-1'),
(1692, 238, '_download_expiry', '-1'),
(1693, 238, '_stock', NULL),
(1694, 238, '_stock_status', 'instock'),
(1695, 238, '_wc_average_rating', '0'),
(1696, 238, '_wc_review_count', '0'),
(1697, 238, '_product_version', '5.4.1'),
(1698, 238, 'product_accordion', ''),
(1699, 238, '_product_accordion', 'field_60c7320f43497'),
(1700, 238, 'seo_content', ''),
(1701, 238, '_seo_content', 'field_5ddbde7399116'),
(1702, 238, 'seo_link', ''),
(1703, 238, '_seo_link', 'field_60c08d70f9afb'),
(1704, 238, 'seo_img', ''),
(1705, 238, '_seo_img', 'field_60c08d4ff9afa'),
(1706, 238, '_edit_lock', '1623771417:1'),
(1707, 239, '_edit_last', '1'),
(1708, 239, '_edit_lock', '1623771626:1'),
(1710, 12, 'seo_content', ''),
(1711, 12, '_seo_content', 'field_5ddbde7399116'),
(1712, 12, 'seo_link', ''),
(1713, 12, '_seo_link', 'field_60c08d70f9afb'),
(1714, 12, 'seo_img', ''),
(1715, 12, '_seo_img', 'field_60c08d4ff9afa'),
(1716, 12, '_upsell_ids', 'a:7:{i:0;i:180;i:1;i:195;i:2;i:196;i:3;i:197;i:4;i:198;i:5;i:199;i:6;i:200;}'),
(1717, 241, '_menu_item_type', 'post_type'),
(1718, 241, '_menu_item_menu_item_parent', '0'),
(1719, 241, '_menu_item_object_id', '72'),
(1720, 241, '_menu_item_object', 'page'),
(1721, 241, '_menu_item_target', ''),
(1722, 241, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1723, 241, '_menu_item_xfn', ''),
(1724, 241, '_menu_item_url', ''),
(1726, 242, '_menu_item_type', 'post_type'),
(1727, 242, '_menu_item_menu_item_parent', '241'),
(1728, 242, '_menu_item_object_id', '68'),
(1729, 242, '_menu_item_object', 'post'),
(1730, 242, '_menu_item_target', ''),
(1731, 242, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1732, 242, '_menu_item_xfn', ''),
(1733, 242, '_menu_item_url', ''),
(1735, 243, '_menu_item_type', 'post_type'),
(1736, 243, '_menu_item_menu_item_parent', '241'),
(1737, 243, '_menu_item_object_id', '66'),
(1738, 243, '_menu_item_object', 'post'),
(1739, 243, '_menu_item_target', ''),
(1740, 243, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1741, 243, '_menu_item_xfn', ''),
(1742, 243, '_menu_item_url', ''),
(1744, 244, '_menu_item_type', 'post_type'),
(1745, 244, '_menu_item_menu_item_parent', '241'),
(1746, 244, '_menu_item_object_id', '64'),
(1747, 244, '_menu_item_object', 'post'),
(1748, 244, '_menu_item_target', ''),
(1749, 244, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1750, 244, '_menu_item_xfn', ''),
(1751, 244, '_menu_item_url', ''),
(1753, 245, '_menu_item_type', 'post_type'),
(1754, 245, '_menu_item_menu_item_parent', '241'),
(1755, 245, '_menu_item_object_id', '62'),
(1756, 245, '_menu_item_object', 'post'),
(1757, 245, '_menu_item_target', ''),
(1758, 245, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1759, 245, '_menu_item_xfn', ''),
(1760, 245, '_menu_item_url', ''),
(1762, 246, '_menu_item_type', 'taxonomy'),
(1763, 246, '_menu_item_menu_item_parent', '0'),
(1764, 246, '_menu_item_object_id', '25'),
(1765, 246, '_menu_item_object', 'product_cat'),
(1766, 246, '_menu_item_target', ''),
(1767, 246, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1768, 246, '_menu_item_xfn', ''),
(1769, 246, '_menu_item_url', ''),
(1771, 247, '_menu_item_type', 'post_type'),
(1772, 247, '_menu_item_menu_item_parent', '246'),
(1773, 247, '_menu_item_object_id', '203'),
(1774, 247, '_menu_item_object', 'product'),
(1775, 247, '_menu_item_target', ''),
(1776, 247, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1777, 247, '_menu_item_xfn', ''),
(1778, 247, '_menu_item_url', ''),
(1780, 248, '_menu_item_type', 'post_type'),
(1781, 248, '_menu_item_menu_item_parent', '246'),
(1782, 248, '_menu_item_object_id', '202'),
(1783, 248, '_menu_item_object', 'product'),
(1784, 248, '_menu_item_target', ''),
(1785, 248, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1786, 248, '_menu_item_xfn', ''),
(1787, 248, '_menu_item_url', ''),
(1789, 249, '_menu_item_type', 'taxonomy'),
(1790, 249, '_menu_item_menu_item_parent', '0'),
(1791, 249, '_menu_item_object_id', '26'),
(1792, 249, '_menu_item_object', 'product_cat'),
(1793, 249, '_menu_item_target', ''),
(1794, 249, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(1795, 249, '_menu_item_xfn', ''),
(1796, 249, '_menu_item_url', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_posts`
--

CREATE TABLE `fmn_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_posts`
--

INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2021-06-08 23:40:24', '2021-06-08 20:40:24', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9c%d7%95%d7%9d-%d7%a2%d7%95%d7%9c%d7%9d', '', '', '2021-06-10 11:15:58', '2021-06-10 08:15:58', '', 0, 'http://flowers.nagar:8888/?p=1', 0, 'post', '', 1),
(2, 1, '2021-06-08 23:40:24', '2021-06-08 20:40:24', '<!-- wp:paragraph -->\n<p>זהו עמוד לדוגמה. הוא שונה מפוסט רגיל בבלוג מכיוון שהוא יישאר במקום אחד ויופיע בתפריט הניווט באתר (ברוב התבניות). רוב האנשים מתחילים עם עמוד אודות המציג אותם למבקרים חדשים באתר. הנה תוכן לדוגמה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>אהלן! אני סטודנט לכלכלה ביום, אך מבלה את הלילות בהגשמת החלום שלי להיות שחקן תאטרון. אני גר ביפו, יש לי כלב נהדר בשם שוקו, אני אוהב ערק אשכוליות בשישי בצהריים (במיוחד תוך כדי משחק שש-בש על חוף הים). ברוכים הבאים לאתר שלי!</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>... או משהו כזה:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>החברה א.א. שומדברים נוסדה בשנת 1971, והיא מספקת שומדברים איכותיים לציבור מאז. א.א. שומדברים ממוקמת בעיר תקוות-ים, מעסיקה מעל 2,000 אנשים ועושה כל מיני דברים מדהים עבור הקהילה התקוות-ימית.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>כמשתמש וורדפרס חדש, כדאי לבקר <a href=\"http://flowers.nagar:8888/wp-admin/\">בלוח הבקרה שלך</a> כדי למחוק את העמוד הזה, וליצור עמודים חדשים עבור התוכן שלך. שיהיה בכיף!</p>\n<!-- /wp:paragraph -->', 'עמוד לדוגמא', '', 'publish', 'closed', 'open', '', 'sample-page', '', '', '2021-06-08 23:40:24', '2021-06-08 20:40:24', '', 0, 'http://flowers.nagar:8888/?page_id=2', 0, 'page', '', 0),
(3, 1, '2021-06-08 23:40:24', '2021-06-08 20:40:24', '<!-- wp:heading --><h2>מי אנחנו</h2><!-- /wp:heading --><!-- wp:paragraph --><p>כתובת האתר שלנו היא: http://flowers.nagar:8888.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>מה המידע האישי שאנו אוספים ומדוע אנחנו אוספים אותו</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>תגובות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כאשר המבקרים משאירים תגובות באתר אנו אוספים את הנתונים המוצגים בטופס התגובה, ובנוסף גם את כתובת ה-IP של המבקר, ואת מחרוזת ה-user agent של הדפדפן שלו כדי לסייע בזיהוי תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>יתכן ונעביר מחרוזת אנונימית שנוצרה מכתובת הדואר האלקטרוני שלך (הנקראת גם hash) לשירות Gravatar כדי לראות אם הנך חבר/ה בשירות. מדיניות הפרטיות של שירות Gravatar זמינה כאן: https://automattic.com/privacy/. לאחר אישור התגובה שלך, תמונת הפרופיל שלך גלויה לציבור בהקשר של התגובה שלך.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>מדיה</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בהעלאה של תמונות לאתר, מומלץ להימנע מהעלאת תמונות עם נתוני מיקום מוטבעים (EXIF GPS). המבקרים באתר יכולים להוריד ולחלץ את כל נתוני מיקום מהתמונות באתר.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>טפסי יצירת קשר</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>עוגיות</h3><!-- /wp:heading --><!-- wp:paragraph --><p>בכתיבת תגובה באתר שלנו, באפשרותך להחליט אם לאפשר לנו לשמור את השם שלך, כתובת האימייל שלך וכתובת האתר שלך בקבצי עוגיות (cookies). השמירה תתבצע לנוחיותך, על מנת שלא יהיה צורך למלא את הפרטים שלך שוב בכתיבת תגובה נוספת. קבצי העוגיות ישמרו לשנה.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה מבקר בעמוד ההתחברות של האתר, נגדיר קובץ עוגיה זמני על מנת לקבוע האם הדפדפן שלך מקבל קבצי עוגיות. קובץ עוגיה זה אינו מכיל נתונים אישיים והוא נמחק בעת סגירת הדפדפן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>כאשר תתחבר, אנחנו גם נגדיר מספר \'עוגיות\' על מנת לשמור את פרטי ההתחברות שלך ואת בחירות התצוגה שלך. עוגיות התחברות תקפות ליומיים, ועוגיות אפשרויות מסך תקפות לשנה. אם תבחר באפשרות &quot;זכור אותי&quot;, פרטי ההתחברות שלך יהיו תקפים למשך שבועיים. אם תתנתק מהחשבון שלך, עוגיות ההתחברות יימחקו.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אם אתה עורך או מפרסם מאמר, קובץ \'עוגיה\' נוסף יישמר בדפדפן שלך. קובץ \'עוגיה\' זה אינו כולל נתונים אישיים ופשוט מציין את מזהה הפוסט של המאמר. הוא יפוג לאחר יום אחד.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>תוכן מוטמע מאתרים אחרים</h3><!-- /wp:heading --><!-- wp:paragraph --><p>כתבות או פוסטים באתר זה עשויים לכלול תוכן מוטבע (לדוגמה, קטעי וידאו, תמונות, מאמרים, וכו\'). תוכן מוטבע מאתרי אינטרנט אחרים דינו כביקור הקורא באתרי האינטרנט מהם מוטבע התוכן.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>אתרים אלו עשויים לאסוף נתונים אודותיך, להשתמש בקבצי \'עוגיות\', להטמיע מעקב של צד שלישי נוסף, ולנטר את האינטראקציה שלך עם תוכן מוטמע זה, לרבות מעקב אחר האינטראקציה שלך עם התוכן המוטמע, אם יש לך חשבון ואתה מחובר לאתר זה.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>אנליטיקה</h3><!-- /wp:heading --><!-- wp:heading --><h2>עם מי אנו חולקים את המידע שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>משך הזמן בו נשמור את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>במידה ותגיב/י על תוכן באתר, התגובה והנתונים אודותיה יישמרו ללא הגבלת זמן, כדי שנוכל לזהות ולאשר את כל התגובות העוקבות באופן אוטומטי.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>עבור משתמשים רשומים באתר (במידה ויש כאלה) אנו מאחסנים גם את המידע האישי שהם מספקים בפרופיל המשתמש שלהם. כל המשתמשים יכולים לראות, לערוך או למחוק את המידע האישי שלהם בכל עת (פרט לשם המשתמש אותו לא ניתן לשנות). גם מנהלי האתר יכולים לראות ולערוך מידע זה.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>אילו זכויות יש לך על המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>אם יש לך חשבון באתר זה, או שהשארת תגובות באתר, באפשרותך לבקש לקבל קובץ של הנתונים האישיים שאנו מחזיקים לגביך, כולל כל הנתונים שסיפקת לנו. באפשרותך גם לבקש שנמחק כל מידע אישי שאנו מחזיקים לגביך. הדבר אינו כולל נתונים שאנו מחויבים לשמור למטרות מנהליות, משפטיות או ביטחוניות.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>להיכן אנו שולחים את המידע שלך</h2><!-- /wp:heading --><!-- wp:paragraph --><p>תגובות מבקרים עלולות להיבדק על ידי שירות אוטומטי למניעת תגובות זבל.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>פרטי ההתקשרות שלך</h2><!-- /wp:heading --><!-- wp:heading --><h2>מידע נוסף</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>כיצד אנו מגינים על המידע שלך</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>אילו הליכי פרצת נתונים יש לנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>מאילו גופי צד-שלישי אנו מקבלים מידע</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>קבלת החלטות אוטומטיות ו/או יצירת פרופילים שאנו עושים עם נתוני המשתמשים שלנו</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>דרישות גילוי רגולטוריות של התעשייה </h3><!-- /wp:heading -->', 'מדיניות פרטיות', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-06-08 23:40:24', '2021-06-08 20:40:24', '', 0, 'http://flowers.nagar:8888/?page_id=3', 0, 'page', '', 0),
(6, 1, '2021-06-08 23:48:27', '2021-06-08 20:48:27', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2021-06-08 23:48:27', '2021-06-08 20:48:27', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(7, 1, '2021-06-08 23:48:28', '2021-06-08 20:48:28', '', 'מוצרים', '', 'publish', 'closed', 'closed', '', 'shop', '', '', '2021-06-15 15:07:02', '2021-06-15 12:07:02', '', 0, 'http://flowers.nagar:8888/shop/', 0, 'page', '', 0),
(8, 1, '2021-06-08 23:48:28', '2021-06-08 20:48:28', '<!-- wp:shortcode -->[woocommerce_cart]<!-- /wp:shortcode -->', 'Cart', '', 'publish', 'closed', 'closed', '', 'cart', '', '', '2021-06-08 23:48:28', '2021-06-08 20:48:28', '', 0, 'http://flowers.nagar:8888/cart/', 0, 'page', '', 0),
(9, 1, '2021-06-08 23:48:28', '2021-06-08 20:48:28', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Checkout', '', 'publish', 'closed', 'closed', '', 'checkout', '', '', '2021-06-08 23:48:28', '2021-06-08 20:48:28', '', 0, 'http://flowers.nagar:8888/checkout/', 0, 'page', '', 0),
(10, 1, '2021-06-08 23:48:28', '2021-06-08 20:48:28', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'My account', '', 'publish', 'closed', 'closed', '', 'my-account', '', '', '2021-06-08 23:48:28', '2021-06-08 20:48:28', '', 0, 'http://flowers.nagar:8888/my-account/', 0, 'page', '', 0),
(11, 1, '2021-06-08 23:51:25', '0000-00-00 00:00:00', '', 'AUTO-DRAFT', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-06-08 23:51:25', '0000-00-00 00:00:00', '', 0, 'http://flowers.nagar:8888/?post_type=product&p=11', 0, 'product', '', 0),
(12, 1, '2021-06-08 23:51:46', '2021-06-08 20:51:46', '', 'מוצר לטסט!', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%98%d7%a1%d7%98', '', '', '2021-06-15 21:27:19', '2021-06-15 18:27:19', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=12', 0, 'product', '', 0),
(13, 1, '2021-06-08 23:58:08', '2021-06-08 20:58:08', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:4:\"side\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'גלרית תמונות', '%d7%92%d7%9c%d7%a8%d7%99%d7%aa-%d7%aa%d7%9e%d7%95%d7%a0%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60bfd92532d6b', '', '', '2021-06-09 12:43:27', '2021-06-09 09:43:27', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=13', 0, 'acf-field-group', '', 0),
(14, 1, '2021-06-08 23:58:08', '2021-06-08 20:58:08', 'a:18:{s:4:\"type\";s:7:\"gallery\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:6:\"insert\";s:6:\"append\";s:7:\"library\";s:3:\"all\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'גלרית תמונות', 'post_gallery', 'publish', 'closed', 'closed', '', 'field_60bfd9909cfa3', '', '', '2021-06-09 12:43:27', '2021-06-09 09:43:27', '', 13, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=14', 0, 'acf-field', '', 0),
(15, 1, '2021-06-09 00:06:16', '2021-06-08 21:06:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"post\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פוסט', '%d7%a4%d7%95%d7%a1%d7%98', 'publish', 'closed', 'closed', '', 'group_60bfd9e7b586b', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=15', 0, 'acf-field-group', '', 0),
(16, 1, '2021-06-09 00:06:16', '2021-06-08 21:06:16', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'המאמרים נוספים', 'המאמרים_נוספים', 'publish', 'closed', 'closed', '', 'field_60bfd9f32a514', '', '', '2021-06-09 00:06:16', '2021-06-08 21:06:16', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&p=16', 0, 'acf-field', '', 0),
(17, 1, '2021-06-09 00:06:16', '2021-06-08 21:06:16', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'same_text', 'publish', 'closed', 'closed', '', 'field_60bfdb8d2a515', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=17', 4, 'acf-field', '', 0),
(18, 1, '2021-06-09 00:06:16', '2021-06-08 21:06:16', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'המאמרים נוספים', 'same_posts', 'publish', 'closed', 'closed', '', 'field_60bfdb9f2a516', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=18', 5, 'acf-field', '', 0),
(19, 1, '2021-06-09 00:10:29', '2021-06-08 21:10:29', '', 'test-post-2', '', 'inherit', 'open', 'closed', '', 'test-post-2', '', '', '2021-06-09 00:10:29', '2021-06-08 21:10:29', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-2.png', 0, 'attachment', 'image/png', 0),
(20, 1, '2021-06-09 00:10:30', '2021-06-08 21:10:30', '', 'test-post-3', '', 'inherit', 'open', 'closed', '', 'test-post-3', '', '', '2021-06-09 00:10:30', '2021-06-08 21:10:30', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-3.png', 0, 'attachment', 'image/png', 0),
(21, 1, '2021-06-09 00:10:31', '2021-06-08 21:10:31', '', 'test-post-4', '', 'inherit', 'open', 'closed', '', 'test-post-4', '', '', '2021-06-09 00:10:31', '2021-06-08 21:10:31', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-4.png', 0, 'attachment', 'image/png', 0),
(22, 1, '2021-06-09 00:10:32', '2021-06-08 21:10:32', '', 'test-post-5', '', 'inherit', 'open', 'closed', '', 'test-post-5', '', '', '2021-06-09 00:10:32', '2021-06-08 21:10:32', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-5.png', 0, 'attachment', 'image/png', 0),
(23, 1, '2021-06-09 00:10:33', '2021-06-08 21:10:33', '', 'test-post-6', '', 'inherit', 'open', 'closed', '', 'test-post-6', '', '', '2021-06-09 00:10:33', '2021-06-08 21:10:33', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-6.png', 0, 'attachment', 'image/png', 0),
(24, 1, '2021-06-09 00:10:49', '2021-06-08 21:10:49', '', 'test-post-1', '', 'inherit', 'open', 'closed', '', 'test-post-1', '', '', '2021-06-09 00:10:49', '2021-06-08 21:10:49', '', 1, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/test-post-1.png', 0, 'attachment', 'image/png', 0),
(25, 1, '2021-06-10 01:47:18', '2021-06-09 22:47:18', '<!-- wp:paragraph -->\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\n<ul>\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\n</ul>\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\n<p style=\"text-align: left;\">&lt;iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5qap5aO4i9A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;</p>\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-autosave-v1', '', '', '2021-06-10 01:47:18', '2021-06-09 22:47:18', '', 1, 'http://flowers.nagar:8888/?p=25', 0, 'revision', '', 0),
(26, 1, '2021-06-09 00:13:37', '2021-06-08 21:13:37', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>&lt;iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5qap5aO4i9A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-09 00:13:37', '2021-06-08 21:13:37', '', 1, 'http://flowers.nagar:8888/?p=26', 0, 'revision', '', 0),
(27, 1, '2021-06-15 15:09:09', '2021-06-08 21:17:05', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=27', 2, 'nav_menu_item', '', 0),
(28, 1, '2021-06-09 00:17:29', '2021-06-08 21:17:29', '', 'דף הבית', '', 'publish', 'closed', 'closed', '', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', '', '', '2021-06-14 15:32:05', '2021-06-14 12:32:05', '', 0, 'http://flowers.nagar:8888/?page_id=28', 0, 'page', '', 0),
(29, 1, '2021-06-09 00:17:29', '2021-06-08 21:17:29', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2021-06-09 00:17:29', '2021-06-08 21:17:29', '', 28, 'http://flowers.nagar:8888/?p=29', 0, 'revision', '', 0),
(30, 1, '2021-06-09 12:05:53', '2021-06-09 09:05:53', '', 'product-1', '', 'inherit', 'open', 'closed', '', 'product-1', '', '', '2021-06-09 12:05:53', '2021-06-09 09:05:53', '', 12, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-1.png', 0, 'attachment', 'image/png', 0),
(31, 1, '2021-06-09 12:14:00', '2021-06-09 09:14:00', '', 'מוצר לטסט! - גדול', 'מידה: גדול', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%98%d7%a1%d7%98', '', '', '2021-06-09 12:16:22', '2021-06-09 09:16:22', '', 12, 'http://flowers.nagar:8888/?post_type=product_variation&#038;p=31', 3, 'product_variation', '', 0),
(32, 1, '2021-06-09 12:14:46', '2021-06-09 09:14:46', '', 'מוצר לטסט! - רגיל', 'מידה: רגיל', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%98%d7%a1%d7%98-2', '', '', '2021-06-09 12:16:22', '2021-06-09 09:16:22', '', 12, 'http://flowers.nagar:8888/?post_type=product_variation&#038;p=32', 1, 'product_variation', '', 0),
(33, 1, '2021-06-09 12:15:34', '2021-06-09 09:15:34', '', 'מוצר לטסט! - ענק', 'מידה: ענק', 'publish', 'closed', 'closed', '', '%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%98%d7%a1%d7%98-3', '', '', '2021-06-09 12:16:22', '2021-06-09 09:16:22', '', 12, 'http://flowers.nagar:8888/?post_type=product_variation&#038;p=33', 0, 'product_variation', '', 0),
(34, 1, '2021-06-09 12:42:40', '2021-06-09 09:42:40', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>&lt;iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5qap5aO4i9A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-09 12:42:40', '2021-06-09 09:42:40', '', 1, 'http://flowers.nagar:8888/?p=34', 0, 'revision', '', 0),
(35, 1, '2021-06-09 12:43:01', '2021-06-09 09:43:01', 'a:7:{s:8:\"location\";a:3:{i:0;a:4:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:4:\"page\";}i:1;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:17:\"views/contact.php\";}i:2;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:13:\"views/faq.php\";}i:3;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:20:\"views/categories.php\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:1:\"7\";}}i:2;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"!=\";s:5:\"value\";s:11:\"product_cat\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות תוכן', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa-%d7%aa%d7%95%d7%9b%d7%9f', 'publish', 'closed', 'closed', '', 'group_5ddbde4d59412', '', '', '2021-06-15 15:04:29', '2021-06-15 12:04:29', '', 0, 'http://flowers.nagar:8888/?p=35', 0, 'acf-field-group', '', 0),
(37, 1, '2021-06-09 12:43:01', '2021-06-09 09:43:01', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'seo_content', 'publish', 'closed', 'closed', '', 'field_5ddbde7399116', '', '', '2021-06-09 13:18:02', '2021-06-09 10:18:02', '', 35, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=37', 0, 'acf-field', '', 0),
(41, 1, '2021-06-09 12:44:37', '2021-06-09 09:44:37', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'seo_link', 'publish', 'closed', 'closed', '', 'field_60c08d70f9afb', '', '', '2021-06-09 13:18:02', '2021-06-09 10:18:02', '', 35, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=41', 1, 'acf-field', '', 0),
(42, 1, '2021-06-09 12:44:37', '2021-06-09 09:44:37', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'seo_img', 'publish', 'closed', 'closed', '', 'field_60c08d4ff9afa', '', '', '2021-06-09 13:18:02', '2021-06-09 10:18:02', '', 35, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=42', 2, 'acf-field', '', 0),
(43, 1, '2021-06-09 12:55:15', '2021-06-09 09:55:15', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 1', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-06-09 13:19:54', '2021-06-09 10:19:54', '', 0, 'http://flowers.nagar:8888/?p=43', 0, 'post', '', 0),
(44, 1, '2021-06-09 12:54:55', '2021-06-09 09:54:55', '', 'post-1', '', 'inherit', 'open', 'closed', '', 'post-1', '', '', '2021-06-09 12:54:55', '2021-06-09 09:54:55', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-1.png', 0, 'attachment', 'image/png', 0),
(45, 1, '2021-06-09 12:54:57', '2021-06-09 09:54:57', '', 'post-2', '', 'inherit', 'open', 'closed', '', 'post-2', '', '', '2021-06-09 12:54:57', '2021-06-09 09:54:57', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-2.png', 0, 'attachment', 'image/png', 0),
(46, 1, '2021-06-09 12:54:58', '2021-06-09 09:54:58', '', 'post-3', '', 'inherit', 'open', 'closed', '', 'post-3', '', '', '2021-06-09 12:54:58', '2021-06-09 09:54:58', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-3.png', 0, 'attachment', 'image/png', 0),
(47, 1, '2021-06-09 12:55:00', '2021-06-09 09:55:00', '', 'post-4', '', 'inherit', 'open', 'closed', '', 'post-4', '', '', '2021-06-09 12:55:00', '2021-06-09 09:55:00', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-4.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2021-06-09 12:55:02', '2021-06-09 09:55:02', '', 'post-5', '', 'inherit', 'open', 'closed', '', 'post-5', '', '', '2021-06-09 12:55:02', '2021-06-09 09:55:02', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-5.png', 0, 'attachment', 'image/png', 0),
(49, 1, '2021-06-09 12:55:03', '2021-06-09 09:55:03', '', 'post-6', '', 'inherit', 'open', 'closed', '', 'post-6', '', '', '2021-06-09 12:55:03', '2021-06-09 09:55:03', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-6.png', 0, 'attachment', 'image/png', 0),
(50, 1, '2021-06-09 12:55:05', '2021-06-09 09:55:05', '', 'post-7', '', 'inherit', 'open', 'closed', '', 'post-7', '', '', '2021-06-09 12:55:05', '2021-06-09 09:55:05', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-7.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2021-06-09 12:55:06', '2021-06-09 09:55:06', '', 'post-8', '', 'inherit', 'open', 'closed', '', 'post-8', '', '', '2021-06-09 12:55:06', '2021-06-09 09:55:06', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-8.png', 0, 'attachment', 'image/png', 0),
(52, 1, '2021-06-09 12:55:08', '2021-06-09 09:55:08', '', 'post-9', '', 'inherit', 'open', 'closed', '', 'post-9', '', '', '2021-06-09 12:55:08', '2021-06-09 09:55:08', '', 43, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/post-9.png', 0, 'attachment', 'image/png', 0),
(53, 1, '2021-06-09 12:55:15', '2021-06-09 09:55:15', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 1', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2021-06-09 12:55:15', '2021-06-09 09:55:15', '', 43, 'http://flowers.nagar:8888/?p=53', 0, 'revision', '', 0),
(54, 1, '2021-06-09 12:55:39', '2021-06-09 09:55:39', 'קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק', 'שם המאמר לורם היפסום 2', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-06-09 13:20:16', '2021-06-09 10:20:16', '', 0, 'http://flowers.nagar:8888/?p=54', 0, 'post', '', 0),
(55, 1, '2021-06-09 12:55:39', '2021-06-09 09:55:39', 'קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק', 'שם המאמר לורם היפסום 2', '', 'inherit', 'closed', 'closed', '', '54-revision-v1', '', '', '2021-06-09 12:55:39', '2021-06-09 09:55:39', '', 54, 'http://flowers.nagar:8888/?p=55', 0, 'revision', '', 0),
(56, 1, '2021-06-09 12:56:03', '2021-06-09 09:56:03', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם היפסום 3', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-06-09 13:20:44', '2021-06-09 10:20:44', '', 0, 'http://flowers.nagar:8888/?p=56', 0, 'post', '', 0),
(57, 1, '2021-06-09 12:56:03', '2021-06-09 09:56:03', 'נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nהועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם היפסום 3', '', 'inherit', 'closed', 'closed', '', '56-revision-v1', '', '', '2021-06-09 12:56:03', '2021-06-09 09:56:03', '', 56, 'http://flowers.nagar:8888/?p=57', 0, 'revision', '', 0),
(58, 1, '2021-06-09 12:56:23', '2021-06-09 09:56:23', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 4', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-06-09 12:56:23', '2021-06-09 09:56:23', '', 0, 'http://flowers.nagar:8888/?p=58', 0, 'post', '', 0),
(59, 1, '2021-06-09 12:56:23', '2021-06-09 09:56:23', 'קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 4', '', 'inherit', 'closed', 'closed', '', '58-revision-v1', '', '', '2021-06-09 12:56:23', '2021-06-09 09:56:23', '', 58, 'http://flowers.nagar:8888/?p=59', 0, 'revision', '', 0),
(60, 1, '2021-06-09 12:56:45', '2021-06-09 09:56:45', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'שם המאמר לורם היפסום 5', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-06-09 13:21:59', '2021-06-09 10:21:59', '', 0, 'http://flowers.nagar:8888/?p=60', 0, 'post', '', 0),
(61, 1, '2021-06-09 12:56:45', '2021-06-09 09:56:45', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nנולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי לורם איפסום דולור סיט אמט, קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'שם המאמר לורם היפסום 5', '', 'inherit', 'closed', 'closed', '', '60-revision-v1', '', '', '2021-06-09 12:56:45', '2021-06-09 09:56:45', '', 60, 'http://flowers.nagar:8888/?p=61', 0, 'revision', '', 0),
(62, 1, '2021-06-09 12:57:07', '2021-06-09 09:57:07', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 6', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-06-09 13:22:22', '2021-06-09 10:22:22', '', 0, 'http://flowers.nagar:8888/?p=62', 0, 'post', '', 0),
(63, 1, '2021-06-09 12:57:07', '2021-06-09 09:57:07', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 6', '', 'inherit', 'closed', 'closed', '', '62-revision-v1', '', '', '2021-06-09 12:57:07', '2021-06-09 09:57:07', '', 62, 'http://flowers.nagar:8888/?p=63', 0, 'revision', '', 0),
(64, 1, '2021-06-09 12:57:28', '2021-06-09 09:57:28', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם היפסום 7', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', '', '', '2021-06-09 13:23:00', '2021-06-09 10:23:00', '', 0, 'http://flowers.nagar:8888/?p=64', 0, 'post', '', 0),
(65, 1, '2021-06-09 12:57:28', '2021-06-09 09:57:28', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף לורם איפסום דולור סיט אמט, מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. הועניב היושבב שערש שמחויט - שלושע ותלברו חשלו שעותלשך וחאית נובש ערששף. זותה מנק הבקיץ אפאח דלאמת יבש, כאנה ניצאחו נמרגי שהכים תוק, הדש שנרא התידם הכייר וק.', 'שם המאמר לורם היפסום 7', '', 'inherit', 'closed', 'closed', '', '64-revision-v1', '', '', '2021-06-09 12:57:28', '2021-06-09 09:57:28', '', 64, 'http://flowers.nagar:8888/?p=65', 0, 'revision', '', 0),
(66, 1, '2021-06-09 12:57:49', '2021-06-09 09:57:49', 'ורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 8', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', '', '', '2021-06-09 13:23:32', '2021-06-09 10:23:32', '', 0, 'http://flowers.nagar:8888/?p=66', 0, 'post', '', 0),
(67, 1, '2021-06-09 12:57:49', '2021-06-09 09:57:49', 'ורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 8', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2021-06-09 12:57:49', '2021-06-09 09:57:49', '', 66, 'http://flowers.nagar:8888/?p=67', 0, 'revision', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(68, 1, '2021-06-09 12:58:10', '2021-06-09 09:58:10', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 9', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-9', '', '', '2021-06-09 13:23:55', '2021-06-09 10:23:55', '', 0, 'http://flowers.nagar:8888/?p=68', 0, 'post', '', 0),
(69, 1, '2021-06-09 12:58:10', '2021-06-09 09:58:10', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nסחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המאמר לורם היפסום 9', '', 'inherit', 'closed', 'closed', '', '68-revision-v1', '', '', '2021-06-09 12:58:10', '2021-06-09 09:58:10', '', 68, 'http://flowers.nagar:8888/?p=69', 0, 'revision', '', 0),
(70, 1, '2021-06-09 12:58:31', '2021-06-09 09:58:31', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 10', '', 'publish', 'open', 'open', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%90%d7%9e%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10', '', '', '2021-06-09 13:59:56', '2021-06-09 10:59:56', '', 0, 'http://flowers.nagar:8888/?p=70', 0, 'post', '', 0),
(71, 1, '2021-06-09 12:58:31', '2021-06-09 09:58:31', 'סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המאמר לורם היפסום 10', '', 'inherit', 'closed', 'closed', '', '70-revision-v1', '', '', '2021-06-09 12:58:31', '2021-06-09 09:58:31', '', 70, 'http://flowers.nagar:8888/?p=71', 0, 'revision', '', 0),
(72, 1, '2021-06-09 13:14:01', '2021-06-09 10:14:01', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'מאמרים', '', 'publish', 'closed', 'closed', '', '%d7%9e%d7%90%d7%9e%d7%a8%d7%99%d7%9d', '', '', '2021-06-09 13:19:13', '2021-06-09 10:19:13', '', 0, 'http://flowers.nagar:8888/?page_id=72', 0, 'page', '', 0),
(73, 1, '2021-06-09 13:14:01', '2021-06-09 10:14:01', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2021-06-09 13:14:01', '2021-06-09 10:14:01', '', 72, 'http://flowers.nagar:8888/?p=73', 0, 'revision', '', 0),
(74, 1, '2021-06-09 13:19:13', '2021-06-09 10:19:13', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'מאמרים', '', 'inherit', 'closed', 'closed', '', '72-revision-v1', '', '', '2021-06-09 13:19:13', '2021-06-09 10:19:13', '', 72, 'http://flowers.nagar:8888/?p=74', 0, 'revision', '', 0),
(75, 1, '2021-06-15 15:09:09', '2021-06-09 10:24:28', ' ', '', '', 'publish', 'closed', 'closed', '', '75', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=75', 3, 'nav_menu_item', '', 0),
(76, 1, '2021-06-15 15:09:09', '2021-06-09 10:24:28', ' ', '', '', 'publish', 'closed', 'closed', '', '76', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=76', 4, 'nav_menu_item', '', 0),
(77, 1, '2021-06-09 14:19:29', '2021-06-09 11:19:29', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'תוספות', '%d7%aa%d7%95%d7%a1%d7%a4%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60c09fae0bd17', '', '', '2021-06-14 15:54:47', '2021-06-14 12:54:47', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=77', 0, 'acf-field-group', '', 0),
(78, 1, '2021-06-09 14:19:29', '2021-06-09 11:19:29', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'באנר', 'באנר', 'publish', 'closed', 'closed', '', 'field_60c0a380df0f9', '', '', '2021-06-09 14:19:29', '2021-06-09 11:19:29', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=78', 0, 'acf-field', '', 0),
(79, 1, '2021-06-09 14:19:29', '2021-06-09 11:19:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'banner_title', 'publish', 'closed', 'closed', '', 'field_60c0a398df0fa', '', '', '2021-06-09 14:19:29', '2021-06-09 11:19:29', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=79', 1, 'acf-field', '', 0),
(80, 1, '2021-06-09 14:19:29', '2021-06-09 11:19:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'banner_subtitle', 'publish', 'closed', 'closed', '', 'field_60c0a39bdf0fb', '', '', '2021-06-09 14:19:29', '2021-06-09 11:19:29', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=80', 2, 'acf-field', '', 0),
(81, 1, '2021-06-09 14:19:29', '2021-06-09 11:19:29', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'banner_link', 'publish', 'closed', 'closed', '', 'field_60c0a3a1df0fc', '', '', '2021-06-09 14:19:29', '2021-06-09 11:19:29', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=81', 3, 'acf-field', '', 0),
(82, 1, '2021-06-09 14:19:46', '2021-06-09 11:19:46', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2021-06-09 14:19:46', '2021-06-09 11:19:46', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/logo.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2021-06-09 14:23:22', '2021-06-09 11:23:22', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:9:\"2048x2048\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'banner_img', 'publish', 'closed', 'closed', '', 'field_60c0a48f64354', '', '', '2021-06-09 14:23:22', '2021-06-09 11:23:22', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=83', 4, 'acf-field', '', 0),
(84, 1, '2021-06-09 14:23:48', '2021-06-09 11:23:48', '', 'middle-im', '', 'inherit', 'open', 'closed', '', 'middle-im', '', '', '2021-06-09 14:23:48', '2021-06-09 11:23:48', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/middle-im.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2021-06-09 16:21:58', '2021-06-09 13:21:58', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-12\">\r\n		[text* text-399 placeholder \"שם:\"]\r\n  </div>\r\n    <div class=\" col-12\">\r\n			[tel* tel-197 placeholder \"טלפון:\"]\r\n  </div>\r\n	    <div class=\"col-12\">\r\n				[textarea textarea-978 placeholder \"תגובה:\"]\r\n  </div>\r\n    <div class=\"col-12\">\r\n			[submit \"שלח\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'טופס בפוטר', '', 'publish', 'closed', 'closed', '', '%d7%98%d7%95%d7%a4%d7%a1-%d7%91%d7%a4%d7%95%d7%98%d7%a8', '', '', '2021-06-09 16:22:29', '2021-06-09 13:22:29', '', 0, 'http://flowers.nagar:8888/?post_type=wpcf7_contact_form&#038;p=85', 0, 'wpcf7_contact_form', '', 0),
(86, 1, '2021-06-09 16:24:35', '2021-06-09 13:24:35', '<div class=\"form-row d-flex justify-content-center align-items-center\">\r\n  <div class=\"col-md-6 col-12\">\r\n		[text* text-625 placeholder \"שם:\"]\r\n  </div>\r\n    <div class=\"col-md-6 col-12\">\r\n			[tel* tel-732 placeholder \"טלפון:\"]\r\n  </div>\r\n	    <div class=\"col-12\">\r\n				[email* email-347 placeholder \"מייל:\"]\r\n  </div>\r\n	    <div class=\"col-12\">\r\n				[textarea textarea-192 placeholder \"סיבת פניה:\"]\r\n	</div>\r\n    <div class=\"col-auto\">\r\n			[submit \"תחזרו אלי בהקדם\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'פופאפ', '', 'publish', 'closed', 'closed', '', '%d7%a4%d7%95%d7%a4%d7%90%d7%a4', '', '', '2021-06-14 16:10:51', '2021-06-14 13:10:51', '', 0, 'http://flowers.nagar:8888/?post_type=wpcf7_contact_form&#038;p=86', 0, 'wpcf7_contact_form', '', 0),
(87, 1, '2021-06-09 16:26:35', '2021-06-09 13:26:35', '<div class=\"form-row d-flex justify-content-center align-items-stretch\">\r\n  <div class=\"col-sm col-12\">\r\n		[email* email-819 placeholder \"המייל שלכם:\"]\r\n  </div>\r\n    <div class=\"col-sm-auto col-12\">\r\n			[submit \"הרשמה\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'טופס עם פרחים', '', 'publish', 'closed', 'closed', '', '%d7%98%d7%95%d7%a4%d7%a1-%d7%a2%d7%9d-%d7%a4%d7%a8%d7%97%d7%99%d7%9d', '', '', '2021-06-09 22:49:50', '2021-06-09 19:49:50', '', 0, 'http://flowers.nagar:8888/?post_type=wpcf7_contact_form&#038;p=87', 0, 'wpcf7_contact_form', '', 0),
(88, 1, '2021-06-09 17:16:54', '2021-06-09 14:16:54', ' ', '', '', 'publish', 'closed', 'closed', '', '88', '', '', '2021-06-09 17:16:54', '2021-06-09 14:16:54', '', 0, 'http://flowers.nagar:8888/?p=88', 1, 'nav_menu_item', '', 0),
(89, 1, '2021-06-09 17:17:26', '2021-06-09 14:17:26', ' ', '', '', 'publish', 'closed', 'closed', '', '89', '', '', '2021-06-09 17:17:26', '2021-06-09 14:17:26', '', 0, 'http://flowers.nagar:8888/?p=89', 1, 'nav_menu_item', '', 0),
(90, 1, '2021-06-09 17:17:26', '2021-06-09 14:17:26', ' ', '', '', 'publish', 'closed', 'closed', '', '90', '', '', '2021-06-09 17:17:26', '2021-06-09 14:17:26', '', 0, 'http://flowers.nagar:8888/?p=90', 2, 'nav_menu_item', '', 0),
(91, 1, '2021-06-09 17:17:27', '2021-06-09 14:17:27', ' ', '', '', 'publish', 'closed', 'closed', '', '91', '', '', '2021-06-09 17:17:27', '2021-06-09 14:17:27', '', 0, 'http://flowers.nagar:8888/?p=91', 3, 'nav_menu_item', '', 0),
(92, 1, '2021-06-09 17:17:27', '2021-06-09 14:17:27', ' ', '', '', 'publish', 'closed', 'closed', '', '92', '', '', '2021-06-09 17:17:27', '2021-06-09 14:17:27', '', 0, 'http://flowers.nagar:8888/?p=92', 4, 'nav_menu_item', '', 0),
(93, 1, '2021-06-09 17:17:27', '2021-06-09 14:17:27', ' ', '', '', 'publish', 'closed', 'closed', '', '93', '', '', '2021-06-09 17:17:27', '2021-06-09 14:17:27', '', 0, 'http://flowers.nagar:8888/?p=93', 5, 'nav_menu_item', '', 0),
(94, 1, '2021-06-09 17:18:04', '2021-06-09 14:18:04', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:22:\"theme-general-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'פרטי קשר', '%d7%a4%d7%a8%d7%98%d7%99-%d7%a7%d7%a9%d7%a8', 'publish', 'closed', 'closed', '', 'group_5dd3a32b38e68', '', '', '2021-06-09 17:18:30', '2021-06-09 14:18:30', '', 0, 'http://flowers.nagar:8888/?p=94', 0, 'acf-field-group', '', 0),
(95, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'טלפון', 'tel', 'publish', 'closed', 'closed', '', 'field_5dd3a33f42e0c', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=95', 0, 'acf-field', '', 0),
(96, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'מייל', 'mail', 'publish', 'closed', 'closed', '', 'field_5dd3a35642e0d', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=96', 1, 'acf-field', '', 0),
(97, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"33\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'פקס', 'fax', 'publish', 'closed', 'closed', '', 'field_5dd3a36942e0e', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=97', 2, 'acf-field', '', 0),
(98, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כתובת', 'address', 'publish', 'closed', 'closed', '', 'field_5dd3a4d142e0f', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=98', 3, 'acf-field', '', 0),
(99, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'מפה (תמונה)', 'map_image', 'publish', 'closed', 'closed', '', 'field_5ddbd67734310', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=99', 4, 'acf-field', '', 0),
(100, 1, '2021-06-09 17:18:03', '2021-06-09 14:18:03', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'פייסבוק', 'facebook', 'publish', 'closed', 'closed', '', 'field_5ddbd6b3cc0cd', '', '', '2021-06-09 17:18:03', '2021-06-09 14:18:03', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=100', 5, 'acf-field', '', 0),
(102, 1, '2021-06-09 17:18:30', '2021-06-09 14:18:30', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"50\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Instagram', 'instagram', 'publish', 'closed', 'closed', '', 'field_60c0cda68ec80', '', '', '2021-06-09 17:18:30', '2021-06-09 14:18:30', '', 94, 'http://flowers.nagar:8888/?post_type=acf-field&p=102', 6, 'acf-field', '', 0),
(103, 1, '2021-06-09 17:19:59', '2021-06-09 14:19:59', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'טופס עם פרחים', 'טופס_עם_פרחים', 'publish', 'closed', 'closed', '', 'field_60c0cddab29ce', '', '', '2021-06-09 17:19:59', '2021-06-09 14:19:59', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=103', 5, 'acf-field', '', 0),
(104, 1, '2021-06-09 17:19:59', '2021-06-09 14:19:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'foo_form_title', 'publish', 'closed', 'closed', '', 'field_60c0cdeab29cf', '', '', '2021-06-09 17:19:59', '2021-06-09 14:19:59', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=104', 6, 'acf-field', '', 0),
(105, 1, '2021-06-09 17:19:59', '2021-06-09 14:19:59', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'foo_form_subtitle', 'publish', 'closed', 'closed', '', 'field_60c0cdefb29d0', '', '', '2021-06-09 17:19:59', '2021-06-09 14:19:59', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=105', 7, 'acf-field', '', 0),
(106, 1, '2021-06-09 17:19:59', '2021-06-09 14:19:59', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'foo_form_img', 'publish', 'closed', 'closed', '', 'field_60c0cdf3b29d1', '', '', '2021-06-09 17:19:59', '2021-06-09 14:19:59', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=106', 8, 'acf-field', '', 0),
(107, 1, '2021-06-09 17:20:15', '2021-06-09 14:20:15', '', 'flowers-form', '', 'inherit', 'open', 'closed', '', 'flowers-form', '', '', '2021-06-09 17:20:15', '2021-06-09 14:20:15', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/flowers-form.png', 0, 'attachment', 'image/png', 0),
(108, 1, '2021-06-10 12:26:46', '2021-06-09 19:04:44', ' ', '', '', 'publish', 'closed', 'closed', '', '108', '', '', '2021-06-10 12:26:46', '2021-06-10 09:26:46', '', 0, 'http://flowers.nagar:8888/?p=108', 1, 'nav_menu_item', '', 0),
(109, 1, '2021-06-10 12:26:46', '2021-06-09 19:04:44', ' ', '', '', 'publish', 'closed', 'closed', '', '109', '', '', '2021-06-10 12:26:46', '2021-06-10 09:26:46', '', 0, 'http://flowers.nagar:8888/?p=109', 2, 'nav_menu_item', '', 0),
(110, 1, '2021-06-09 23:09:58', '2021-06-09 20:09:58', '<div class=\"form-row d-flex justify-content-end align-items-stretch\">\r\n  <div class=\"col-md-6 col-12\">\r\n		[text* text-477 placeholder \"שם:\"]\r\n  </div>\r\n    <div class=\"col-md-6 col-12\">\r\n			[tel* tel-300 placeholder \"טלפון:\"]\r\n  </div>\r\n	    <div class=\"col-12\">\r\n				[email* email-239 \"מייל:\"]\r\n  </div>\r\n	    <div class=\"col-12\">\r\n				[textarea textarea-757 placeholder \"סיבת פניה:\"]\r\n	</div>\r\n    <div class=\"col-lg-6 col-12\">\r\n			[submit \"תחזרו אלי בהקדם\"]\r\n	</div>\r\n</div>\n1\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[_site_admin_email]\nמאת: [your-name] <[your-email]>\r\nנושא: [your-subject]\r\n\r\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [your-email]\n\n\n\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@flowers.nagar>\n[your-email]\nתוכן ההודעה:\r\n[your-message]\r\n\r\n-- \r\nאימייל זה נשלח מטופס יצירת קשר ב [_site_title] ([_site_url])\nReply-To: [_site_admin_email]\n\n\n\nההודעה נשלחה.\nארעה שגיאה בשליחת ההודעה.\nקיימת שגיאה בשדה אחד או יותר. נא לבדוק ולנסות שוב.\nארעה שגיאה בשליחת ההודעה.\nעליך להסכים לתנאים לפני שליחת ההודעה.\nזהו שדה חובה.\nהשדה ארוך מדי.\nהשדה קצר מדי.\nשגיאה לא ידועה בהעלאת הקובץ.\nאין לך הרשאות להעלות קבצים בפורמט זה.\nהקובץ גדול מדי.\nשגיאה בהעלאת הקובץ.\nשדה התאריך אינו נכון.\nהתאריך מוקדם מהתאריך המותר.\nהתאריך מאוחר מהתאריך המותר.\nפורמט המספר אינו תקין.\nהמספר קטן מהמינימום המותר.\nהמספר גדול מהמקסימום המותר.\nהתשובה לשאלת הביטחון אינה נכונה.\nכתובת האימייל שהוזנה אינה תקינה.\nהקישור אינו תקין.\nמספר הטלפון אינו תקין.', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-06-09 23:24:36', '2021-06-09 20:24:36', '', 0, 'http://flowers.nagar:8888/?post_type=wpcf7_contact_form&#038;p=110', 0, 'wpcf7_contact_form', '', 0),
(111, 1, '2021-06-09 23:10:46', '2021-06-09 20:10:46', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'צור קשר', '', 'publish', 'closed', 'closed', '', '%d7%a6%d7%95%d7%a8-%d7%a7%d7%a9%d7%a8', '', '', '2021-06-10 00:01:35', '2021-06-09 21:01:35', '', 0, 'http://flowers.nagar:8888/?page_id=111', 0, 'page', '', 0),
(112, 1, '2021-06-09 23:10:46', '2021-06-09 20:10:46', '', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '111-revision-v1', '', '', '2021-06-09 23:10:46', '2021-06-09 20:10:46', '', 111, 'http://flowers.nagar:8888/?p=112', 0, 'revision', '', 0),
(113, 1, '2021-06-15 15:09:09', '2021-06-09 20:11:23', ' ', '', '', 'publish', 'closed', 'closed', '', '113', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=113', 8, 'nav_menu_item', '', 0),
(114, 1, '2021-06-10 00:01:35', '2021-06-09 21:01:35', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'צור קשר', '', 'inherit', 'closed', 'closed', '', '111-revision-v1', '', '', '2021-06-10 00:01:35', '2021-06-09 21:01:35', '', 111, 'http://flowers.nagar:8888/?p=114', 0, 'revision', '', 0),
(115, 1, '2021-06-10 00:51:06', '2021-06-09 21:51:06', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק.\r\n\r\nבראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק', 'אודות', '', 'publish', 'closed', 'closed', '', '%d7%90%d7%95%d7%93%d7%95%d7%aa', '', '', '2021-06-10 01:22:36', '2021-06-09 22:22:36', '', 0, 'http://flowers.nagar:8888/?page_id=115', 0, 'page', '', 0),
(116, 1, '2021-06-10 00:51:06', '2021-06-09 21:51:06', '', 'אודות', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2021-06-10 00:51:06', '2021-06-09 21:51:06', '', 115, 'http://flowers.nagar:8888/?p=116', 0, 'revision', '', 0),
(117, 1, '2021-06-10 00:52:35', '2021-06-09 21:52:35', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"views/about.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'אודות', '%d7%90%d7%95%d7%93%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60c137d121381', '', '', '2021-06-10 00:56:23', '2021-06-09 21:56:23', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=117', 0, 'acf-field-group', '', 0),
(118, 1, '2021-06-10 00:52:35', '2021-06-09 21:52:35', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוקים עם תוכן', 'בלוקים_עם_תוכן', 'publish', 'closed', 'closed', '', 'field_60c137d25b6c9', '', '', '2021-06-10 00:52:35', '2021-06-09 21:52:35', '', 117, 'http://flowers.nagar:8888/?post_type=acf-field&p=118', 0, 'acf-field', '', 0),
(119, 1, '2021-06-10 00:52:35', '2021-06-09 21:52:35', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'בלוק עם תוכן', 'about_content_block', 'publish', 'closed', 'closed', '', 'field_60c137e45b6ca', '', '', '2021-06-10 00:52:35', '2021-06-09 21:52:35', '', 117, 'http://flowers.nagar:8888/?post_type=acf-field&p=119', 1, 'acf-field', '', 0),
(120, 1, '2021-06-10 00:52:35', '2021-06-09 21:52:35', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'about_content', 'publish', 'closed', 'closed', '', 'field_60c137f65b6cb', '', '', '2021-06-10 00:52:35', '2021-06-09 21:52:35', '', 119, 'http://flowers.nagar:8888/?post_type=acf-field&p=120', 0, 'acf-field', '', 0),
(121, 1, '2021-06-10 00:52:35', '2021-06-09 21:52:35', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'about_img', 'publish', 'closed', 'closed', '', 'field_60c1380f5b6cc', '', '', '2021-06-10 00:52:35', '2021-06-09 21:52:35', '', 119, 'http://flowers.nagar:8888/?post_type=acf-field&p=121', 1, 'acf-field', '', 0),
(122, 1, '2021-06-10 00:53:51', '2021-06-09 21:53:51', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם וידאו', 'בלוק_עם_וידאו', 'publish', 'closed', 'closed', '', 'field_60c1382e2f168', '', '', '2021-06-10 00:53:51', '2021-06-09 21:53:51', '', 117, 'http://flowers.nagar:8888/?post_type=acf-field&p=122', 2, 'acf-field', '', 0),
(123, 1, '2021-06-10 00:53:51', '2021-06-09 21:53:51', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'about_video_text', 'publish', 'closed', 'closed', '', 'field_60c1383d2f169', '', '', '2021-06-10 00:53:51', '2021-06-09 21:53:51', '', 117, 'http://flowers.nagar:8888/?post_type=acf-field&p=123', 3, 'acf-field', '', 0),
(124, 1, '2021-06-10 00:53:51', '2021-06-09 21:53:51', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור לוידאו', 'about_video_link', 'publish', 'closed', 'closed', '', 'field_60c1384d2f16a', '', '', '2021-06-10 00:53:51', '2021-06-09 21:53:51', '', 117, 'http://flowers.nagar:8888/?post_type=acf-field&p=124', 4, 'acf-field', '', 0),
(125, 1, '2021-06-10 00:56:09', '2021-06-09 21:56:09', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק.\r\n\r\nבראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק', 'אודות', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2021-06-10 00:56:09', '2021-06-09 21:56:09', '', 115, 'http://flowers.nagar:8888/?p=125', 0, 'revision', '', 0),
(126, 1, '2021-06-10 00:56:42', '2021-06-09 21:56:42', '', 'about-img-2', '', 'inherit', 'open', 'closed', '', 'about-img-2', '', '', '2021-06-10 00:56:42', '2021-06-09 21:56:42', '', 115, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/about-img-2.png', 0, 'attachment', 'image/png', 0),
(127, 1, '2021-06-10 00:56:55', '2021-06-09 21:56:55', '', 'about-img', '', 'inherit', 'open', 'closed', '', 'about-img', '', '', '2021-06-10 00:56:55', '2021-06-09 21:56:55', '', 115, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/about-img.png', 0, 'attachment', 'image/png', 0),
(128, 1, '2021-06-10 00:58:24', '2021-06-09 21:58:24', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק.\r\n\r\nבראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק', 'אודות', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2021-06-10 00:58:24', '2021-06-09 21:58:24', '', 115, 'http://flowers.nagar:8888/?p=128', 0, 'revision', '', 0),
(129, 1, '2021-06-15 15:09:09', '2021-06-09 21:58:33', ' ', '', '', 'publish', 'closed', 'closed', '', '129', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=129', 5, 'nav_menu_item', '', 0),
(130, 1, '2021-06-10 01:22:36', '2021-06-09 22:22:36', '<h2>הסיפור שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף, . תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק.\r\n\r\nבראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק', 'אודות', '', 'inherit', 'closed', 'closed', '', '115-revision-v1', '', '', '2021-06-10 01:22:36', '2021-06-09 22:22:36', '', 115, 'http://flowers.nagar:8888/?p=130', 0, 'revision', '', 0),
(131, 1, '2021-06-10 01:34:03', '2021-06-09 22:34:03', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>&lt;iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5qap5aO4i9A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-10 01:34:03', '2021-06-09 22:34:03', '', 1, 'http://flowers.nagar:8888/?p=131', 0, 'revision', '', 0),
(132, 1, '2021-06-10 01:47:21', '2021-06-09 22:47:21', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p style=\"text-align: left;\">&lt;iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/5qap5aO4i9A\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen&gt;&lt;/iframe&gt;</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-10 01:47:21', '2021-06-09 22:47:21', '', 1, 'http://flowers.nagar:8888/?p=132', 0, 'revision', '', 0),
(133, 1, '2021-06-10 01:47:49', '2021-06-09 22:47:49', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-10 01:47:49', '2021-06-09 22:47:49', '', 1, 'http://flowers.nagar:8888/?p=133', 0, 'revision', '', 0),
(134, 1, '2021-06-10 11:11:30', '2021-06-10 08:11:30', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'תוכן עם וידאו', 'תוכן_עם_וידאו', 'publish', 'closed', 'closed', '', 'field_60c1c8e94ad59', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=134', 3, 'acf-field', '', 0),
(135, 1, '2021-06-10 11:11:30', '2021-06-10 08:11:30', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"40\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'קישור לוידאו', 'post_video_link', 'publish', 'closed', 'closed', '', 'field_60c1c9074ad5a', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=135', 1, 'acf-field', '', 0),
(136, 1, '2021-06-10 11:11:30', '2021-06-10 08:11:30', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תוכן', 'post_video_content', 'publish', 'closed', 'closed', '', 'field_60c1c91c4ad5b', '', '', '2021-06-10 11:13:24', '2021-06-10 08:13:24', '', 15, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=136', 2, 'acf-field', '', 0),
(137, 1, '2021-06-10 11:14:38', '2021-06-10 08:14:38', '<!-- wp:paragraph -->\r\n<p>ברוכים הבאים לוורדפרס. זה הפוסט הראשון באתר. ניתן למחוק או לערוך אותו, ולהתחיל לכתוב.לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מ נת. לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</p>\r\n<p>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג ורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לת סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול,ם ברשג - ולתיע ם גדדיש. קוויז דומור ליאמום בלינך רוג צה. לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק </p>\r\n<ul>\r\n<li>לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית סחטיר בלובק.</li>\r\n<li>תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם</li>\r\n<li>ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט מוסן מנת.</li>\r\n<li>לורם איפסום דולור סיט אמט, ליבם סולגק. בראיט ולחת צורק מונחף.</li>\r\n<li>תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק?</li>\r\n</ul>\r\n<p>לתיג ישבעס. ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בג צה.</p>\r\n<p>לפמעט מוסן מנת. קולהע צופעט למרקוח איב ן איף, ברומץ כלרשט מיחוצים. קלאצי נולום ארווס סאפיאן - פוסיל יס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק</p>\r\n<!-- /wp:paragraph -->', 'שלום עולם! TEST POST', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2021-06-10 11:14:38', '2021-06-10 08:14:38', '', 1, 'http://flowers.nagar:8888/?p=137', 0, 'revision', '', 0),
(138, 1, '2021-06-10 12:11:43', '2021-06-10 09:11:43', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:13:\"views/faq.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'שאלות ותשובות', '%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60c1d74066f00', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=138', 0, 'acf-field-group', '', 0),
(139, 1, '2021-06-10 12:15:14', '2021-06-10 09:15:14', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'בלוק עם שאלות ותשובות', 'faq_block', 'publish', 'closed', 'closed', '', 'field_60c1d7b1a823a', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 138, 'http://flowers.nagar:8888/?post_type=acf-field&p=139', 0, 'acf-field', '', 0),
(140, 1, '2021-06-10 12:15:14', '2021-06-10 09:15:14', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'faq_block_title', 'publish', 'closed', 'closed', '', 'field_60c1d7cda823b', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 139, 'http://flowers.nagar:8888/?post_type=acf-field&p=140', 0, 'acf-field', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(141, 1, '2021-06-10 12:15:14', '2021-06-10 09:15:14', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'פריט', 'faq_item', 'publish', 'closed', 'closed', '', 'field_60c1d7dea823c', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 139, 'http://flowers.nagar:8888/?post_type=acf-field&p=141', 1, 'acf-field', '', 0),
(142, 1, '2021-06-10 12:15:14', '2021-06-10 09:15:14', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'שאלה', 'faq_question', 'publish', 'closed', 'closed', '', 'field_60c1d7f9a823d', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 141, 'http://flowers.nagar:8888/?post_type=acf-field&p=142', 0, 'acf-field', '', 0),
(143, 1, '2021-06-10 12:15:14', '2021-06-10 09:15:14', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'תשובה', 'faq_answer', 'publish', 'closed', 'closed', '', 'field_60c1d80da823e', '', '', '2021-06-10 12:15:14', '2021-06-10 09:15:14', '', 141, 'http://flowers.nagar:8888/?post_type=acf-field&p=143', 1, 'acf-field', '', 0),
(144, 1, '2021-06-10 12:15:34', '2021-06-10 09:15:34', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'שאלתם ענינו', '', 'publish', 'closed', 'closed', '', '%d7%a9%d7%90%d7%9c%d7%95%d7%aa-%d7%95%d7%aa%d7%a9%d7%95%d7%91%d7%95%d7%aa', '', '', '2021-06-10 12:18:20', '2021-06-10 09:18:20', '', 0, 'http://flowers.nagar:8888/?page_id=144', 0, 'page', '', 0),
(145, 1, '2021-06-10 12:15:34', '2021-06-10 09:15:34', '', 'שאלות ותשובות', '', 'inherit', 'closed', 'closed', '', '144-revision-v1', '', '', '2021-06-10 12:15:34', '2021-06-10 09:15:34', '', 144, 'http://flowers.nagar:8888/?p=145', 0, 'revision', '', 0),
(146, 1, '2021-06-10 12:15:53', '2021-06-10 09:15:53', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'שאלתם ענינו', '', 'inherit', 'closed', 'closed', '', '144-revision-v1', '', '', '2021-06-10 12:15:53', '2021-06-10 09:15:53', '', 144, 'http://flowers.nagar:8888/?p=146', 0, 'revision', '', 0),
(147, 1, '2021-06-10 12:18:20', '2021-06-10 09:18:20', 'לורם איפסום מט, קונסקסינג אלית סחטיר בלובק אלית סחטיר בלובק', 'שאלתם ענינו', '', 'inherit', 'closed', 'closed', '', '144-revision-v1', '', '', '2021-06-10 12:18:20', '2021-06-10 09:18:20', '', 144, 'http://flowers.nagar:8888/?p=147', 0, 'revision', '', 0),
(148, 1, '2021-06-10 12:26:46', '2021-06-10 09:26:46', ' ', '', '', 'publish', 'closed', 'closed', '', '148', '', '', '2021-06-10 12:26:46', '2021-06-10 09:26:46', '', 0, 'http://flowers.nagar:8888/?p=148', 3, 'nav_menu_item', '', 0),
(149, 1, '2021-06-10 12:26:46', '2021-06-10 09:26:46', ' ', '', '', 'publish', 'closed', 'closed', '', '149', '', '', '2021-06-10 12:26:46', '2021-06-10 09:26:46', '', 0, 'http://flowers.nagar:8888/?p=149', 4, 'nav_menu_item', '', 0),
(150, 1, '2021-06-10 12:26:46', '2021-06-10 09:26:46', ' ', '', '', 'publish', 'closed', 'closed', '', '150', '', '', '2021-06-10 12:26:46', '2021-06-10 09:26:46', '', 0, 'http://flowers.nagar:8888/?p=150', 5, 'nav_menu_item', '', 0),
(151, 1, '2021-06-15 15:09:09', '2021-06-10 09:26:56', ' ', '', '', 'publish', 'closed', 'closed', '', '151', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=151', 6, 'nav_menu_item', '', 0),
(152, 1, '2021-06-15 15:09:09', '2021-06-14 07:35:38', ' ', '', '', 'publish', 'closed', 'closed', '', '152', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=152', 1, 'nav_menu_item', '', 0),
(153, 1, '2021-06-14 13:22:16', '2021-06-14 10:22:16', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"views/home.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'דף הבית', '%d7%93%d7%a3-%d7%94%d7%91%d7%99%d7%aa', 'publish', 'closed', 'closed', '', 'group_60c72dcbb935d', '', '', '2021-06-14 13:31:51', '2021-06-14 10:31:51', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=153', 0, 'acf-field-group', '', 0),
(154, 1, '2021-06-14 13:22:58', '2021-06-14 10:22:58', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים', 'בלוק_עם_מוצרים', 'publish', 'closed', 'closed', '', 'field_60c72de79143c', '', '', '2021-06-14 13:24:12', '2021-06-14 10:24:12', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=154', 4, 'acf-field', '', 0),
(155, 1, '2021-06-14 13:23:13', '2021-06-14 10:23:13', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק ראשי', 'בלוק_ראשי', 'publish', 'closed', 'closed', '', 'field_60c72e04c07db', '', '', '2021-06-14 13:23:13', '2021-06-14 10:23:13', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=155', 0, 'acf-field', '', 0),
(156, 1, '2021-06-14 13:23:39', '2021-06-14 10:23:39', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לקולקציה המלאה', 'main_link', 'publish', 'closed', 'closed', '', 'field_60c72e17bf1c4', '', '', '2021-06-14 13:24:12', '2021-06-14 10:24:12', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=156', 3, 'acf-field', '', 0),
(157, 1, '2021-06-14 13:24:05', '2021-06-14 10:24:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'main_title', 'publish', 'closed', 'closed', '', 'field_60c72e2d046e8', '', '', '2021-06-14 13:24:12', '2021-06-14 10:24:12', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=157', 1, 'acf-field', '', 0),
(158, 1, '2021-06-14 13:24:05', '2021-06-14 10:24:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת משנה', 'main_subtitle', 'publish', 'closed', 'closed', '', 'field_60c72e38046e9', '', '', '2021-06-14 13:24:12', '2021-06-14 10:24:12', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=158', 2, 'acf-field', '', 0),
(159, 1, '2021-06-14 13:24:58', '2021-06-14 10:24:58', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_products_1', 'publish', 'closed', 'closed', '', 'field_60c72e517b8e1', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=159', 6, 'acf-field', '', 0),
(160, 1, '2021-06-14 13:24:58', '2021-06-14 10:24:58', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_products_1_title', 'publish', 'closed', 'closed', '', 'field_60c72e697b8e2', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=160', 5, 'acf-field', '', 0),
(161, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'אודות', 'אודות', 'publish', 'closed', 'closed', '', 'field_60c72e87509eb', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=161', 7, 'acf-field', '', 0),
(162, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'home_about_text', 'publish', 'closed', 'closed', '', 'field_60c72e8f509ec', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=162', 8, 'acf-field', '', 0),
(163, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"65\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:5:\"large\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'תמונה', 'home_about_img', 'publish', 'closed', 'closed', '', 'field_60c72ea2509ed', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=163', 9, 'acf-field', '', 0),
(164, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:2:\"35\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_about_link', 'publish', 'closed', 'closed', '', 'field_60c72ebd509ee', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=164', 10, 'acf-field', '', 0),
(165, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קטגוריות מומלצות', 'קטגוריות_מומלצות', 'publish', 'closed', 'closed', '', 'field_60c72ece509ef', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=165', 11, 'acf-field', '', 0),
(166, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_cats_title', 'publish', 'closed', 'closed', '', 'field_60c72edc509f0', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=166', 12, 'acf-field', '', 0),
(167, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:11:\"product_cat\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:6:\"object\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', 'קטגוריות מומלצות', 'home_cats', 'publish', 'closed', 'closed', '', 'field_60c72ee8509f1', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=167', 13, 'acf-field', '', 0),
(168, 1, '2021-06-14 13:27:31', '2021-06-14 10:27:31', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור לכל הקטגוריות', 'home_cats_link', 'publish', 'closed', 'closed', '', 'field_60c72efb509f2', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=168', 14, 'acf-field', '', 0),
(169, 1, '2021-06-14 13:28:05', '2021-06-14 10:28:05', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים 2', '_העתק', 'publish', 'closed', 'closed', '', 'field_60c72f20c06e2', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=169', 15, 'acf-field', '', 0),
(170, 1, '2021-06-14 13:28:05', '2021-06-14 10:28:05', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_products_2', 'publish', 'closed', 'closed', '', 'field_60c72f25c06e3', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=170', 17, 'acf-field', '', 0),
(171, 1, '2021-06-14 13:28:05', '2021-06-14 10:28:05', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_products_2_title', 'publish', 'closed', 'closed', '', 'field_60c72f2bc06e4', '', '', '2021-06-14 13:28:32', '2021-06-14 10:28:32', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&#038;p=171', 16, 'acf-field', '', 0),
(173, 1, '2021-06-14 13:30:29', '2021-06-14 10:30:29', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מאמרים', 'בלוק_עם_מאמרים', 'publish', 'closed', 'closed', '', 'field_60c72f8016255', '', '', '2021-06-14 13:30:29', '2021-06-14 10:30:29', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=173', 18, 'acf-field', '', 0),
(174, 1, '2021-06-14 13:30:29', '2021-06-14 10:30:29', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_posts_title', 'publish', 'closed', 'closed', '', 'field_60c72f9216256', '', '', '2021-06-14 13:30:29', '2021-06-14 10:30:29', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=174', 19, 'acf-field', '', 0),
(175, 1, '2021-06-14 13:30:29', '2021-06-14 10:30:29', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:4:\"post\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מאמרים', 'home_posts', 'publish', 'closed', 'closed', '', 'field_60c72fa116257', '', '', '2021-06-14 13:30:29', '2021-06-14 10:30:29', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=175', 20, 'acf-field', '', 0),
(176, 1, '2021-06-14 13:30:29', '2021-06-14 10:30:29', 'a:6:{s:4:\"type\";s:4:\"link\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";}', 'קישור', 'home_posts_link', 'publish', 'closed', 'closed', '', 'field_60c72fb016258', '', '', '2021-06-14 13:30:29', '2021-06-14 10:30:29', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=176', 21, 'acf-field', '', 0),
(177, 1, '2021-06-14 13:31:51', '2021-06-14 10:31:51', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'בלוק עם מוצרים 3 (עכשיו בעונה)', 'בלוק_עם_מוצרים_3_עכשיו_בעונה', 'publish', 'closed', 'closed', '', 'field_60c72fca7fbd2', '', '', '2021-06-14 13:31:51', '2021-06-14 10:31:51', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=177', 22, 'acf-field', '', 0),
(178, 1, '2021-06-14 13:31:51', '2021-06-14 10:31:51', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'home_products_3_title', 'publish', 'closed', 'closed', '', 'field_60c72fef7fbd3', '', '', '2021-06-14 13:31:51', '2021-06-14 10:31:51', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=178', 23, 'acf-field', '', 0),
(179, 1, '2021-06-14 13:31:51', '2021-06-14 10:31:51', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'home_products_3', 'publish', 'closed', 'closed', '', 'field_60c72ffd7fbd4', '', '', '2021-06-14 13:31:51', '2021-06-14 10:31:51', '', 153, 'http://flowers.nagar:8888/?post_type=acf-field&p=179', 24, 'acf-field', '', 0),
(180, 1, '2021-06-14 13:37:34', '2021-06-14 10:37:34', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'שם המוצר לורם היפסום 1', 'לורם איפסום מט, קונספיסינג אלית סחטיר בלובק. תצטנפל בלינדו אס לכימפו, דול, צוט ומעיוט', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', '', '', '2021-06-14 13:38:40', '2021-06-14 10:38:40', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=180', 0, 'product', '', 0),
(181, 1, '2021-06-14 13:37:02', '2021-06-14 10:37:02', '', 'product-2', '', 'inherit', 'open', 'closed', '', 'product-2', '', '', '2021-06-14 13:37:02', '2021-06-14 10:37:02', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-2.png', 0, 'attachment', 'image/png', 0),
(182, 1, '2021-06-14 13:37:03', '2021-06-14 10:37:03', '', 'product-3', '', 'inherit', 'open', 'closed', '', 'product-3', '', '', '2021-06-14 13:37:03', '2021-06-14 10:37:03', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-3.png', 0, 'attachment', 'image/png', 0),
(183, 1, '2021-06-14 13:37:04', '2021-06-14 10:37:04', '', 'product-4', '', 'inherit', 'open', 'closed', '', 'product-4', '', '', '2021-06-14 13:37:04', '2021-06-14 10:37:04', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-4.png', 0, 'attachment', 'image/png', 0),
(184, 1, '2021-06-14 13:37:05', '2021-06-14 10:37:05', '', 'product-5', '', 'inherit', 'open', 'closed', '', 'product-5', '', '', '2021-06-14 13:37:05', '2021-06-14 10:37:05', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-5.png', 0, 'attachment', 'image/png', 0),
(185, 1, '2021-06-14 13:37:06', '2021-06-14 10:37:06', '', 'product-6', '', 'inherit', 'open', 'closed', '', 'product-6', '', '', '2021-06-14 13:37:06', '2021-06-14 10:37:06', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-6.png', 0, 'attachment', 'image/png', 0),
(186, 1, '2021-06-14 13:37:08', '2021-06-14 10:37:08', '', 'product-7', '', 'inherit', 'open', 'closed', '', 'product-7', '', '', '2021-06-14 13:37:08', '2021-06-14 10:37:08', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-7.png', 0, 'attachment', 'image/png', 0),
(187, 1, '2021-06-14 13:37:09', '2021-06-14 10:37:09', '', 'product-8', '', 'inherit', 'open', 'closed', '', 'product-8', '', '', '2021-06-14 13:37:09', '2021-06-14 10:37:09', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-8.png', 0, 'attachment', 'image/png', 0),
(188, 1, '2021-06-14 13:37:10', '2021-06-14 10:37:10', '', 'product-9', '', 'inherit', 'open', 'closed', '', 'product-9', '', '', '2021-06-14 13:37:10', '2021-06-14 10:37:10', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-9.png', 0, 'attachment', 'image/png', 0),
(189, 1, '2021-06-14 13:37:11', '2021-06-14 10:37:11', '', 'product-10', '', 'inherit', 'open', 'closed', '', 'product-10', '', '', '2021-06-14 13:37:11', '2021-06-14 10:37:11', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-10.png', 0, 'attachment', 'image/png', 0),
(190, 1, '2021-06-14 13:37:12', '2021-06-14 10:37:12', '', 'product-11', '', 'inherit', 'open', 'closed', '', 'product-11', '', '', '2021-06-14 13:37:12', '2021-06-14 10:37:12', '', 180, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/product-11.png', 0, 'attachment', 'image/png', 0),
(191, 1, '2021-06-14 13:39:47', '2021-06-14 10:39:47', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"product\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מוצר', '%d7%9e%d7%95%d7%a6%d7%a8', 'publish', 'closed', 'closed', '', 'group_60c731ea05601', '', '', '2021-06-14 13:41:07', '2021-06-14 10:41:07', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=191', 0, 'acf-field-group', '', 0),
(192, 1, '2021-06-14 13:41:07', '2021-06-14 10:41:07', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'תוכן חוזר', 'product_accordion', 'publish', 'closed', 'closed', '', 'field_60c7320f43497', '', '', '2021-06-14 13:41:07', '2021-06-14 10:41:07', '', 191, 'http://flowers.nagar:8888/?post_type=acf-field&p=192', 0, 'acf-field', '', 0),
(193, 1, '2021-06-14 13:41:07', '2021-06-14 10:41:07', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'acc_title', 'publish', 'closed', 'closed', '', 'field_60c7322b43498', '', '', '2021-06-14 13:41:07', '2021-06-14 10:41:07', '', 192, 'http://flowers.nagar:8888/?post_type=acf-field&p=193', 0, 'acf-field', '', 0),
(194, 1, '2021-06-14 13:41:07', '2021-06-14 10:41:07', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'טקסט', 'acc_content', 'publish', 'closed', 'closed', '', 'field_60c7323443499', '', '', '2021-06-14 13:41:07', '2021-06-14 10:41:07', '', 192, 'http://flowers.nagar:8888/?post_type=acf-field&p=194', 1, 'acf-field', '', 0),
(195, 1, '2021-06-14 13:43:32', '2021-06-14 10:43:32', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המוצר לורם היפסום 2', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', '', '', '2021-06-14 13:43:48', '2021-06-14 10:43:48', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=195', 0, 'product', '', 0),
(196, 1, '2021-06-14 13:44:29', '2021-06-14 10:44:29', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המוצר לורם היפסום 3', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', '', '', '2021-06-14 13:44:29', '2021-06-14 10:44:29', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=196', 0, 'product', '', 0),
(197, 1, '2021-06-14 13:45:17', '2021-06-14 10:45:17', 'קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף', 'שם המוצר לורם היפסום 4', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', '', '', '2021-06-14 13:45:17', '2021-06-14 10:45:17', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=197', 0, 'product', '', 0),
(198, 1, '2021-06-14 13:45:50', '2021-06-14 10:45:50', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המוצר לורם היפסום 5', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', '', '', '2021-06-14 13:45:50', '2021-06-14 10:45:50', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=198', 0, 'product', '', 0),
(199, 1, '2021-06-14 13:46:30', '2021-06-14 10:46:30', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המוצר לורם היפסום 6', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', '', '', '2021-06-14 13:46:30', '2021-06-14 10:46:30', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=199', 0, 'product', '', 0),
(200, 1, '2021-06-14 13:46:59', '2021-06-14 10:46:59', 'קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n\r\nקונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'שם המוצר לורם היפסום 7', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', '', '', '2021-06-14 13:46:59', '2021-06-14 10:46:59', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=200', 0, 'product', '', 0),
(201, 1, '2021-06-14 13:47:30', '2021-06-14 10:47:30', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המוצר לורם היפסום 8', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', '', '', '2021-06-14 13:47:30', '2021-06-14 10:47:30', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=201', 0, 'product', '', 0),
(202, 1, '2021-06-14 13:47:56', '2021-06-14 10:47:56', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n\r\nצש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', 'שם המוצר לורם היפסום 9', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-9', '', '', '2021-06-14 13:47:56', '2021-06-14 10:47:56', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=202', 0, 'product', '', 0),
(203, 1, '2021-06-14 13:48:19', '2021-06-14 10:48:19', 'סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט\r\n\r\nקוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.', 'שם המוצר לורם היפסום 10', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10', '', '', '2021-06-14 13:48:19', '2021-06-14 10:48:19', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=203', 0, 'product', '', 0),
(204, 1, '2021-06-14 13:51:54', '2021-06-14 10:51:54', '', 'cat-1', '', 'inherit', 'open', 'closed', '', 'cat-1', '', '', '2021-06-14 13:51:54', '2021-06-14 10:51:54', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-1.png', 0, 'attachment', 'image/png', 0),
(205, 1, '2021-06-14 13:51:55', '2021-06-14 10:51:55', '', 'cat-2', '', 'inherit', 'open', 'closed', '', 'cat-2', '', '', '2021-06-14 13:51:55', '2021-06-14 10:51:55', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-2.png', 0, 'attachment', 'image/png', 0),
(206, 1, '2021-06-14 13:51:57', '2021-06-14 10:51:57', '', 'cat-3', '', 'inherit', 'open', 'closed', '', 'cat-3', '', '', '2021-06-14 13:51:57', '2021-06-14 10:51:57', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-3.png', 0, 'attachment', 'image/png', 0),
(207, 1, '2021-06-14 13:51:58', '2021-06-14 10:51:58', '', 'cat-4', '', 'inherit', 'open', 'closed', '', 'cat-4', '', '', '2021-06-14 13:51:58', '2021-06-14 10:51:58', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-4.png', 0, 'attachment', 'image/png', 0),
(208, 1, '2021-06-14 13:51:59', '2021-06-14 10:51:59', '', 'cat-5', '', 'inherit', 'open', 'closed', '', 'cat-5', '', '', '2021-06-14 13:51:59', '2021-06-14 10:51:59', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-5.png', 0, 'attachment', 'image/png', 0),
(209, 1, '2021-06-14 13:52:00', '2021-06-14 10:52:00', '', 'cat-6', '', 'inherit', 'open', 'closed', '', 'cat-6', '', '', '2021-06-14 13:52:00', '2021-06-14 10:52:00', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-6.png', 0, 'attachment', 'image/png', 0),
(210, 1, '2021-06-14 13:52:02', '2021-06-14 10:52:02', '', 'cat-7', '', 'inherit', 'open', 'closed', '', 'cat-7', '', '', '2021-06-14 13:52:02', '2021-06-14 10:52:02', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-7.png', 0, 'attachment', 'image/png', 0),
(211, 1, '2021-06-14 13:52:03', '2021-06-14 10:52:03', '', 'cat-8', '', 'inherit', 'open', 'closed', '', 'cat-8', '', '', '2021-06-14 13:52:03', '2021-06-14 10:52:03', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-8.png', 0, 'attachment', 'image/png', 0),
(212, 1, '2021-06-14 13:52:04', '2021-06-14 10:52:04', '', 'cat-9', '', 'inherit', 'open', 'closed', '', 'cat-9', '', '', '2021-06-14 13:52:04', '2021-06-14 10:52:04', '', 0, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/cat-9.png', 0, 'attachment', 'image/png', 0),
(213, 1, '2021-06-14 14:06:24', '2021-06-14 11:06:24', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ', 'כל קטגוריות שלנו', '', 'publish', 'closed', 'closed', '', '%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa', '', '', '2021-06-15 19:48:24', '2021-06-15 16:48:24', '', 0, 'http://flowers.nagar:8888/?page_id=213', 0, 'page', '', 0),
(214, 1, '2021-06-14 14:06:24', '2021-06-14 11:06:24', '', 'קטגוריות', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2021-06-14 14:06:24', '2021-06-14 11:06:24', '', 213, 'http://flowers.nagar:8888/?p=214', 0, 'revision', '', 0),
(215, 1, '2021-06-14 14:08:13', '2021-06-14 11:08:13', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:20:\"views/categories.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'קטגוריות', '%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%95%d7%aa', 'publish', 'closed', 'closed', '', 'group_60c73867eb34e', '', '', '2021-06-14 14:09:11', '2021-06-14 11:09:11', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=215', 0, 'acf-field-group', '', 0),
(216, 1, '2021-06-14 14:08:13', '2021-06-14 11:08:13', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'cat_products_title', 'publish', 'closed', 'closed', '', 'field_60c7386a2c527', '', '', '2021-06-14 14:08:13', '2021-06-14 11:08:13', '', 215, 'http://flowers.nagar:8888/?post_type=acf-field&p=216', 0, 'acf-field', '', 0),
(217, 1, '2021-06-14 14:08:13', '2021-06-14 11:08:13', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:7:\"product\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:3:{i:0;s:6:\"search\";i:1;s:9:\"post_type\";i:2;s:8:\"taxonomy\";}s:8:\"elements\";a:1:{i:0;s:14:\"featured_image\";}s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'מוצרים', 'cat_products', 'publish', 'closed', 'closed', '', 'field_60c7387e2c528', '', '', '2021-06-14 14:08:13', '2021-06-14 11:08:13', '', 215, 'http://flowers.nagar:8888/?post_type=acf-field&p=217', 1, 'acf-field', '', 0),
(218, 1, '2021-06-14 14:08:40', '2021-06-14 11:08:40', '<h2>כל קטגוריות שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ', 'קטגוריות', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2021-06-14 14:08:40', '2021-06-14 11:08:40', '', 213, 'http://flowers.nagar:8888/?p=218', 0, 'revision', '', 0),
(219, 1, '2021-06-14 14:09:43', '2021-06-14 11:09:43', '<h2>כל קטגוריות שלנו</h2>\r\nלורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ', 'קטגוריות', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2021-06-14 14:09:43', '2021-06-14 11:09:43', '', 213, 'http://flowers.nagar:8888/?p=219', 0, 'revision', '', 0),
(220, 1, '2021-06-14 14:10:47', '2021-06-14 11:10:47', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינ', 'כל קטגוריות שלנו', '', 'inherit', 'closed', 'closed', '', '213-revision-v1', '', '', '2021-06-14 14:10:47', '2021-06-14 11:10:47', '', 213, 'http://flowers.nagar:8888/?p=220', 0, 'revision', '', 0),
(221, 1, '2021-06-15 15:09:09', '2021-06-14 11:22:48', ' ', '', '', 'publish', 'closed', 'closed', '', '221', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=221', 7, 'nav_menu_item', '', 0),
(222, 1, '2021-06-14 15:08:38', '2021-06-14 12:08:38', '', 'תמונת ראשי', '', 'inherit', 'open', 'closed', '', '%d7%aa%d7%9e%d7%95%d7%a0%d7%aa-%d7%a8%d7%90%d7%a9%d7%99', '', '', '2021-06-14 15:08:38', '2021-06-14 12:08:38', '', 28, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/תמונת-ראשי.png', 0, 'attachment', 'image/png', 0),
(223, 1, '2021-06-14 15:08:49', '2021-06-14 12:08:49', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2021-06-14 15:08:49', '2021-06-14 12:08:49', '', 28, 'http://flowers.nagar:8888/?p=223', 0, 'revision', '', 0),
(224, 1, '2021-06-14 15:11:51', '2021-06-14 12:11:51', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2021-06-14 15:11:51', '2021-06-14 12:11:51', '', 28, 'http://flowers.nagar:8888/?p=224', 0, 'revision', '', 0),
(225, 1, '2021-06-14 15:31:54', '2021-06-14 12:31:54', '', 'about-home', '', 'inherit', 'open', 'closed', '', 'about-home', '', '', '2021-06-14 15:31:54', '2021-06-14 12:31:54', '', 28, 'http://flowers.nagar:8888/wp-content/uploads/2021/06/about-home.png', 0, 'attachment', 'image/png', 0),
(226, 1, '2021-06-14 15:32:05', '2021-06-14 12:32:05', '', 'דף הבית', '', 'inherit', 'closed', 'closed', '', '28-revision-v1', '', '', '2021-06-14 15:32:05', '2021-06-14 12:32:05', '', 28, 'http://flowers.nagar:8888/?p=226', 0, 'revision', '', 0),
(227, 1, '2021-06-14 15:54:47', '2021-06-14 12:54:47', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'פופאפ', 'פופאפ', 'publish', 'closed', 'closed', '', 'field_60c75185bd6e3', '', '', '2021-06-14 15:54:47', '2021-06-14 12:54:47', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=227', 9, 'acf-field', '', 0),
(228, 1, '2021-06-14 15:54:47', '2021-06-14 12:54:47', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'pop_form_title', 'publish', 'closed', 'closed', '', 'field_60c7518ebd6e4', '', '', '2021-06-14 15:54:47', '2021-06-14 12:54:47', '', 77, 'http://flowers.nagar:8888/?post_type=acf-field&p=228', 10, 'acf-field', '', 0),
(229, 1, '2021-06-15 15:03:03', '2021-06-15 12:03:03', '', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-06-15 15:03:03', '2021-06-15 12:03:03', '', 7, 'http://flowers.nagar:8888/?p=229', 0, 'revision', '', 0),
(230, 1, '2021-06-15 15:06:30', '2021-06-15 12:06:30', 'a:7:{s:8:\"location\";a:2:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:4:\"page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:1:\"7\";}}i:1;a:1:{i:0;a:3:{s:5:\"param\";s:8:\"taxonomy\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"product_cat\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'מוצרים', '%d7%9e%d7%95%d7%a6%d7%a8%d7%99%d7%9d', 'publish', 'closed', 'closed', '', 'group_60c8975453b03', '', '', '2021-06-15 15:06:30', '2021-06-15 12:06:30', '', 0, 'http://flowers.nagar:8888/?post_type=acf-field-group&#038;p=230', 0, 'acf-field-group', '', 0),
(231, 1, '2021-06-15 15:06:30', '2021-06-15 12:06:30', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'קטגוריות דומות', 'קטגוריות_דומות', 'publish', 'closed', 'closed', '', 'field_60c89781309c7', '', '', '2021-06-15 15:06:30', '2021-06-15 12:06:30', '', 230, 'http://flowers.nagar:8888/?post_type=acf-field&p=231', 0, 'acf-field', '', 0),
(232, 1, '2021-06-15 15:06:30', '2021-06-15 12:06:30', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'כותרת', 'shop_cats_title', 'publish', 'closed', 'closed', '', 'field_60c89789309c8', '', '', '2021-06-15 15:06:30', '2021-06-15 12:06:30', '', 230, 'http://flowers.nagar:8888/?post_type=acf-field&p=232', 1, 'acf-field', '', 0),
(233, 1, '2021-06-15 15:06:30', '2021-06-15 12:06:30', 'a:13:{s:4:\"type\";s:8:\"taxonomy\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:8:\"taxonomy\";s:11:\"product_cat\";s:10:\"field_type\";s:8:\"checkbox\";s:8:\"add_term\";i:1;s:10:\"save_terms\";i:0;s:10:\"load_terms\";i:0;s:13:\"return_format\";s:6:\"object\";s:8:\"multiple\";i:0;s:10:\"allow_null\";i:0;}', 'קטגוריות דומות', 'shop_cats', 'publish', 'closed', 'closed', '', 'field_60c8979f309c9', '', '', '2021-06-15 15:06:30', '2021-06-15 12:06:30', '', 230, 'http://flowers.nagar:8888/?post_type=acf-field&p=233', 2, 'acf-field', '', 0),
(234, 1, '2021-06-15 15:07:02', '2021-06-15 12:07:02', '', 'מוצרים', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-06-15 15:07:02', '2021-06-15 12:07:02', '', 7, 'http://flowers.nagar:8888/?p=234', 0, 'revision', '', 0),
(235, 1, '2021-06-15 15:09:09', '2021-06-15 12:08:23', ' ', '', '', 'publish', 'closed', 'closed', '', '235', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=235', 9, 'nav_menu_item', '', 0),
(236, 1, '2021-06-15 15:09:09', '2021-06-15 12:09:09', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה – לתכי מורגם בורק? לתיג ישבעס.', '', '', 'publish', 'closed', 'closed', '', '236', '', '', '2021-06-15 15:09:09', '2021-06-15 12:09:09', '', 0, 'http://flowers.nagar:8888/?p=236', 10, 'nav_menu_item', '', 0),
(237, 1, '2021-06-15 18:38:43', '2021-06-15 15:38:43', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 'שם המוצר לורם היפסום 11', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-11', '', '', '2021-06-15 18:38:44', '2021-06-15 15:38:44', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=237', 0, 'product', '', 0),
(238, 1, '2021-06-15 18:39:15', '2021-06-15 15:39:15', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n\r\nקולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי סחטיר בלובק. תצטנפל בלינדו למרקל אס לכימפו, דול, צוט ומעיוט - לפתיעם ברשג - ולתיעם גדדיש. קוויז דומור ליאמום בלינך רוגצה. לפמעט', 'שם המוצר לורם היפסום 12', '', 'publish', 'open', 'closed', '', '%d7%a9%d7%9d-%d7%94%d7%9e%d7%95%d7%a6%d7%a8-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-12', '', '', '2021-06-15 18:39:15', '2021-06-15 15:39:15', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=238', 0, 'product', '', 0);
INSERT INTO `fmn_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(239, 1, '2021-06-15 18:40:26', '0000-00-00 00:00:00', 'קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\n\nמוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף', 'שם המוצר לורם היפסום 13', '', 'draft', 'open', 'closed', '', '', '', '', '2021-06-15 18:40:26', '2021-06-15 15:40:26', '', 0, 'http://flowers.nagar:8888/?post_type=product&#038;p=239', 0, 'product', '', 0),
(240, 1, '2021-06-15 19:41:14', '0000-00-00 00:00:00', '', 'AUTO-DRAFT', '', 'auto-draft', 'open', 'closed', '', '', '', '', '2021-06-15 19:41:14', '0000-00-00 00:00:00', '', 0, 'http://flowers.nagar:8888/?post_type=product&p=240', 0, 'product', '', 0),
(241, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '241', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=241', 1, 'nav_menu_item', '', 0),
(242, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '242', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=242', 2, 'nav_menu_item', '', 0),
(243, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '243', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=243', 3, 'nav_menu_item', '', 0),
(244, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '244', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=244', 4, 'nav_menu_item', '', 0),
(245, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '245', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=245', 5, 'nav_menu_item', '', 0),
(246, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה – לתכי מורגם בורק? לתיג ישבעס.', '', '', 'publish', 'closed', 'closed', '', '246', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=246', 6, 'nav_menu_item', '', 0),
(247, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '247', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=247', 7, 'nav_menu_item', '', 0),
(248, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', ' ', '', '', 'publish', 'closed', 'closed', '', '248', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=248', 8, 'nav_menu_item', '', 0),
(249, 1, '2021-06-16 12:02:57', '2021-06-16 08:41:47', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.', '', '', 'publish', 'closed', 'closed', '', '249', '', '', '2021-06-16 12:02:57', '2021-06-16 09:02:57', '', 0, 'http://flowers.nagar:8888/?p=249', 9, 'nav_menu_item', '', 0),
(253, 1, '2021-06-16 13:43:08', '2021-06-16 10:43:08', '<!-- wp:shortcode -->[yith_wcwl_wishlist]<!-- /wp:shortcode -->', 'רשימת משאלות', '', 'publish', 'closed', 'closed', '', 'wishlist', '', '', '2021-06-16 13:43:08', '2021-06-16 10:43:08', '', 0, 'http://flowers.nagar:8888/wishlist/', 0, 'page', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_termmeta`
--

CREATE TABLE `fmn_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_termmeta`
--

INSERT INTO `fmn_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 15, 'product_count_product_cat', '11'),
(2, 18, 'order_pa_size', '0'),
(3, 19, 'order_pa_size', '0'),
(4, 20, 'order_pa_size', '0'),
(5, 25, 'order', '0'),
(6, 25, 'display_type', ''),
(7, 25, 'thumbnail_id', '204'),
(8, 26, 'order', '0'),
(9, 26, 'display_type', ''),
(10, 26, 'thumbnail_id', '205'),
(11, 27, 'order', '0'),
(12, 27, 'display_type', ''),
(13, 27, 'thumbnail_id', '206'),
(14, 28, 'order', '0'),
(15, 28, 'display_type', ''),
(16, 28, 'thumbnail_id', '207'),
(17, 29, 'order', '0'),
(18, 29, 'display_type', ''),
(19, 29, 'thumbnail_id', '208'),
(20, 30, 'order', '0'),
(21, 30, 'display_type', ''),
(22, 30, 'thumbnail_id', '209'),
(23, 31, 'order', '0'),
(24, 31, 'display_type', ''),
(25, 31, 'thumbnail_id', '210'),
(26, 32, 'order', '0'),
(27, 32, 'display_type', ''),
(28, 32, 'thumbnail_id', '211'),
(29, 33, 'order', '0'),
(30, 33, 'display_type', ''),
(31, 33, 'thumbnail_id', '212'),
(32, 34, 'order', '0'),
(33, 34, 'display_type', ''),
(34, 34, 'thumbnail_id', '48'),
(35, 27, 'product_count_product_cat', '1'),
(36, 29, 'product_count_product_cat', '1'),
(37, 25, 'product_count_product_cat', '1'),
(38, 33, 'product_count_product_cat', '1');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_terms`
--

CREATE TABLE `fmn_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_terms`
--

INSERT INTO `fmn_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'כללי', '%d7%9b%d7%9c%d7%9c%d7%99', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'Header menu', 'header-menu', 0),
(17, 'Header Down Menu', 'header-down-menu', 0),
(18, 'רגיל', 'regular', 0),
(19, 'גדול', 'big', 0),
(20, 'ענק', 'large', 0),
(21, '111', '111', 0),
(22, 'סוגי פרחים', '%d7%a1%d7%95%d7%92%d7%99-%d7%a4%d7%a8%d7%97%d7%99%d7%9d', 0),
(23, 'עמודים מובילים', '%d7%a2%d7%9e%d7%95%d7%93%d7%99%d7%9d-%d7%9e%d7%95%d7%91%d7%99%d7%9c%d7%99%d7%9d', 0),
(24, 'Footer menu', 'footer-menu', 0),
(25, 'שם הקטגוריה לורם היפסום 1', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-1', 0),
(26, 'שם הקטגוריה לורם היפסום 2', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-2', 0),
(27, 'שם הקטגוריה לורם היפסום 3', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-3', 0),
(28, 'שם הקטגוריה לורם היפסום 4', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-4', 0),
(29, 'שם הקטגוריה לורם היפסום 5', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-5', 0),
(30, 'שם הקטגוריה לורם היפסום 6', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-6', 0),
(31, 'שם הקטגוריה לורם היפסום 7', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-7', 0),
(32, 'שם הקטגוריה לורם היפסום 8', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-8', 0),
(33, 'שם הקטגוריה לורם היפסום 9', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-9', 0),
(34, 'שם הקטגוריה לורם היפסום 10', '%d7%a9%d7%9d-%d7%94%d7%a7%d7%98%d7%92%d7%95%d7%a8%d7%99%d7%94-%d7%9c%d7%95%d7%a8%d7%9d-%d7%94%d7%99%d7%a4%d7%a1%d7%95%d7%9d-10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_relationships`
--

CREATE TABLE `fmn_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_relationships`
--

INSERT INTO `fmn_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(12, 4, 0),
(12, 15, 0),
(12, 18, 0),
(12, 19, 0),
(12, 20, 0),
(27, 16, 0),
(35, 1, 0),
(43, 1, 0),
(54, 1, 0),
(56, 1, 0),
(58, 1, 0),
(60, 1, 0),
(62, 1, 0),
(64, 1, 0),
(66, 1, 0),
(68, 1, 0),
(70, 21, 0),
(75, 16, 0),
(76, 16, 0),
(88, 22, 0),
(89, 23, 0),
(90, 23, 0),
(91, 23, 0),
(92, 23, 0),
(93, 23, 0),
(94, 1, 0),
(108, 24, 0),
(109, 24, 0),
(113, 16, 0),
(129, 16, 0),
(148, 24, 0),
(149, 24, 0),
(150, 24, 0),
(151, 16, 0),
(152, 16, 0),
(180, 2, 0),
(180, 15, 0),
(195, 2, 0),
(195, 15, 0),
(196, 2, 0),
(196, 15, 0),
(197, 2, 0),
(197, 15, 0),
(198, 2, 0),
(198, 15, 0),
(199, 2, 0),
(199, 15, 0),
(200, 2, 0),
(200, 15, 0),
(201, 2, 0),
(201, 15, 0),
(202, 2, 0),
(202, 15, 0),
(203, 2, 0),
(203, 15, 0),
(221, 16, 0),
(235, 16, 0),
(236, 16, 0),
(237, 2, 0),
(237, 27, 0),
(237, 29, 0),
(238, 2, 0),
(238, 25, 0),
(238, 33, 0),
(241, 17, 0),
(242, 17, 0),
(243, 17, 0),
(244, 17, 0),
(245, 17, 0),
(246, 17, 0),
(247, 17, 0),
(248, 17, 0),
(249, 17, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_term_taxonomy`
--

CREATE TABLE `fmn_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_term_taxonomy`
--

INSERT INTO `fmn_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 10),
(2, 2, 'product_type', '', 0, 12),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 1),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 11),
(16, 16, 'nav_menu', '', 0, 10),
(17, 17, 'nav_menu', '', 0, 9),
(18, 18, 'pa_size', '', 0, 1),
(19, 19, 'pa_size', '', 0, 1),
(20, 20, 'pa_size', '', 0, 1),
(21, 21, 'category', '', 0, 1),
(22, 22, 'nav_menu', '', 0, 1),
(23, 23, 'nav_menu', '', 0, 5),
(24, 24, 'nav_menu', '', 0, 5),
(25, 25, 'product_cat', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n', 0, 1),
(26, 26, 'product_cat', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n', 0, 0),
(27, 27, 'product_cat', 'קונסקטורר אדיפיסינג אלית. סת אלמנקום ניסי נון ניבאה. דס איאקוליס וולופטה דיאם. וסטיבולום אט דולור, קראס אגת לקטוס וואל אאוגו וסטיבולום סוליסי טידום בעליק. קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n', 0, 1),
(28, 28, 'product_cat', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.', 0, 0),
(29, 29, 'product_cat', 'מוסן מנת. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף נולום ארווס סאפיאן - פוסיליס קוויס, אקווזמן קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף\r\n\r\n', 0, 1),
(30, 30, 'product_cat', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\nצש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n', 0, 0),
(31, 31, 'product_cat', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n', 0, 0),
(32, 32, 'product_cat', 'קוואזי במר מודוף. אודיפו בלאסטיק מונופץ קליר, בנפת נפקט למסון בלרק - וענוף לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק.\r\n', 0, 0),
(33, 33, 'product_cat', 'לורם איפסום דולור סיט אמט, קונסקטורר אדיפיסינג אלית קולהע צופעט למרקוח איבן איף, ברומץ כלרשט מיחוצים. קלאצי לפרומי בלוף קינץ תתיח לרעח. לת צשחמי צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורך. גולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט ליבם סולגק. בראיט ולחת צורק מונחף, בגורמי מגמש. תרבנך וסתעד לכנו סתשם השמה - לתכי מורגם בורק? לתיג ישבעס.\r\n', 0, 1),
(34, 34, 'product_cat', 'צש בליא, מנסוטו צמלח לביקו ננבי, צמוקו בלוקריה שיצמה ברורק. לורם איפסום דולור סיט אמט, להאמית קרהשק סכעיט דז מא, מנכם למטכין נשואי מנורךגולר מונפרר סוברט לורם שבצק יהול, לכנוץ בעריר גק ליץ, ושבעגט. קולורס מונפרד אדנדום סילקוף, מרגשי ומרגשח. עמחליף קונדימנטום קורוס בליקרה, נונסטי קלובר בריקנה סטום, לפריקך תצטריק לרטי.\r\n', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_usermeta`
--

CREATE TABLE `fmn_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_usermeta`
--

INSERT INTO `fmn_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'dev_admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', 'he_IL'),
(12, 1, 'fmn_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'fmn_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"7d5adb9e17c50b23c5f13fc29936bb79759baa87a602f5ab69ce89425a291348\";a:4:{s:10:\"expiration\";i:1624001714;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:120:\"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36\";s:5:\"login\";i:1623828914;}}'),
(17, 1, 'fmn_dashboard_quick_press_last_post_id', '4'),
(18, 1, '_woocommerce_tracks_anon_id', 'woo:+x7hnH96lpLATdeTZKscZIsl'),
(19, 1, 'last_update', '1623230193'),
(20, 1, 'woocommerce_admin_activity_panel_inbox_last_read', '1623229779630'),
(21, 1, 'woocommerce_admin_task_list_tracked_started_tasks', '{\"products\":1}'),
(22, 1, 'dismissed_no_secure_connection_notice', '1'),
(23, 1, 'wc_last_active', '1623801600'),
(24, 1, 'fmn_user-settings', 'libraryContent=browse'),
(25, 1, 'fmn_user-settings-time', '1623186814'),
(26, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(27, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:12:\"add-post_tag\";i:1;s:15:\"add-product_tag\";}'),
(28, 1, '_order_count', '0'),
(29, 1, 'billing_first_name', ''),
(30, 1, 'billing_last_name', ''),
(31, 1, 'billing_company', ''),
(32, 1, 'billing_address_1', ''),
(33, 1, 'billing_address_2', ''),
(34, 1, 'billing_city', ''),
(35, 1, 'billing_postcode', ''),
(36, 1, 'billing_country', ''),
(37, 1, 'billing_state', ''),
(38, 1, 'billing_phone', ''),
(39, 1, 'billing_email', 'maxf@leos.co.il'),
(40, 1, 'shipping_first_name', ''),
(41, 1, 'shipping_last_name', ''),
(42, 1, 'shipping_company', ''),
(43, 1, 'shipping_address_1', ''),
(44, 1, 'shipping_address_2', ''),
(45, 1, 'shipping_city', ''),
(46, 1, 'shipping_postcode', ''),
(47, 1, 'shipping_country', ''),
(48, 1, 'shipping_state', ''),
(49, 1, 'closedpostboxes_product', 'a:0:{}'),
(50, 1, 'metaboxhidden_product', 'a:1:{i:0;s:7:\"slugdiv\";}'),
(51, 1, 'nav_menu_recently_edited', '17'),
(52, 1, 'closedpostboxes_nav-menus', 'a:0:{}'),
(54, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:3:{s:32:\"884983780a6ef210d475d875ba900a1e\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"884983780a6ef210d475d875ba900a1e\";s:10:\"product_id\";i:198;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:179;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:179;s:8:\"line_tax\";i:0;}s:32:\"e0f8bd20bacd56281e77b6bbb7cca098\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"e0f8bd20bacd56281e77b6bbb7cca098\";s:10:\"product_id\";i:199;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:60;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:60;s:8:\"line_tax\";i:0;}s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";a:7:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";s:10:\"product_id\";i:180;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";}}}');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_users`
--

CREATE TABLE `fmn_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_users`
--

INSERT INTO `fmn_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'dev_admin', '$P$B1koR3hPFCbCSoxbpMUpMUteJe8azv.', 'dev_admin', 'maxf@leos.co.il', '', '2021-06-08 20:40:24', '', 0, 'dev_admin');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_notes`
--

CREATE TABLE `fmn_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  `layout` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `image` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'info'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_notes`
--

INSERT INTO `fmn_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`, `layout`, `image`, `is_deleted`, `icon`) VALUES
(1, 'paypal_ppcp_gtm_2021', 'marketing', 'en_US', 'Offer more options with the new PayPal', 'Get the latest PayPal extension for a full suite of payment methods with extensive currency and country coverage.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(2, 'facebook_pixel_api_2021', 'marketing', 'en_US', 'Improve the performance of your Facebook ads', 'Enable Facebook Pixel and Conversions API through the latest version of Facebook for WooCommerce for improved measurement and ad targeting capabilities.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(3, 'facebook_ec_2021', 'marketing', 'en_US', 'Sync your product catalog with Facebook to help boost sales', 'A single click adds all products to your Facebook Business Page shop. Product changes are automatically synced, with the flexibility to control which products are listed.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(4, 'ecomm-need-help-setting-up-your-store', 'info', 'en_US', 'Need help setting up your Store?', 'Schedule a free 30-min <a href=\"https://wordpress.com/support/concierge-support/\">quick start session</a> and get help from our specialists. We’re happy to walk through setup steps, show you around the WordPress.com dashboard, troubleshoot any issues you may have, and help you the find the features you need to accomplish your goals for your site.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(5, 'woocommerce-services', 'info', 'en_US', 'WooCommerce Shipping & Tax', 'WooCommerce Shipping &amp; Tax helps get your store “ready to sell” as quickly as possible. You create your products. We take care of tax calculation, payment processing, and shipping label printing! Learn more about the extension that you just installed.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(6, 'ecomm-unique-shopping-experience', 'info', 'en_US', 'For a shopping experience as unique as your customers', 'Product Add-Ons allow your customers to personalize products while they’re shopping on your online store. No more follow-up email requests—customers get what they want, before they’re done checking out. Learn more about this extension that comes included in your plan.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(7, 'wc-admin-getting-started-in-ecommerce', 'info', 'en_US', 'Getting Started in eCommerce - webinar', 'We want to make eCommerce and this process of getting started as easy as possible for you. Watch this webinar to get tips on how to have our store up and running in a breeze.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(8, 'your-first-product', 'info', 'en_US', 'Your first product', 'That\'s huge! You\'re well on your way to building a successful online store — now it’s time to think about how you\'ll fulfill your orders.<br /><br />Read our shipping guide to learn best practices and options for putting together your shipping strategy. And for WooCommerce stores in the United States, you can print discounted shipping labels via USPS with <a href=\"https://href.li/?https://woocommerce.com/shipping\" target=\"_blank\">WooCommerce Shipping</a>.', '{}', 'unactioned', 'woocommerce.com', '2021-06-08 20:51:47', NULL, 0, 'plain', '', 0, 'info'),
(9, 'wc-square-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with Square you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(10, 'wc-square-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with Square and Apple Pay ', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(11, 'wcpay-apple-pay-is-now-available', 'marketing', 'en_US', 'Apple Pay is now available with WooCommerce Payments!', 'Increase your conversion rate by offering a fast and secure checkout with <a href=\"https://woocommerce.com/apple-pay/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">Apple Pay</a>®. It’s free to get started with <a href=\"https://woocommerce.com/payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_applepay\" target=\"_blank\">WooCommerce Payments</a>.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(12, 'wcpay-apple-pay-boost-sales', 'marketing', 'en_US', 'Boost sales with Apple Pay', 'Now that you accept Apple Pay® with WooCommerce Payments you can increase conversion rates by letting your customers know that Apple Pay® is available. Here’s a marketing guide to help you get started.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(13, 'wcpay-apple-pay-grow-your-business', 'marketing', 'en_US', 'Grow your business with WooCommerce Payments and Apple Pay', 'Now more than ever, shoppers want a fast, simple, and secure online checkout experience. Increase conversion rates by letting your customers know that you now accept Apple Pay®.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(14, 'wc-admin-optimizing-the-checkout-flow', 'info', 'en_US', 'Optimizing the checkout flow', 'It\'s crucial to get your store\'s checkout as smooth as possible to avoid losing sales. Let\'s take a look at how you can optimize the checkout experience for your shoppers.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(15, 'wc-admin-first-five-things-to-customize', 'info', 'en_US', 'The first 5 things to customize in your store', 'Deciding what to start with first is tricky. To help you properly prioritize, we\'ve put together this short list of the first few things you should customize in WooCommerce.', '{}', 'unactioned', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(16, 'wc-payments-qualitative-feedback', 'info', 'en_US', 'WooCommerce Payments setup - let us know what you think', 'Congrats on enabling WooCommerce Payments for your store. Please share your feedback in this 2 minute survey to help us improve the setup process.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(17, 'share-your-feedback-on-paypal', 'info', 'en_US', 'Share your feedback on PayPal', 'Share your feedback in this 2 minute survey about how we can make the process of accepting payments more useful for your store.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(18, 'wcpay_instant_deposits_gtm_2021', 'marketing', 'en_US', 'Get paid within minutes – Instant Deposits for WooCommerce Payments', 'Stay flexible with immediate access to your funds when you need them – including nights, weekends, and holidays. With <a href=\"https://woocommerce.com/products/woocommerce-payments/?utm_source=inbox&amp;utm_medium=product&amp;utm_campaign=wcpay_instant_deposits\">WooCommerce Payments\'</a> new Instant Deposits feature, you’re able to transfer your earnings to a debit card within minutes.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(19, 'google_listings_and_ads_install', 'marketing', 'en_US', 'Drive traffic and sales with Google', 'Reach online shoppers to drive traffic and sales for your store by showcasing products across Google, for free or with ads.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(20, 'wc-subscriptions-security-update-3-0-15', 'info', 'en_US', 'WooCommerce Subscriptions security update!', 'We recently released an important security update to WooCommerce Subscriptions. To ensure your site\'s data is protected, please upgrade <strong>WooCommerce Subscriptions to version 3.0.15</strong> or later.<br /><br />Click the button below to view and update to the latest Subscriptions version, or log in to <a href=\"https://woocommerce.com/my-dashboard\">WooCommerce.com Dashboard</a> and navigate to your <strong>Downloads</strong> page.<br /><br />We recommend always using the latest version of WooCommerce Subscriptions, and other software running on your site, to ensure maximum security.<br /><br />If you have any questions we are here to help — just <a href=\"https://woocommerce.com/my-account/create-a-ticket/\">open a ticket</a>.', '{}', 'pending', 'woocommerce.com', '2021-06-08 20:48:29', NULL, 0, 'plain', '', 0, 'info'),
(21, 'wc-admin-onboarding-email-marketing', 'info', 'en_US', 'Sign up for tips, product updates, and inspiration', 'We\'re here for you - get tips, product updates and inspiration straight to your email box', '{}', 'unactioned', 'woocommerce-admin', '2021-06-08 20:48:30', NULL, 0, 'plain', '', 0, 'info'),
(22, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-08 20:48:30', NULL, 0, 'plain', '', 0, 'info'),
(23, 'wc-admin-learn-more-about-variable-products', 'info', 'en_US', 'Learn more about variable products', 'Variable products are a powerful product type that lets you offer a set of variations on a product, with control over prices, stock, image and more for each variation. They can be used for a product like a shirt, where you can offer a large, medium and small and in different colors.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-08 20:51:47', NULL, 0, 'plain', '', 0, 'info'),
(24, 'wc-admin-filter-by-product-variations-in-reports', 'info', 'en_US', 'חדש – ניתן לסנן לפי סוגי מוצרים בדוחות של הזמנות ושל מוצרים', 'אחת האפשרויות הכי מבוקשות שלנו הושקה לשימוש! אפשר להציג תובנות של כל סוגי המוצרים בדוחות של הזמנות ושל מוצרים.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-09 20:53:29', NULL, 0, 'banner', 'http://flowers.nagar:8888/wp-content/plugins/woocommerce/packages/woocommerce-admin/images/admin_notes/filter-by-product-variations-note.svg', 0, 'info'),
(25, 'wc-admin-choosing-a-theme', 'marketing', 'en_US', 'רוצה לבחור ערכת עיצוב?', 'כדאי לעיין בערכות העיצוב שתואמות ל-WooCommerce ולבחור ערכת עיצוב שמתאימה לצרכים של המותג והעסק שלך.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-09 20:53:29', NULL, 0, 'plain', '', 0, 'info'),
(26, 'wc-admin-insight-first-product-and-payment', 'survey', 'en_US', 'תובנות', 'More than 80% of new merchants add the first product and have at least one payment method set up during the first week.<br><br>Do you find this type of insight useful?', '{}', 'unactioned', 'woocommerce-admin', '2021-06-09 20:53:29', NULL, 0, 'plain', '', 0, 'info'),
(27, 'wc-admin-customizing-product-catalog', 'info', 'en_US', 'איך להתאים אישית את קטלוג המוצרים שלך', 'כדאי ליצור קטלוג מוצרים ותמונות מוצר שנראים נהדר ומתאימים למותג שלך. המדריך הזה מספק את כל העצות שחשוב לדעת כדי להציג את המוצרים שלך בצורה מושכת בחנות.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-09 20:53:29', NULL, 0, 'plain', '', 0, 'info'),
(28, 'wc-admin-mobile-app', 'info', 'en_US', 'להתקין את האפליקציה של Woo לנייד', 'כדאי להתקין את האפליקציה של WooCommerce לנייד כדי לנהל הזמנות, לקבל הודעות על מכירות ולהציג מדדים חשובים – בכל מקום.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-14 07:31:46', NULL, 0, 'plain', '', 0, 'info'),
(29, 'wc-admin-onboarding-payments-reminder', 'info', 'en_US', 'להתחיל לקבל תשלומים בחנות שלך!', 'לגבות תשלומים באמצעות הספק שמתאים לך – ניתן לבחור בין מעל 100 ספקי תשלום שונים ב-WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-14 07:31:46', NULL, 0, 'plain', '', 0, 'info'),
(30, 'wc-admin-marketing-intro', 'info', 'en_US', 'להתחבר לקהל שלך', 'באפשרותך להרחיב את קהל הלקוחות שלך ולהגדיל את המכירות בעזרת כלי שיווק שנועדו לשימוש ב-WooCommerce.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-14 07:31:46', NULL, 0, 'plain', '', 0, 'info'),
(31, 'woocommerce-core-update-5-4-0', 'info', 'en_US', 'Update to WooCommerce 5.4.1 now', 'WooCommerce 5.4.1 addresses a checkout issue discovered in WooCommerce 5.4. We recommend upgrading to WooCommerce 5.4.1 as soon as possible.', '{}', 'unactioned', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(32, 'wcpay-promo-2020-11', 'marketing', 'en_US', 'wcpay-promo-2020-11', 'wcpay-promo-2020-11', '{}', 'pending', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(33, 'wcpay-promo-2020-12', 'marketing', 'en_US', 'wcpay-promo-2020-12', 'wcpay-promo-2020-12', '{}', 'pending', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(34, 'wcpay-promo-2021-6-incentive-1', 'marketing', 'en_US', 'Special offer: Save 50% on transaction fees for up to $125,000 in payments', 'Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp222\">WooCommerce Payments</a> to your store today.\n                Get a discounted rate of 1.5% + $0.15 on all transactions – that’s 50% off the standard fee on up to $125,000 in processed payments (or six months, whichever comes first). Act now – this offer is available for a limited time only.\n                <br /><br />By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-half-off-six-promotion/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp222\">Terms of Service</a>.', '{}', 'pending', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(35, 'wcpay-promo-2021-6-incentive-2', 'marketing', 'en_US', 'Special offer: No transaction fees* for up to three months', 'Save big when you add <a href=\"https://woocommerce.com/payments/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp233\">WooCommerce Payments</a> to your store today. Pay zero transaction fees* on up to $25,000 in processed payments (or three months, whichever comes first). Act now – this offer is available for a limited time only. *Currency conversion fees excluded.\n                <br /><br />By clicking \"Get WooCommerce Payments,\" you agree to the promotional <a href=\"https://woocommerce.com/terms-conditions/woocommerce-payments-no-transaction-fees-for-three-promotion/?utm_medium=notification&amp;utm_source=product&amp;utm_campaign=wcpay_exp233\">Terms of Service</a>.', '{}', 'pending', 'woocommerce.com', '2021-06-14 07:31:47', NULL, 0, 'plain', '', 0, 'info'),
(36, 'wc-admin-usage-tracking-opt-in', 'info', 'en_US', 'באפשרותך לעזור לנו לשפר את WooCommerce באמצעות מעקב אחר השימוש', 'איסוף של נתוני השימוש מאפשר לנו לשפר את WooCommerce. אנחנו נתחשב בנתונים של החנות שלך בבדיקה של תכונות חדשות ונוכל לבדוק את איכות העדכונים ואת היעילות של שיפורים חדשים. תמיד אפשר לבקר במקטע <a href=\"http://flowers.nagar:8888/wp-admin/admin.php?page=wc-settings&#038;tab=advanced&#038;section=woocommerce_com\" target=\"_blank\">הגדרות</a> ולהספיק את שיתוף הנתונים. <a href=\"https://woocommerce.com/usage-tracking\" target=\"_blank\">למידע נוסף</a> על הנתונים שאנחנו אוספים.', '{}', 'unactioned', 'woocommerce-admin', '2021-06-15 20:48:36', NULL, 0, 'plain', '', 0, 'info');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_admin_note_actions`
--

CREATE TABLE `fmn_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  `actioned_text` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonce_action` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `nonce_name` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_admin_note_actions`
--

INSERT INTO `fmn_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`, `actioned_text`, `nonce_action`, `nonce_name`) VALUES
(22, 21, 'yes-please', 'Yes please!', 'https://woocommerce.us8.list-manage.com/subscribe/post?u=2c1434dc56f9506bf3c3ecd21&amp;id=13860df971&amp;SIGNUPPAGE=plugin', 'actioned', 0, '', NULL, NULL),
(44, 22, 'connect', 'Connect', '?page=wc-addons&section=helper', 'unactioned', 0, '', NULL, NULL),
(87, 23, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/variable-product/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(235, 24, 'learn-more', 'מידע נוסף', 'https://docs.woocommerce.com/document/woocommerce-analytics/#variations-report', 'actioned', 0, '', NULL, NULL),
(236, 25, 'visit-the-theme-marketplace', 'מעבר לחנות של ערכות העיצוב', 'https://woocommerce.com/product-category/themes/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(237, 26, 'affirm-insight-first-product-and-payment', 'כן', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(238, 26, 'affirm-insight-first-product-and-payment', 'לא', '', 'actioned', 0, 'תודה על המשוב', NULL, NULL),
(239, 27, 'day-after-first-product', 'מידע נוסף', 'https://docs.woocommerce.com/document/woocommerce-customizer/?utm_source=inbox', 'actioned', 0, '', NULL, NULL),
(261, 28, 'learn-more', 'מידע נוסף', 'https://woocommerce.com/mobile/', 'actioned', 0, '', NULL, NULL),
(262, 29, 'view-payment-gateways', 'מידע נוסף', 'https://woocommerce.com/product-category/woocommerce-extensions/payment-gateways/', 'actioned', 1, '', NULL, NULL),
(263, 30, 'open-marketing-hub', 'לפתוח את מרכז השיווק', 'http://flowers.nagar:8888/wp-admin/admin.php?page=wc-admin&path=/marketing', 'actioned', 0, '', NULL, NULL),
(912, 36, 'tracking-opt-in', 'להפעיל מעקב אחר שימוש', '', 'actioned', 1, '', NULL, NULL),
(937, 1, 'open_wc_paypal_payments_product_page', 'Learn more', 'https://woocommerce.com/products/woocommerce-paypal-payments/', 'actioned', 1, '', NULL, NULL),
(938, 2, 'upgrade_now_facebook_pixel_api', 'Upgrade now', 'plugin-install.php?tab=plugin-information&plugin=&section=changelog', 'actioned', 1, '', NULL, NULL),
(939, 3, 'learn_more_facebook_ec', 'Learn more', 'https://woocommerce.com/products/facebook/', 'unactioned', 1, '', NULL, NULL),
(940, 4, 'set-up-concierge', 'Schedule free session', 'https://wordpress.com/me/concierge', 'actioned', 1, '', NULL, NULL),
(941, 5, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/woocommerce-shipping-and-tax/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(942, 6, 'learn-more-ecomm-unique-shopping-experience', 'Learn more', 'https://docs.woocommerce.com/document/product-add-ons/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(943, 7, 'watch-the-webinar', 'Watch the webinar', 'https://youtu.be/V_2XtCOyZ7o', 'actioned', 1, '', NULL, NULL),
(944, 8, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/ecommerce-shipping-solutions-guide/?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(945, 9, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-boost-sales', 'actioned', 1, '', NULL, NULL),
(946, 10, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=square-grow-your-business', 'actioned', 1, '', NULL, NULL),
(947, 11, 'add-apple-pay', 'Add Apple Pay', '/admin.php?page=wc-settings&tab=checkout&section=woocommerce_payments', 'actioned', 1, '', NULL, NULL),
(948, 11, 'learn-more', 'Learn more', 'https://docs.woocommerce.com/document/payments/apple-pay/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_applepay', 'actioned', 1, '', NULL, NULL),
(949, 12, 'boost-sales-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-boost-sales', 'actioned', 1, '', NULL, NULL),
(950, 13, 'grow-your-business-marketing-guide', 'See marketing guide', 'https://developer.apple.com/apple-pay/marketing/?utm_source=inbox&utm_campaign=wcpay-grow-your-business', 'actioned', 1, '', NULL, NULL),
(951, 14, 'optimizing-the-checkout-flow', 'Learn more', 'https://woocommerce.com/posts/optimizing-woocommerce-checkout?utm_source=inbox', 'actioned', 1, '', NULL, NULL),
(952, 15, 'learn-more', 'Learn more', 'https://woocommerce.com/posts/first-things-customize-woocommerce/?utm_source=inbox', 'unactioned', 1, '', NULL, NULL),
(953, 16, 'qualitative-feedback-from-new-users', 'Share feedback', 'https://automattic.survey.fm/wc-pay-new', 'actioned', 1, '', NULL, NULL),
(954, 17, 'share-feedback', 'Share feedback', 'http://automattic.survey.fm/paypal-feedback', 'unactioned', 1, '', NULL, NULL),
(955, 18, 'learn-more', 'Learn about Instant Deposits eligibility', 'https://docs.woocommerce.com/document/payments/instant-deposits/?utm_source=inbox&utm_medium=product&utm_campaign=wcpay_instant_deposits', 'actioned', 1, '', NULL, NULL),
(956, 19, 'get-started', 'Get started', 'https://woocommerce.com/products/google-listings-and-ads', 'actioned', 1, '', NULL, NULL),
(957, 20, 'update-wc-subscriptions-3-0-15', 'View latest version', 'http://flowers.nagar:8888/wp-admin/admin.php?page=wc-admin&page=wc-addons&section=helper', 'actioned', 1, '', NULL, NULL),
(958, 31, 'update-wc-core-5-4-0', 'How to update WooCommerce', 'https://docs.woocommerce.com/document/how-to-update-woocommerce/', 'actioned', 1, '', NULL, NULL),
(959, 34, 'get-woo-commerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL),
(960, 35, 'get-woocommerce-payments', 'Get WooCommerce Payments', 'admin.php?page=wc-admin&action=setup-woocommerce-payments', 'actioned', 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_category_lookup`
--

CREATE TABLE `fmn_wc_category_lookup` (
  `category_tree_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_category_lookup`
--

INSERT INTO `fmn_wc_category_lookup` (`category_tree_id`, `category_id`) VALUES
(15, 15);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_customer_lookup`
--

CREATE TABLE `fmn_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `state` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_download_log`
--

CREATE TABLE `fmn_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_coupon_lookup`
--

CREATE TABLE `fmn_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_product_lookup`
--

CREATE TABLE `fmn_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_stats`
--

CREATE TABLE `fmn_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `total_sales` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_order_tax_lookup`
--

CREATE TABLE `fmn_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_product_meta_lookup`
--

CREATE TABLE `fmn_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(19,4) DEFAULT NULL,
  `max_price` decimal(19,4) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  `tax_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'taxable',
  `tax_class` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_product_meta_lookup`
--

INSERT INTO `fmn_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`, `tax_status`, `tax_class`) VALUES
(12, '1234', 0, 0, '80.0000', '210.0000', 0, 20, 'instock', 0, '0.00', 0, 'taxable', ''),
(31, '', 0, 0, '100.0000', '100.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(32, '', 0, 0, '80.0000', '80.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(33, '', 0, 0, '210.0000', '210.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', 'parent'),
(180, '', 0, 0, '28.0000', '28.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(195, '', 0, 0, '111.0000', '111.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(196, '', 0, 0, '67.0000', '67.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(197, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(198, '', 0, 0, '179.0000', '179.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(199, '', 0, 0, '60.0000', '60.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(200, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(201, '', 0, 0, '88.0000', '88.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(202, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(203, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(237, '', 0, 0, '90.0000', '90.0000', 1, NULL, 'instock', 0, '0.00', 0, 'taxable', ''),
(238, '', 0, 0, '0.0000', '0.0000', 0, NULL, 'instock', 0, '0.00', 0, 'taxable', '');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_reserved_stock`
--

CREATE TABLE `fmn_wc_reserved_stock` (
  `order_id` bigint(20) NOT NULL,
  `product_id` bigint(20) NOT NULL,
  `stock_quantity` double NOT NULL DEFAULT '0',
  `timestamp` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `expires` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_tax_rate_classes`
--

CREATE TABLE `fmn_wc_tax_rate_classes` (
  `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_wc_tax_rate_classes`
--

INSERT INTO `fmn_wc_tax_rate_classes` (`tax_rate_class_id`, `name`, `slug`) VALUES
(1, 'Reduced rate', 'reduced-rate'),
(2, 'Zero rate', 'zero-rate'),
(3, 'תעריף מוזל', '%d7%aa%d7%a2%d7%a8%d7%99%d7%a3-%d7%9e%d7%95%d7%96%d7%9c'),
(4, 'תעריף אפסי', '%d7%aa%d7%a2%d7%a8%d7%99%d7%a3-%d7%90%d7%a4%d7%a1%d7%99');

-- --------------------------------------------------------

--
-- Table structure for table `fmn_wc_webhooks`
--

CREATE TABLE `fmn_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_api_keys`
--

CREATE TABLE `fmn_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_attribute_taxonomies`
--

CREATE TABLE `fmn_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_attribute_taxonomies`
--

INSERT INTO `fmn_woocommerce_attribute_taxonomies` (`attribute_id`, `attribute_name`, `attribute_label`, `attribute_type`, `attribute_orderby`, `attribute_public`) VALUES
(1, 'size', 'מידה', 'select', 'name', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_downloadable_product_permissions`
--

CREATE TABLE `fmn_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_log`
--

CREATE TABLE `fmn_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_itemmeta`
--

CREATE TABLE `fmn_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_order_items`
--

CREATE TABLE `fmn_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokenmeta`
--

CREATE TABLE `fmn_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_payment_tokens`
--

CREATE TABLE `fmn_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_sessions`
--

CREATE TABLE `fmn_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Dumping data for table `fmn_woocommerce_sessions`
--

INSERT INTO `fmn_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(2, '1', 'a:8:{s:4:\"cart\";s:1349:\"a:3:{s:32:\"884983780a6ef210d475d875ba900a1e\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"884983780a6ef210d475d875ba900a1e\";s:10:\"product_id\";i:198;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:179;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:179;s:8:\"line_tax\";i:0;}s:32:\"e0f8bd20bacd56281e77b6bbb7cca098\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"e0f8bd20bacd56281e77b6bbb7cca098\";s:10:\"product_id\";i:199;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:60;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:60;s:8:\"line_tax\";i:0;}s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";s:10:\"product_id\";i:180;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:28;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:28;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:393:\"a:15:{s:8:\"subtotal\";s:3:\"267\";s:12:\"subtotal_tax\";i:0;s:14:\"shipping_total\";s:1:\"0\";s:12:\"shipping_tax\";i:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";i:0;s:12:\"discount_tax\";i:0;s:19:\"cart_contents_total\";s:3:\"267\";s:17:\"cart_contents_tax\";i:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:1:\"0\";s:7:\"fee_tax\";i:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:3:\"267\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:3896:\"a:9:{s:32:\"46f9e068807741b7dfcf412398a7eef2\";a:11:{s:3:\"key\";s:32:\"46f9e068807741b7dfcf412398a7eef2\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:31;s:9:\"variation\";a:1:{s:17:\"attribute_pa_size\";s:3:\"big\";}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"06e4f508a457597233b07b5e4bbcb3d5\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:100;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:100;s:8:\"line_tax\";i:0;}s:32:\"f4c83c7b4156d97100f9ffb0ad4bb71b\";a:11:{s:3:\"key\";s:32:\"f4c83c7b4156d97100f9ffb0ad4bb71b\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:32;s:9:\"variation\";a:1:{s:17:\"attribute_pa_size\";s:7:\"regular\";}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"5cdb2e207db91273bcde3e653a2d6f81\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:80;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:80;s:8:\"line_tax\";i:0;}s:32:\"312126aa3a62a80ff7ea6353844188e5\";a:11:{s:3:\"key\";s:32:\"312126aa3a62a80ff7ea6353844188e5\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:33;s:9:\"variation\";a:1:{s:17:\"attribute_pa_size\";s:5:\"large\";}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"d69a22520d783ae7cb13e01b29decbc6\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:210;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:210;s:8:\"line_tax\";i:0;}s:32:\"0e65972dce68dad4d52d063967f0a705\";a:11:{s:3:\"key\";s:32:\"0e65972dce68dad4d52d063967f0a705\";s:10:\"product_id\";i:198;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:358;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:358;s:8:\"line_tax\";i:0;}s:32:\"84d9ee44e457ddef7f2c4f25dc8fa865\";a:11:{s:3:\"key\";s:32:\"84d9ee44e457ddef7f2c4f25dc8fa865\";s:10:\"product_id\";i:199;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:4;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:240;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:240;s:8:\"line_tax\";i:0;}s:32:\"0336dcbab05b9d5ad24f4333c7658a0e\";a:11:{s:3:\"key\";s:32:\"0336dcbab05b9d5ad24f4333c7658a0e\";s:10:\"product_id\";i:195;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:111;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:111;s:8:\"line_tax\";i:0;}s:32:\"d4b2c90ced36261fdd8204aa493c73d3\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"d4b2c90ced36261fdd8204aa493c73d3\";s:10:\"product_id\";i:195;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:111;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:111;s:8:\"line_tax\";i:0;}s:32:\"bbf8a76169a5c1dd6c5b6a522a821236\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"bbf8a76169a5c1dd6c5b6a522a821236\";s:10:\"product_id\";i:196;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";s:1:\"1\";s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:67;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:67;s:8:\"line_tax\";i:0;}s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";a:12:{s:11:\"custom_data\";a:1:{s:4:\"type\";N;}s:3:\"key\";s:32:\"c0ff71802db2fe63f931a64fa9d336e2\";s:10:\"product_id\";i:180;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:3;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:84;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:84;s:8:\"line_tax\";i:0;}}\";s:8:\"customer\";s:729:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:25:\"2021-06-09T12:16:33+03:00\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:0:\"\";s:7:\"country\";s:2:\"IL\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:0:\"\";s:16:\"shipping_country\";s:2:\"IL\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:15:\"maxf@leos.co.il\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;}', 1623954763);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zones`
--

CREATE TABLE `fmn_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_locations`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_shipping_zone_methods`
--

CREATE TABLE `fmn_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rates`
--

CREATE TABLE `fmn_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_woocommerce_tax_rate_locations`
--

CREATE TABLE `fmn_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl`
--

CREATE TABLE `fmn_yith_wcwl` (
  `ID` bigint(20) NOT NULL,
  `prod_id` bigint(20) NOT NULL,
  `quantity` int(11) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `wishlist_id` bigint(20) DEFAULT NULL,
  `position` int(11) DEFAULT '0',
  `original_price` decimal(9,3) DEFAULT NULL,
  `original_currency` char(3) DEFAULT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `on_sale` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl`
--

INSERT INTO `fmn_yith_wcwl` (`ID`, `prod_id`, `quantity`, `user_id`, `wishlist_id`, `position`, `original_price`, `original_currency`, `dateadded`, `on_sale`) VALUES
(1, 201, 1, 1, 1, 0, '88.000', 'USD', '2021-06-16 10:46:54', 0),
(2, 200, 1, 1, 1, 0, '0.000', 'USD', '2021-06-16 10:48:12', 0),
(3, 202, 1, 1, 1, 0, '0.000', 'USD', '2021-06-16 11:40:49', 0),
(4, 203, 1, 1, 1, 0, '0.000', 'USD', '2021-06-16 11:40:58', 0),
(5, 196, 1, 1, 1, 0, '67.000', 'USD', '2021-06-16 11:42:36', 0),
(6, 199, 1, 1, 1, 0, '60.000', 'USD', '2021-06-16 11:44:08', 0),
(7, 195, 1, 1, 1, 0, '111.000', 'USD', '2021-06-16 11:44:16', 0),
(8, 197, 1, 1, 1, 0, '0.000', 'USD', '2021-06-16 11:44:40', 0),
(9, 198, 1, 1, 1, 0, '179.000', 'USD', '2021-06-16 11:45:03', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fmn_yith_wcwl_lists`
--

CREATE TABLE `fmn_yith_wcwl_lists` (
  `ID` bigint(20) NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `wishlist_slug` varchar(200) NOT NULL,
  `wishlist_name` text,
  `wishlist_token` varchar(64) NOT NULL,
  `wishlist_privacy` tinyint(1) NOT NULL DEFAULT '0',
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expiration` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fmn_yith_wcwl_lists`
--

INSERT INTO `fmn_yith_wcwl_lists` (`ID`, `user_id`, `session_id`, `wishlist_slug`, `wishlist_name`, `wishlist_token`, `wishlist_privacy`, `is_default`, `dateadded`, `expiration`) VALUES
(1, 1, NULL, '', '', 'L0DQYQHA9W1W', 0, 1, '2021-06-16 07:46:54', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Indexes for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Indexes for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Indexes for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10)),
  ADD KEY `woo_idx_comment_type` (`comment_type`);

--
-- Indexes for table `fmn_links`
--
ALTER TABLE `fmn_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Indexes for table `fmn_options`
--
ALTER TABLE `fmn_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Indexes for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Indexes for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Indexes for table `fmn_term_relationships`
--
ALTER TABLE `fmn_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Indexes for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Indexes for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Indexes for table `fmn_users`
--
ALTER TABLE `fmn_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Indexes for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  ADD PRIMARY KEY (`note_id`);

--
-- Indexes for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `note_id` (`note_id`);

--
-- Indexes for table `fmn_wc_category_lookup`
--
ALTER TABLE `fmn_wc_category_lookup`
  ADD PRIMARY KEY (`category_tree_id`,`category_id`);

--
-- Indexes for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  ADD PRIMARY KEY (`customer_id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD PRIMARY KEY (`download_log_id`),
  ADD KEY `permission_id` (`permission_id`),
  ADD KEY `timestamp` (`timestamp`);

--
-- Indexes for table `fmn_wc_order_coupon_lookup`
--
ALTER TABLE `fmn_wc_order_coupon_lookup`
  ADD PRIMARY KEY (`order_id`,`coupon_id`),
  ADD KEY `coupon_id` (`coupon_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_product_lookup`
--
ALTER TABLE `fmn_wc_order_product_lookup`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_order_stats`
--
ALTER TABLE `fmn_wc_order_stats`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `date_created` (`date_created`),
  ADD KEY `customer_id` (`customer_id`),
  ADD KEY `status` (`status`(191));

--
-- Indexes for table `fmn_wc_order_tax_lookup`
--
ALTER TABLE `fmn_wc_order_tax_lookup`
  ADD PRIMARY KEY (`order_id`,`tax_rate_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `date_created` (`date_created`);

--
-- Indexes for table `fmn_wc_product_meta_lookup`
--
ALTER TABLE `fmn_wc_product_meta_lookup`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `virtual` (`virtual`),
  ADD KEY `downloadable` (`downloadable`),
  ADD KEY `stock_status` (`stock_status`),
  ADD KEY `stock_quantity` (`stock_quantity`),
  ADD KEY `onsale` (`onsale`),
  ADD KEY `min_max_price` (`min_price`,`max_price`);

--
-- Indexes for table `fmn_wc_reserved_stock`
--
ALTER TABLE `fmn_wc_reserved_stock`
  ADD PRIMARY KEY (`order_id`,`product_id`);

--
-- Indexes for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  ADD PRIMARY KEY (`tax_rate_class_id`),
  ADD UNIQUE KEY `slug` (`slug`(191));

--
-- Indexes for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  ADD PRIMARY KEY (`webhook_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  ADD PRIMARY KEY (`key_id`),
  ADD KEY `consumer_key` (`consumer_key`),
  ADD KEY `consumer_secret` (`consumer_secret`);

--
-- Indexes for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  ADD PRIMARY KEY (`attribute_id`),
  ADD KEY `attribute_name` (`attribute_name`(20));

--
-- Indexes for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  ADD PRIMARY KEY (`permission_id`),
  ADD KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  ADD KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`);

--
-- Indexes for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `level` (`level`);

--
-- Indexes for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `order_item_id` (`order_item_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  ADD PRIMARY KEY (`order_item_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `payment_token_id` (`payment_token_id`),
  ADD KEY `meta_key` (`meta_key`(32));

--
-- Indexes for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  ADD PRIMARY KEY (`token_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  ADD PRIMARY KEY (`session_id`),
  ADD UNIQUE KEY `session_key` (`session_key`);

--
-- Indexes for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  ADD PRIMARY KEY (`zone_id`);

--
-- Indexes for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `location_id` (`location_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  ADD PRIMARY KEY (`instance_id`);

--
-- Indexes for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  ADD PRIMARY KEY (`tax_rate_id`),
  ADD KEY `tax_rate_country` (`tax_rate_country`),
  ADD KEY `tax_rate_state` (`tax_rate_state`(2)),
  ADD KEY `tax_rate_class` (`tax_rate_class`(10)),
  ADD KEY `tax_rate_priority` (`tax_rate_priority`);

--
-- Indexes for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  ADD PRIMARY KEY (`location_id`),
  ADD KEY `tax_rate_id` (`tax_rate_id`),
  ADD KEY `location_type_code` (`location_type`(10),`location_code`(20));

--
-- Indexes for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `prod_id` (`prod_id`);

--
-- Indexes for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `wishlist_token` (`wishlist_token`),
  ADD KEY `wishlist_slug` (`wishlist_slug`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_actions`
--
ALTER TABLE `fmn_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_claims`
--
ALTER TABLE `fmn_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_groups`
--
ALTER TABLE `fmn_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `fmn_actionscheduler_logs`
--
ALTER TABLE `fmn_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `fmn_commentmeta`
--
ALTER TABLE `fmn_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_comments`
--
ALTER TABLE `fmn_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_links`
--
ALTER TABLE `fmn_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_options`
--
ALTER TABLE `fmn_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1468;

--
-- AUTO_INCREMENT for table `fmn_postmeta`
--
ALTER TABLE `fmn_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1797;

--
-- AUTO_INCREMENT for table `fmn_posts`
--
ALTER TABLE `fmn_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=254;

--
-- AUTO_INCREMENT for table `fmn_termmeta`
--
ALTER TABLE `fmn_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `fmn_terms`
--
ALTER TABLE `fmn_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fmn_term_taxonomy`
--
ALTER TABLE `fmn_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `fmn_usermeta`
--
ALTER TABLE `fmn_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT for table `fmn_users`
--
ALTER TABLE `fmn_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_notes`
--
ALTER TABLE `fmn_wc_admin_notes`
  MODIFY `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `fmn_wc_admin_note_actions`
--
ALTER TABLE `fmn_wc_admin_note_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=961;

--
-- AUTO_INCREMENT for table `fmn_wc_customer_lookup`
--
ALTER TABLE `fmn_wc_customer_lookup`
  MODIFY `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  MODIFY `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_wc_tax_rate_classes`
--
ALTER TABLE `fmn_wc_tax_rate_classes`
  MODIFY `tax_rate_class_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `fmn_wc_webhooks`
--
ALTER TABLE `fmn_wc_webhooks`
  MODIFY `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_api_keys`
--
ALTER TABLE `fmn_woocommerce_api_keys`
  MODIFY `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_attribute_taxonomies`
--
ALTER TABLE `fmn_woocommerce_attribute_taxonomies`
  MODIFY `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_downloadable_product_permissions`
--
ALTER TABLE `fmn_woocommerce_downloadable_product_permissions`
  MODIFY `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_log`
--
ALTER TABLE `fmn_woocommerce_log`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_itemmeta`
--
ALTER TABLE `fmn_woocommerce_order_itemmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_order_items`
--
ALTER TABLE `fmn_woocommerce_order_items`
  MODIFY `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokenmeta`
--
ALTER TABLE `fmn_woocommerce_payment_tokenmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_payment_tokens`
--
ALTER TABLE `fmn_woocommerce_payment_tokens`
  MODIFY `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_sessions`
--
ALTER TABLE `fmn_woocommerce_sessions`
  MODIFY `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zones`
--
ALTER TABLE `fmn_woocommerce_shipping_zones`
  MODIFY `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_locations`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_shipping_zone_methods`
--
ALTER TABLE `fmn_woocommerce_shipping_zone_methods`
  MODIFY `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rates`
--
ALTER TABLE `fmn_woocommerce_tax_rates`
  MODIFY `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_woocommerce_tax_rate_locations`
--
ALTER TABLE `fmn_woocommerce_tax_rate_locations`
  MODIFY `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl`
--
ALTER TABLE `fmn_yith_wcwl`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `fmn_yith_wcwl_lists`
--
ALTER TABLE `fmn_yith_wcwl_lists`
  MODIFY `ID` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `fmn_wc_download_log`
--
ALTER TABLE `fmn_wc_download_log`
  ADD CONSTRAINT `fk_fmn_wc_download_log_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `fmn_woocommerce_downloadable_product_permissions` (`permission_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
